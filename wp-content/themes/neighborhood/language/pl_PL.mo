��    =        S   �      8     9     A     ]     d     h     {     �     �  
   �     �     �     �     �     �     �     �  
     
        )  
   -  ;   8     t     �     �     �     �     �  	   �     �     �     �     
            	        '     .     K     d  "   u     �     �     �     �  �   �     �  *   �  
   �     �     �     �     �     	     	     0	  1   8	     j	     y	     �	     �	  �  �	     Y     p  	   �     �     �  +   �     �     �  	                !   &     H     P     ]     j  
   �     �     �     �  A   �  	   �  
               '   +     S     [     c     ~     �     �     �     �     �     �     �     �       $   "     G     \     t     �  �   �     1  !   B     d     t  #   �     �     �     �     �     �  5   �     ,     @  	   X     b     
         %             9               ;           0           	       ,            4   *      8   '         $                  )         "                       =   .             <   5      :      (   3          2   7              1   +              #       !   6                -   &       /       0 items 0 items in the shopping bag 1 item All Back to my account Billing &amp; Shipping Billing Address Cart Subtotal Categories Continue shopping Description Go to your wishlist Item Login Logout Lost your password? My Account Need Help? New No results No search results could be found, please try another query. Order Total Out of Stock Password Proceed to checkout Product added to wishlist. Quantity Quantity: Registered customers Remember me Remove this item Sale! Search Share Share on: Share: Ship to a different address? Ship to billing address? Shipping Address Showing %1$d-%2$d of %3$d products Showing all %d products Showing the single product Sign Out Sign Up Sorry but we couldn't find the page you are looking for. Please check to make sure you've typed the URL correctly. You may also want to search for what you are looking for. Subtotal Unfortunately, your shopping bag is empty. Unit Price Unit Price: Username or email address View View all %d results View shopping bag View your shopping bag Welcome You currently have no items in your shopping bag. Your selection Your shopping bag is empty in the shopping bag items Project-Id-Version: Neighborhood
POT-Creation-Date: 2017-04-02 00:09+0100
PO-Revision-Date: 2018-03-09 14:12+0100
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 0 produktów w koszyku 0 produktów w koszyku 1 produkt Wszystko Wróć do Mojego Konta Adres rozliczeniowy &amp; Adres do wysyłki Adres rozliczeniowy Suma częściowa Kategorie Kontynuować zakupy Nazwa Przejdź do swojej listy życzeń Produkt Zaloguj się Wyloguj się Nie pamiętasz hasła? Moje konto Potrzebujesz pomocy? Nowość Brak wyników Brak wyników wyszukiwania. Spróbuj użyć inne słowa kluczowe. Łącznie Wyprzedany Нasło Złóż zamówienie Produkt dodany do twojej listy życzeń Ilość Ilość Logowanie na konto klienta Zapamiętaj mnie Usuń ten produkt Rabat! Szukaj Udostępnić Udostępnij na: Udostępnić Dostawa na inny adres? Dostawa na adres rozliczeniowy? Adres do wysyłki Pokazuje %1$d-%2$d z %3$d produktów Pokazuje %d produkty Pokazuję jeden produkt Wyloguj się Zarejestruj się Przepraszamy, ale nie mogliśmy znaleźć strony, której szukasz. Prosimy sprawdzić, czy wpisałeś poprawny adres strony. Możesz też użyć wyszukiwarki: Suma częściowa Niestety Twój koszyk jest pusty. Cena za sztukę Cena za sztukę Podaj nazwę użytkownika lub email Produktów na stronie Pokazuje %d produkty Zobacz koszyk Zobacz koszyk Witamy Obecnie nie masz żadnych produktów w Twoim koszyku. Zawartość koszyka Twój koszyk jest pusty w koszyku Produkty 