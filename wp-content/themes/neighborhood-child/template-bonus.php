<?php
/**
 *  Template name: Bonusy
 */
    get_header();
    $src = get_stylesheet_directory_uri();
?>
<div class="bonus-page">
    <?php if ( is_user_logged_in() ): ?>
        <section class="points">
            <h2>Twoje punkty w programie</h2>
            <div class="points__wrapper">
                <div class="points__box">
                    <h3>Aktualne punkty</h3>
                    <div class="points__circles-wrapper">
                        <div class="points__circles">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <span class="points__value"><?= do_shortcode('[bonuspoints_current]'); ?></span>
                    </div>
                    <div class="points__row">
                        <p>Użyte punkty</p>
                        <span><?= do_shortcode('[bonuspoints_used]'); ?> pkt</span>
                    </div>
                </div>
                <div class="points__history">
                    <h3>Historia dodania bonusów</h3>
                    <?= do_shortcode('[bonuspoints]'); ?>
                    <ul class="points__history-list">
                        <li>
                            <span class="title">Zakupy 15.10.2020</span>
                            <span class="value plus">+5 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 02.10.2020</span>
                            <span class="value plus">+7 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 29.09.2020</span>
                            <span class="value minuse">-5 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 22.09.2020</span>
                            <span class="value plus">+5 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 09.09.2020</span>
                            <span class="value minuse">-8 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 12.08.2020</span>
                            <span class="value plus">+1 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 15.10.2020</span>
                            <span class="value plus">+5 pkt</span>
                        </li>
                        <li>
                            <span class="title">Zakupy 15.10.2020</span>
                            <span class="value plus">+5 pkt</span>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <section class="rules">
        <h2>Zasady programu bonusowego</h2>
        <div class="rules__wrapper">
            <ul>
                <li>
                    <img src="<?= $src; ?>/images/icon-cart.svg" alt="">
                    <div class="rules__text">
                        <?php the_field('bonus_step1'); ?>
                    </div>
                </li>
                <li>
                    <img src="<?= $src; ?>/images/icon-cash.svg" alt="">
                    <div class="rules__text">
                        <?php the_field('bonus_step2'); ?>
                    </div>
                </li>
                <li>
                    <img src="<?= $src; ?>/images/icon-label.svg" alt="">
                    <div class="rules__text">
                        <?php the_field('bonus_step3'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <section class="documentation">
        <h2>Dokumentacja</h2>
        <ul>
            <?php $file1 = get_field('file_1'); if($file1): ?>
                <li>
                    <a href="<?= $file1['url']; ?>" target="_blank"><?= $file1['title']; ?></a>
                </li>
            <?php endif; ?>
            <?php $file2 = get_field('file_2'); if($file2): ?>
                <li>
                    <a href="<?= $file2['url']; ?>" target="_blank"><?= $file2['title']; ?></a>
                </li>
            <?php endif; ?>
        </ul>
    </section>
</div>
<?php get_footer(); ?>