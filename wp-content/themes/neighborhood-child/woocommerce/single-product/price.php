<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$src = get_stylesheet_directory_uri();
?>
<?php if(get_field('bonus_points_add', $_product->id)): ?>
		<div class="price bonus-points">
			<div class="bonus-points__info">
				<img src="<?= $src; ?>/images/icon-info.svg" alt="Program bonusowy">
			</div>
			<?php echo $product->get_price_html(); ?>
		</div>
		<div class="bonus-points__hover">Produkt biorący udział w programie bonusowym!</div>
	<?php else: ?>
		<span class="price"><?php echo $product->get_price_html(); ?></span>
	<?php endif?>
<!-- <p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p> -->
