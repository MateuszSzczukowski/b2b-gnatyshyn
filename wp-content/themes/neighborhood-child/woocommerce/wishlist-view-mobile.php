<?php
/**
 * Wishlist page template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 1.0.6
 */

?>

<div class="container">

<?php

global $wpdb, $yith_wcwl, $woocommerce, $catalog_mode;

$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
$myaccount_page_url = $shop_url = "";
if ( $myaccount_page_id ) {
  $myaccount_page_url = get_permalink( $myaccount_page_id );
}

$shop_page_url = "";
if ( version_compare( WOOCOMMERCE_VERSION, "2.1.0" ) >= 0 ) {
	$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
} else {
	$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
}

$user_id = "";

if( isset( $_GET['user_id'] ) && !empty( $_GET['user_id'] ) ) {
    $user_id = $_GET['user_id'];
} elseif( is_user_logged_in() ) {
    $user_id = get_current_user_id();
}

$current_page = 1;
$limit_sql = '';

if( $pagination == 'yes' ) {
    $count = array();

    if( is_user_logged_in() ) {
        $count = $wpdb->get_results( $wpdb->prepare( 'SELECT COUNT(*) as `cnt` FROM `' . YITH_WCWL_TABLE . '` WHERE `user_id` = %d', $user_id  ), ARRAY_A );
        $count = $count[0]['cnt'];
    } else {
        $count[0]['cnt'] = count( yith_getcookie( 'yith_wcwl_products' ) );
    }

    $total_pages = $count/$per_page;
    if( $total_pages > 1 ) {
        $current_page = max( 1, get_query_var( 'page' ) );

        $page_links = paginate_links( array(
            'base' => get_pagenum_link( 1 ) . '%_%',
            'format' => '&page=%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'show_all' => true
        ) );
    }

    $limit_sql = "LIMIT " . ( $current_page - 1 ) * 1 . ',' . $per_page;
}

if ($user_id != "") {
$wishlist = $wpdb->get_results(
				$wpdb->prepare( "SELECT * FROM `" . YITH_WCWL_TABLE . "` WHERE `user_id` = %s" . $limit_sql, $user_id ),
			ARRAY_A );
} else {
	$wishlist = yith_getcookie( 'yith_wcwl_products' );
}

// Start wishlist page printing
if ( version_compare( WOOCOMMERCE_VERSION, "2.1.0" ) >= 0 ) {
wc_print_notices();
} else {
$woocommerce->show_messages();
}
 ?>
	<div id="yith-wcwl-messages"></div>

	<?php sf_woo_help_bar(); ?>

	<div class="my-account-left">

		<h4 class="lined-heading"><span><?php _e("My Account", "swiftframework"); ?></span></h4>
		<ul class="nav my-account-nav">
			<?php if( is_user_logged_in() ) { ?>
			<li><a href="<?php echo $myaccount_page_url; ?>"><?php _e("Back to my account", "swiftframework"); ?></a></li>
			<?php } else { ?>
			<li><a href="<?php echo $shop_page_url; ?>"><?php _e("Shop", "swiftframework"); ?></a></li>
			<li><a href="<?php echo $myaccount_page_url; ?>"><?php _e("Create Account / Login", "swiftframework"); ?></a></li>
			<?php } ?>
		</ul>

	</div>

	<div class="my-account-right tab-content">

		<form id="yith-wcwl-form" action="<?php echo esc_url( $yith_wcwl->get_wishlist_url() ) ?>" method="post">
		    <?php
				do_action( 'yith_wcwl_before_wishlist_title' );
				$count_items = count($wishlist);
                $wishlist_title = get_option( 'yith_wcwl_wishlist_title' );
                $i = 0;
				if( !empty( $wishlist_title ) )
					if($count_items == 1) {
						echo apply_filters( 'yith_wcwl_wishlist_title', '<h3 class="wish-list-title">' . $wishlist_title . '</h3><span class="count-items">('. $count_items .' przedmiot)</span>' ); }
					if($count_items > 1) {
						echo apply_filters( 'yith_wcwl_wishlist_title', '<h3 class="wish-list-title">' . $wishlist_title . '</h3><span class="count-items">('. $count_items .' przedmioty)</span>' ); }

				do_action( 'yith_wcwl_before_wishlist' );
		    ?>
           <ul id="wish-list" class="products">
				<?php
                    if( count( $wishlist ) > 0 ) :

		                foreach( $wishlist as $item ) :
		                    global $product;
            	            if ( function_exists( 'wc_get_product' ) ) {
            		            $product = wc_get_product( $item['prod_id'] );
            	            }
            	            else{
            		            $product = get_product( $item['prod_id'] );
            	            }

                            if( $product !== false && $product->exists() ) :
            	                $availability = $product->get_availability();
                                $stock_status = $availability['class'];
				?>
								<li class="product type-product status-publish has-post-thumbnail first instock shipping-taxable purchasable product-type-simple">

									<figure class="product-transition" style="padding-bottom: 270px;">

										<div class="product-img-wrap">
											<div class="product-image">
												<?php echo $product->get_image() ?>
											</div>
											<div class="product-image second-image">
												<?php
													$attachment_ids = is_callable( array( $product, 'get_gallery_image_ids' ) ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();

													$img_count = 0;

													if ($attachment_ids) {

														foreach ( $attachment_ids as $attachment_id ) {

															if ( sf_get_post_meta( $attachment_id, '_woocommerce_exclude_image', true ) )
																continue;

															echo wp_get_attachment_image( $attachment_id, 'shop_catalog' );

															$img_count++;

															if ($img_count == 1) break;

														}

													}
												?>
											</div>
										</div>
										<?php if ( version_compare( get_option('yith_wcwl_version'), "2.0" ) >= 0 ) {    ?>
													<div class="remove-btn"> <a href="<?php echo esc_url( add_query_arg( 'remove_from_wishlist', $item['prod_id']) ) ?>" class="remove remove_from_wishlist" title="<?php _e( 'Remove this product', 'yit' ) ?>"><span></span><span></span></a></div>
										<?php } else { ?>
													<div class="remove-btn"><a href="javascript:void(0)" onclick="remove_item_from_wishlist( '<?php echo esc_url( $yith_wcwl->get_remove_url( $item['ID'] ) )?>', 'yith-wcwl-row-<?php echo $item['ID'] ?>');" class="remove"  data-product-id="<?php echo $item['prod_id']; ?>" title="<?php _e( 'Remove this product', 'swiftframework' ) ?>"><span></span><span></span></a>
										<?php } ?>

                                        <?php if (!$catalog_mode && $show_add_to_cart) { ?>
                                            <figcaption>
                                                <div class="shop-actions clearfix">
                                                    <?php
                                                        if ( isset( $stock_status ) && $stock_status != 'Out' ) {
                                                            wc_get_template( 'loop/add-to-cart.php' );
                                                        }
                                                    ?>
                                                </div>
                                            </figcaption>
                                        <?php } ?>

										<a class="product-hover-link" href="<?php echo esc_url( get_permalink( apply_filters( 'woocommerce_in_cart_product', $item['prod_id'] ) ) ) ?>"></a>

									</figure>

									<div class="product-details">
										<h3><a href="<?php echo esc_url( get_permalink( apply_filters( 'woocommerce_in_cart_product', $item['prod_id'] ) ) ) ?>"><?php echo apply_filters( 'woocommerce_in_cartproduct_title', $product->get_title(), $product ) ?></a></h3>
										<?php
											if ( function_exists('wc_get_product_tag_list') ) {
												$product_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->id;
												echo wc_get_product_tag_list( $product_id, ' / ', '<span class="posted_in">', '</span>' );
											} else {
												echo $product->get_categories( ', ', '<span class="posted_in">', '</span>' );
											}
										?>
									</div>
								</li>

				<?php
                            endif;
		                endforeach;
					endif;
				?>
			</ul>
		</form>
	</div>

</div>
