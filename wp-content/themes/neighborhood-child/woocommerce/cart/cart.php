<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

global $has_products, $product, $include_isotope;

$has_products = true;
$include_isotope = true;
$src = get_stylesheet_directory_uri();
?>

<?php

	$cart_count = WC()->cart->cart_contents_count;
	if ($cart_count === 1) {
		$cart_count .= ' przedmiot';
	} else if ($cart_count > 21 && ($cart_count % 10 == 2 || $cart_count % 10 == 3 || $cart_count % 10 == 4) || ($cart_count < 5 && $cart_count > 1)) {
		$cart_count .= ' przedmioty';
	} else {
		$cart_count .= ' przedmiotów';
	}

	do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

		<div class="row">

			<div class="span12">

				<h3 class="bag-summary"><?php _e('Your selection', 'swiftframework');?> <span>(<?php echo $cart_count; ?>)</span></h3>

				<?php do_action( 'woocommerce_before_cart_table' ); ?>

				<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
					<thead>
						<tr>
							<th class="product-thumbnail"><?php esc_html_e( 'Item', 'swiftframework' ); ?></th>
							<th class="product-name"><?php esc_html_e( 'Description', 'swiftframework' ); ?></th>
							<th class="product-price"><?php esc_html_e( 'Unit Price', 'swiftframework' ); ?></th>
							<th class="product-quantity"><?php esc_html_e( 'Quantity', 'swiftframework' ); ?></th>
							<th class="product-subtotal"><?php esc_html_e( 'Subtotal', 'swiftframework' ); ?></th>
							<th class="product-remove">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php do_action( 'woocommerce_before_cart_contents' ); ?>

						<?php
						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
								?>
								<?php if ( wp_is_mobile() ){ ?>
								<div class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

									<div class="product-name" data-title="<?php esc_attr_e( 'Product', 'swiftframework' ); ?>">
										<?php
											$product_title = "";
											if ( version_compare( WC_VERSION, '2.7', '>=' ) ) {
												$product_title = $_product->get_name();
											} else {
												$product_title = $_product->get_title();
											}

											if ( ! $_product->is_visible() ){

												echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';

											} else {
												$_product = $cart_item['data'];
												$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

												if ( $_product->is_type( 'variation' ) ) {
													if ( ! $product_permalink ) {
														echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';
													} else {
														echo sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() );
													}
												}
												else
												{
													echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';
												}
											}


											// Meta data
											if (function_exists('wc_get_formatted_cart_item_data')) {
												echo wc_get_formatted_cart_item_data( $cart_item );
											} else {
												echo WC()->cart->get_item_data( $cart_item );
											}

				               				// Backorder notification
				               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
				               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'swiftframework' ) . '</p>';
										?>
									</div>
									<div class="product-remove clearfix">
										<?php
											// @codingStandardsIgnoreLine
											if (function_exists('wc_get_cart_remove_url')) {
												echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
													'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
													esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
													__( 'Remove this item', 'swiftframework' ),
													esc_attr( $product_id ),
													esc_attr( $_product->get_sku() )
												), $cart_item_key );
											} else {
												echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
													'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
													esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
													__( 'Remove this item', 'swiftframework' ),
													esc_attr( $product_id ),
													esc_attr( $_product->get_sku() )
												), $cart_item_key );
											}
										?>
									</div>

								<div class="cart-item">
									<div class="product-thumbnail">
										<?php
										if ( ! $_product->is_visible() ){

											} else {

												echo '
												<script>
													var url_string =  "' . $product_permalink .'";
													var url = new URL(url_string);
													var c = url.searchParams.get("attribute_pa_kolor");
													var d = "<div class=color-in-image><div class=pa_kolor_v_"+ c +"></div></div>";
													document.write(d);
												</script>
												';
											}
										?>
										<?php
											$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

											if ( ! $_product->is_visible() )
												echo $thumbnail;
											else
												printf( '<a href="%s">%s</a>', $product_permalink, $thumbnail );
										?>
									</div>
									<div class="product-category-item product-tags-item ">
									<?php /* echo wc_get_product_category_list( $cart_item['product_id'] ); */ ?>
									<?php

										$current_tags = get_the_terms( $cart_item['product_id'], 'product_tag' );

										foreach ($current_tags as $tag) {

											$tag_title = $tag->name; // tag name
											$tag_link = get_term_link( $tag );// tag archive link

											echo '<a href="'.$tag_link.'"><span>'.$tag_title.'</span> </a>';
										}
									?>

									</div>
									<div class="product-price" data-title="<?php esc_attr_e( 'Price', 'swiftframework' ); ?>">
										<?php
											echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
										?>
									</div>
									<div class="product-subtotal" style="display:none;" data-title="<?php esc_attr_e( 'Total', 'swiftframework' ); ?>">
										<?php
											echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
										?>


									</div>
									<div class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'swiftframework' ); ?>">
										<?php
											if ( $_product->is_sold_individually() ) {
												$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
											} else {
												$product_quantity = woocommerce_quantity_input( array(
													'input_name'  => "cart[{$cart_item_key}][qty]",
													'input_value' => $cart_item['quantity'],
													'max_value'   => $_product->get_max_purchase_quantity(),
													'min_value'   => '0',
													'product_name'  => $_product->get_name()
												), $_product, false );
											}

											echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
										?>

									</div>
									</div>
									<div class="product-edit">
										<?php
											$product_title = "";
											if ( version_compare( WC_VERSION, '2.7', '>=' ) ) {
												$product_title = $_product->get_name();
											} else {
												$product_title = $_product->get_title();
											}
											if ( ! $_product->is_visible() )
												echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';
											else
												echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">Edytuj</a>', $product_permalink, $product_title ), $cart_item, $cart_item_key );

											// Meta data
											if (function_exists('wc_get_formatted_cart_item_data')) {
												echo wc_get_formatted_cart_item_data( $cart_item );
											} else {
												echo WC()->cart->get_item_data( $cart_item );
											}

				               				// Backorder notification
				               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
				               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'swiftframework' ) . '</p>';
										?>
									</div>
								</div>



								<?php } else { ?>
								<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

								<td class="product-thumbnail">
										<?php
											$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

											if ( ! $_product->is_visible() )
												echo $thumbnail;
											else
												printf( '<a href="%s">%s</a>', $product_permalink, $thumbnail );
										?>
									</td>

									<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'swiftframework' ); ?>">
										<div class="cart-product-name-bonus">
											<?php
												$product_title = "";
												if ( version_compare( WC_VERSION, '2.7', '>=' ) ) {
													$product_title = $_product->get_name();
												} else {
													$product_title = $_product->get_title();
												}
												if ( ! $_product->is_visible() )
													echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';
												else
													echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', $product_permalink, $product_title ), $cart_item, $cart_item_key );

												// Meta data
												if (function_exists('wc_get_formatted_cart_item_data')) {
													echo wc_get_formatted_cart_item_data( $cart_item );
												} else {
													echo WC()->cart->get_item_data( $cart_item );
												}

												// Backorder notification
												if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
													echo '<p class="backorder_notification">' . __( 'Available on backorder', 'swiftframework' ) . '</p>';
											?>
											<?php if(get_field('bonus_points_add', $_product->id)): ?>
												<div class="bonus-points__info">
													<img src="<?= $src; ?>/images/icon-info.svg" alt="Program bonusowy">
												</div>
												<div class="bonus-points__hover">Produkt biorący udział w programie bonusowym!</div>
											<?php endif; ?>
										</div>
									</td>

									<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'swiftframework' ); ?>">
										<?php
											echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
										?>
									</td>

									<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'swiftframework' ); ?>">
										<?php
											if ( $_product->is_sold_individually() ) {
												$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
											} else {
												$product_quantity = woocommerce_quantity_input( array(
													'input_name'  => "cart[{$cart_item_key}][qty]",
													'input_value' => $cart_item['quantity'],
													'max_value'   => $_product->get_max_purchase_quantity(),
													'min_value'   => '0',
													'product_name'  => $_product->get_name()
												), $_product, false );
											}

											echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
										?>
									</td>

									<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'swiftframework' ); ?>">
										<?php
											echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
										?>
									</td>

									<td class="product-remove clearfix">
										<?php
											// @codingStandardsIgnoreLine
											if (function_exists('wc_get_cart_remove_url')) {
												echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
													'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
													esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
													__( 'Remove this item', 'swiftframework' ),
													esc_attr( $product_id ),
													esc_attr( $_product->get_sku() )
												), $cart_item_key );
											} else {
												echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
													'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
													esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
													__( 'Remove this item', 'swiftframework' ),
													esc_attr( $product_id ),
													esc_attr( $_product->get_sku() )
												), $cart_item_key );
											}
										?>
									</td>
								<?php } ?>
								</tr>
								<?php
							}
						}

						do_action( 'woocommerce_cart_contents' );
						?>
						<!-- <tr>
						<td style="border-top:0;">
						<div class="cart-total">
						<span class="left">Total</span>
						<span class="right">
							<?php
							global $woocommerce;
							echo $woocommerce->cart->cart_contents_total;
							?>
						 zł</span>
						</div>
						</td>
						</tr> -->
						</tbody>
				</table>
					<div class="wc-proceed-to-checkout update-cart-cta">
						<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
					</div>
					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<div class="coupon__wrapper">
								<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Kod kuponu', 'swiftframework' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Zrealizuj kupon', 'swiftframework' ); ?>" />
							</div>
							<div class="clearfix"></div>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>

					<div class="wc-proceed-to-checkout cta-buttons-checkout">
						<a class="continue-shopping button" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>"><?php _e('Kontynuuj zakupy', 'swiftframework'); ?></a>
						<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
					</div>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart' ); ?>

					<?php do_action( 'woocommerce_after_cart_contents' ); ?>

				<?php do_action( 'woocommerce_after_cart_table' ); ?>

			</div>

		</div>

	</form>

	<div class="cart-collaterals">
		<?php
			/**
			 * Cart collaterals hook.
			 *
			 * @hooked woocommerce_cross_sell_display
			 * @hooked woocommerce_cart_totals - 10
			 */

		 	do_action( 'woocommerce_cart_collaterals' );

		?>
	</div>

	<?php do_action( 'woocommerce_after_cart' ); ?>
