<?php
/**
 *  Template name: Login page
 */
get_header(); the_post(); ?>

    <?php if ( !is_user_logged_in() ): ?>
        <div class="login">
            <div class="login__wrapper">
                <div class="login__content-top">
                    <h2>Logowanie na konto klienta</h2>
                    <div class="text">Nie zarejestrowany? <a href="<?php bloginfo('url'); ?>/rejestracja">Załóż konto</a></div>
                </div>
                <div class="login__form">
                    <?php wc_print_notices(); ?>
                    <form class="woocommerce-form woocommerce-form-login login" method="post">

                        <?php do_action( 'woocommerce_login_form_start' ); ?>

                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="<?php esc_html_e( 'Username or email address', 'swiftframework' ); ?>" /><?php // @codingStandardsIgnoreLine ?>
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" placeholder="<?php esc_html_e( 'Password', 'swiftframework' ); ?>"/>
                        </p>

                        <?php do_action( 'woocommerce_login_form' ); ?>

                        <p class="form-row">
                            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                            <button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Login', 'swiftframework' ); ?>"><?php esc_html_e( 'Login', 'swiftframework' ); ?></button>
                        </p>


                        <?php do_action( 'woocommerce_login_form_end' ); ?>

                        <div class="login__form-tools">
                            <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'swiftframework' ); ?></a>
                            <div class="login__form-rm">
                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                    <input id="remember-me" class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'swiftframework' ); ?></span>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <p>Kontynuując, akceptujesz nasze <a href="#">Warunki Użytkowania</a> i <a href="#">Politykę Prywatności</a>.</p>
        </div>

    <?php else: ?>
        <?php
            $url = get_home_url();
            header("Location: " . $url);
            exit();
        ?>
    <?php endif; ?>

<?php get_footer(); ?>