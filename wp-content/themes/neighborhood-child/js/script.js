jQuery.noConflict();
jQuery('.show-main-nav').on('click', function() {
  jQuery(this).toggleClass('active');
  jQuery('#main-navigation').toggleClass('activenav');

	jQuery("#main-navigation li").each(function(n) {

		jQuery(this).toggleClass('navigationanim');

	});


});



jQuery('.filter-first').on('click', function() {
	jQuery(this).removeClass('active-filter');
	jQuery(this).addClass('deactive-filter');
	jQuery('.chw-widget-area').addClass('active-filter');
});
jQuery('.filter-close').on('click', function() {
	jQuery('.filter-first').addClass('active-filter');
	jQuery('.filter-first').removeClass('deactive-filter');
	jQuery('.chw-widget-area').addClass('deactive-filter');
	jQuery('.chw-widget-area').removeClass('active-filter');
});
jQuery(function($) {
    jQuery(document).ready(function() {
        jQuery("#readMore, #readLess").click(function(){
            jQuery(".collapsed-content").toggle('slow', 'swing');
            jQuery(".full-content").toggle('slow', 'swing');
            jQuery("#readMore").toggle();// "read more link id"
            return false;
        });
		jQuery('.scrolling-wrapper div').addClass('scrolling-wrapper-card');

    });
});
jQuery('.accordion-heading').on('click', function() {
  jQuery(this).toggleClass('active');

});

var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);


jQuery(document).ready(function() {
    jQuery(".select-items div").each(function(i) {
        jQuery(this).attr('id', 'item' + (i+1));
    });
});



jQuery(function($) {
    jQuery(document).ready(function() {

		jQuery('#item1').on('click', function() {
			var url = "?orderby=popularity";
			window.location = url;
		});
		jQuery('#item2').on('click', function() {
			var url = "?orderby=rating";
			window.location = url;
		});
		jQuery('#item3').on('click', function() {
			var url = "?orderby=date";
			window.location = url;
		});
		jQuery('#item4').on('click', function() {
			var url = "?orderby=price";
			window.location = url;
		});
		jQuery('#item5').on('click', function() {
			var url = "?orderby=price-desc";
			window.location = url;
		});


    });
});


jQuery(function($) {
    jQuery(document).ready(function() {

		var urlfull = jQuery('#url-address-full').data("siteurl");
		var address = jQuery('#url-address-full').data("siteurl");
		var url1 = jQuery('#url-address').data("siteurl");
		var content = document.createTextNode("select-selected");
		var address1 = url1 + "?orderby=popularity";
		var address2 = url1 + "?orderby=rating";
		var address3 = url1 + "?orderby=date";
		var address4 = url1 + "?orderby=price";
		var address5 = url1 + "?orderby=price-desc";
		if (url1 == address)
		{
			jQuery('.select-selected').text('Sortuj');
		}
		if (urlfull == address1)
		{
			var theDiv = document.getElementById("item1");
			theDiv.appendChild(content);
		}
		if (urlfull == address2)
		{
			var theDiv = document.getElementById("item2");
			theDiv.appendChild(content);
		}
		if (urlfull == address3)
		{
			var theDiv = document.getElementById("item3");
			theDiv.appendChild(content);
		}
		if (urlfull == address4)
		{
			var theDiv = document.getElementById("item4");
			theDiv.appendChild(content);
		}
		if (urlfull == address5)
		{
			var theDiv = document.getElementById("item5");
			theDiv.appendChild(content);
		}
		jQuery('bag-header .bag-header').text('MÓJ KOSZYK');

		jQuery('#billing_wybor_uzytkownika_field label:has(input:checked)').addClass('active-label');
	});
});
jQuery('#billing_wybor_uzytkownika_field label').click(function (e) {

jQuery('#billing_wybor_uzytkownika_field label').removeClass('active-label');
jQuery(this).addClass('active-label');

});


jQuery(".page-id-16692 #afreg_additional_16690").prop('required', true);
jQuery(".page-id-16692 #afreg_additional_16690").attr('oninvalid', "this.setCustomValidity('Numer NIP jest wymagany')");
jQuery(".page-id-16692 #afreg_additional_16690").attr('oninput', "setCustomValidity('')");

jQuery(".page-id-16692 #afreg_additionalshowhide_16690 span.required").append('*');
jQuery(".page-id-8 .new-user-text").text('Rejestrując się zyskujesz dostęp do historii zamówień, schowek na produkty, szybki i wygodny proces składania kolejnych zamówień.');
jQuery(".page-id-16692 .new-user-text").text('Prowadzisz salon fryzjerski? Zarejestruj się w panelu biznesowym, aby zyskać dostęp do pełnej oferty sklepu, większych rabatów i programu lojalnościowego. Dostęp do panelu będzie możliwy po weryfikacji Twojej firmy.');

jQuery(".page-id-8 .individual-customer").addClass('active');
jQuery(".page-id-16692 .salon").addClass('active');
jQuery(".page-id-16692 .individual-customer").removeClass('active');

jQuery('.wishlist').click(function() {
  location.reload();
});
