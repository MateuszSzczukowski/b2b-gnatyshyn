<?php

	function neighborhood_enqueue_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css', '', '1.0' );
		wp_enqueue_style( 'custom-b2b', get_stylesheet_directory_uri().'/assets/css/custom-b2b.css', '', '1.0' );

	}
	add_action( 'wp_enqueue_scripts', 'neighborhood_enqueue_styles' );

	function jstofooter() {
    ?>
	<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/script.js'></script>



	<?php
	}
	add_action('wp_footer', 'jstofooter');

/* LOGO
	================================================== */
	if (!function_exists('sf_logo_child')) {
	function sf_logo_child($logo_class) {

		//VARIABLES
		global $woocommerce;
		$options = get_option('sf_neighborhood_options');
		$show_cart = $options['show_cart'];
		$logo = $retina_logo = "";
		if (isset($options['logo_upload'])) {
		$logo = $options['logo_upload'];
		}
		if (isset($options['retina_logo_upload'])) {
		$retina_logo = $options['retina_logo_upload'];
		}


		if ($retina_logo == "") {
		$retina_logo = $logo;
		}
		$logo_output = "";
		$logo_alt = get_bloginfo( 'name' );
		$logo_link_url = home_url();
		$disable_search = false;
		if (isset($options['disable_search'])) {
			$disable_search = $options['disable_search'];
		}

		$logos = array('logo', 'retina_logo');
		foreach ($logos as $this_logo) {
			if (stripos(${$this_logo}, 'http://') === 0) {
				${$this_logo} = substr(${$this_logo}, 5);
			}
		}
		$cart_total = "";
			if ( WC()->cart->prices_include_tax ) {
			$cart_total = WC()->cart->get_cart_total();
			} else {
			$cart_total = WC()->cart->get_cart_subtotal();
			}
		$cart_count = WC()->cart->cart_contents_count;


		// LOGO OUTPUT
		$logo_output .= '<div id="logo" class="'.$logo_class.' clearfix ';
		if ( wp_is_mobile() ){

			$logo_output .= '">'. "\n";
		}
		else {

			$logo_output .= ' koloba">'. "\n";

		}

		$logo_output .= '<a class="logo-link" href="'.$logo_link_url.'">'. "\n";
		$logo_output .= '<img class="standard" src="'.$logo.'" alt="'.$logo_alt.'" />'. "\n";
		$logo_output .= '<img class="retina" src="'.$retina_logo.'" alt="'.$logo_alt.'" />'. "\n";

		$logo_output .= '</a>'. "\n";
		$logo_output .= '<div class="header-icon">'. "\n";
		$logo_output .= '<a href="#" class="show-main-nav">&nbsp;</a>'. "\n";

		if ($woocommerce && $show_cart) {
			if ( wp_is_mobile() ){
				$logo_output .= '<a href="'.esc_url( wc_get_cart_url() ).'" class="mobile-cart-link"><i class="sf-cart"></i><span>'.$cart_count.'</span></a>'. "\n";
			}
			else {
			$logo_output .= '<nav class="std-menu cart-icon-child"><div><ul class="menu">' . sf_get_cart_child() . '</ul></div></nav>';
			}
		}
		if( is_user_logged_in() ) {
			$wishlist_count = YITH_WCWL()->count_products();
			if (($wishlist_count == '0') or ($wishlist_count =='')){
			$logo_output .= '<a href="'. get_site_url() .'/wishlist/" class="mobile-cart-link"><i class="sf-wishlist"></i><span></span></a>'. "\n";
			} else{
			$logo_output .= '<a href="'. get_site_url() .'/wishlist/" class="mobile-cart-link"><i class="sf-wishlist"></i><span>'.$wishlist_count.'</span></a>'. "\n";
			}
		}
		$logo_output .= '<a href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'" class="mobile-cart-link"><i class="sf-account"></i></a>'. "\n";


		$logo_output .= '</div>'. "\n";


		if (!$disable_search) {
		$logo_output .= '<a href="#" class="hidden-desktop mobile-search-link"><i class="fa-search"></i></a>'. "\n";
		}
		$logo_output .= '</div>'. "\n";
		$logo_output .= '</div>'. "\n";


		// LOGO RETURN
		return $logo_output;
	}
	}

if (!function_exists('sf_get_cart_child')) {
		function sf_get_cart_child() {

			$cart_output = "";

			// Check if WooCommerce is active
			if ( sf_woocommerce_activated() ) {

				global $woocommerce;

				$options = get_option('sf_neighborhood_options');
				$show_cart_count = false;
				if (isset($options['show_cart_count'])) {
					$show_cart_count = $options['show_cart_count'];
				}

				$cart_total = "";
				if ( WC()->cart->prices_include_tax ) {
				$cart_total = WC()->cart->get_cart_total();
				} else {
				$cart_total = WC()->cart->get_cart_subtotal();
				}
				$cart_count = WC()->cart->cart_contents_count;
				$cart_count_text = sf_product_items_text($cart_count);
				$price_display_suffix  = get_option( 'woocommerce_price_display_suffix' );

				if ((empty($cart_count)) or ($cart_count == '0')) {
					$cart_output .= '<li class="shopping-bag-lak"><i class="sf-cart"></i></li>';
				}
				else {
				if ($show_cart_count) {
					$cart_output .= '<li class="parent shopping-bag-item"><a class="cart-contents" href="'.esc_url( wc_get_cart_url() ).'" title="'.__("View your shopping bag", "swiftframework").'"><i class="sf-cart"></i>'.$cart_count.' ('.$cart_total.')</a>';
				} else {
					$cart_output .= '<li class="parent shopping-bag-item"><a class="cart-contents" href="'.esc_url( wc_get_cart_url() ).'" title="'.__("View your shopping bag", "swiftframework").'"><i class="sf-cart"></i>'.$cart_total.'</a>';
				}
				}
	            $cart_output .= '<ul class="sub-menu">';
	            $cart_output .= '<li>';
				$cart_output .= '<div class="shopping-bag">';

	            if ( sizeof($cart_count)>=0 ) {


								$cart_count_text = $cart_count;
								if ($cart_count === 1) {
									$cart_count_text .= ' przedmiot';
								} else if ($cart_count > 21 && ($cart_count % 10 == 2 || $cart_count % 10 == 3 || $cart_count % 10 == 4) || ($cart_count < 5 && $cart_count > 1)) {
									$cart_count_text .= ' przedmioty1';
								} else {
									$cart_count_text .= ' przedmiotów1';
								}

	            	$cart_output .= '<div class="bag-header">MÓJ KOSZYK</div>';

	            	$cart_output .= '<div class="bag-contents">';

	            	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

	                    $_product     		 = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
	                    $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

							$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							$product_title     = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
							$product_short_title = ( strlen( $product_title ) > 25 ) ? substr( $product_title, 0, 22 ) . '...' : $product_title;

	                        $cart_output .= '<div class="bag-product clearfix product-id-' . $cart_item['product_id'] . '">';
	                        $cart_output .= '<figure><a class="bag-product-img" href="' . get_permalink( $cart_item['product_id'] ) . '">' . $_product->get_image() . '</a></figure>';
	                        $cart_output .= '<div class="bag-product-details">';
	                        $cart_output .= '<div class="bag-product-title"><a href="' . get_permalink( $cart_item['product_id'] ) . '">' . apply_filters( 'woocommerce_cart_widget_product_title', $product_short_title, $_product ) . '</a></div>';
	                        $cart_output .= '<div class="bag-product-price">' . __( "Unit Price:", 'swiftframework' ) . '
	                        ' . $product_price . '</div>';
	                        $cart_output .= '<div class="bag-product-quantity">' . __( 'Quantity:', 'swiftframework' ) . ' ' . apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ) . '</div>';
	                        $cart_output .= '</div>';
	                        if (function_exists('wc_get_cart_remove_url')) {
	                        	$cart_output .= apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
	                        							'<a href="%s" class="remove remove-product" title="%s" data-ajaxurl="'.admin_url( 'admin-ajax.php' ).'" data-product-qty="'. $cart_item['quantity'] .'"  data-product-id="%s" data-product_sku="%s">&times;</a>',
	                        							esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
	                        							__( 'Remove this item', 'swiftframework' ),
	                        							esc_attr( $product_id ),
	                        							esc_attr( $_product->get_sku() )
	                        						), $cart_item_key );
	                        } else {
	                        	$cart_output .= apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
	                        							'<a href="%s" class="remove remove-product" title="%s" data-ajaxurl="'.admin_url( 'admin-ajax.php' ).'" data-product-qty="'. $cart_item['quantity'] .'"  data-product-id="%s" data-product_sku="%s">&times;</a>',
	                        							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
	                        							__( 'Remove this item', 'swiftframework' ),
	                        							esc_attr( $product_id ),
	                        							esc_attr( $_product->get_sku() )
	                        						), $cart_item_key );
	                        }
	                        $cart_output .= '</div>';
	                    }
					}


	                $cart_output .= '</div>';

	                $cart_output .= '<div class="bag-buttons">';

	                $cart_output .= '<a class="sf-roll-button bag-button" href="' . esc_url( wc_get_cart_url() ) . '"><span>'.__('View shopping bag', 'swiftframework').'</span><span>'.__('View shopping bag', 'swiftframework').'</span></a>';

	                $cart_output .= '<a class="sf-roll-button checkout-button" href="'.esc_url( wc_get_checkout_url() ).'"><span >'.__('Proceed to checkout', 'swiftframework').'</span><span>'.__('Proceed to checkout', 'swiftframework').'</span></a>';

	               	$cart_output .= '</div>';

	            } else {

	           		$cart_output .= '<div class="bag-header">MÓJ KOSZYK</div>';

	           		$cart_output .= '<div class="bag-empty">'.__('Unfortunately, your shopping bag is empty.','swiftframework').'</div>';

	            	$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );

	            	$cart_output .= '<div class="bag-buttons">';

	            	$cart_output .= '<a class="sf-roll-button shop-button" href="'.esc_url( $shop_page_url ).'"><span>'.__('Go to the shop', 'swiftframework').'</span><span>'.__('Go to the shop', 'swiftframework').'</span></a>';

	            	$cart_output .= '</div>';

	            }

			    $cart_output .= '</div>';
	            $cart_output .= '</li>';
	            $cart_output .= '</ul>';
	            $cart_output .= '</li>';

	        }

			return $cart_output;

		}
	}
if (!function_exists('sf_mobile_search_child')) {
	function sf_mobile_search_child() {

		$options = get_option('sf_neighborhood_options');
		$mobile_search_output = '';
		$header_search_pt = "any";
		if (isset($options['header_search_pt'])) {
			$header_search_pt = $options['header_search_pt'];
		}

		$mobile_search_output .= '<div class="mobile_search"><div class="widget_search"><form method="get" class="search-form" action="'.home_url().'/"><input type="text" placeholder="'.__("Search", "swiftframework").'" name="s" autocomplete="off" />';
		if ( $header_search_pt != "any" ) {
		    $mobile_search_output .= '<input type="hidden" name="post_type" value="' . $header_search_pt . '" />';
		}
		$mobile_search_output .= '</form></div></div>';

		return $mobile_search_output;
	}
	}
if (!function_exists('sf_header_child')) {
	function sf_header_child() {

		// VARIABLES
		$options = get_option('sf_neighborhood_options');
		$header_layout = $options['header_layout'];
		$show_cart = $options['show_cart'];
		$show_wishlist = $options['show_wishlist'];
		$disable_search = false;
		if (isset($options['disable_search'])) {
			$disable_search = $options['disable_search'];
		}
		$header_search_pt = "any";
		if (isset($options['header_search_pt'])) {
			$header_search_pt = $options['header_search_pt'];
		}
		$header_output = $main_menu = '';

		if ($header_layout == "header-1") {

		$header_output .= '<header id="header" class="clearfix">'. "\n";
		$header_output .= '<div class="container">'. "\n";
		$header_output .= '<div class="header-row row">'. "\n";
		$header_output .= '<div class="header-left span4">'.sf_woo_links('header-menu', 'logo-left').'</div>'. "\n";
		$header_output .= sf_logo_child('span4 logo-center');

		$header_output .= '</div> <!-- CLOSE .row -->'. "\n";
		$header_output .= '</div> <!-- CLOSE .container -->'. "\n";
		$header_output .= '</header>'. "\n";
		$header_output .= sf_mobile_search_child();
		$header_output .= '<div id="main-nav">'. "\n";
		$header_output .= sf_main_menu_child('main-navigation', 'full');
		$header_output .= '</div>'. "\n";

		} else if ($header_layout == "header-2") {

		$header_output .= '<header id="header" class="clearfix">'. "\n";
		$header_output .= '<div class="container">'. "\n";
		$header_output .= '<div class="header-row row">'. "\n";
		$header_output .= sf_logo_child('span4 logo-left');
		$header_output .= '<div class="header-right span8">'.sf_aux_links('header-menu').'</div>'. "\n";
		$header_output .= '</div> <!-- CLOSE .row -->'. "\n";
		$header_output .= '</div> <!-- CLOSE .container -->'. "\n";
		$header_output .= '</header>'. "\n";
		$header_output .= sf_mobile_search_child();
		$header_output .= '<div id="main-nav">'. "\n";
		$header_output .= sf_main_menu_child('main-navigation', 'full');
		$header_output .= '</div>'. "\n";

		} else if ($header_layout == "header-3") {

		$header_output .= '<header id="header" class="clearfix">'. "\n";
		$header_output .= '<div class="container">'. "\n";
		$header_output .= '<div class="header-row row">'. "\n";
		$header_output .= '<div class="header-left span8">'.sf_aux_links('header-menu').'</div>'. "\n";
		$header_output .= sf_logo_child('span4 logo-right');
		$header_output .= '</div> <!-- CLOSE .row -->'. "\n";
		$header_output .= '</div> <!-- CLOSE .container -->'. "\n";
		$header_output .= '</header>'. "\n";
		$header_output .= sf_mobile_search_child();
		$header_output .= '<div id="main-nav">'. "\n";
		$header_output .= sf_main_menu_child('main-navigation', 'full');
		$header_output .= '</div>'. "\n";

		} else if ($header_layout == "header-4") {

		$header_output .= '<header id="header" class="clearfix">'. "\n";
		$header_output .= '<div class="container">'. "\n";
		$header_output .= '<div class="header-row row">'. "\n";
		$header_output .= sf_logo_child('span4 logo-left');
		$header_output .= '<div class="header-right span8">';
		$header_output .= '<nav class="std-menu">'. "\n";
		$header_output .= '<ul class="menu">'. "\n";
		if ($show_cart) {

		}
		if ( class_exists( 'YITH_WCWL_UI' ) &&  $show_wishlist)  {
		$header_output .= sf_get_wishlist();
		}
		if (!$disable_search) {
		$header_output .= '<li class="menu-search no-hover"><a href="#"><i class="fa-search"></i></a>'. "\n";
		$header_output .= '<ul class="sub-menu">'. "\n";
		$header_output .= '<li><div class="ajax-search-wrap"><div class="ajax-loading"></div><form method="get" class="ajax-search-form" action="'.home_url().'/">';
		if ( $header_search_pt != "any" ) {
		    $header_output .= '<input type="hidden" name="post_type" value="' . $header_search_pt . '" />';
		}
		$header_output .= '<input type="text" placeholder="'.__("Search", "swiftframework").'" name="s" autocomplete="off" /></form><div class="ajax-search-results"></div></div></li>'. "\n";
		$header_output .= '</ul>'. "\n";
		$header_output .= '</li>'. "\n";
		}
		$header_output .= '</ul>'. "\n";
		$header_output .= '</nav>'. "\n";
		$header_output .= sf_main_menu_child('main-navigation');
		$header_output .= '</div>'. "\n";
		$header_output .= '</div> <!-- CLOSE .row -->'. "\n";
		$header_output .= '</div> <!-- CLOSE .container -->'. "\n";
		$header_output .= '</header>'. "\n";
		$header_output .= sf_mobile_search_child();

		} else {

		$header_output .= '<header id="header" class="clearfix">'. "\n";
		$header_output .= '<div class="container">'. "\n";
		$header_output .= '<div class="header-row row">'. "\n";
		$header_output .= sf_logo_child('span4 logo-right');
		$header_output .= '<div class="header-left span8">';
		$header_output .= sf_main_menu_child('main-navigation');
		$header_output .= '<nav class="std-menu">'. "\n";
		$header_output .= '<ul class="menu">'. "\n";
		if ($show_cart) {

		}
		if ( class_exists( 'YITH_WCWL_UI' ) &&  $show_wishlist)  {
		$header_output .= sf_get_wishlist();
		}
		if (!$disable_search) {
		$header_output .= '<li class="menu-search no-hover"><a href="#"><i class="fa-search"></i></a>'. "\n";
		$header_output .= '<ul class="sub-menu">'. "\n";
		$header_output .= '<li><div class="ajax-search-wrap"><div class="ajax-loading"></div><form method="get" class="ajax-search-form" action="'.home_url().'/">';
		if ( $header_search_pt != "any" ) {
		    $header_output .= '<input type="hidden" name="post_type" value="' . $header_search_pt . '" />';
		}
		$header_output .= '<input type="text" placeholder="'.__("Search", "swiftframework").'" name="s" autocomplete="off" /></form><div class="ajax-search-results"></div></div></li>'. "\n";
		$header_output .= '</ul>'. "\n";
		$header_output .= '</li>'. "\n";
		}
		$header_output .= '</ul>'. "\n";
		$header_output .= '</nav>'. "\n";
		$header_output .= '</div>'. "\n";
		$header_output .= '</div> <!-- CLOSE .row -->'. "\n";
		$header_output .= '</div> <!-- CLOSE .container -->'. "\n";
		$header_output .= '</header>'. "\n";
		$header_output .= sf_mobile_search_child();

		}

		// HEADER RETURN
		return $header_output;

	}
	}
	/* MENU
	================================================== */
	if (!function_exists('sf_main_menu_child')) {
		function sf_main_menu_child($id, $layout = "") {

			// VARIABLES
			$options = get_option('sf_neighborhood_options');
			$show_cart = $options['show_cart'];
			$show_wishlist = $options['show_wishlist'];
			$disable_search = false;
			if (isset($options['disable_search'])) {
				$disable_search = $options['disable_search'];
			}
			$header_search_pt = "any";
			if (isset($options['header_search_pt'])) {
				$header_search_pt = $options['header_search_pt'];
			}
			$mobile_translation_enabled = false;
			if ( isset($options['enable_mobile_translation']) ) {
				$mobile_translation_enabled = $options['enable_mobile_translation'];
			}
			$mobile_account_enabled = false;
			if ( isset($options['enable_mobile_account']) ) {
				$mobile_account_enabled = $options['enable_mobile_account'];
			}
			$menu_output = $menu_full_output = "";
			$main_menu_args = array(
				'echo'            => false,
				'theme_location' => 'main_navigation',
				'walker'         => new sf_mega_menu_walker,
				'fallback_cb' => ''
			);

			// MOBILE TRANSLATION OUTPUT
			$mobile_translation = "";
			if ( $mobile_translation_enabled ) {
				$mobile_translation = '<ul id="mobile-header-languages" class="header-languages sub-menu">'. "\n";
				if (function_exists( 'language_flags' )) {
					$mobile_translation .= language_flags();
				}
				$mobile_translation .= '</ul>'. "\n";
			}

			// MOBILE ACCOUNT OUTPUT
			$mobile_account = "";
			if ( $mobile_account_enabled ) {
				$login_url           = wp_login_url();
	            $logout_url          = wp_logout_url( home_url() );
	            $my_account_link     = get_admin_url();
	            $myaccount_page_id   = get_option( 'woocommerce_myaccount_page_id' );
	            if ( $myaccount_page_id ) {
	                $my_account_link = get_permalink( $myaccount_page_id );
	                $logout_url      = wp_logout_url( get_permalink( $myaccount_page_id ) );
	                $login_url       = get_permalink( $myaccount_page_id );
	                if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
	                    $logout_url = str_replace( 'http:', 'https:', $logout_url );
	                    $login_url  = str_replace( 'http:', 'https:', $login_url );
	                }
	            }
	            $login_url        = apply_filters( 'sf_header_login_url', $login_url );
	            $register_url	  = apply_filters( 'sf_header_register_url', wp_registration_url() );
	            $my_account_link  = apply_filters( 'sf_header_myaccount_url', $my_account_link );

				if ( get_option( 'woocommerce_enable_myaccount_registration' ) && $myaccount_page_id ) {
					$register_url = apply_filters( 'sf_header_register_url', $my_account_link );
				}
				$mobile_account = '<ul id="mobile-account-options" class="sub-menu">'. "\n";
				if ( is_user_logged_in() ) {
				    $mobile_account .= '<li><a href="' . $my_account_link . '" class="admin-link">' . __( "My Account", 'swiftframework' ) . '</a></li>' . "\n";
				    $mobile_account .= '<li><a href="' . $logout_url . '">' . __( "Logout", 'swiftframework' ) . '</a></li>' . "\n";
				} else {
				    $mobile_account .= '<li><a href="' . $login_url . '">' . __( "Login", 'swiftframework' ) . '</a></li>' . "\n";
				    $mobile_account .= '<li><a href="' . $register_url . '">' . __( "Sign Up", 'swiftframework' ) . '</a></li>' . "\n";
				}
				$mobile_account .= '</ul>'. "\n";
			}

			// MENU OUTPUT
			if ($id == "mini-navigation") {
				$menu_output .= '<nav id="'.$id.'" class="mini-menu clearfix">'. "\n";
			} else {
				$menu_output .= '<nav id="'.$id.'" class="std-menu clearfix">'. "\n";
			}

			if ( function_exists('wp_nav_menu') ) {
				$menu_output .= wp_nav_menu( $main_menu_args );
			}

			$menu_output .= $mobile_account;

			$menu_output .= $mobile_translation;

			$menu_output .= '</nav>'. "\n";


			// FULL WIDTH MENU OUTPUT
			if ($layout == "full") {
				$menu_full_output .= '<div class="container">'. "\n";
				$menu_full_output .= '<div class="row">'. "\n";
				$menu_full_output .= '<div class="span9">'. "\n";
				$menu_full_output .= $menu_output . "\n";
				$menu_full_output .= '</div>'. "\n";
				$menu_full_output .= '<div class="span3 header-right">'. "\n";

				$menu_full_output .= '<div class="desktop-search">' . sf_mobile_search_child() . '</div>';




				$menu_full_output .= '</div>'. "\n";
				$menu_full_output .= '</div>'. "\n";
				$menu_full_output .= '</div>'. "\n";

				$menu_output = $menu_full_output;
			}


			// MENU RETURN
			return $menu_output;
		}
	}
function filter_widgets_init() {

    register_sidebar( array(
        'name'          => 'Filter product for mobile',
        'id'            => 'filter-product-mobile',
        'before_widget' => '<div class="filter-product-mobile">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="filter-title">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'filter_widgets_init' );

function my_filter_plugin_updates( $value ) {
   if( isset( $value->response['woocommerce-ajax-filters/woocommerce-filters.php'] ) ) {
      unset( $value->response['woocommerce-ajax-filters/woocommerce-filters.php'] );
    }
    return $value;
}
add_filter( 'site_transient_update_plugins', 'my_filter_plugin_updates' );

function my_custom_shortcode ( $attr ){

    global $woocommerce;
    return $woocommerce->cart->total;
}
add_shortcode ('totalcart','my_custom_shortcode');

function myprefix_exclude_ipad( $is_mobile ) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    }
    return $is_mobile ;
}
add_filter( 'wp_is_mobile', 'myprefix_exclude_ipad' );

add_filter( 'woocommerce_output_related_products_args', function( $args )
{
    $args = wp_parse_args( array( 'posts_per_page' => 5 ), $args );
    return $args;
});

add_filter( 'woocommerce_product_related_posts_relate_by_category', function() {
    return false;
});
// PHP: Remove "(optional)" from our non required fields

function remove_checkout_optional_fields_label( $field, $key, $args, $value ) {
    // Only on checkout page
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
        $field = str_replace( $optional, '', $field );
    }
    return $field;
}
add_filter( 'woocommerce_form_field' , 'remove_checkout_optional_fields_label', 10, 4 );

function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}
add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );

/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' );
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * @snippet       WooCommerce User Registration Shortcode
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 4.0
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_shortcode( 'wc_reg_form_bbloomer', 'bbloomer_separate_registration_form' );

function bbloomer_separate_registration_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return;
   ob_start();

   // NOTE: THE FOLLOWING <FORM></FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
   // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY

   do_action( 'woocommerce_before_customer_login_form' );

   ?>
      <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

         <?php do_action( 'woocommerce_register_form_start' ); ?>

         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

            <div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <!-- <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label> -->
               <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="<?php esc_html_e( 'Username', 'woocommerce' ); ?>*" /><?php // @codingStandardsIgnoreLine ?>
            </div>

         <?php endif; ?>

         <div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <!-- <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label> -->
            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="<?php esc_html_e( 'Email address', 'woocommerce' ); ?>*" /><?php // @codingStandardsIgnoreLine ?>
         </div>

         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

            <div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <!-- <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label> -->
               <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>*" />
            </div>

         <?php else : ?>

            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

         <?php endif; ?>

         <?php do_action( 'woocommerce_register_form' ); ?>

         <p class="woocommerce-FormRow form-row">
            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
         </p>

         <?php do_action( 'woocommerce_register_form_end' ); ?>

      </form>

   <?php

   return ob_get_clean();
}

/**
 * @snippet       WooCommerce User Login Shortcode
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 4.0
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_shortcode( 'wc_login_form_bbloomer', 'bbloomer_separate_login_form' );
function bbloomer_separate_login_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return;
   ob_start();
   woocommerce_login_form( array( 'redirect' => 'https://google.pl' ) );
   return ob_get_clean();
}