<?php
/**
 *  Template name: Register page
 */
get_header(); the_post(); ?>

    <div class="login">
        <?php if(false): ?>
            <a href="index.html" class="go-back">Wróć</a>
        <?php endif; ?>
        <div class="login__wrapper">
            <div class="login__content-top">
                <h2>Zarejestruj nowe konto</h2>
                <div class="text register">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="login__form">
                <?= do_shortcode('[wc_reg_form_bbloomer]'); ?>
            </div>
        </div>
        <p>Kontynuując, akceptujesz nasze <a href="#">Warunki Użytkowania</a> i <a href="#">Politykę Prywatności</a>.</p>
    </div>

<?php get_footer(); ?>