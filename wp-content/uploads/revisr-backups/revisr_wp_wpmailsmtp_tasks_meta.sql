
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wpmailsmtp_tasks_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wpmailsmtp_tasks_meta` WRITE;
/*!40000 ALTER TABLE `wp_wpmailsmtp_tasks_meta` DISABLE KEYS */;
INSERT INTO `wp_wpmailsmtp_tasks_meta` VALUES (1,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-22 13:26:38'),(2,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-23 13:57:27'),(3,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-24 17:24:43'),(4,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-26 07:53:00'),(5,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-27 10:09:08'),(6,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-28 11:26:05'),(7,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-29 12:26:04'),(8,'wp_mail_smtp_admin_notifications_update','W10=','2020-08-30 20:29:30'),(9,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-01 01:27:01'),(10,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-02 08:13:19'),(11,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-03 08:22:22'),(12,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-04 10:17:33'),(13,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-06 06:49:12'),(14,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-07 13:59:38'),(15,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-08 18:38:38'),(16,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-10 09:54:22'),(17,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-12 08:38:36'),(18,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-13 09:11:51'),(19,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-14 09:12:31'),(20,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-15 09:58:01'),(21,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-16 11:24:45'),(22,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-17 12:05:06'),(23,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-20 10:02:03'),(24,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-21 10:50:00'),(25,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-22 11:58:41'),(26,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-23 14:54:06'),(27,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-24 15:01:49'),(28,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-25 15:21:11'),(29,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-26 15:22:18'),(30,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-28 08:20:03'),(31,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-29 11:31:22'),(32,'wp_mail_smtp_admin_notifications_update','W10=','2020-09-30 11:43:17'),(33,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-01 14:09:51'),(34,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-05 11:46:45'),(35,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-06 11:49:01'),(36,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-07 13:41:58'),(37,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-08 16:14:35'),(38,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-10 15:59:32'),(39,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-12 08:49:55'),(40,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-13 10:51:14'),(41,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-16 10:50:23'),(42,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-18 10:00:49'),(43,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-19 12:56:23'),(44,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-21 10:41:31'),(45,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-22 13:30:09'),(46,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-26 15:38:32'),(47,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-27 19:00:36'),(48,'wp_mail_smtp_admin_notifications_update','W10=','2020-10-29 16:47:51'),(49,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-01 17:56:13'),(50,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-02 19:24:09'),(51,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-03 19:42:26'),(52,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-04 19:55:32'),(53,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-06 09:32:56'),(54,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-07 13:06:50'),(55,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-08 16:31:06'),(56,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-09 20:14:05'),(57,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-11 12:26:22'),(58,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-12 16:40:41'),(59,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-13 19:57:18'),(60,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-15 15:15:46'),(61,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-16 20:41:21'),(62,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-17 21:09:15'),(63,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-19 10:10:18'),(64,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-20 10:12:17'),(65,'wp_mail_smtp_admin_notifications_update','W10=','2020-11-22 11:15:10');
/*!40000 ALTER TABLE `wp_wpmailsmtp_tasks_meta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

