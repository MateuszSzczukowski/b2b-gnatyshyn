
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=389 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
INSERT INTO `wp_commentmeta` VALUES (328,5333,'rating','5'),(108,710,'rating','5'),(110,710,'verified','0'),(129,905,'rating','5'),(131,905,'verified','0'),(44,132,'is_customer_note','1'),(135,906,'rating','5'),(137,906,'verified','0'),(120,712,'rating','5'),(122,712,'verified','0'),(141,907,'rating','5'),(143,907,'verified','0'),(147,908,'rating','5'),(149,908,'verified','0'),(157,926,'rating','5'),(159,926,'verified','0'),(325,5230,'verified','0'),(324,5230,'rating','5'),(322,5229,'rating','5'),(323,5229,'verified','0'),(164,997,'rating','5'),(166,997,'verified','0'),(319,5029,'rating','5'),(320,5029,'verified','0'),(170,998,'rating','5'),(172,998,'verified','0'),(178,1070,'rating','5'),(180,1070,'verified','0'),(192,1104,'rating','5'),(194,1104,'verified','0'),(199,1133,'rating','5'),(201,1133,'verified','0'),(205,1134,'rating','5'),(207,1134,'verified','0'),(227,1377,'rating','5'),(229,1377,'verified','0'),(213,1270,'rating','5'),(215,1270,'verified','0'),(317,4467,'verified','0'),(219,1283,'rating','5'),(221,1283,'verified','0'),(342,5914,'rating','5'),(343,5914,'verified','0'),(233,1378,'rating','5'),(235,1378,'verified','0'),(248,2720,'rating','5'),(250,2720,'verified','0'),(268,2894,'verified','0'),(295,3635,'verified','0'),(266,2894,'rating','5'),(308,4165,'rating','5'),(310,4165,'verified','0'),(287,3634,'rating','5'),(289,3634,'verified','0'),(293,3635,'rating','5'),(272,2895,'rating','5'),(274,2895,'verified','0'),(329,5333,'verified','0'),(330,5728,'rating','5'),(331,5728,'verified','0'),(332,5731,'rating','5'),(333,5731,'verified','0'),(315,4467,'rating','5'),(278,2896,'rating','5'),(280,2896,'verified','0'),(299,3650,'rating','5'),(301,3650,'verified','0'),(354,6063,'rating','5'),(355,6063,'verified','0'),(356,6064,'rating','5'),(357,6064,'verified','0'),(358,6149,'rating','5'),(359,6149,'verified','0'),(360,6150,'rating','5'),(361,6150,'verified','0'),(362,6151,'rating','5'),(363,6151,'verified','0'),(369,7184,'rating','5'),(370,7184,'verified','0'),(371,7185,'rating','5'),(372,7185,'verified','0'),(373,7186,'rating','5'),(374,7186,'verified','0'),(375,7187,'rating','5'),(376,7187,'verified','0'),(377,7211,'rating','5'),(378,7211,'verified','0'),(379,7266,'rating','5'),(380,7266,'verified','0'),(381,7267,'rating','5'),(382,7267,'verified','0'),(383,7268,'rating','5'),(384,7268,'verified','0'),(385,7269,'rating','5'),(386,7269,'verified','0'),(387,7465,'rating','5'),(388,7465,'verified','0');
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

