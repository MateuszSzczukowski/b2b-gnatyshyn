
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wc_admin_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wc_admin_notes` (
  `note_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info',
  `content_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wc_admin_notes` WRITE;
/*!40000 ALTER TABLE `wp_wc_admin_notes` DISABLE KEYS */;
INSERT INTO `wp_wc_admin_notes` VALUES (1,'wc-admin-historical-data','update','en_US','WooCommerce Admin: Historical Analytics Data','To view your historical analytics data, you must process your existing orders and customers.','info','{}','actioned','woocommerce-admin','2020-03-12 17:04:21',NULL,0,'',NULL,0),(3,'wc-admin-wc-helper-connection','info','en_US','Connect to WooCommerce.com','Connect to get important product notifications and updates.','info','{}','unactioned','woocommerce-admin','2020-03-12 18:04:21',NULL,0,'',NULL,0),(4,'wc-update-db-reminder','update','en_US','Aktualizacja bazy danych WooCommerce wykonana','Aktualizacja danych WooCommerce została zakończona. Dziękujemy za aktualizację do najnowszej wersji!','info','{}','actioned','woocommerce-core','2020-02-03 19:04:21',NULL,0,'plain',NULL,0),(6,'wc-admin-mobile-app','info','en_US','Install Woo mobile app','Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.','phone','{}','unactioned','woocommerce-admin','2020-03-14 18:04:50',NULL,0,'',NULL,0),(7,'wc-admin-onboarding-email-marketing','info','en_US','Tips, product updates, and inspiration','Jesteśmy tu dla Ciebie - poznaj porady, aktualizacje produktów i inspiracje bezpośrednio do swojej skrzynki pocztowej.','mail','{}','unactioned','woocommerce-admin','2020-03-14 18:04:50',NULL,0,'',NULL,0),(16,'wc-admin-orders-milestone','info','en_US','Congratulations on processing 1000 orders!','Another order milestone! Take a look at your Orders Report to review your orders to date.','trophy','{}','unactioned','woocommerce-admin','2020-06-07 18:08:53',NULL,0,'',NULL,0),(17,'wc-admin-marketing-intro','info','en_US','Connect with your audience','Grow your customer base and increase your sales with marketing tools built for WooCommerce.','speaker','{}','unactioned','woocommerce-admin','2020-06-15 18:10:20',NULL,0,'',NULL,0),(9,'wc-admin-facebook-extension','info','en_US','Market on Facebook','Grow your business by targeting the right people and driving sales with Facebook. You can install this free extension now.','thumbs-up','{}','unactioned','woocommerce-admin','2020-03-15 18:04:31',NULL,0,'',NULL,0),(15,'wc-admin-new-sales-record','info','en_US','New sales record!','Woohoo, May 13th was your record day for sales! Net Sales was 4 320,00 zł beating the previous record of 3 106,00 zł set on March 25th.','trophy','{\"old_record_date\":\"2020-03-25\",\"old_record_amt\":3106,\"new_record_date\":\"2020-05-13\",\"new_record_amt\":4320}','unactioned','woocommerce-admin','2020-05-14 18:09:19',NULL,0,'',NULL,0),(18,'wc-admin-store-notice-giving-feedback-2','info','en_US','Give feedback','Now that you’ve chosen us as a partner, our goal is to make sure we\'re providing the right tools to meet your needs. We\'re looking forward to having your feedback on the store setup experience so we can improve it in the future.','info','{}','unactioned','woocommerce-admin','2020-07-13 18:10:05',NULL,0,'plain','',0),(26,'mailchimp-for-woocommerce-incomplete-install','warning','en_US','Mailchimp For WooCommerce','Plugin is not yet connected to a Mailchimp account. To complete the connection, open the settings page.','info','{\"getting_started\":true,\"activated\":1605617229,\"activated_formatted\":\"November 17th\"}','unactioned','mailchimp-for-woocommerce','2020-11-17 11:47:09',NULL,0,'plain','',0),(25,'wc-admin-coupon-page-moved','update','en_US','Zarządzanie kuponami zostało przeniesione!','Coupons can now be managed from Marketing &gt; Coupons. Click the button below to remove the legacy WooCommerce &gt; Coupons menu item.','info','{}','actioned','woocommerce-admin','2020-09-09 11:11:58',NULL,0,'plain','',0),(20,'wc-admin-onboarding-profiler-reminder','update','en_US','Witaj w WooCommerce! Skonfiguruj swój sklep i zacznij sprzedaż','Pomożemy ci w przejściu przez etapy konfiguracji sklepu.','info','{}','actioned','woocommerce-admin','2020-08-26 07:16:55',NULL,0,'plain','',0),(21,'wc-admin-draw-attention','info','en_US','Jak przyciągnąć uwagę do Twojego sklepu internetowego','To get you started, here are seven ways to boost your sales and avoid getting drowned out by similar, mass-produced products competing for the same buyers.','info','{}','unactioned','woocommerce-admin','2020-09-05 08:50:38',NULL,0,'plain','',0),(22,'ecomm-need-help-setting-up-your-store','info','en_US','Need help setting up your Store?','Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.','info','{}','pending','woocommerce.com','2020-09-05 02:51:40',NULL,0,'plain','',0),(23,'woocommerce-services','info','en_US','WooCommerce Services','WooCommerce Services helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.','info','{}','pending','woocommerce.com','2020-09-05 02:51:40',NULL,0,'plain','',0),(24,'ecomm-unique-shopping-experience','info','en_US','For a shopping experience as unique as your customers','Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.','info','{}','pending','woocommerce.com','2020-09-05 02:51:40',NULL,0,'plain','',0);
/*!40000 ALTER TABLE `wp_wc_admin_notes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

