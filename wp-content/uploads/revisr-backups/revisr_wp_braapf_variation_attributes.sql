
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_braapf_variation_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_braapf_variation_attributes` (
  `post_id` bigint NOT NULL,
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  UNIQUE KEY `uniqueid` (`post_id`,`taxonomy`),
  KEY `post_id` (`post_id`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_braapf_variation_attributes` WRITE;
/*!40000 ALTER TABLE `wp_braapf_variation_attributes` DISABLE KEYS */;
INSERT INTO `wp_braapf_variation_attributes` VALUES (12191,'pa_rozmiar'),(12196,'pa_kolor'),(12206,'pa_kolor'),(12229,'pa_srednica'),(12249,'pa_rozmiar'),(12254,'pa_kolor'),(12322,'pa_rozmiar'),(12324,'pa_rozmiar'),(12327,'pa_rozmiar'),(12331,'pa_kolor'),(12334,'pa_kolor'),(12342,'pa_kolor'),(12344,'pa_kolor'),(12354,'pa_kolor'),(12369,'pa_kolor'),(12396,'pa_kolor'),(12411,'pa_kolor'),(12418,'pa_kolor'),(12425,'pa_kolor'),(12436,'pa_kolor'),(12458,'pa_kolor'),(12461,'pa_kolor'),(12466,'pa_kolor'),(12476,'pa_kolor'),(12487,'pa_kolor'),(12493,'pa_rozmiar'),(12537,'pa_kolor'),(12549,'pa_kolor'),(12555,'pa_kolor'),(12560,'pa_kolor'),(12567,'pa_inne-produkty'),(12599,'pa_kolor'),(12602,'pa_kolor'),(12611,'pa_kolor'),(12616,'pa_kolor'),(12621,'pa_kolor'),(12629,'pa_kolor'),(12635,'pa_kolor'),(12645,'pa_kolor'),(12653,'pa_kolor'),(12662,'pa_kolor'),(12697,'pa_kolor'),(12702,'pa_kolor'),(12707,'pa_kolor'),(12713,'pa_kolor'),(12715,'pa_kolor'),(12722,'pa_kolor'),(12727,'pa_kolor'),(12770,'pa_kolor'),(12782,'pa_kolor'),(12788,'pa_kolor'),(12800,'pa_kolor'),(13027,'pa_grubosc'),(13033,'pa_inne-produkty'),(13131,'pa_kolor'),(13135,'pa_kolor'),(13144,'pa_kolor'),(13147,'pa_kolor'),(13179,'pa_dlugosc'),(13275,'pa_pojemnosc'),(13282,'pa_inne-produkty'),(13298,'pa_pojemnosc'),(13498,'pa_kolor'),(13507,'pa_kolor'),(13525,'pa_kolor'),(13530,'pa_kolor'),(13540,'pa_srednica'),(13665,'pa_kolor'),(13755,'pa_kolor'),(13954,'pa_kolor'),(13977,'pa_kolor'),(14446,'pa_kolor'),(14455,'pa_kolor'),(14467,'pa_kolor'),(14467,'pa_typ'),(14495,'pa_srednica'),(14570,'pa_kolor'),(15138,'pa_rozmiar'),(15165,'pa_rozmiar'),(15500,'pa_kolor'),(15510,'pa_kolor'),(15807,'pa_rozmiar'),(15850,'pa_kolor'),(15922,'pa_dlugosc'),(16261,'pa_kolor'),(16289,'pa_stezenie'),(16308,'pa_kolor'),(16433,'pa_stezenie-aktywatora'),(16457,'pa_pojemnosc'),(16460,'pa_pojemnosc'),(16477,'pa_pojemnosc'),(16483,'pa_pojemnosc'),(16563,'pa_inne-produkty'),(16585,'pa_inne-produkty'),(16625,'pa_inne-produkty'),(16625,'pa_pojemnosc'),(16659,'pa_pojemnosc'),(16816,'pa_stezenie'),(16855,'pa_pojemnosc'),(16860,'pa_pojemnosc'),(16867,'pa_pojemnosc'),(16870,'pa_pojemnosc'),(16876,'pa_pojemnosc'),(16881,'pa_pojemnosc');
/*!40000 ALTER TABLE `wp_braapf_variation_attributes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

