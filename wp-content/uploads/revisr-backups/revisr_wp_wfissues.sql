
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wfissues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wfissues` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `time` int unsigned NOT NULL,
  `lastUpdated` int unsigned NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `severity` tinyint unsigned NOT NULL,
  `ignoreP` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ignoreC` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `shortMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `longMsg` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `lastUpdated` (`lastUpdated`),
  KEY `status` (`status`),
  KEY `ignoreP` (`ignoreP`),
  KEY `ignoreC` (`ignoreC`)
) ENGINE=MyISAM AUTO_INCREMENT=18050 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wfissues` WRITE;
/*!40000 ALTER TABLE `wp_wfissues` DISABLE KEYS */;
INSERT INTO `wp_wfissues` VALUES (18046,1594674425,1594674425,'new','knownfile',50,'7d1d02caa825dbcd7e455bdfba4f8459','5c7a27a31796c51a2a727ff0a90a0e67','Modified plugin file: wp-content/plugins/wc-variations-radio-buttons/wc-variations-radio-buttons.php','This file belongs to plugin \"WC Variations Radio Buttons\" version \"2.0.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:78:\"wp-content/plugins/wc-variations-radio-buttons/wc-variations-radio-buttons.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:27:\"WC Variations Radio Buttons\";s:8:\"cVersion\";s:5:\"2.0.2\";s:4:\"cKey\";s:59:\"wc-variations-radio-buttons/wc-variations-radio-buttons.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(18047,1594674425,1594674425,'new','knownfile',50,'2757da301ceda50a3d4f60da90dd4589','3d6fbb837fec3ea17b1d384fa51a73e9','Modified plugin file: wp-content/plugins/woocommerce-ajax-filters/templates/slider.php','This file belongs to plugin \"Advanced AJAX Product Filters for WooCommerce\" version \"1.4.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:64:\"wp-content/plugins/woocommerce-ajax-filters/templates/slider.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:45:\"Advanced AJAX Product Filters for WooCommerce\";s:8:\"cVersion\";s:5:\"1.4.2\";s:4:\"cKey\";s:48:\"woocommerce-ajax-filters/woocommerce-filters.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(18048,1594675120,1594675120,'new','easyPassword',75,'8bf1211fd4b7b94528899de0a43b9fb3','2a84fec6ed923894bcdd177c46399248','User \"czykita666\" with \'subscriber\' access has a very easy password.','A user with \'subscriber\' access has a password that is very easy to guess. Please either change it or ask the user to change their password.','a:6:{s:2:\"ID\";i:378;s:10:\"user_login\";s:10:\"czykita666\";s:10:\"user_email\";s:20:\"czykita666@gmail.com\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:12:\"editUserLink\";s:60:\"https://gnatyshyn-shop.pl/wp-admin/user-edit.php?user_id=378\";}'),(18049,1594800746,1594800746,'new','wfPluginUpgrade',50,'6fbe5aaad3833a7628c6c8dc3c460354','6fbe5aaad3833a7628c6c8dc3c460354','The Plugin \"Jetpack z WordPress.com\" needs an upgrade (8.7 -> 8.7.1).','You need to upgrade \"Jetpack z WordPress.com\" to the newest version to ensure you have any security fixes the developer has released.','a:21:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:23:\"Jetpack z WordPress.com\";s:9:\"PluginURI\";s:19:\"https://jetpack.com\";s:7:\"Version\";s:3:\"8.7\";s:11:\"Description\";s:317:\"Sprowadź moc chmury WordPress.com do własnego bloga WordPress. Jetpack pozwoli ci na połączenie własnego bloga z kontem na WordPress.com aby móc korzystać z jego zaawansowanych funkcji, zwykle dostępnych tylko dla użytkowników WordPress.com. <cite>Autor: <a href=\"https://jetpack.com\">Automattic</a>.</cite>\";s:6:\"Author\";s:44:\"<a href=\"https://jetpack.com\">Automattic</a>\";s:9:\"AuthorURI\";s:19:\"https://jetpack.com\";s:10:\"TextDomain\";s:7:\"jetpack\";s:10:\"DomainPath\";s:11:\"/languages/\";s:7:\"Network\";b:0;s:10:\"RequiresWP\";s:3:\"5.3\";s:11:\"RequiresPHP\";s:3:\"5.6\";s:5:\"Title\";s:57:\"<a href=\"https://jetpack.com\">Jetpack z WordPress.com</a>\";s:10:\"AuthorName\";s:10:\"Automattic\";s:10:\"pluginFile\";s:81:\"/home/gnatys/public_html/gnatyshyn-shop.pl/wp-content/plugins/jetpack/jetpack.php\";s:10:\"newVersion\";s:5:\"8.7.1\";s:4:\"slug\";s:7:\"jetpack\";s:5:\"wpURL\";s:37:\"https://wordpress.org/plugins/jetpack\";s:10:\"vulnerable\";b:0;}');
/*!40000 ALTER TABLE `wp_wfissues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

