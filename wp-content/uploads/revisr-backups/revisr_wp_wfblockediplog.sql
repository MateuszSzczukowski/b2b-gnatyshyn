
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wfblockediplog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wfblockediplog` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `countryCode` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blockCount` int unsigned NOT NULL DEFAULT '0',
  `unixday` int unsigned NOT NULL,
  `blockType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'generic',
  PRIMARY KEY (`IP`,`unixday`,`blockType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wfblockediplog` WRITE;
/*!40000 ALTER TABLE `wp_wfblockediplog` DISABLE KEYS */;
INSERT INTO `wp_wfblockediplog` VALUES (_binary '\0\0\0\0\0\0\0\0\0\0��(X�\�','US',1,18454,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��%;5]','FR',1,18453,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��gR\�','US',1,18430,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��Y\�/\�','RU',1,18442,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��gR\�','US',1,18448,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��ؗ�','US',2,18439,'brute'),(_binary '\0\0\0\0\0\0\0\0\0\0���\�\�\�','US',1,18440,'brute'),(_binary '\0\0\0\0\0\0\0\0\0\0���\�\�\�','US',2,18440,'brute'),(_binary '\0\0\0\0\0\0\0\0\0\0��4���','NL',1,18441,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��ؗ�5','US',1,18441,'brute'),(_binary '\0\0\0\0\0\0\0\0\0\0��4\�5q','US',1,18450,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��gR\�','US',1,18433,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��gR\�','US',1,18432,'waf'),(_binary '\0\0\0\0\0\0\0\0\0\0��A`Q\"','US',2,18435,'brute');
/*!40000 ALTER TABLE `wp_wfblockediplog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

