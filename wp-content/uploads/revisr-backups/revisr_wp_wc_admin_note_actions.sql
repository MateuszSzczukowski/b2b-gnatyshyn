
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wc_admin_note_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wc_admin_note_actions` (
  `action_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `note_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`action_id`),
  KEY `note_id` (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1824 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wc_admin_note_actions` WRITE;
/*!40000 ALTER TABLE `wp_wc_admin_note_actions` DISABLE KEYS */;
INSERT INTO `wp_wc_admin_note_actions` VALUES (1,1,'get-started','Get Started','?page=wc-admin&path=/analytics/settings&import=true','actioned',1,''),(3,3,'connect','Połącz','?page=wc-addons&section=helper','actioned',0,''),(877,20,'skip-profiler','Pomiń konfigurację','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=wc-admin&reset_profiler=0','actioned',0,''),(16,6,'learn-more','Dowiedz się więcej','https://woocommerce.com/mobile/','actioned',0,''),(17,7,'yes-please','Tak, proszę!','https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin','actioned',0,''),(27,16,'review-orders','Review your orders','?page=wc-admin&path=/analytics/orders','actioned',0,''),(28,17,'open-marketing-hub','Open marketing hub','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=wc-admin&path=/marketing','actioned',0,''),(19,9,'learn-more','Dowiedz się więcej','https://woocommerce.com/products/facebook/','unactioned',0,''),(20,9,'install-now','Install now','','unactioned',1,''),(26,15,'view-report','Zobacz raport','?page=wc-admin&path=/analytics/revenue&period=custom&compare=previous_year&after=2020-05-13&before=2020-05-13','actioned',0,''),(29,18,'share-feedback','Share feedback','https://automattic.survey.fm/new-onboarding-survey','actioned',0,''),(1805,26,'settings','Open Settings','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=mailchimp-woocommerce','actioned',0,''),(876,20,'continue-profiler','Kontynuuj konfigurację sklepu','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=wc-admin&enable_onboarding=1','unactioned',1,''),(1822,23,'learn-more','Learn more','https://docs.woocommerce.com/document/woocommerce-services/?utm_source=inbox','unactioned',1,''),(1754,21,'learn-more','Dowiedz się więcej','https://woocommerce.com/posts/how-to-make-your-online-store-stand-out/?utm_source=inbox','actioned',1,''),(1821,22,'set-up-concierge','Schedule free session','https://wordpress.com/me/concierge','actioned',1,''),(1804,25,'remove-legacy-coupon-menu','Usuń przestarzałe menu kuponów','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=wc-admin&action=remove-coupon-menu','actioned',1,''),(1823,24,'learn-more-ecomm-unique-shopping-experience','Learn more','https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox','actioned',1,''),(1803,4,'update-db_done','Dziękujemy!','https://gnatyshyn-shop.pl/wp-admin/admin.php?page=mailchimp-woocommerce&wc-hide-notice=update&_wc_notice_nonce=c6550079f0','actioned',1,'');
/*!40000 ALTER TABLE `wp_wc_admin_note_actions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

