
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_revisr` (
  `id` mediumint NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  `user` varchar(60) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_revisr` WRITE;
/*!40000 ALTER TABLE `wp_revisr` DISABLE KEYS */;
INSERT INTO `wp_revisr` VALUES (1,'2020-08-24 12:40:15','Successfully created a new repository.','init','vollthegreat'),(2,'2020-08-24 13:34:35','Successfully backed up the database.','backup','vollthegreat'),(3,'2020-08-24 13:34:35','There was an error committing the changes to the local repository.','error','vollthegreat'),(4,'2020-08-24 13:41:30','There was an error committing the changes to the local repository.','error','vollthegreat'),(5,'2020-08-24 13:46:26','Committed <a href=\"https://gnatyshyn-shop.pl/wp-admin/admin.php?page=revisr_view_commit&commit=869705c&success=true\">#869705c</a> to the local repository.','commit','vollthegreat'),(6,'2020-08-24 13:46:51','Reverted to commit <a href=\"https://gnatyshyn-shop.pl/wp-admin/admin.php?page=revisr_view_commit&commit=869705c\">#869705c</a>.','revert','vollthegreat'),(7,'2020-08-24 13:49:47','Successfully backed up the database.','backup','Nikita'),(8,'2020-08-24 13:49:48','Committed <a href=\"https://gnatyshyn-shop.pl/wp-admin/admin.php?page=revisr_view_commit&commit=538b4ce&success=true\">#538b4ce</a> to the local repository.','commit','Nikita'),(9,'2020-08-24 13:49:48','Error pushing changes to the remote repository.','error','Nikita'),(10,'2020-08-24 13:51:27','Successfully backed up the database.','backup','vollthegreat'),(11,'2020-08-24 14:18:29','Successfully pushed 2 commits to origin/master.','push','vollthegreat'),(12,'2020-11-18 10:40:40','Error staging files.','error','vollthegreat'),(13,'2020-11-18 10:40:40','There was an error committing the changes to the local repository.','error','vollthegreat'),(14,'2020-11-22 18:41:54','Error staging files.','error','Nikita'),(15,'2020-11-22 18:41:54','There was an error committing the changes to the local repository.','error','Nikita'),(16,'2020-11-22 18:42:28','Error staging files.','error','Nikita'),(17,'2020-11-22 18:42:29','There was an error committing the changes to the local repository.','error','Nikita'),(18,'2020-11-22 18:46:21','Successfully backed up the database.','backup','vollthegreat'),(19,'2020-11-22 18:46:21','Error staging files.','error','vollthegreat'),(20,'2020-11-22 18:46:21','There was an error committing the changes to the local repository.','error','vollthegreat');
/*!40000 ALTER TABLE `wp_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

