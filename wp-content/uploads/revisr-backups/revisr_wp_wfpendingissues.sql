
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wfpendingissues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wfpendingissues` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `time` int unsigned NOT NULL,
  `lastUpdated` int unsigned NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `severity` tinyint unsigned NOT NULL,
  `ignoreP` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ignoreC` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `shortMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `longMsg` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `lastUpdated` (`lastUpdated`),
  KEY `status` (`status`),
  KEY `ignoreP` (`ignoreP`),
  KEY `ignoreC` (`ignoreC`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wfpendingissues` WRITE;
/*!40000 ALTER TABLE `wp_wfpendingissues` DISABLE KEYS */;
INSERT INTO `wp_wfpendingissues` VALUES (1,1594673638,1594673638,'new','knownfile',50,'2e267707a4485c09bfa3649504515092','41ba73f653afcc9129b45fe54b4d0f66','Modified plugin file: wp-content/plugins/one-click-demo-import/vendor/autoload.php','This file belongs to plugin \"One Click Demo Import\" version \"2.5.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:60:\"wp-content/plugins/one-click-demo-import/vendor/autoload.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:21:\"One Click Demo Import\";s:8:\"cVersion\";s:5:\"2.5.2\";s:4:\"cKey\";s:47:\"one-click-demo-import/one-click-demo-import.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(2,1594673638,1594673638,'new','knownfile',50,'355946c6a15e69e2eb8c2be3b9150d71','b3753226b3fe239c72afc0b6736244f2','Modified plugin file: wp-content/plugins/one-click-demo-import/vendor/composer/autoload_real.php','This file belongs to plugin \"One Click Demo Import\" version \"2.5.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:74:\"wp-content/plugins/one-click-demo-import/vendor/composer/autoload_real.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:21:\"One Click Demo Import\";s:8:\"cVersion\";s:5:\"2.5.2\";s:4:\"cKey\";s:47:\"one-click-demo-import/one-click-demo-import.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(3,1594673638,1594673638,'new','knownfile',50,'a7d35c258d37c2231e32588458f59e98','46ad10b960944095b31f30629c0a5c1f','Modified plugin file: wp-content/plugins/one-click-demo-import/vendor/composer/autoload_static.php','This file belongs to plugin \"One Click Demo Import\" version \"2.5.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:76:\"wp-content/plugins/one-click-demo-import/vendor/composer/autoload_static.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:21:\"One Click Demo Import\";s:8:\"cVersion\";s:5:\"2.5.2\";s:4:\"cKey\";s:47:\"one-click-demo-import/one-click-demo-import.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(4,1594673687,1594673687,'new','knownfile',50,'7d1d02caa825dbcd7e455bdfba4f8459','5c7a27a31796c51a2a727ff0a90a0e67','Modified plugin file: wp-content/plugins/wc-variations-radio-buttons/wc-variations-radio-buttons.php','This file belongs to plugin \"WC Variations Radio Buttons\" version \"2.0.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:78:\"wp-content/plugins/wc-variations-radio-buttons/wc-variations-radio-buttons.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:27:\"WC Variations Radio Buttons\";s:8:\"cVersion\";s:5:\"2.0.2\";s:4:\"cKey\";s:59:\"wc-variations-radio-buttons/wc-variations-radio-buttons.php\";s:10:\"haveIssues\";s:7:\"plugins\";}'),(5,1594673771,1594673771,'new','knownfile',50,'2757da301ceda50a3d4f60da90dd4589','3d6fbb837fec3ea17b1d384fa51a73e9','Modified plugin file: wp-content/plugins/woocommerce-ajax-filters/templates/slider.php','This file belongs to plugin \"Advanced AJAX Product Filters for WooCommerce\" version \"1.4.2\" and has been modified from the file that is distributed by WordPress.org for this version. Please use the link to see how the file has changed. If you have modified this file yourself, you can safely ignore this warning. If you see a lot of changed files in a plugin that have been made by the author, then try uninstalling and reinstalling the plugin to force an upgrade. Doing this is a workaround for plugin authors who don\'t manage their code correctly. [See our FAQ on www.wordfence.com for more info]','a:9:{s:4:\"file\";s:64:\"wp-content/plugins/woocommerce-ajax-filters/templates/slider.php\";s:5:\"cType\";s:6:\"plugin\";s:7:\"canDiff\";b:1;s:6:\"canFix\";b:1;s:9:\"canDelete\";b:0;s:5:\"cName\";s:45:\"Advanced AJAX Product Filters for WooCommerce\";s:8:\"cVersion\";s:5:\"1.4.2\";s:4:\"cKey\";s:48:\"woocommerce-ajax-filters/woocommerce-filters.php\";s:10:\"haveIssues\";s:7:\"plugins\";}');
/*!40000 ALTER TABLE `wp_wfpendingissues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

