
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_wfcrawlers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wp_wfcrawlers` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `patternSig` binary(16) NOT NULL,
  `status` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastUpdate` int unsigned NOT NULL,
  `PTR` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  PRIMARY KEY (`IP`,`patternSig`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_wfcrawlers` WRITE;
/*!40000 ALTER TABLE `wp_wfcrawlers` DISABLE KEYS */;
INSERT INTO `wp_wfcrawlers` VALUES (_binary '\0\0\0\0\0\0\0\0\0\0��B�I\�',_binary '�����>�b0�oQ1�\�','verified',1594580154,'crawl-66-249-73-242.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�F_',_binary '�����>�b0�oQ1�\�','verified',1594764382,'crawl-66-249-70-95.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�K\�',_binary '�����>�b0�oQ1�\�','verified',1594768990,'crawl-66-249-75-232.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�K\�',_binary '�����>�b0�oQ1�\�','verified',1594241887,'crawl-66-249-75-236.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@I',_binary '�����>�b0�oQ1�\�','verified',1594351306,'crawl-66-249-64-73.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@K',_binary '�����>�b0�oQ1�\�','verified',1594351631,'crawl-66-249-64-75.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@U',_binary '�����>�b0�oQ1�\�','verified',1594352234,'crawl-66-249-64-85.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@W',_binary '�����>�b0�oQ1�\�','verified',1594352530,'crawl-66-249-64-87.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@M',_binary '�����>�b0�oQ1�\�','verified',1594352990,'crawl-66-249-64-77.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�@Y',_binary '�����>�b0�oQ1�\�','verified',1594366501,'crawl-66-249-64-89.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�F]',_binary '�����>�b0�oQ1�\�','verified',1594423572,'crawl-66-249-70-93.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�F[',_binary '�����>�b0�oQ1�\�','verified',1594423574,'crawl-66-249-70-91.googlebot.com'),(_binary '\0\0\0\0\0\0\0\0\0\0��B�B�',_binary '�����>�b0�oQ1�\�','verified',1594242053,'crawl-66-249-66-155.googlebot.com');
/*!40000 ALTER TABLE `wp_wfcrawlers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

