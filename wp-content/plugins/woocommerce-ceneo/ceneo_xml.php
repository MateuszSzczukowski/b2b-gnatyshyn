<?php
/**
 * @deprecated
 *
 * This handle is deprecated. Do not use it.
 */

define('XML_GENERATOR', 'ceneo');

$plugin_dir = dirname(__FILE__);
chdir('../../../');
require_once( 'wp-load.php' );

$storageFactory = new \CeneoVendor\WPDesk\PluginBuilder\Storage\StorageFactory();
$plugin = $storageFactory->create_storage()->get_from_storage( WooCommerce_Ceneo_Plugin::class );
$generatorFactory = new WPDesk\Xml\Xml_Factory( $plugin );
$generator = $generatorFactory->get_generator(XML_GENERATOR);
header( 'Content-Type: text/xml' );
echo html_entity_decode( $generator->get_xml() );
