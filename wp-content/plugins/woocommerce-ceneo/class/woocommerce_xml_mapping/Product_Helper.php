<?php

namespace WPDesk\Woocommerce_Xml_Mapping;

/**
 * Class to easy access parent and post data of product. It also caches last access for memory saving
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Product_Helper {
	const PRODUCT = 'product';
	const PARENT = 'parent';
	const POST = 'post';
	const PARENT_POST = 'parent_post';
	const ID = 'id';
	const SELF = 'self';

	/** @var array */
	private static $last_used;

	public function __construct( \WC_Product $product ) {
		if ( self::$last_used === null || $this->get_product()->get_id() !== $product->get_id() ) {
			self::$last_used = [
				self::PRODUCT => $product,
				self::ID      => $product->get_id(),
				self::SELF    => $this
			];
		}
	}

	/**
	 * @param $id
	 *
	 * @return Product_Helper
	 */
	public static function create_from_id( $id ) {
		if ( self::$last_used === null || self::$last_used[ self::ID ] !== $id ) {
			$product = wc_get_product( $id );
			if( !empty( $product ) ){
				return new Product_Helper( $product );
			}
			throw new \Exception('Product not found');
		}

		return self::$last_used[ self::SELF ];
	}

	/**
	 * If product IS a variant of parent product (NOT have variants)
	 *
	 * @return bool
	 */
	public function is_variant() {
		return $this->get_product()->is_type( 'variation' );
	}

	/**
	 * If product have variants.
	 *
	 * @return bool
	 */
	public function is_variable() {
		return $this->get_product()->is_type( 'variable' );
	}

	/**
	 * @return \WC_Product
	 */
	public function get_product() {
		return self::$last_used[ self::PRODUCT ];
	}

	/**
	 * @return \WP_Post
	 */
	public function get_post() {
		if ( ! isset( self::$last_used[ self::POST ] ) || self::$last_used[ self::POST ] === null ) {
			self::$last_used[ self::POST ] = get_post( $this->get_product()->get_id() );
		}

		return self::$last_used[ self::POST ];
	}

	/**
	 * @return \WP_Post
	 */
	public function get_parent_post() {
		if ( empty( self::$last_used[ self::PARENT_POST ] ) ) {
			self::$last_used[ self::PARENT_POST ] = get_post( $this->get_post()->post_parent );
		}

		return self::$last_used[ self::PARENT_POST ];
	}

	/**
	 * @return \WC_Product
	 */
	public function get_parent_product() {
		if ( empty( self::$last_used[ self::PARENT ] ) ) {
			self::$last_used[ self::PARENT ] = wc_get_product( $this->get_post()->post_parent );
		}

		return self::$last_used[ self::PARENT ];
	}


	/**
	 * @param string $attribute_meta_key meta key.
	 *
	 * @param bool $single true if single meta.
	 * @param bool $parent_fallback true if meta should fallback by parent product.
	 *
	 * @return mixed
	 */
	public function get_variation_meta( $attribute_meta_key, $single = true, $parent_fallback = true ){
		$value = null;
		if( $this->is_variant() ) {
			$value = wpdesk_get_variation_meta( $this->get_product(), $attribute_meta_key, $single );
			if ( empty( $value ) && true === $parent_fallback ) {
				$value = wpdesk_get_product_meta( $this->get_parent_product(), $attribute_meta_key, $single );
			};
		}
		return $value;
	}

}
