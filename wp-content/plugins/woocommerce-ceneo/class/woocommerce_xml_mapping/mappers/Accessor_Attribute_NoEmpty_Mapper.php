<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

/**
 * Maps value provided by given accessor to xml - if the given value is not empty
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Accessor_Attribute_NoEmpty_Mapper extends Accessor_Attribute_Mapper {

	public function map_to_xml( \DOMNode $element, $source ) {
		$value = $this->accessor->get_value( $source );

		if ( $element instanceof \DOMElement && ! empty( $value ) ) {
			$element->setAttribute( $this->name, esc_attr( $value ) );
		}

		return $element;
	}
}