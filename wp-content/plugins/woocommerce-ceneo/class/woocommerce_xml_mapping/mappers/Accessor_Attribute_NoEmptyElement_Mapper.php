<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

/**
 * Maps value provided by given accessor to xml attribute AND if mapped value is empty - removes whole element
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Accessor_Attribute_NoEmptyElement_Mapper extends Accessor_Attribute_Mapper {

	public function map_to_xml( \DOMNode $element, $source ) {
		$element = parent::map_to_xml( $element, $source );
		if ( $element instanceof \DOMElement ) {
			if ( empty( $element->getAttribute( $this->name ) ) ) {
				$element->parentNode->removeChild( $element );

				return $element->parentNode;
			}
		}

		return $element;
	}
}