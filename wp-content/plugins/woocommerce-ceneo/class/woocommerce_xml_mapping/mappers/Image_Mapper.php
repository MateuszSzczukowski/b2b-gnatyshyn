<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;

/**
 * Homebook main image
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Mappers
 */
class Image_Mapper extends Abstract_Xml_Parent_Element_Mapper {

	protected $accessor;
	private $default;
	private $name;

	public function __construct( Product_ImageUrl_Accessor $accessor, $name, $default = false) {
		$this->accessor = $accessor;
		$this->default = $default;
		$this->name = $name;
	}

	public function map_to_xml( \DOMNode $element, $source ) {
		$element->appendChild( $element->ownerDocument->createCDATASection( $this->accessor->get_value( $source ) ) );
		if( $this->default ) {
			$element->setAttribute( 'default', 'true' );
		}
		$this->append_mappers_data( $element, $source );

		return $element;
	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	protected function get_name() {
		return $this->name;
	}
}
