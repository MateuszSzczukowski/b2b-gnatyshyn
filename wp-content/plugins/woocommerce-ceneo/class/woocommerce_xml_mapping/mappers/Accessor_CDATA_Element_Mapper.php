<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Accessors\Accessor;

/**
 * Maps value provided by given accessor to CDATA value
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Accessor_CDATA_Element_Mapper extends Abstract_Xml_Parent_Element_Mapper {
	/** @var string */
	protected $name;

	/** @var Accessor */
	protected $accessor;

	/**
	 * @param Accessor $accessor How to get access to source value
	 * @param string   $name Name of xml attribute
	 */
	public function __construct( Accessor $accessor, $name ) {
		$this->accessor = $accessor;
		$this->name     = $name;
	}

	protected function get_name() {
		return $this->name;
	}

	public function map_to_xml( \DOMNode $element, $source ) {
		$element->appendChild( $element->ownerDocument->createCDATASection( $this->accessor->get_value( $source ) ) );
		$this->append_mappers_data( $element, $source );

		return $element;
	}
}