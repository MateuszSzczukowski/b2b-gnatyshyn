<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Maps value provided by given accessor to CDATA value
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Accessor_CDATA_Element_NoEmpty_Mapper extends Accessor_CDATA_Element_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->accessor->get_value( $source ) );
	}

}
