<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Accessors\Accessor;

/**
 * Maps array value provided by given accessor to xml tree with parent and children
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Array_Element_Mapper extends Abstract_Xml_Parent_Element_Mapper {

	/** @var Accessor */
	protected $array_accessor;

	/** @var string */
	protected $parent_name;

	/**
	 * @param Accessor $array_accessor Accessor that returns array as a source
	 * @param string $parent_name Name of the parent in the structure
	 */
	public function __construct( Accessor $array_accessor, $parent_name ) {
		$this->array_accessor = $array_accessor;
		$this->parent_name    = $parent_name;
	}

	public function map_to_xml( \DOMNode $element, $source ) {
		$array = $this->array_accessor->get_value( $source );
		if ( is_array( $array ) && count( $array ) > 0 ) {
			foreach ( $array as $source_element ) {
				$this->append_mappers_data( $element, $source_element );
			}
		}

		return $element;
	}

	protected function get_name() {
		return $this->parent_name;
	}
}
