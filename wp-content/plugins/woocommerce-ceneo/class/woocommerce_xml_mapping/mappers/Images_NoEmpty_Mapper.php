<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * No empty images mapper.
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Mappers
 */
class Images_NoEmpty_Mapper extends Images_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return $this->mapper->is_empty( $source );
	}
}
