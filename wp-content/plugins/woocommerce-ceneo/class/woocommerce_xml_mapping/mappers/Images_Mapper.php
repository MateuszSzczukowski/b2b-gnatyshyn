<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;

/**
 * Images mapper.
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Mappers
 */
class Images_Mapper extends Abstract_Xml_Parent_Element_Mapper {

	protected $mapper;
	private $name;

	public function __construct( Abstract_Xml_Parent_Element_Mapper $mapper, $name ) {
		$this->name = $name;
		$this->mapper = $mapper;
		$this->add_mapper( $this->mapper );
	}

	protected function get_name() {
		return $this->name;
	}

}
