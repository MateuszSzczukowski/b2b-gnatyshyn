<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

/**
 * Maps value provided by given accessor to xml attribute
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
class Accessor_Attribute_Mapper extends Abstract_Accessor_Attribute_Mapper {
	public function map_to_xml( \DOMNode $element, $source ) {
		if ( $element instanceof \DOMElement ) {
			$element->setAttribute( $this->name, esc_attr( $this->accessor->get_value( $source ) ) );
		}

		return $element;
	}
}
