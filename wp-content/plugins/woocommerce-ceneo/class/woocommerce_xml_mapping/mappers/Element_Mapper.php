<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;

/**
 * Mapper that creates an xml element
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Mappers
 */
class Element_Mapper extends Abstract_Xml_Parent_Element_Mapper {
	/** @var string */
	private $name;

	public function __construct( $name ) {
		$this->name = $name;
	}

	protected function get_name() {
		return $this->name;
	}
}