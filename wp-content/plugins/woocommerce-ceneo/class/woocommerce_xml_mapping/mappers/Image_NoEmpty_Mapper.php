<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Image no empty mapper.
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Mappers
 */
class Image_NoEmpty_Mapper extends Image_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->accessor->get_value( $source) );
	}


}
