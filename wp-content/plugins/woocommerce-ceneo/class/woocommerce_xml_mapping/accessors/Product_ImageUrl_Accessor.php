<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves image url from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_ImageUrl_Accessor implements Accessor {
	/**
	 * @param \WC_Product $source
	 *
	 * @return string Empty string when no image was found
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );

		$image_id = $this->get_image_id( $source );
		if ( $image_id === null ) {
			if ( $product_helper->is_variant() ) {
				$image_id = $this->get_image_id( $product_helper->get_parent_product() );
			} else if ( $product_helper->is_variable() ) {
				$image_id = $this->get_first_variable_image( $source->get_children() );
			}
		}

		if ( $image_id !== null ) {
			return $this->get_image_url( $image_id );
		}

		return '';
	}


	/**
	 * @param array $variants_ids int[].
	 *
	 * @return string|null
	 */
	private function get_first_variable_image( array $variants_ids ){
		foreach ( $variants_ids as $id ) {
			$product = wc_get_product( $id );
			$image_id = $this->get_image_id( $product );
			if ( $image_id !== null ) {
				return $image_id;
			}
		}
		return null;
	}

	/**
	 * @param int $image_id
	 *
	 * @return string|null
	 */
	private function get_image_url( $image_id ) {
		return wp_get_attachment_image_src( $image_id, 'full' )[0];
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return int|null
	 */
	private function get_image_id( $source ) {
		$id = get_post_thumbnail_id( wpdesk_get_product_id( $source ) );

		return empty($id)? null : $id;
	}
}
