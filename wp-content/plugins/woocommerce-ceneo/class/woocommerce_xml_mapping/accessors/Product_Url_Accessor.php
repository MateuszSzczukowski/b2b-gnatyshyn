<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Retrieves url from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Url_Accessor implements Accessor {

	const WPDESK_FILTER_PRODUCT_PERMALINK = 'wpdesk_ceneo_product_permalink';

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		global $sitepress;
		$permalink = $source->get_permalink();
		if( $sitepress instanceof \SitePress ){
			$default_lang =  apply_filters( self::WPDESK_FILTER_PRODUCT_PERMALINK, \WPDesk_Product_Generator::DEFAULT_EXPORT_LANG, $permalink );
			$permalink = apply_filters( 'wpml_permalink', $permalink, $default_lang );
		}
		return htmlspecialchars( $permalink );
	}
}
