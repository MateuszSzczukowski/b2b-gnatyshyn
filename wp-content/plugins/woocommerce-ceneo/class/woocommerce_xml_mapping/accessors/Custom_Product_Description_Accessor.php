<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves description from \WC_Product
 *
 * @TODO: remove get_settings_value and use some OO
 * @TODO: clean code
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Custom_Product_Description_Accessor implements Accessor {

	const ALTERNATIVE_NAME = 'woocommerce_ceneo_alternative_desc';

	public function __construct() {
		$this->integrate_visual_composer();
	}

	/**
	 * Integrate with shortcodes of visual composer
	 */
	private function integrate_visual_composer() {
		static $once;
		if ( ! $once ) {
			$once = true;
			if ( class_exists( 'WPBMap' ) ) {
				\WPBMap::addAllMappedShortcodes();
			}
		}
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$source_helper = new Product_Helper( $source );
		$is_variation  = $source_helper->is_variant();
		if ( $is_variation ) {
			$parent_product = $source_helper->get_parent_product();
		}

		if ( $is_variation ) {
			$desc = wpdesk_get_variation_meta( $source, self::ALTERNATIVE_NAME, true );
		} else {
			$desc = wpdesk_get_product_meta( $source, self::ALTERNATIVE_NAME, true );
		}
		if ( empty( $desc ) ) {
			if ( $is_variation ) {
				$desc = get_post_meta( wpdesk_get_variation_id( $source ), '_variation_description', true );
				if ( empty( $desc ) ) {
					$desc = wpdesk_get_product_meta( $parent_product, self::ALTERNATIVE_NAME, true );
					if ( empty( $desc ) ) {
						if ( $this->get_settings_value( 'disable_the_content_filter' ) === 'on' ) {
							$desc = strip_tags( $source_helper->get_parent_post()->post_content );
						} else {
							$desc = strip_tags( apply_filters( 'the_content',
								$source_helper->get_parent_post()->post_content ) );
						}
					}
				}
			} else {
				if ( $this->get_settings_value( 'disable_the_content_filter' ) === 'on' ) {
					$desc = strip_tags( $source_helper->get_post()->post_content );
				} else {
					$desc = strip_tags( apply_filters( 'the_content', $source_helper->get_post()->post_content ) );
				}
			}
		}

		return apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.self::ALTERNATIVE_NAME, $desc, $source->get_id() );
	}

	/**
	 * @param $key
	 *
	 * @return mixed|void
	 *
	 * @deprecated To remove asap
	 */
	private function get_settings_value( $key ) {
		return get_option( 'woocommerce_ceneo_' . $key, '' );
	}
}
