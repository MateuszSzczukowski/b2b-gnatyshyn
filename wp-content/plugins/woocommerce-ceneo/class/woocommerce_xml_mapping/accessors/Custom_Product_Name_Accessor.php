<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves product name compatible from \WC_Product metabox settings.
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Custom_Product_Name_Accessor extends Product_Name_Accessor {

	const ALTERNATIVE_NAME = 'woocommerce_ceneo_alternative_name';

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$is_variation   = $product_helper->is_variant();

		if ( $is_variation ) {
			$name = $product_helper->get_variation_meta( self::ALTERNATIVE_NAME );
			if ( empty( $name ) ) {
				$name = $this->get_variation_name($source);
			}
		}else{
			$name = wpdesk_get_product_meta( $source, self::ALTERNATIVE_NAME, true );
		}

		if ( empty( $name ) ) {
			$name = parent::get_value( $source );
		}

		return apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.self::ALTERNATIVE_NAME, $name, $source->get_id() );
	}
}
