<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Retrieves WooCommerce attributes from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Attributes_Accessor implements Accessor {
	const ATTRIBUTE_VALUE = 'attribute_value';
	const ATTRIBUTE_NAME = 'attribute_name';

	const ATTRIBUTE_GLUE = ', ';

	/**
	 * Converts attributes to string
	 *
	 * @param \WC_Product $source
	 *
	 * @return array
	 */
	public function get_value( $source ) {
		$attributes = $source->get_attributes();
		/**
		 * Attributes can be:
		 * - empty
		 * - array of strings;
		 * - array of \WC_Product_Attribute. each \WC_Product_Attribute have string options;
		 * - array of \WC_Product_Attribute. each \WC_Product_Attribute have terms options.
		 */
		if ( $attributes && is_array( $attributes ) > 0 ) {
			if ( $this->are_attributes_oop( $attributes ) ) {
				return $this->map_oop_attributes_to_assoc_format( $attributes );
			}

			return $this->map_string_attributes_to_assoc_format( $attributes );
		}

		return [];
	}

	/**
	 * Maps attributes to format used by woocommerce_xml_mapping
	 *
	 * @param array $attributes
	 *
	 * @return array
	 */
	private function map_oop_attributes_to_assoc_format( array $attributes ) {
		return array_map( function ( \WC_Product_Attribute $attribute ) {
			if ( $attribute->is_taxonomy() ) {
				$values = $this->get_names_from_terms( $attribute->get_terms() );

				$taxonomy_object = $attribute->get_taxonomy_object();
				if (isset($taxonomy_object) && $taxonomy_object instanceof \stdClass && isset($taxonomy_object->attribute_label)) {
					$name = $taxonomy_object->attribute_label;
				} else {
					$name = $attribute->get_name();
				}
			} else {
				$name = $attribute->get_name();
				$values = $attribute->get_options();
			}

			return [
				self::ATTRIBUTE_NAME  => $name,
				self::ATTRIBUTE_VALUE => implode( self::ATTRIBUTE_GLUE, $values )
			];
		}, $attributes );
	}

	/**
	 * Converts terms array to array with terms names
	 *
	 * @param \WP_Term[] $terms
	 *
	 * @return string[]
	 */
	protected function get_names_from_terms( array $terms ) {
		return array_reduce( $terms, function ( $carry, \WP_Term $item ) {
			array_unshift( $carry, $item->name );

			return $carry;
		}, [] );
	}

	/**
	 * Maps attributes to format used by woocommerce_xml_mapping
	 *
	 * @param array $attributes
	 *
	 * @return array
	 */
	private function map_string_attributes_to_assoc_format( array $attributes ) {
		$converted = [];
		foreach ( $attributes as $key => $value ) {
			$taxonomy = get_taxonomy($key);
			if ($taxonomy instanceof \WP_Taxonomy) {
				$name = $taxonomy->labels->singular_name;
				if( empty($value) ){
					$terms = get_terms($taxonomy->name);
					if( is_array($terms)){
						$result = [];
						foreach( $terms as $term ){
							$result[] = $term->name;
						}
						$value = implode(self::ATTRIBUTE_GLUE, $result);
					}
				}
			} else {
				$name = $key;
			}


			$converted[] = [
				self::ATTRIBUTE_NAME  => $name,
				self::ATTRIBUTE_VALUE => $value
			];
		}

		return $converted;
	}

	/**
	 * Checks if attributes are using WC_Product_Attribute or just plain string
	 *
	 * @param array $attributes
	 *
	 * @return bool
	 */
	protected function are_attributes_oop( array $attributes ) {
		$first = reset( $attributes );

		return $first instanceof \WC_Product_Attribute;
	}
}
