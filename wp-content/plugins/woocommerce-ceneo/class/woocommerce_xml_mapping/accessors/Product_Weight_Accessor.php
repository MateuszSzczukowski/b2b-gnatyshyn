<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Retrieves weight from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Weight_Accessor implements Accessor {
	/**
	 * @param \WC_Product $source
	 *
	 * @return int|null
	 */
	public function get_value( $source ) {
		if ($source->get_weight() !== '') {
			return wc_get_weight( wc_format_decimal( $source->get_weight() ), 'kg' );
		}
		return null;
	}
}