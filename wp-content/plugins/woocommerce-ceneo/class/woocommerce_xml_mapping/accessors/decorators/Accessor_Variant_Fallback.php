<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors\Decorators;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * If used and product is a variant then returns value for a parent product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Accessor_Variant_Fallback implements Accessor {
	/** @var Accessor */
	private $decorated;

	public function __construct( Accessor $decorated ) {
		$this->decorated = $decorated;
	}

	/**
	 * @param \WC_Product $source
	 * @return mixed
	 */
	public function get_value( $source ) {
		$helper = new Product_Helper( $source );
		if ( $helper->is_variant() && $helper->get_parent_product() instanceof \WC_Product ) {
			return $this->decorated->get_value( $helper->get_parent_product() );
		}

		return $this->decorated->get_value( $source );
	}


}