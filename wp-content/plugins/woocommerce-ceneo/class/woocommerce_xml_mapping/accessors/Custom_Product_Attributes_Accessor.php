<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves custom WooCommerce attributes from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Custom_Product_Attributes_Accessor extends Product_Attributes_Accessor {

	const WPDESK_FILTER_PRODUCT_META_PREFIX = 'wpdesk_ceneo_product_meta_';

	private $attributes_keys = [];

	/**
	 * @param string[] $keys.
	 */
	public function set_attributes_keys( array $keys ){
		$this->attributes_keys = $keys;
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with below array format.
	 *  [
	 *      [
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_NAME => (string) $key,
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_VALUE => (string) $value,
	 *      ],
	 *      ...
	 *  ]
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$attributes = [];
		foreach ( $this->attributes_keys as $attribute_meta_key => $val ) {
			$value              = null;
			if ( $product_helper->is_variant() ) {
				$value = $product_helper->get_variation_meta( $attribute_meta_key );
			} else {
				$value = get_post_meta( $product_helper->get_product()->get_id(), $attribute_meta_key, true );
			}

			$value = apply_filters( self::WPDESK_FILTER_PRODUCT_META_PREFIX.$attribute_meta_key, $value, $source->get_id() );

			if ( ! empty( $value ) ) {
				$attributes[] = [
					self::ATTRIBUTE_NAME  => $val,
					self::ATTRIBUTE_VALUE => $value
				];
			}
		}

		return $attributes;
	}
}
