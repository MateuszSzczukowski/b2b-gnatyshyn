<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Retrieves stock size from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Stock_Accessor implements Accessor {
	/**
	 * @param \WC_Product $source
	 *
	 * @return int
	 */
	public function get_value( $source ) {
		if ( $source->is_in_stock() ) {
			if ( $source->get_stock_quantity() !== null ) {
				return $source->get_stock_quantity();
			}

			return 99;
		}

		return 0;
	}
}