<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Retrieves price from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Price_Accessor implements Accessor {
	/**
	 * @param \WC_Product $source
	 *
	 * @return float
	 */
	public function get_value( $source ) {
		return number_format( round( wpdesk_get_price_including_tax( $source ), 2 ), 2, '.', '' );
	}
}
