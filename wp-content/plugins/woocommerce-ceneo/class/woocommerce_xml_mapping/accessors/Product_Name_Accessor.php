<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves product name from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Name_Accessor implements Accessor {
	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$is_variation   = $product_helper->is_variant();

		if ( $is_variation ) {
			return $this->get_variation_name( $source );
		}

		return $source->get_title();
	}

	/**
	 * Return name if product is a variant
	 *
	 * @param \WC_Product_Variation $source
	 *
	 * @return string
	 */
	protected function get_variation_name( \WC_Product_Variation $source ) {
		$name       = $source->get_title();
		$attributes = $source->get_attributes();
		$name       .= ' - ';
		$first      = true;
		foreach ( $attributes as $attribute_name => $attribute_value ) {
			if ( taxonomy_exists( $attribute_name ) ) {
				$term = get_term_by( 'slug', $attribute_value, $attribute_name );
				if ( ! is_wp_error( $term ) && ! empty( $term->name ) ) {
					$attribute_value = $term->name;
				}
			}
			if ( ! $first ) {
				$name .= ', ';
			} else {
				$first = false;
			}
			$name .= $attribute_value;
		}

		return $name;
	}
}