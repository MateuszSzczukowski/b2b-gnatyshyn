<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;
use WPDesk\Xml\Integrations\Category_Tree_Interface;

/**
 * Retrieves categories from \WC_Product
 *
 * @package WPDesk\Woocommerce_Xml_Mapping\Accessors
 */
class Product_Category_Accessor implements Accessor {

	const CATEGORY_STRING_LIMIT = 255;

	/** @var Category_Tree_Interface */
	private $xml_category;

	public function set_xml_category( Category_Tree_Interface $xml_category ){
		$this->xml_category = $xml_category;
	}

	private function is_xml_category_exists(){
		return isset( $this->xml_category );
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$is_variation   = $product_helper->is_variant();
		if ($is_variation) {
			$categories = get_the_terms( wpdesk_get_product_id( $product_helper->get_parent_product() ), 'product_cat' );
		} else {
			$categories = get_the_terms( wpdesk_get_product_id( $source ), 'product_cat' );
		}

		if( $this->is_xml_category_exists() ) {
			$xmlCategoryId = - 1;
			if ( ! empty( $categories ) ) {
				foreach ( $categories as $category ) {
					$newXmlCategoryId = get_option( $this->xml_category->get_category_prefix().'_' . $category->term_id );

					if ( $newXmlCategoryId > $xmlCategoryId ) {
						$xmlCategoryId = $newXmlCategoryId;
					}
				}
			}

			if ( $xmlCategoryId > - 1 && $xmlCategoryId !== '' && $xmlCategoryId !== null ) {
				return $this->xml_category->get_category_with_full_name( $xmlCategoryId );
			}
		}

		if ( ! empty( $categories ) ) {
			$string = $tmp = '';
			foreach ( $categories as $cat ) {
				$tmp .= empty( $string ) ? $cat->name : ' - ' . $cat->name;
				if ( strlen( $tmp ) >= self::CATEGORY_STRING_LIMIT ) {
					break;
				} else {
					$string = $tmp;
				}
			}

			return $string;
		}

		return '';
	}
}
