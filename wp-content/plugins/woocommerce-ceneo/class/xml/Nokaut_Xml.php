<?php
namespace WPDesk\Xml;

use WPDesk\Nokaut_Xml_Mapping\Nokaut_Xml_Product_Mapper;

/**
 * Nokaut XML - creates Nokaut XML files.
 */
class Nokaut_Xml extends Abstract_Xml {

	const DEFAULT_EMPTY_XML_CONTENT   = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<!DOCTYPE nokaut SYSTEM \"http://www.nokaut.pl/integracja/nokaut.dtd\">\n<nokaut generator=\"WooCommerce\" >\n<offers>\n</offers>\n</nokaut>";
	const XML_GENERATE_AJAX_HANDLE    = 'generate_nokaut_xml';
	const XML_PROCESS_AJAX_HANDLE     = 'process_nokaut_xml';
	const XML_URL                     = 'wp-admin/admin-ajax.php?action=' . self::XML_GENERATE_AJAX_HANDLE;
	const XML_FILE_NAME               = 'nokaut.xml';
	const TMP_XML_FILE_NAME           = 'nokaut_tmp.xml';
	const XML_UPLOADS_DIR_NAME        = 'ceneo';
	const TRANSIENT_GENERATED         = 'nokaut_transient_generated';
	const TRANSIENT_EXPORTED_PRODUCTS = 'nokaut_transient_exported_products';
	const TRANSIENT_LAST_PRODUCT_ID   = 'nokaut_transient_last_product_id';
	const TRANSIENT_LOCK              = 'nokaut_transient_lock';

	/**
	 * @return \WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper
	 */
	protected function create_product_mapper(){
		return new Nokaut_Xml_Product_Mapper();
	}

	/**
	 * Prepares XML root element.
	 *
	 * @return DOMElement
	 */
	protected function prepare_root_element() {
		$xml_document = new \DOMDocument( '1.0', 'UTF-8' );
		$implementation = new \DOMImplementation();
		$doctype = $implementation->createDocumentType('nokaut', '', 'http://www.nokaut.pl/integracja/nokaut.dtd');
		$xml_document->appendChild( $doctype );
		$root_element = $xml_document->createElement( 'nokaut' );
		$root_element->setAttribute( 'generator', 'WooCommerce' );
		$offers_element = $xml_document->createElement( 'offers' );
		$root_element->appendChild( $offers_element );
		$xml_document->appendChild( $root_element );
		return $offers_element;
	}
}
