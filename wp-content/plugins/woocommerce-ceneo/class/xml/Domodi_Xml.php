<?php
namespace WPDesk\Xml;

use WPDesk\Domodi_Xml_Mapping\Domodi_Xml_Product_Mapper;

/**
 * Domodi XML - creates Domodi XML files.
 */
class Domodi_Xml extends Abstract_Xml {

	const DEFAULT_EMPTY_XML_CONTENT   = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<offers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1\">\n</offers>";
	const XML_GENERATE_AJAX_HANDLE    = 'generate_domodi_xml';
	const XML_PROCESS_AJAX_HANDLE     = 'process_domodi_xml';
	const XML_URL                     = 'wp-admin/admin-ajax.php?action=' . self::XML_GENERATE_AJAX_HANDLE;
	const XML_FILE_NAME               = 'domodi.xml';
	const TMP_XML_FILE_NAME           = 'domodi_tmp.xml';
	const XML_UPLOADS_DIR_NAME        = 'ceneo';
	const TRANSIENT_GENERATED         = 'domodi_transient_generated';
	const TRANSIENT_EXPORTED_PRODUCTS = 'domodi_transient_exported_products';
	const TRANSIENT_LAST_PRODUCT_ID   = 'domodi_transient_last_product_id';
	const TRANSIENT_LOCK              = 'domodi_transient_lock';

	/**
	 * @return \WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper
	 */
	protected function create_product_mapper(){
		return new Domodi_Xml_Product_Mapper();
	}
}
