<?php
namespace WPDesk\Xml;

use WooCommerce_Ceneo_Plugin;
use WPDesk_Product_Generator;

/**
 * Abstraction layer for XML class.
 */
abstract class Abstract_Xml {

	const DEFAULT_EMPTY_XML_CONTENT   = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<offers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1\">\n</offers>";
	const XML_GENERATE_AJAX_HANDLE    = 'generate_export_xml';
	const XML_PROCESS_AJAX_HANDLE     = 'process_export_xml';
	const XML_URL                     = 'wp-admin/admin-ajax.php?action=' . self::XML_GENERATE_AJAX_HANDLE;
	const XML_FILE_NAME               = 'export.xml';
	const TMP_XML_FILE_NAME           = 'export_tmp.xml';
	const XML_UPLOADS_DIR_NAME        = 'ceneo';
	const MAX_PRODUCTS_IN_BATCH       = 20;
	const MAX_PROCESS_TIME            = 25;
	const MAX_PROCESS_TIME_DELAY      = 5;
	const TRANSIENT_GENERATED         = 'transient_generated';
	const TRANSIENT_EXPORTED_PRODUCTS = 'transient_exported_products';
	const TRANSIENT_LAST_PRODUCT_ID   = 'transient_last_product_id';
	const TRANSIENT_LOCK              = 'transient_lock';

	/** @var WPDesk_Product_Generator */
	protected $product_generator;

	/**
	 * WooCommerce_Ceneo_Plugin instance.
	 *
	 * @var WooCommerce_Ceneo_Plugin
	 */
	protected $plugin;

	public function __construct( WooCommerce_Ceneo_Plugin $plugin ) {
		$this->plugin = $plugin;
		$this->product_generator = new WPDesk_Product_Generator();
	}

	public function get_xml() {
		$xml_file_path = $this->get_xml_file_path();
		if ( file_exists( $xml_file_path ) ) {
			return file_get_contents( $this->get_xml_file_path() );
		}else{
			$this->force_generate_xml();
			return file_get_contents( $this->get_or_create_xml_file_path() );
		}
	}

	/**
	 * Creates full path and empty file if destination file not exists.
	 *
	 * @param string $path_to_file
	 *
	 */
	private function create_empty_xml_file( $path_to_file ) {
		$dir = dirname( $path_to_file );
		if ( ! file_exists( $path_to_file ) ) {
			wp_mkdir_p( $dir );
			file_put_contents( $path_to_file, static::DEFAULT_EMPTY_XML_CONTENT );
		}
	}

	/**
	 * Get temporary or main xml file path.
	 *
	 * @param bool $temp  is temp. xml file.
	 *
	 * @return string
	 */
	private function get_xml_file_path( $temp = false ) {
		$xml_file    = ( false === $temp ) ? static::XML_FILE_NAME : static::TMP_XML_FILE_NAME;
		$uploads_dir = trailingslashit( wp_upload_dir()['basedir'] ) . static::XML_UPLOADS_DIR_NAME;
		return $uploads_dir . '/' . $xml_file;
	}

	/**
	 * Get or create xml file path.
	 *
	 * @param bool $temp is temp. xml file.
	 *
	 * @return string
	 */
	private function get_or_create_xml_file_path( $temp = false ) {
		$path_to_file = $this->get_xml_file_path( $temp );
		if ( ! file_exists( $path_to_file ) ) {
			$this->create_empty_xml_file( $path_to_file );
		}

		return $path_to_file;
	}

	/**
	 * If generating process is not finished - get products and process them to save in temp. xml file. If there is nothing to save just finish process.
	 */
	public function generate_xml() {
		$is_generated    = $this->is_file_generated();
		$should_split_variants = $this->should_split_variants();
		$all_products = $this->product_generator->count_products_to_export( $should_split_variants );
		$is_locked = $this->is_locked();

		if ( false === $is_generated && false === $is_locked ) {
			$this->set_lock();
			$last_product_id       = $this->get_last_product_id();
			$offers_xml_element      = ( $last_product_id > 0 ) ? $this->prepare_root_element_from_file() : $this->prepare_root_element();
			$generator             = $this->product_generator->generate_post_ids( $should_split_variants, $last_product_id );
			if ( empty( $generator ) ) {
				$is_generated = $this->finish_generate_process();
			} else {
				$this->process_products( $offers_xml_element, $generator );
			}
			$this->release_lock();
		}

		return $this->prepare_response( $is_generated, $is_locked, $all_products );
	}

	private function prepare_response( $is_generated, $is_locked, $all_products ){

		if ( true === $is_generated ) {
			$status = 'finished';
		} else {
			$status = ( true === $is_locked ) ? 'wait' : 'process';
		}

		return array(
			'status'            => $status,
			'all_product_count' => $all_products,
			'exported_products'    => ( true === $is_generated ) ? $all_products : $this->get_exported_products_count()
		);
	}

	private function is_locked() {
		return ( false === get_transient( static::TRANSIENT_LOCK ) ) ? false : true;
	}

	private function set_lock() {
		set_transient( static::TRANSIENT_LOCK, '1', static::MAX_PROCESS_TIME );
	}

	private function release_lock() {
		delete_transient( static::TRANSIENT_LOCK );
	}

	private function should_split_variants(){
		return $this->plugin->getSettingValue( 'variants', '' ) != '';
	}

	/**
	 * Force to generate xml file.
	 */
	public function force_generate_xml() {
		static::clear_generate_progress();
		$this->generate_xml();
		$this->finish_generate_process();
		static::clear_generate_progress();
	}

	/**
	 * Find single element in DOM tree and return it, if file not exists return false.
	 *
	 * @param \DOMDocument $xml_document
	 * @param string      $query xpath string.
	 *
	 * @return bool|\DOMElement
	 */
	protected function find_single_in_dom_tree( \DOMDocument $xml_document, $query ) {
		$xpath  = new \DOMXPath( $xml_document );
		$result = $xpath->query( $query );
		if ( isset( $result[0] ) ) {
			return $result[0];
		}

		return false;
	}

	/**
	 * Loads temp. xml file and return root element if exists, otherwise create new root element.
	 *
	 * @return DOMElement
	 */
	private function prepare_root_element_from_file() {
		$xml_document = new \DOMDocument();
		$xml_document->load( $this->get_or_create_xml_file_path( true ) );
		$offers = $this->find_single_in_dom_tree( $xml_document, "//offers" );

		return ( false === $offers ) ? $this->prepare_root_element() : $offers;
	}

	/**
	 * Prepares XML root element.
	 *
	 * @return DOMElement
	 */
	protected function prepare_root_element() {
		$xml_document = new \DOMDocument( '1.0', 'UTF-8' );
		$root_element = $xml_document->createElement( 'offers' );
		$root_element->setAttribute( 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance' );
		$root_element->setAttribute( 'version', '1' );
		$xml_document->appendChild( $root_element );

		return $root_element;
	}

	/**
	 * Appends product xml data to a given DOM element
	 *
	 * @param int        $post_id
	 * @param \DOMElement $parent
	 */
	protected function append_product( $post_id, \DOMElement $parent ) {
		$helper  = \WPDesk\Woocommerce_Xml_Mapping\Product_Helper::create_from_id( $post_id );
		$product = $helper->get_product();

		$product_mapper = $this->create_product_mapper();
		$productNode    = $product_mapper->map_to_xml( $product_mapper->create_xml_element( $parent->ownerDocument ),
			$product );
		$parent->appendChild( $productNode );
	}

	/**
	 * Add products to group, save state about last product id and save progress to temp xml file.
	 */
	protected function process_products( \DOMElement $offers_xml_element, array $generator ) {
		$i                  = 1;
		$start_time         = microtime( true );
		$max_execution_time = intval( ini_get( 'max_execution_time' ) );

		foreach ( $generator as $id ) {
			$this->append_product( $id, $offers_xml_element );
			if ( $i >= static::MAX_PRODUCTS_IN_BATCH || false === next( $generator ) ) {
				$this->flush_to_tmp_xml( $offers_xml_element );
				$this->set_last_product_id( $id );
				$this->set_exported_products_count( $i );
				if ( $this->is_process_should_finish( $start_time, $max_execution_time ) ) {
					break;
				}
				$i = 0;
			}
			$i ++;
		}
	}

	protected function get_exported_products_count(){
		$exported_products = get_transient( static::TRANSIENT_EXPORTED_PRODUCTS );
		return false === $exported_products? 0 : (int) $exported_products;
	}

	protected function set_exported_products_count( $number_of_products ){
		$exported_products = $this->get_exported_products_count();
		set_transient( static::TRANSIENT_EXPORTED_PRODUCTS, $exported_products + $number_of_products );
	}

	protected function set_last_product_id( $id ){
		set_transient( static::TRANSIENT_LAST_PRODUCT_ID, $id );
	}

	private function get_last_product_id() {
		$last_product_id = get_transient( static::TRANSIENT_LAST_PRODUCT_ID );

		return is_numeric( $last_product_id ) ? (int) $last_product_id : 0;
	}

	private function is_file_generated(){
		return ( '1' === get_transient( static::TRANSIENT_GENERATED ) ) ? true : false;
	}

	/**
	 * Check remaining time and retun true if time is over.
	 *
	 * @param float $start_time
	 * @param float $max_execution_time
	 *
	 * @return bool
	 */
	protected function is_process_should_finish( $start_time, $max_execution_time ) {
		$time_elapsed = microtime( true ) - $start_time;
		if ( $max_execution_time > 0 ) {
			return ( $time_elapsed > ( static::MAX_PROCESS_TIME - static::MAX_PROCESS_TIME_DELAY ) || $time_elapsed > ( $max_execution_time - static::MAX_PROCESS_TIME_DELAY ) );
		} else {
			return ( $time_elapsed > ( static::MAX_PROCESS_TIME - static::MAX_PROCESS_TIME_DELAY ) );
		}
	}

	/**
	 * Save portion of data to temporary xml file.
	 */
	protected function flush_to_tmp_xml( \DOMElement $offers_xml_element ) {
		$offers_xml_element->ownerDocument->save( $this->get_or_create_xml_file_path( true ) );
	}

	/**
	 * Copy temporary file as generated xml and add flag to database about finished process.
	 */
	private function finish_generate_process() {
		if ( copy( $this->get_xml_file_path( true ), $this->get_xml_file_path() ) ) {
			set_transient( static::TRANSIENT_GENERATED, '1' );
			return true;
		}
		return false;
	}

	public function __toString() {
		return $this->get_xml();
	}

	public function regenerate() {
		static::clear_generate_progress();
	}

	public static function clear_generate_progress(){
		delete_transient( static::TRANSIENT_GENERATED );
		delete_transient( static::TRANSIENT_LAST_PRODUCT_ID );
		delete_transient( static::TRANSIENT_EXPORTED_PRODUCTS );
		delete_transient( static::TRANSIENT_LOCK );
	}

	/**
	 * @return \WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper
	 */
	abstract protected function create_product_mapper();

}
