<?php
namespace WPDesk\Xml;

use WPDesk\Homebook_Xml_Mapping\Homebook_Xml_Product_Mapper;

/**
 * Homebook XML - creates Homebook XML files.
 */
class Homebook_Xml extends Abstract_Xml {

	const DEFAULT_EMPTY_XML_CONTENT   = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<offers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1\">\n</offers>";
	const XML_GENERATE_AJAX_HANDLE    = 'generate_homebook_xml';
	const XML_PROCESS_AJAX_HANDLE     = 'process_homebook_xml';
	const XML_URL                     = 'wp-admin/admin-ajax.php?action=' . self::XML_GENERATE_AJAX_HANDLE;
	const XML_FILE_NAME               = 'homebook.xml';
	const TMP_XML_FILE_NAME           = 'homebook_tmp.xml';
	const XML_UPLOADS_DIR_NAME        = 'ceneo';
	const TRANSIENT_GENERATED         = 'homebook_transient_generated';
	const TRANSIENT_EXPORTED_PRODUCTS = 'homebook_transient_exported_products';
	const TRANSIENT_LAST_PRODUCT_ID   = 'homebook_transient_last_product_id';
	const TRANSIENT_LOCK              = 'homebook_transient_lock';

	/**
	 * @return \WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper
	 */
	protected function create_product_mapper(){
		return new Homebook_Xml_Product_Mapper();
	}
}
