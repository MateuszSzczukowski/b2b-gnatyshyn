<?php

namespace WPDesk\Xml\Integrations;

/**
 * Major refactor is required here.
 */
class WCI_Ceneo implements WCI_Interface, Category_Tree_Interface{

	CONST META_KEY_PREFIX = 'woocommerce_ceneo_term';

	/** @var string */
	private static $path;

	/**
	 * @var WCI_Ceneo
	 */
	private static $instance;

	protected $daneXMLFilePath = '/assets/ceneo.xml';
	protected $daneXMLPath = 'http://api.ceneo.pl/Kategorie/dane.xml';
	protected $data;
	protected $XmlArrayOfCategory;

	protected $ceneoGroups = array(
		'books'     => array(
			'name'   => 'Książki',
			'fields' => array(
				'Autor'       => array(
					'title'   => 'Imię i nazwisko autora',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Ilosc_stron' => array(
					'title'   => 'Informacja na temat liczby stron',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Wydawnictwo' => array(
					'title'   => 'Nazwa wydawnictwa',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Rok_wydania' => array(
					'title'   => 'Rok publikacji książki',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Oprawa'      => array(
					'title'   => 'Rodzaj oprawy np., miękka, twarda',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Format'      => array(
					'title'   => 'Wymiary książki np. B5, A5, 172x245cm, 15.5x22.5cm',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Spis_tresci' => array(
					'title'   => 'Link do spisu treści',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				),
				'Fragment'    => array(
					'title'   => 'Link do fragmentu książki',
					'type'    => 'text',
					'label'   => '',
					'default' => ''
				)
			)
		),
		'tires'     => array(
			'name'   => 'Opony',
			'fields' => array(
				'SAP'             => array(
					'title' => 'Kod producenta',
					'type'  => 'text',
					'label' => ''
				),
				'Model'           => array(
					'title' => 'Model opony',
					'type'  => 'text',
					'label' => ''
				),
				'Szerokosc_opony' => array(
					'title' => 'Szerokość opony w milimetrach',
					'type'  => 'text',
					'label' => ''
				),
				'Profil'          => array(
					'title' => 'Profil opony',
					'type'  => 'text',
					'label' => ''
				),
				'Srednica_kola'   => array(
					'title' => 'Średnica osadzenia podana w calach',
					'type'  => 'text',
					'label' => ''
				),
				'Indeks_predkosc' => array(
					'title' => 'Indeks dopuszczalnej prędkości',
					'type'  => 'text',
					'label' => ''
				),
				'Indeks_nosnosc'  => array(
					'title' => 'Maksymalne obciążenie w funtach',
					'type'  => 'text',
					'label' => ''
				),
				'Sezon'           => array(
					'title' => 'Sezonowość np. Zimowe, Letnie, Całoroczne',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'rims'      => array(
			'name'   => 'Felgi i kołpaki',
			'fields' => array(
				'Kod_producenta' => array(
					'title' => 'Kod nadawany produktowi przez producenta',
					'type'  => 'text',
					'label' => ''
				),
				'Rozmiar'        => array(
					'title' => 'Szerokość i zewnętrzna średnica w calach np. 6,5x15',
					'type'  => 'text',
					'label' => ''
				),
				'Rozstaw_srub'   => array(
					'title' => 'Liczba śrub mocujących i średnica okręgu, na której znajdują się otwory np. 5x110',
					'type'  => 'text',
					'label' => ''
				),
				'Odsadzenie'     => array(
					'title' => '(tylko dla felg) Odległości między płaszczyzną montażową obręczy, a jej środkiem symetrii (ET)',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'perfumes'  => array(
			'name'   => 'Perfumy',
			'fields' => array(
				'Kod_producenta' => array(
					'title' => 'Kod nadawany produktowi przez producenta',
					'type'  => 'text',
					'label' => ''
				),
				'Linia'          => array(
					'title' => 'Linia zapachu – seria np. Miss Pucci, Orange Celebration of Happiness',
					'type'  => 'text',
					'label' => ''
				),
				'Rodzaj'         => array(
					'title' => 'Rodzaj produktu np. Woda perfumowana, Woda toaletowa, Woda kolońska, Dezodorant roll on, Dezodorant sztyft',
					'type'  => 'text',
					'label' => ''
				),
				'Pojemnosc'      => array(
					'title' => 'Pojemność podana w mililitrach np. 50 ml, 100 ml',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'music'     => array(
			'name'   => 'Płyty muzyczne',
			'fields' => array(
				'Wykonawca' => array(
					'title' => 'Imię i nazwisko wykonawcy lub nazwa zespołu',
					'type'  => 'text',
					'label' => ''
				),
				'Nosnik'    => array(
					'title' => 'Rodzaj nośnika np. DVD, CD',
					'type'  => 'text',
					'label' => ''
				),
				'Wytwornia' => array(
					'title' => 'Nazwa wytwórni muzycznej',
					'type'  => 'text',
					'label' => ''
				),
				'Gatunek'   => array(
					'title' => 'Gatunek muzyczny',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'games'     => array(
			'name'   => 'Gry PC / Gry na konsole',
			'fields' => array(
				'Kod_producenta' => array(
					'title' => 'Kod nadawany produktowi przez producenta',
					'type'  => 'text',
					'label' => ''
				),
				'Platforma'      => array(
					'title' => 'Platforma, na jaką jest przeznaczona gra np. PC, PS2, Xbox360',
					'type'  => 'text',
					'label' => ''
				),
				'Gatunek'        => array(
					'title' => 'Gatunek gry np. Akcji, Wyścigi',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'movies'    => array(
			'name'   => 'Filmy',
			'fields' => array(
				'Rezyser'          => array(
					'title' => 'Imię i nazwisko reżysera filmu',
					'type'  => 'text',
					'label' => ''
				),
				'Nosnik'           => array(
					'title' => 'Rodzaj nośnika np. DVD, VCD, Blu-Ray',
					'type'  => 'text',
					'label' => ''
				),
				'Wytwornia'        => array(
					'title' => 'Nazwa wytwórni filmowej',
					'type'  => 'text',
					'label' => ''
				),
				'Obsada'           => array(
					'title' => 'Aktorzy grający w danym filmie',
					'type'  => 'text',
					'label' => ''
				),
				'Tytul_oryginalny' => array(
					'title' => 'Oryginalny tytuł filmu',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'medicines' => array(
			'name'   => 'Leki, suplementy',
			'fields' => array(
				'BLOZ_12'   => array(
					'title' => 'Identyfikator leku – konieczne jest podanie minimum jednego z kodów dla leków i produktów aptecznych. Zalecane jest podawanie obydwu kodów dla każdego produktu. Dla artykułów, które nie posiadają kodu Bloz12 należy podać kod Bloz7.',
					'type'  => 'text',
					'label' => ''
				),
				'BLOZ_7'    => array(
					'title' => 'Identyfikator leku – konieczne jest podanie minimum jednego z kodów dla leków i produktów aptecznych. Zalecane jest podawanie obydwu kodów dla każdego produktu. Dla artykułów, które nie posiadają kodu Bloz12 należy podać kod Bloz7.',
					'type'  => 'text',
					'label' => ''
				),
				'Ilosc'     => array(
					'title' => 'Liczba tabletek, pojemność butelki np. 12szt., 250ml',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'grocery'   => array(
			'name'   => 'Delikatesy',
			'fields' => array(
				'Ilosc'     => array(
					'title' => 'Ilość w opakowaniu np.. 12szt., 2kg',
					'type'  => 'text',
					'label' => ''
				)
			)

		),
		'clothes'   => array(
			'name'   => 'Odzież, obuwie, dodatki',
			'fields' => array(
				'Model'        => array(
					'title' => 'Model produktu',
					'type'  => 'text',
					'label' => ''
				),
				'Kolor'        => array(
					'title' => 'Kolor dominujący, w przypadku gdy produkt występuje w kilku wariantach kolorystycznych powinna się pojawić oddzielna oferta dla każdego koloru (pole wymagane)',
					'type'  => 'text',
					'label' => ''
				),
				'Rozmiar'      => array(
					'title' => 'Rozmiar, w przypadku gdy produkt jest dostępny w różnych rozmiarach poszczególne wartości powinny zostać oddzielone średnikiem np. ‘S;L;XL’(pole wymagane)',
					'type'  => 'text',
					'label' => ''
				),
				'Kod_produktu' => array(
					'title' => 'Kod nadawany przez producenta',
					'type'  => 'text',
					'label' => ''
				),
				'Sezon'        => array(
					'title' => 'Sezon np. ‘wiosna/lato’',
					'type'  => 'text',
					'label' => ''
				),
				'Fason'        => array(
					'title' => 'Rozumiany, jako fason pojedyncza wartość np. ‘rurki’, ‘dzwony’, ‘szerokie’, ‘szmizjerka’',
					'type'  => 'text',
					'label' => ''
				),
				'ProductSetId' => array(
					'title' => 'Oznaczenie zestawu',
					'type'  => 'text',
					'label' => ''
				)
			)
		),
		'other'     => array( // must be last
			'name'   => 'Inne',
			'fields' => array(
				'Producent'      => array(
					'title' => 'Producent danego produktu',
					'type'  => 'text',
					'label' => ''
				),
				'Kod_producenta' => array(
					'title' => 'Kod nadawany produktowi przez producenta',
					'type'  => 'text',
					'label' => ''
				),
			)
		)
	);

	private function __construct() {
	}

	/**
	 * return \WCI_Ceneo
	 */
	public static function get_instance() {
		if (empty(self::$instance)) {
			self::$instance = new WCI_Ceneo();
		}
		return self::$instance;
	}

	public static function set_path( $path ) {
		self::$path = $path;
	}

	public function get_ceneo_groups_array() {
		return $this->ceneoGroups;
	}

	/**
	 * Get XML file data
	 *
	 * @return string
	 */
	protected function get_xml_data() {
		$data = file_get_contents(str_replace(['/class', '\\class'], '', self::$path) . $this->daneXMLFilePath );

		return trim( $data );
	}

	public function get_category_with_full_name( $categoryId ) {
		$this->load_array_of_category();
		$categories = $this->XmlArrayOfCategory->xpath( "//Id[.=$categoryId]/parent::*" );

		$cName = array();
		while ( ! empty( $categories[0] ) ) {
			if ( ! empty( $categories[0]->Name ) ) {
				$cName[] = (string) $categories[0]->Name;

				// 2.4 only first element of category path
				//return implode('/', $cName);
			}
			$categories = $categories[0]->xpath( "parent::*" );
		}


		if ( ! empty( $cName ) ) {
			return implode( '/', array_reverse( $cName ) );
		} else {
			return '';
		}

	}


	/**
	 * @param int $categoryId
	 *
	 * @return SimpleXMLElement[]
	 */
	public function get_category_attributes( $categoryId ) {
		$this->load_array_of_category();
		$attributes = $this->XmlArrayOfCategory->xpath( "//Id[.=$categoryId]/parent::*/Subcategories/Attributes/Attribute" );
		return is_array($attributes)? $attributes : array();
	}

	public function get_ceneo_category_tree_html_options_array() {
		$this->load_array_of_category();
		$options = $this->get_ceneo_category_tree_html_options_recursive( $this->XmlArrayOfCategory );

		return $options;
	}

	private function get_ceneo_category_tree_html_options_recursive( $xmlRoot, $parentNameArray = array() ) {
		$options = array();

		foreach ( $xmlRoot->children() as $xmlCategory ) {
			$options[ (string) $xmlCategory->Id ] = (string) $xmlCategory->Name;
			if ( ! empty( $xmlCategory->Subcategories ) ) {
				$options[ (string) $xmlCategory->Id . 'group' ] = implode( ' - ',
					array_merge( $parentNameArray, array( (string) $xmlCategory->Name ) ) );
				$options                                        = $options + $this->get_ceneo_category_tree_html_options_recursive( $xmlCategory->Subcategories,
						$parentNameArray + array( (string) $xmlCategory->Name ) );
			}
		}

		return $options;
	}

	public function get_product_ceneo_group( $product_id ) {
		$product = wc_get_product( $product_id );

		if ( $product && $product->is_type( 'variation' ) ) {
			$parent_product = wpdesk_get_variation_parent_id( $product );
			if ( $parent_product ) {
				$woocommerce_ceneo_ceneo_group = wpdesk_get_variation_meta( $product, 'woocommerce_ceneo_ceneo_group',
					true );
				if ( ! empty( $woocommerce_ceneo_ceneo_group ) ) {
					return $woocommerce_ceneo_ceneo_group;
				} else {
					$woocommerce_ceneo_ceneo_group = wpdesk_get_product_meta( wpdesk_get_variation_parent_id( $product ),
						'woocommerce_ceneo_ceneo_group', true );
					if ( ! empty( $woocommerce_ceneo_ceneo_group ) ) {
						return $woocommerce_ceneo_ceneo_group;
					}
				}
			} else {
				return 'other';
			}
		} else {
			$woocommerce_ceneo_ceneo_group = wpdesk_get_product_meta( $product, 'woocommerce_ceneo_ceneo_group', true );
			if ( ! empty( $woocommerce_ceneo_ceneo_group ) ) {
				return $woocommerce_ceneo_ceneo_group;
			}
		}

		return 'other';
	}

	/**
	 * @param string $term
	 *
	 * @return array
	 */
	public function get_category_tree_html_options_array( $term ){
		$result = array();

		$ceneoCategoriesOptions = $this->get_ceneo_category_tree_html_options_array();
		foreach ( $ceneoCategoriesOptions as $key => $value ) {
			if ( stripos( $value, $term ) !== false && stripos( $key, 'group' ) === false ) {
				$result[] = array( 'value' => $key, 'text' => $value );
			}
		}
		return $result;
	}

	public function get_category_prefix() {
		return self::META_KEY_PREFIX;
	}

	private function load_array_of_category(){
		if ( empty( $this->data ) ) {
			$this->data = $this->get_xml_data();
		}
		if ( empty( $this->XmlArrayOfCategory ) ) {
			$this->XmlArrayOfCategory = new \SimpleXMLElement( $this->data );
		}
	}

}
