<?php

namespace WPDesk\Xml\Integrations;

/**
 * Retrieves array data from given term
 *
 */
interface WCI_Interface {

	/**
	 * @param integer $categoryId
	 *
	 * @return string with category name.
	 */
	public function get_category_with_full_name( $categoryId );

	/**
	 * @return string
	 */
	public function get_category_prefix();

}
