<?php

namespace WPDesk\Xml\Integrations;

/**
 * Nokaut categories class.
 */
class WCI_Nokaut implements WCI_Interface, Category_Tree_Interface{

	CONST META_KEY_PREFIX = 'woocommerce_ceneo_term_nokaut';

	/** @var string */
	private static $path;

	/**
	 * @var WCI_Nokaut
	 */
	private static $instance;

	protected $daneXMLFilePath = '/assets/nokaut.xml';
	protected $daneXMLPath = 'https://sprzedawca.nokaut.pl/integracja/kategorie.xml';
	protected $data;
	protected $XmlArrayOfCategory;

	private function __construct() {}

	/**
	 * return \WCI_Nokaut
	 */
	public static function get_instance() {
		if (empty(self::$instance)) {
			self::$instance = new WCI_Nokaut();
		}
		return self::$instance;
	}

	public static function set_path( $path ) {
		self::$path = $path;
	}

	/**
	 * Get XML file data
	 *
	 * @return string
	 */
	protected function get_xml_data() {
		$data = file_get_contents(str_replace(['/class', '\\class'], '', self::$path) . $this->daneXMLFilePath );
		return trim( $data );
	}

	/**
	 * @param integer $categoryId
	 *
	 * @return string with category name or empty string ''.
	 */
	public function get_category_with_full_name( $categoryId ) {
		$categories = $this->get_nokaut_category_tree_html_options_array();
		return ( isset( $categories[ $categoryId ] ) ) ? $categories[ $categoryId ] : '';
	}


	/**
	 * @return array in the format below.
	 * $result = [
	 *      (integer) $category_id => (string) $category_name,
	 *      ...
	 *  ]
	 */
	public function get_nokaut_category_tree_html_options_array() {
		if ( empty( $this->data ) ) {
			$this->data = $this->get_xml_data();
		}
		if ( empty( $this->XmlArrayOfCategory ) ) {
			$doc = new \DOMDocument();
			$doc->loadXML($this->data);
			$this->XmlArrayOfCategory = $doc->getElementsByTagName("category");
		}

		$options = array();
		foreach ( $this->XmlArrayOfCategory as $category ) {
			foreach ( $category->childNodes as $child ) {
				if ( $child->nodeType == XML_CDATA_SECTION_NODE ) {
					$options[ $category->getAttribute('id') ] = $child->textContent;
				}
			}
		}
		return $options;
	}


	/**
	 * @param string $term term name.
	 *
	 * @return array in the format below.
	 * $result = [
	 *      (string) 'value' => (integer) $category_id,
	 *      (string) 'text' => (string) $category_name,
	 *  ],
	 *  ...
	 */
	public function get_category_tree_html_options_array( $term ){
		$result = array();

		$categoriesOptions = $this->get_nokaut_category_tree_html_options_array();
		foreach ( $categoriesOptions as $category_id => $category_name ) {
			if ( stripos( $category_name, $term ) !== false ) {
				$result[] = array( 'value' => $category_id, 'text' => $category_name );
			}
		}
		return $result;
	}

	public function get_category_prefix() {
		return self::META_KEY_PREFIX;
	}

}
