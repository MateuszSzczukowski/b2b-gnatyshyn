<?php

namespace WPDesk\Xml\Integrations;

/**
 * Retrieves array data from given term
 *
 */
interface Category_Tree_Interface {

	/**
	 * @param string $term
	 *
	 * @return array
	 */
	public function get_category_tree_html_options_array( $term );

}
