<?php
namespace WPDesk\Xml;

use WooCommerce_Ceneo_Plugin;
use WPDesk\Xml\Integrations\WCI_Ceneo;
use WPDesk\Xml\Integrations\WCI_Nokaut;
use WPDesk\Xml\Integrations\Category_Tree_Interface;

/**
 * XML Factory create and return instances of Abstract_Xml class & Category_Tree_Interface interface.
 */
class Xml_Factory {

	private $plugin;

	public function __construct( WooCommerce_Ceneo_Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * @param string $action name.
	 *
	 * @return Abstract_Xml
	 */
	public function get_generator( $action ) {
		$action = strtolower( $action );
		if ( strpos( $action, 'nokaut' ) !== false ) {
			$generator = new Nokaut_Xml( $this->plugin );
		} else if ( strpos( $action, 'domodi' ) !== false ) {
			$generator = new Domodi_Xml( $this->plugin );
		} elseif ( strpos( $action, 'homebook' ) !== false ) {
			$generator = new Homebook_Xml( $this->plugin );
		} else {
			$generator = new Ceneo_Xml( $this->plugin );
		}

		return $generator;
	}

	/**
	 * @param string $action name.
	 *
	 * @return Category_Tree_Interface
	 */
	public function get_category_mapper( $action ){
		$action = strtolower( $action );
		if ( strpos( $action, 'nokaut' ) !== false ) {
			$mapper = WCI_Nokaut::get_instance();
		} else {
			$mapper = WCI_Ceneo::get_instance();
		}
		$mapper::set_path( $this->plugin->plugin_path );

		return $mapper;
	}

	public static function is_xml_generator_available( $class_name ){
		return ( class_exists( $class_name ) && is_subclass_of( (string) $class_name, Abstract_Xml::class ) );
	}


	/**
	 * @param string $generator_name.
	 *
	 * @return string
	 */
	public static function get_generator_class_by_name( $generator_name ){
		$generator_name = ucfirst( strtolower( (string) $generator_name ) );
		$class_name     = '\WPDesk\Xml\\' . $generator_name . '_Xml';
		if( self::is_xml_generator_available( $class_name ) ){
			return $class_name;
		}
		return '';
	}
}
