<?php
namespace WPDesk\Xml;

use WooCommerce_Ceneo_Plugin;
use WPDesk\Xml\Integrations\WCI_Ceneo;
use WPDesk\Ceneo_Xml_Mapping\Ceneo_Xml_Product_Mapper;

/**
 * Ceneo XML - creates Ceneo XML files.
 */
class Ceneo_Xml extends Abstract_Xml {

	const DEFAULT_EMPTY_XML_CONTENT   = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<offers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1\">\n</offers>";
	const XML_GENERATE_AJAX_HANDLE    = 'generate_ceneo_xml';
	const XML_PROCESS_AJAX_HANDLE     = 'process_ceneo_xml';
	const XML_URL                     = 'wp-admin/admin-ajax.php?action=' . self::XML_GENERATE_AJAX_HANDLE;
	const XML_FILE_NAME               = 'ceneo.xml';
	const TMP_XML_FILE_NAME           = 'ceneo_tmp.xml';
	const XML_UPLOADS_DIR_NAME        = 'ceneo';
	const TRANSIENT_GENERATED         = 'ceneo_transient_generated';
	const TRANSIENT_EXPORTED_PRODUCTS = 'ceneo_transient_exported_products';
	const TRANSIENT_LAST_PRODUCT_ID   = 'ceneo_transient_last_product_id';
	const TRANSIENT_LOCK              = 'ceneo_transient_lock';

	/** @var WCI_Ceneo */
	private $ceneo;

	/** @var \DOMElement[] */
	private $groups_cache = [];

	public function __construct( WooCommerce_Ceneo_Plugin $plugin ) {
		parent::__construct( $plugin );
		WCI_Ceneo::set_path( $plugin->plugin_path );
		$this->ceneo             = WCI_Ceneo::get_instance();
	}

	/**
	 * @return \WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper
	 */
	protected function create_product_mapper(){
		return new Ceneo_Xml_Product_Mapper();
	}

	/**
	 * Prepares xml element for product group.
	 *
	 * @param \DOMElement $rootElement
	 * @param int        $product_id
	 *
	 * @return \DOMElement Group element
	 */
	private function prepare_product_group( \DOMElement $rootElement, $product_id ) {
		$group_key = $this->ceneo->get_product_ceneo_group( $product_id );
		if ( ! isset( $this->groups_cache[ $group_key ] ) ) {
			$searchResult                     = $this->find_single_in_dom_tree( $rootElement->ownerDocument, "//group[@name='" . $group_key . "']" );
			if( false === $searchResult ){
				$this->groups_cache[ $group_key ] = $group = $rootElement->ownerDocument->createElement( 'group' );
				$rootElement->appendChild( $group );
				$group->setAttribute( 'name', $group_key );
			}else{
				$this->groups_cache[ $group_key ] = $searchResult;
			}
		}
		$group = $this->groups_cache[ $group_key ];

		return $group;
	}

	/**
	 * Add products to group, save state about last product id and save progress to temp xml file.
	 */
	protected function process_products( \DOMElement $root_xml_element, array $generator ) {
		$i                  = 1;
		$start_time         = microtime( true );
		$max_execution_time = intval( ini_get( 'max_execution_time' ) );

		foreach ( $generator as $id ) {
			$group = $this->prepare_product_group( $root_xml_element, $id );
			$this->append_product( $id, $group );
			if ( $i >= self::MAX_PRODUCTS_IN_BATCH || false === next( $generator ) ) {
				$this->flush_to_tmp_xml( $root_xml_element );
				$this->set_last_product_id( $id );
				$this->set_exported_products_count( $i );
				if ( $this->is_process_should_finish( $start_time, $max_execution_time ) ) {
					break;
				}
				$i = 0;
			}
			$i ++;
		}
	}
}
