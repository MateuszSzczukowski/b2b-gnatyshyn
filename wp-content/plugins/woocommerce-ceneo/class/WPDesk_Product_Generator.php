<?php

use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
/**
 * Generates ids of products to export
 */
class WPDesk_Product_Generator {
	const DEFAULT_EXPORT_LANG = 'pl';
	const WPDESK_FILTER_PRODUCT_SHOULD_EXPORT = 'wpdesk_ceneo_product_should_export';
	const PRODUCT_DISABLED_META_NAME = 'woocommerce_ceneo_disabled';
	const LIMIT = 300;

	/**
	 * Generates set of id's with posts to export
	 *
	 * @param bool $with_variants
	 *
	 * @return integer
	 */
	public function count_products_to_export( $with_variants ){
		global $wpdb;
		$where_post_type = " AND p.post_type = 'product' ";
		if ( $with_variants ) {
			$where_post_type = " AND p.post_type in ( 'product', 'product_variation' ) AND p.ID NOT IN ( SELECT p2.post_parent FROM {$wpdb->posts} p2 WHERE post_type = 'product_variation') ";
		}
		$result = $wpdb->get_var( "SELECT COUNT(DISTINCT p.ID) FROM {$wpdb->posts} p WHERE p.post_status = 'publish' ". $where_post_type );
		return is_numeric( $result )? (int) $result: 0;
	}

	/**
	 * Generates set of id's with posts to export
	 *
	 * @param bool $with_variants
	 * @param int $last_product_id last checked product id - stored in database.
	 *
	 * @return int[]
	 */
	public function generate_post_ids( $with_variants, $last_product_id = 0 ) {
		global $wpdb;
		$result = array();

		$where_post_type = " AND p.post_type = 'product' ";
		if ( $with_variants ) {
			// POSTS THAT HAVE NO VARIANTS OR ARE VARIANTS THEMSELVES
			$where_post_type = " AND p.post_type in ( 'product', 'product_variation' ) AND p.ID NOT IN ( SELECT p2.post_parent FROM {$wpdb->posts} p2 WHERE post_type = 'product_variation') ";
		}

		$ids_objects = $wpdb->get_col( "SELECT DISTINCT p.ID FROM {$wpdb->posts} p WHERE p.post_status = 'publish' AND p.ID > ". (int)$last_product_id. $where_post_type . " ORDER BY ID, post_date DESC " );

		foreach ( $ids_objects as $id ) {
			if ( $this->should_export( $id ) ) {
				$result[] = (int) $id;
				if ( count( $result ) >= self::LIMIT ) {
					break;
				}
			}
		}
		return $result;
	}


	/**
	 * @param integer $product_id.
	 *
	 * @return bool
	 */
	private function should_export( $product_id ){
		if( false === $this->should_export_according_to_enable( $product_id ) ){
			return false;
		}

		try {
			$product_helper = Product_Helper::create_from_id( $product_id );
			return $this->should_export_according_to_lang( $product_helper ) && $this->should_export_according_to_stock( $product_helper );
		}catch( \Exception $e ){
			return false;
		}
	}

	/**
	 *
	 * @param integer $product_id.
	 *
	 * @return string
	 */
	private function should_export_according_to_enable( $product_id ) {
		$attribute_meta_key = self::PRODUCT_DISABLED_META_NAME;
		$disabled = get_post_meta( $product_id, $attribute_meta_key, true );
		$disabled = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.$attribute_meta_key, $disabled, $product_id );
		if ( $disabled == 1 ) {
			return false;
		}

		try {
			$product_helper = Product_Helper::create_from_id( $product_id );
			if ( $product_helper->is_variant() ) {
				$parent = $product_helper->get_parent_product();
				if ( $parent && $parent instanceof \WC_Product ) {
					$disabled = wpdesk_get_product_meta( $product_helper->get_parent_product(), self::PRODUCT_DISABLED_META_NAME, true );
					if ( $disabled == 1 ) {
						return false;
					}
				} else {
					return false;
				}
			}
		} catch ( \Exception $e ){
			return false;
		}

		return true;
	}

	/**
	 * Should product be exported or not
	 *
	 * @param Product_Helper $product_helper
	 *
	 * @return bool
	 */
	private function should_export_according_to_lang( Product_Helper $product_helper ) {
		$product = $product_helper->get_product();
		$language = $this->get_sitepress_product_language( $product );

		$shouldBeSaved = $language === null || self::DEFAULT_EXPORT_LANG === $language;

		return apply_filters( self::WPDESK_FILTER_PRODUCT_SHOULD_EXPORT, $shouldBeSaved, $product, $language );
	}

	/**
	 * Should product be exported or not
	 *
	 * @param Product_Helper $product_helper
	 *
	 * @return bool
	 */
	private function should_export_according_to_stock( Product_Helper $product_helper ) {
		$product = $product_helper->get_product();
		if ( empty( get_option( 'woocommerce_ceneo_zero_stock', '' ) ) ) {
			if ( $product->is_in_stock() ) {
				if ( $product->get_stock_quantity() !== null ) {
					return $product->get_stock_quantity() > 0;
				}

				return true;
			}

			return false;
		}

		return true;
	}


	/**
	 * Returns product language from WPML if it's possible
	 *
	 * @param WC_Product $product
	 *
	 * @return null|string
	 */
	private function get_sitepress_product_language( WC_Product $product ) {
		/** @var SitePress $sitepress */
		global $sitepress;

		if ( $sitepress instanceof \SitePress ) {
			return $sitepress->get_language_for_element( wpdesk_get_product_id( $product ), 'post_product' );
		}

		return null;
	}
}
