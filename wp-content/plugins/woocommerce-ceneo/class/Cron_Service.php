<?php

/**
 * Cron Service - init cron jobs and schedules.
 */
class Cron_Service {

	private $plugin;

	public function __construct( WooCommerce_Ceneo_Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	public function hooks(){
		add_filter( 'cron_schedules', array( $this, 'ceneo_cron_schedules' ) );
		$this->init_scheduled_tasks();
	}

	/**
	 * @param $schedules
	 *
	 * @return array
	 */
	public function ceneo_cron_schedules( $schedules ){
		$schedules['every_minute'] = array(
			'interval' => 60,
			'display'  => __( 'Every minute' ),
		);
		$schedules['every_two_hours'] = array(
			'interval' => 7200,
			'display'  => __( 'Every 2 hours' ),
		);
		$schedules['every_three_hours'] = array(
			'interval' => 10800,
			'display'  => __( 'Every 3 hours' ),
		);
		$schedules['every_six_hours'] = array(
			'interval' => 21600,
			'display'  => __( 'Every 6 hours' ),
		);
		$schedules['every_twelve_hours'] = array(
			'interval' => 43200,
			'display'  => __( 'Every 12 hours' ),
		);
		$schedules['every_two_days'] = array(
			'interval' => 172800,
			'display'  => __( 'Every two days' ),
		);
		$schedules['every_three_days'] = array(
			'interval' => 259200,
			'display'  => __( 'Every three days' ),
		);
		$schedules['once_a_week'] = array(
			'interval' => 604800,
			'display'  => __( 'Once a week' ),
		);

		return $schedules;
	}

	/**
	 * Init schedules if they are not added or reload them if schedule has changed.
	 */
	private function init_scheduled_tasks() {
		$schedule = $this->plugin->getSettingValue( 'cron_schedule', 'hourly' );
		$cron_items = $this->plugin->getSettingValue( 'cron_items', array_keys( WooCommerce_Ceneo_Plugin::get_price_comparison_items() ) );
		if ( $this->is_schedules_should_refresh( $cron_items ) || $this->is_schedule_changed( $schedule ) || $this->is_cron_items_changed( $cron_items ) ) {
			self::remove_schedules();

			foreach ( WooCommerce_Ceneo_Plugin::get_price_comparison_items() as $key => $val ) {
				if( in_array( $key, $cron_items ) ) {
					wp_schedule_event( time(), 'every_minute', 'woocommerce_ceneo_processing_xml', array( 'action' => $key ) );
					wp_schedule_event( time(), $schedule, 'woocommerce_ceneo_refresh_xml', array( 'action' => $key ) );
				}
			}

			set_transient( 'woocommerce_ceneo_regenerate_schedule', $schedule );
			set_transient( 'woocommerce_ceneo_regenerate_cron_items', $cron_items );

		}
	}

	private function is_cron_items_changed( $cron_items ){
		return ( $cron_items != get_transient( 'woocommerce_ceneo_regenerate_cron_items') );
	}

	private function is_schedule_changed( $schedule ){
		return ( $schedule !== get_transient( 'woocommerce_ceneo_regenerate_schedule') );
	}

	public static function remove_schedules() {
		foreach ( WooCommerce_Ceneo_Plugin::get_price_comparison_items() as $key => $val ) {
			wp_unschedule_event( time(), 'woocommerce_ceneo_processing_xml', array( 'action' => $key ) );
			wp_clear_scheduled_hook( 'woocommerce_ceneo_processing_xml', array( 'action' => $key ) );
			wp_unschedule_event( time(), 'woocommerce_ceneo_refresh_xml', array( 'action' => $key ) );
			wp_clear_scheduled_hook( 'woocommerce_ceneo_refresh_xml', array( 'action' => $key ) );
		}

		wp_unschedule_event( time(), 'woocommerce_ceneo_regenerate' );
		wp_clear_scheduled_hook( 'woocommerce_ceneo_regenerate' );
		wp_unschedule_event( time(), 'woocommerce_ceneo_generate_xml' );
		wp_clear_scheduled_hook( 'woocommerce_ceneo_generate_xml' );
	}


	/**
	 * @param string[] $cron_items.
	 *
	 * @return bool
	 */
	private function is_schedules_should_refresh( $cron_items_name ) {
		foreach ( WooCommerce_Ceneo_Plugin::get_price_comparison_items() as $key => $val ) {
			if ( in_array( $key, $cron_items_name ) ) {
				$processing = ( false === wp_next_scheduled( 'woocommerce_ceneo_processing_xml', array( 'action' => $key ) ) );
				$regenerate = ( false === wp_next_scheduled( 'woocommerce_ceneo_refresh_xml', array( 'action' => $key ) ) );
				if ( $processing || $regenerate ) {
					return true;
				}
			}
		}

		return false;
	}

}
