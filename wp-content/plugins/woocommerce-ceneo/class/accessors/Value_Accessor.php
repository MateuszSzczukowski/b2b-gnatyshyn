<?php

namespace WPDesk\Accessors;

/**
 * Returns same value as given
 *
 * @package WPDesk\Accessors
 */
class Value_Accessor implements Accessor {
	/**
	 * @param mixed $source
	 *
	 * @return mixed
	 */
	public function get_value( $source ) {
		return $source;
	}
}