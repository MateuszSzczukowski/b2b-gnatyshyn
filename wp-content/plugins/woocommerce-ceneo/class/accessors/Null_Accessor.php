<?php

namespace WPDesk\Accessors;

/**
 * Returns null for any given source
 *
 * @package WPDesk\Accessors
 */
class Null_Accessor implements Accessor {

	public function get_value( $source ) {
		return null;
	}
}