<?php

namespace WPDesk\Accessors;

/**
 * Decorates other accessor with given callable
 *
 * @package WPDesk\Accessors
 */
class Escaped_Accessor implements Accessor {
	/** @var Accessor */
	private $decorated;

	/** @var callable */
	private $escape_function;

	/**
	 * Escaped_Accessor constructor.
	 *
	 * @param Accessor $decorated
	 * @param callable $escape_function As argument gets source
	 */
	public function __construct( Accessor $decorated, $escape_function ) {
		$this->decorated       = $decorated;
		$this->escape_function = $escape_function;
	}

	public function get_value( $source ) {
		$escape_function = $this->escape_function;

		return $escape_function( $this->decorated->get_value( $source ) );
	}

}
