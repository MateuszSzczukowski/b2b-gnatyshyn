<?php

namespace WPDesk\Accessors;

/**
 * Retrieves array value for given key
 *
 * @package WPDesk\Accessors
 */
class Array_Value_Accessor implements Accessor {
	/** @var string */
	private $key;

	/**
	 * @param string $key
	 */
	public function __construct( $key ) {
		$this->key = $key;
	}

	/**
	 * @param array $source
	 *
	 * @return mixed
	 */
	public function get_value( $source ) {
		if ( isset( $source[ $this->key ] ) ) {
			return $source[ $this->key ];
		}

		return null;
	}
}
