<?php

namespace WPDesk\Accessors;

/**
 * Retrieves property value using given property name
 *
 * @package WPDesk\Accessors
 */
class Property_Accessor implements Accessor {
	/** @var string */
	private $property_name;

	public function __construct( $property_name ) {
		$this->property_name = $property_name;
	}

	public function get_value( $source ) {
		$getter = 'get_' . $this->property_name;
		if ( method_exists( $source, $getter ) ) {
			return $source->{$getter}();
		}

		return $source->{$this->property_name};
	}
}