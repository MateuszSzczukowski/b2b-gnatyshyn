<?php

namespace WPDesk\Accessors;

/**
 * Retrieves data from given source
 *
 * @package WPDesk\Accessors
 */
interface Accessor {
	/**
	 * @param mixed $source
	 *
	 * @return mixed
	 */
	public function get_value( $source );
}