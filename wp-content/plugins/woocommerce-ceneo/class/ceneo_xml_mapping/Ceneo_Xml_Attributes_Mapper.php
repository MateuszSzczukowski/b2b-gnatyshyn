<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Accessors\Array_Value_Accessor;
use WPDesk\Ceneo_Xml_Mapping\Accessors\Ceneo_Product_Attributes_Accessor_Delegator;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Array_Element_Mapper;

/**
 * Ceneo attributes
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Attributes_Mapper extends Array_Element_Mapper {

	public function __construct() {

		parent::__construct( new Ceneo_Product_Attributes_Accessor_Delegator( new Custom_Product_Attributes_Accessor() ), 'attrs' );

		$attribute = new Accessor_CDATA_Element_Mapper(
			new Array_Value_Accessor( Ceneo_Product_Attributes_Accessor_Delegator::ATTRIBUTE_VALUE ), 'a'
		);

		$attribute->add_mapper( new Accessor_Attribute_Mapper(
				new Array_Value_Accessor( Ceneo_Product_Attributes_Accessor_Delegator::ATTRIBUTE_NAME ), 'name' )
		);

		$this->add_mapper( $attribute );
	}
}
