<?php

namespace WPDesk\Ceneo_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;
use WPDesk\Xml\Integrations\WCI_Ceneo;

/**
 * Retrieves ceneo attributes and WooCommerce attributes from \WC_Product
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Ceneo_Product_Attributes_Accessor extends Product_Attributes_Accessor {

	/** @var WCI_Ceneo */
	private $wci_ceneo;

	public function __construct() {
		$this->wci_ceneo = WCI_Ceneo::get_instance();
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with below array format.
	 *  [
	 *      [
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_NAME => (string) $key,
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_VALUE => (string) $value,
	 *      ],
	 *      ...
	 *  ]
	 */
	public function get_value( $source ) {
		return array_merge(
			$this->get_ceneo_group_attributes( $source ),
			parent::get_value( $source )
		);
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array @see self::get_value() return method for array format.
	 */
	private function get_ceneo_group_attributes( $source ) {
		$product_helper = new Product_Helper( $source );

		if ( $product_helper->is_variant() ) {
			$variation_ceneo_group = wpdesk_get_variation_meta( $source, 'woocommerce_ceneo_ceneo_group', true );
		}

		$attributes = [];

		$groups = $this->wci_ceneo->get_ceneo_groups_array();
		$product_group = $this->wci_ceneo->get_product_ceneo_group($product_helper->get_product()->get_id());
		$fields = $groups[$product_group]['fields'];

		foreach ( array_keys( $fields ) as $key ) {
			$value              = null;
			$attribute_meta_key = 'woocommerce_ceneo_' . $key;
			if ( $product_helper->is_variant() ) {
				if ( ! empty( $variation_ceneo_group ) ) {
					$value = $product_helper->get_variation_meta( $attribute_meta_key );
				} else {
					$value = wpdesk_get_product_meta( $product_helper->get_parent_product(), $attribute_meta_key,
						true );
				}
			} else {
				$value = wpdesk_get_product_meta( $product_helper->get_product(), $attribute_meta_key, true );
			}

			if ( ! empty( $value ) ) {
				$attributes[] = [
					self::ATTRIBUTE_NAME  => $key,
					self::ATTRIBUTE_VALUE => $value
				];
			}
		}

		return $attributes;
	}
}
