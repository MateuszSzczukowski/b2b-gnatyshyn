<?php

namespace WPDesk\Ceneo_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;
use WPDesk\Xml\Integrations\WCI_Ceneo;

/**
 * Retrieves ceneo attributes mapped by \WC_Product Category
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Ceneo_Product_Attributes_Category_Accessor extends Product_Attributes_Accessor {

	/** @var WCI_Ceneo */
	private $wci_ceneo;

	/**
	 * @var \WP_Term
	 */
	private $category;

	/**
	 * @var array
	 */
	private $mapped_category_attributes;

	/**
	 * @var array
	 */
	private $mapped_integration_attributes;

	/**
	 * Ceneo_Product_Attributes_Category_Accessor constructor.
	 *
	 * @param \WP_Term $category
	 * @param array $mapped_category_attributes @see self::get_value() return method for array format.
	 * @param array $mapped_integration_attributes @see self::get_value() return method for array format.
	 */
	public function __construct( \WP_Term $category, array $mapped_category_attributes, array $mapped_integration_attributes ) {
		$this->category                      = $category;
		$this->mapped_category_attributes    = $mapped_category_attributes;
		$this->mapped_integration_attributes = $mapped_integration_attributes;
		$this->wci_ceneo                     = WCI_Ceneo::get_instance();
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with below array format.
	 *  [
	 *      [
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_NAME => (string) $key,
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_VALUE => (string) $value,
	 *      ],
	 *      ...
	 *  ]
	 */
	public function get_value( $source ) {
		return array_merge(
			$this->get_attributes_array( $source, $this->mapped_category_attributes ),
			$this->get_attributes_array( $source, $this->mapped_integration_attributes )
		);
	}

	/**
	 * @param \WC_Product $source
	 * @param array       $mapped_attributes @see self::get_value() return method for array format.
	 *
	 * @return array @see self::get_value() return method for array format.
	 */
	private function get_attributes_array( \WC_Product $source, array $mapped_attributes ) {
		$attributes = [];

		foreach ( $mapped_attributes as $key => $val ) {
			if ( empty( $val ) || ! is_string( $val ) ) {
				continue;
			}

			$value = $this->is_attribute( $val ) ? $this->get_attribute( $source, $val ) : $this->get_attribute_from_meta( $source, $val );

			if ( ! empty( $value ) ) {
				$attributes[] = [
					self::ATTRIBUTE_NAME => ( 'SKU' === $key )? 'Kod producenta': $key,
					self::ATTRIBUTE_VALUE => $value
				];
			}
		}

		return $attributes;
	}

	/**
	 * @param string $val
	 *
	 * @return bool
	 */
	private function is_attribute( $val ) {
		return is_numeric( $val );
	}

	/**
	 * @param \WC_Product $source
	 * @param string      $val
	 *
	 * @return string
	 */
	private function get_attribute( \WC_Product $source, $val ) {
		$product_helper = new Product_Helper( $source );
		$woo_attributes = $product_helper->is_variant() ? $product_helper->get_parent_product()->get_attributes() : $source->get_attributes();


		if ( $this->are_attributes_oop( $woo_attributes ) ) {
			foreach ( $woo_attributes as $attribute ) {
				if ( $attribute->is_taxonomy() && $val === strval( $attribute->get_id() ) ) {

					if ( $product_helper->is_variant() ) {
						$names = $this->get_name_from_variation_term( $attribute->get_terms(), $source->get_attributes() );
					} else {
						$names = implode( self::ATTRIBUTE_GLUE, $this->get_names_from_terms( $attribute->get_terms() ) );
					}

					return strval( $names );
				}
			}
		}

		return '';
	}


	/**
	 * @param \WC_Product $source
	 * @param string      $val
	 *
	 * @return string
	 */
	private function get_attribute_from_meta( \WC_Product $source, $val ) {
		$product_helper = new Product_Helper( $source );

		if ( $source->is_type( 'variation' ) ) {
			$value = get_post_meta( $product_helper->get_product()->get_id(), $val, true );
			if ( empty( $value ) ) {
				$value = get_post_meta( $product_helper->get_parent_product()->get_id(), $val, true );
			}
		} else {
			$value = get_post_meta( $product_helper->get_product()->get_id(), $val, true );
		}

		return strval( $value );
	}

	private function get_name_from_variation_term( array $woo_terms, array $variation_term_slug ) {
		foreach ( $woo_terms as $term ) {
			if ( in_array( $term->slug, $variation_term_slug ) ) {
				return strval( $term->name );
			}
		}

		return '';
	}
}
