<?php

namespace WPDesk\Ceneo_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;

/**
 * Avail can not be 0 in Ceneo
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Ceneo_Product_Avail_Accessor implements Accessor  {

	const CENEO_AVAIL_PRESALE    = 110;
	const CENEO_AVAIL_BACKORDER  = 90;
	const CENEO_AVAIL_ON_14_DAYS = 14;
	const CENEO_AVAIL_ON_7_DAYS  = 7;
	const CENEO_AVAIL_ON_3_DAYS  = 3;
	const CENEO_AVAIL_ON_STOCK   = 1;

	/**
	 * @param \WC_Product $source
	 *
	 * @return int
	 */
	public function get_value( $source ) {
		$availStatus = get_option( 'woocommerce_ceneo_zero_stock', '' );

		if( $source->is_on_backorder() ){
			return self::CENEO_AVAIL_BACKORDER;
		}

		if ( ! empty( $availStatus ) ) {
			if ( $source->is_in_stock() ) {
				if ( null !== $source->get_stock_quantity() ) {
					return ( $source->get_stock_quantity() > 0 )? self::CENEO_AVAIL_ON_STOCK : (int)$availStatus;
				}
				return self::CENEO_AVAIL_ON_STOCK;
			}
			return (int)$availStatus;
		}

		return self::CENEO_AVAIL_ON_STOCK;
	}
}
