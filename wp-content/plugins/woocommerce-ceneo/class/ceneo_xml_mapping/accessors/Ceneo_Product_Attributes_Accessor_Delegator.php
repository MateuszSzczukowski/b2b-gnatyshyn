<?php

namespace WPDesk\Ceneo_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Delegate tasks according to mapping data type and return it's value.
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Ceneo_Product_Attributes_Accessor_Delegator extends Product_Attributes_Accessor {


	const ATTRIBUTES = [
		'woocommerce_ceneo_ISBN'      => 'ISBN',
		'woocommerce_ceneo_EAN'       => 'EAN',
		'woocommerce_ceneo_Producent' => 'Producent',
		'_sku'                        => 'Kod producenta'
	];

	private $last_source;
	private $last_value;


	/** @var Custom_Product_Attributes_Accessor */
	private $accessor;

	public function __construct( Custom_Product_Attributes_Accessor $accessor ) {
		$this->accessor = $accessor;
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with associative format see bellow.
	 *  [
	 *      [
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_NAME => (string) $key,
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_VALUE => (string) $value,
	 *      ],
	 *      ...
	 *  ]
	 */
	public function get_value( $source ) {
		if( $source === $this->last_source){
			return $this->last_value;
		}

		$product_category           = $this->get_product_category( $source );
		$mapped_category_attributes = ( $product_category instanceof \WP_Term ) ? $this->get_category_attributes( $product_category ) : array();

		if ( $this->has_category_mapped_attributes( $mapped_category_attributes ) ) {
			$accessor = new Ceneo_Product_Attributes_Category_Accessor( $product_category, $mapped_category_attributes, $this->get_integration_attributes() );
		} else {
			$accessor = new Ceneo_Product_Attributes_Accessor();
		}

		$this->last_source = $source;
		$this->last_value = $this->merge_attributes( $this->get_ceneo_attributes( $source ), $accessor->get_value( $source ) );
		return $this->last_value;
	}


	/**
	 * @param array $fallback_attributes with array format same as return value.
	 * @param array $attributes with array format same as return value.
	 *
	 * @return array @see self::get_value() return method for array format.
	 */
	private function merge_attributes( array $fallback_attributes, array $attributes ){
		foreach ( $fallback_attributes as $fallback_val ) {
			$exists = false;
			foreach ( $attributes as $attribute_val ) {
				if( $fallback_val[ self::ATTRIBUTE_NAME ] === $attribute_val[ self::ATTRIBUTE_NAME ] ){
					$exists = true;
					break;
				}
			}
			if( false === $exists ){
				$attributes[] = $fallback_val;
			}
		}

		return $attributes;

	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with associative format see bellow.
	 * @see Ceneo_Product_Category_Attributes_Accessor::get_value( $source )
	 */
	private function get_ceneo_attributes( $source ) {
		$this->accessor->set_attributes_keys( self::ATTRIBUTES );
		return $this->accessor->get_value( $source );
	}

	/**
	 * @return array with associative keys (see below).
	 *  [
	 *      (string) $attribute_name => (string) $attribute_id,
	 *      ...
	 *  ],
	 */
	private function get_category_attributes( \WP_Term $category ) {
		return (array) get_option( 'woocommerce_ceneo_attr_cat_' . $category->term_id, array() );
	}

	/**
	 * @return array with associative keys (see below).
	 *  [
	 *      (string) $attribute_name => (string) $attribute_id,
	 *      ...
	 *  ],
	 */
	private function get_integration_attributes() {
		return (array) get_option( 'woocommerce_ceneo_attr_integration', array() );
	}


	/**
	 * @param array $mapped_category_attributes.
	 *
	 * @return bool
	 */
	private function has_category_mapped_attributes( array $mapped_category_attributes ) {
		if ( ! empty( $mapped_category_attributes ) ) {
			foreach ( $mapped_category_attributes as $attribute_id ) {
				if ( ! empty( $attribute_id ) ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return bool|\WP_Term
	 */
	private function get_product_category( $source ) {
		$product_helper = new Product_Helper( $source );

		$product    = ( $product_helper->is_variant() ) ? $product_helper->get_parent_product() : $source;
		$categories = get_the_terms( wpdesk_get_product_id( $product ), 'product_cat' );

		$ceneoCategoryId = - 1;
		$wpTerm          = null;
		if ( ! empty( $categories ) ) {
			foreach ( $categories as $category ) {
				$newCeneoCategoryId = get_option( 'woocommerce_ceneo_term_' . $category->term_id );
				if ( $newCeneoCategoryId > $ceneoCategoryId ) {
					$ceneoCategoryId = $newCeneoCategoryId;
					$wpTerm          = $category;
				}
			}
		}

		if ( $ceneoCategoryId > - 1 && is_numeric( $ceneoCategoryId ) ) {
			return $wpTerm;
		}

		return false;
	}
}
