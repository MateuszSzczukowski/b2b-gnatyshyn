<?php

namespace WPDesk\Ceneo_Xml_Mapping\Accessors;


use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Stock_Accessor;

/**
 * Stock can not be negative in Ceneo
 *
 * @package WPDesk\Ceneo_Xml_Mapping\Accessors
 */
class Ceneo_Product_Stock_Accessor extends Product_Stock_Accessor {

	/**
	 * @param \WC_Product $source
	 *
	 * @return int
	 */
	public function get_value( $source ) {
		if( $source->is_on_backorder() ){
			return 0;
		}
		return max( 0, parent::get_value( $source ) );
	}
}
