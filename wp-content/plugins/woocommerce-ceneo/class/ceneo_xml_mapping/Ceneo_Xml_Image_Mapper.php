<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_NoEmptyElement_Mapper;

/**
 * Ceneo main image
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Image_Mapper extends Abstract_Xml_Parent_Element_Mapper {

	public function __construct() {
		$this->add_mapper( new Accessor_Attribute_NoEmptyElement_Mapper( new Product_ImageUrl_Accessor(), 'url' ) );
	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	protected function get_name() {
		return 'main';
	}
}
