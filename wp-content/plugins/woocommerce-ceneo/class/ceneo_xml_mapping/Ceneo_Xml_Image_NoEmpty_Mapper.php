<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_NoEmptyElement_Mapper;

/**
 * Ceneo main image
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Image_NoEmpty_Mapper extends Ceneo_Xml_Image_Mapper implements Xml_NoEmpty_Mapper {

	private $accessor;

	public function __construct() {
		$this->accessor = new Product_ImageUrl_Accessor();
		$this->add_mapper( new Accessor_Attribute_NoEmptyElement_Mapper( $this->accessor, 'url' ) );
	}

	public function is_empty( $source ) {
		return empty( $this->accessor->get_value( $source) );
	}


}
