<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Accessors\Property_Accessor;
use WPDesk\Ceneo_Xml_Mapping\Accessors\Ceneo_Product_Stock_Accessor;
use WPDesk\Ceneo_Xml_Mapping\Accessors\Ceneo_Product_Avail_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Decorators\Accessor_Variant_Fallback;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Category_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Name_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Description_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Url_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Price_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Weight_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_Mapper;
use WPDesk\Xml\Integrations\WCI_Ceneo;

/**
 * Ceneo product
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Product_Mapper extends Abstract_Xml_Parent_Element_Mapper {
	public function __construct() {
		$this->add_mapper( new Accessor_Attribute_Mapper( new Property_Accessor( 'id' ), 'id' ) );
		$this->add_mapper( new Accessor_Attribute_Mapper( new Product_Url_Accessor(),	'url') );
		$this->add_mapper( new Accessor_Attribute_Mapper( new Product_Price_Accessor(), 'price' ) );
		$this->add_mapper( new Accessor_Attribute_NoEmpty_Mapper( new Ceneo_Product_Stock_Accessor(), 'stock' ) );
		$this->add_mapper( new Accessor_Attribute_NoEmpty_Mapper( new Ceneo_Product_Avail_Accessor(), 'avail' ) );
		$this->add_mapper( new Accessor_Attribute_NoEmpty_Mapper(
				new Accessor_Variant_Fallback( new Product_Weight_Accessor() ), 'weight' )
		);
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Name_Accessor(), 'name' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Description_Accessor(), 'desc' ) );

		$category = new Product_Category_Accessor();
		$category->set_xml_category( WCI_Ceneo::get_instance() );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( $category, 'cat' ) );

		$this->add_mapper( new Ceneo_Xml_Images_NoEmpty_Mapper() );
		$this->add_mapper( new Ceneo_Xml_Attributes_NoEmpty_Mapper() );
	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	protected function get_name() {
		return 'o';
	}
}
