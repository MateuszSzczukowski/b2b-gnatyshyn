<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Ceneo attributes
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Attributes_NoEmpty_Mapper extends Ceneo_Xml_Attributes_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->array_accessor->get_value( $source ) );
	}
}
