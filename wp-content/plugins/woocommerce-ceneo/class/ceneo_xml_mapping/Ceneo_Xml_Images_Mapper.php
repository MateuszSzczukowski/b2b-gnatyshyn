<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;

/**
 * Ceneo images
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Images_Mapper extends Abstract_Xml_Parent_Element_Mapper {

	public function __construct() {
		$this->add_mapper( new Ceneo_Xml_Image_Mapper() );
	}

	protected function get_name() {
		return 'imgs';
	}

}
