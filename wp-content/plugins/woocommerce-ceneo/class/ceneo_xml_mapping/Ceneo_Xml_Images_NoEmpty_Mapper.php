<?php

namespace WPDesk\Ceneo_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Ceneo images
 *
 * @package WPDesk\Ceneo_Xml_Mapping
 */
class Ceneo_Xml_Images_NoEmpty_Mapper extends Ceneo_Xml_Images_Mapper implements Xml_NoEmpty_Mapper {

	private $mapper;

	public function __construct() {
		$this->mapper = new Ceneo_Xml_Image_NoEmpty_Mapper();
		$this->add_mapper( $this->mapper );
	}

	public function is_empty( $source ) {
		return $this->mapper->is_empty( $source );
	}
}
