<?php

use WPDesk\Xml\Xml_Factory;

/**
 * Class WooCommerce_Ceneo_Plugin
 */
class WooCommerce_Ceneo_Plugin extends \CeneoVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin {

	/**
	 * Scripts version
	 *
	 * @var string
	 */
	public $scripts_version;

	/**
	 * Plugin path.
	 *
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Template path.
	 *
	 * @var string
	 */
	public $template_path;

	/**
	 * @var Cron_Service
	 */
	private $cron_service;

	/**
	 * @var Xml_Factory
	 */
	private $xml_factory;

	/**
	 * WooCommerce_Furgonetka_Plugin constructor.
	 *
	 * @param \CeneoVendor\WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( \CeneoVendor\WPDesk_Plugin_Info $plugin_info ) {
		$this->plugin_info = $plugin_info;
		$this->cron_service = new Cron_Service( $this );
		$this->xml_factory = new Xml_Factory( $this );

		parent::__construct( $this->plugin_info );
	}

	/**
	 * Init base variables for plugin
	 */
	public function init_base_variables() {
		$this->plugin_url         = $this->plugin_info->get_plugin_url();
		$this->plugin_path        = $this->plugin_info->get_plugin_dir();
		$this->template_path      = $this->plugin_info->get_text_domain();
		$this->plugin_text_domain = $this->plugin_info->get_text_domain();
		$this->template_path      = $this->plugin_info->get_text_domain();
		$this->scripts_version    = $this->plugin_info->get_version();
		$this->plugin_namespace   = 'woocommerce_ceneo';
	}

	/**
	 * Init plugin
	 */
	public function init() {
		parent::init();
		if ( is_admin() ) {
			$this->ceneoAdmin = new CeneoAdmin( $this );
		}
	}

	public static function get_price_comparison_items(){
		return array(
			'ceneo'    => __( 'Ceneo', 'woocommerce_ceneo' ),
			'nokaut'   => __( 'Nokaut', 'woocommerce_ceneo' ),
			'domodi'   => __( 'Domodi', 'woocommerce_ceneo' ),
			'homebook' => __( 'Homebook', 'woocommerce_ceneo' )
		);
	}

	public function hooks() {
		parent::hooks();
		add_action( 'woocommerce_ceneo_processing_xml', array( $this, 'woocommerce_ceneo_processing_xml' ) );
		add_action( 'woocommerce_ceneo_refresh_xml', array( $this, 'woocommerce_ceneo_refresh_xml' ) );

		foreach ( self::get_price_comparison_items() as $key => $val ) {
			$class_name = Xml_Factory::get_generator_class_by_name( $key );
			if ( !empty( $class_name ) ) {
				add_action( 'wp_ajax_' . $class_name::XML_GENERATE_AJAX_HANDLE, array( $this, 'ajax_generate_xml_action' ) );
				add_action( 'wp_ajax_nopriv_' . $class_name::XML_GENERATE_AJAX_HANDLE, array( $this, 'ajax_generate_xml_action' ) );
				add_action( 'wp_ajax_' . $class_name::XML_PROCESS_AJAX_HANDLE, array( $this, 'ajax_process_xml_action' ) );
				add_action( 'wp_ajax_nopriv_' . $class_name::XML_PROCESS_AJAX_HANDLE, array( $this, 'ajax_process_xml_action' ) );
			}
		}

		add_action( 'init', array( get_class( $this ), 'init_pretty_ceneo_url_action' ) );
		$this->cron_service->hooks();
	}

	/**
	 * Plugin actions links.
	 *
	 * @param array $links Links.
	 *
	 * @return array
	 */
	public function links_filter( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=woocommerce_ceneo' ) . '">' . __( 'Ustawienia', 'woocommerce_ceneo' ) . '</a>',
			'<a href="https://wpde.sk/ceneo-docs">' . __( 'Docs', 'woocommerce_ceneo' ) . '</a>',
			'<a href="https://www.wpdesk.pl/support/">' . __( 'Support', 'woocommerce_ceneo' ) . '</a>',
		);

		return array_merge( $plugin_links, $links );
	}

	/**
	 * Pretty ceneo url
	 */
	public static function init_pretty_ceneo_url_action() {
		foreach ( self::get_price_comparison_items() as $key => $val ) {
			$class_name = Xml_Factory::get_generator_class_by_name( $key );
			if ( !empty( $class_name ) ) {
				add_rewrite_rule( '^' . $class_name::XML_FILE_NAME . '?', $class_name::XML_URL , 'top' );
			}
		}
	}

	/**
	 * Ajax handle for xml generatation
	 * @return void
	 */
	public function ajax_generate_xml_action() { //only show generated file
		header( 'Content-Type: text/xml' );
		$action = isset( $_GET['action'] ) ? sanitize_text_field( $_GET['action'] ) : '';
		$factory = new Xml_Factory( $this );
		$generator = $factory->get_generator( $action );
		echo html_entity_decode( $generator->get_xml() );
		exit();
	}

	/**
	 * Ajax handle for xml processgeneratation
	 * @return void
	 */
	public function ajax_process_xml_action() {
		$init = isset( $_GET['init'] ) && 'true' === $_GET['init'];
		$action = isset( $_GET['action'] ) ? sanitize_text_field( $_GET['action'] ) : '';
		$this->woocommerce_ceneo_processing_xml( $action, $init );
	}

	/**
	 * Cron handle for xml generatation
	 * @return void
	 */
	public function woocommerce_ceneo_processing_xml( $action, $init = false ) {
			$generator = $this->xml_factory->get_generator( $action );
			if( true === $init ){
				$generator->regenerate();
			}
			$state     = $generator->generate_xml();
			if ( wp_doing_ajax() ) {
				wp_send_json( $state );
			}
		exit();
	}

	/**
	 * Cron handle for xml regeneratation
	 * @return void
	 */
	public function woocommerce_ceneo_refresh_xml( $action ) {
		$generator = $this->xml_factory->get_generator( $action );
		$generator->regenerate();
		exit();
	}

	/**
	 * Gets setting value
	 *
	 * @param string $name    Option name.
	 * @param string $default Default.
	 *
	 * @return mixed
	 */
	public function getSettingValue( $name, $default = null ) {
		return get_option( $this->plugin_namespace . '_' . $name, $default );
	}

	public function setSettingValue( $name, $value ) {
		return update_option( $this->plugin_namespace . '_' . $name, $value );
	}

	public function isSettingValue( $name ) {
		$option = get_option( $this->plugin_namespace . '_' . $name );

		return ! empty( $option );
	}

	public function getNamespace() {
		return $this->plugin_namespace;
	}

	public function get_template_path() {
		return $this->template_path;
	}

	/**
	 * Renders end returns selected template
	 *
	 * @param string $name name of the template
	 * @param string $path additional inner path to the template
	 * @param array  $args args accesible from template
	 *
	 * @return string
	 */
	public function load_template( $name, $path = '', $args = array() ) {
		$plugin_template_path = trailingslashit( $this->plugin_path ) . 'templates/';

		// Look within passed path within the theme - this is priority.
		$template = locate_template(
			array(
				trailingslashit( $this->get_template_path() ) . trailingslashit( $path ) . $name . '.php',
			)
		);

		if ( ! $template ) {
			$template = $plugin_template_path . trailingslashit( $path ) . $name . '.php';
		}

		extract( $args );
		ob_start();
		include( $template );

		return ob_get_clean();
	}
}
