<?php

namespace WPDesk\Homebook_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Homebook attributes.
 *
 * @package WPDesk\Homebook_Xml_Mapping
 */
class Homebook_Xml_Attributes_NoEmpty_Mapper extends Homebook_Xml_Attributes_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->array_accessor->get_value( $source ) );
	}
}
