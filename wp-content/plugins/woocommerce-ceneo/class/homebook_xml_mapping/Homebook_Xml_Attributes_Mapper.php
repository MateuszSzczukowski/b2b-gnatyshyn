<?php

namespace WPDesk\Homebook_Xml_Mapping;

use WPDesk\Accessors\Array_Value_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Array_Element_Mapper;
use WPDesk\Homebook_Xml_Mapping\Accessors\Homebook_Product_Attributes_Accessor;
use \WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;

/**
 * Homebook attributes.
 *
 * @package WPDesk\Homebook_Xml_Mapping
 */
class Homebook_Xml_Attributes_Mapper extends Array_Element_Mapper {

	public function __construct() {

		parent::__construct( new Homebook_Product_Attributes_Accessor( new Custom_Product_Attributes_Accessor() ), 'attrs' );

		$attribute = new Accessor_CDATA_Element_Mapper(
			new Array_Value_Accessor( Homebook_Product_Attributes_Accessor::ATTRIBUTE_VALUE ), 'attr'
		);

		$attribute->add_mapper( new Accessor_Attribute_Mapper(
				new Array_Value_Accessor( Homebook_Product_Attributes_Accessor::ATTRIBUTE_NAME ), 'name' )
		);

		$this->add_mapper( $attribute );
	}
}
