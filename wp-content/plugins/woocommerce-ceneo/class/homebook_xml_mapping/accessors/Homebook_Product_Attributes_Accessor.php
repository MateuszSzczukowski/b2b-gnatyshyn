<?php

namespace WPDesk\Homebook_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Abstract_Custom_Product_Attributes_Accessor;

/**
 * Retrieves homebook attributes and WooCommerce attributes from \WC_Product
 *
 * @package WPDesk\Homebook_Xml_Mapping\Accessors
 */
class Homebook_Product_Attributes_Accessor extends Product_Attributes_Accessor {

	const ATTRIBUTES = [
		'woocommerce_ceneo_EAN'       => 'EAN',
		'woocommerce_ceneo_Producent' => 'Producent',
		'_sku'                        => 'Kod_producenta'
	];

	/** @var Custom_Product_Attributes_Accessor */
	private $accessor;

	public function __construct( Custom_Product_Attributes_Accessor $accessor ) {
		$this->accessor = $accessor;
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array with below array format.
	 *  [
	 *      [
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_NAME => (string) $key,
	 *        (string) Product_Attributes_Accessor::ATTRIBUTE_VALUE => (string) $value,
	 *      ],
	 *      ...
	 *  ]
	 */
	public function get_value( $source ) {
		return array_merge(
			parent::get_value( $source ),
			$this->get_homebook_attributes( $source )
		);
	}

	/**
	 * @param \WC_Product $source
	 *
	 * @return array @see self::get_value() return method for array format.
	 */
	private function get_homebook_attributes( $source ) {
		$this->accessor->set_attributes_keys( self::ATTRIBUTES );
		return $this->accessor->get_value( $source );
	}
}
