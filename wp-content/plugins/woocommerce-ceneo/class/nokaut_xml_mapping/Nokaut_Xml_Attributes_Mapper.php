<?php

namespace WPDesk\Nokaut_Xml_Mapping;

use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Attribute_Mapper;

/**
 * Nokaut attributes
 *
 * @package WPDesk\Nokaut_Xml_Mapping
 */
class Nokaut_Xml_Attributes_Mapper extends Accessor_Attribute_Mapper {


	public function map_to_xml( \DOMNode $element, $source ) {
		$root_element = $element->ownerDocument;
		$array        = $this->accessor->get_value( $source );
		if ( is_array( $array ) && count( $array ) > 0 ) {
			foreach ( $array as $source_element ) {
				if ( $element instanceof \DOMElement ) {
					$property_element = $root_element->createElement( $this->name );
					$property_element->setAttribute( 'name', $source_element[ Nokaut_Product_Attributes_Accessor::ATTRIBUTE_NAME ] );
					$property_element->appendChild( $element->ownerDocument->createCDATASection( $source_element[ Nokaut_Product_Attributes_Accessor::ATTRIBUTE_VALUE ] ) );
					$element->appendChild( $property_element );
				}
			}
		}

		return $element;
	}
}
