<?php

namespace WPDesk\Nokaut_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;

/**
 * Retrieves categories from \WC_Product
 *
  * @package WPDesk\Nokaut_Xml_Mapping\Accessors
 */
class Nokaut_Product_Producer_Accessor implements Accessor {

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$attribute_meta_key = 'woocommerce_ceneo_Producent';
		$producer = $source->get_meta( $attribute_meta_key, true);
		$producer = ( !empty( $producer ))? $producer : '';
		return apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.$attribute_meta_key, $producer, $source->get_id() );
	}
}
