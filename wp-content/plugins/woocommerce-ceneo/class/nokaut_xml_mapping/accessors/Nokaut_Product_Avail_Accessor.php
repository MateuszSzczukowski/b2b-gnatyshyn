<?php

namespace WPDesk\Nokaut_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Ceneo_Xml_Mapping\Accessors\Ceneo_Product_Avail_Accessor;
/**
 *
 * @package WPDesk\Nokaut_Xml_Mapping\Accessors
 */
class Nokaut_Product_Avail_Accessor implements Accessor  {

	const NOKAUT_AVAIL_CHECK_IN_SHOP = 4;
	const NOKAUT_AVAIL_BACKORDER = 3;
	const NOKAUT_AVAIL_OVER_A_WEEK  = 2;
	const NOKAUT_AVAIL_ON_WEEK  = 1;
	const NOKAUT_AVAIL_ON_STOCK  = 0;

	/**
	 * @param \WC_Product $source
	 *
	 * @return int
	 */
	public function get_value( $source ) {
		$availStatus = get_option( 'woocommerce_ceneo_zero_stock', '' );
		if( $source->is_on_backorder() ){
			return self::NOKAUT_AVAIL_BACKORDER;
		}
		if ( is_numeric( $availStatus ) ) {
			$availStatus = $this->convert_ceneo_status_to_nokaut( (int) $availStatus );
			if ( $source->is_in_stock() ) {
				if ( null !== $source->get_stock_quantity() ) {
					return ( $source->get_stock_quantity() > 0 )? self::NOKAUT_AVAIL_ON_STOCK : $availStatus;
				}
				return self::NOKAUT_AVAIL_ON_STOCK;
			}
			return $availStatus;
		}

		return self::NOKAUT_AVAIL_ON_STOCK;
	}

	/**
	 * @param integer $ceneo_status
	 *
	 * @return int
	 */
	private function convert_ceneo_status_to_nokaut( $ceneo_status ){
		if( Ceneo_Product_Avail_Accessor::CENEO_AVAIL_ON_STOCK === $ceneo_status ){
			return self::NOKAUT_AVAIL_ON_STOCK;
		}

		if( Ceneo_Product_Avail_Accessor::CENEO_AVAIL_ON_3_DAYS === $ceneo_status || Ceneo_Product_Avail_Accessor::CENEO_AVAIL_ON_7_DAYS === $ceneo_status ){
			return self::NOKAUT_AVAIL_ON_WEEK;
		}

		if( Ceneo_Product_Avail_Accessor::CENEO_AVAIL_ON_14_DAYS === $ceneo_status ){
			return self::NOKAUT_AVAIL_OVER_A_WEEK;
		}

		if( Ceneo_Product_Avail_Accessor::CENEO_AVAIL_BACKORDER === $ceneo_status || Ceneo_Product_Avail_Accessor::CENEO_AVAIL_PRESALE === $ceneo_status ){
			return self::NOKAUT_AVAIL_BACKORDER;
		}

		return self::NOKAUT_AVAIL_CHECK_IN_SHOP;
	}
}
