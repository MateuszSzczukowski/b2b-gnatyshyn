<?php

namespace WPDesk\Nokaut_Xml_Mapping\Accessors;

use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Name_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves product name compatible with nokaut settings from \WC_Product
 *
 *
 * @package WPDesk\Nokaut_Xml_Mapping\Accessors
 */
class Nokaut_Product_Name_Accessor extends Product_Name_Accessor {
	const WOOCOMMERCE_CENEO_ALTERNATIVE_NAME = 'woocommerce_ceneo_alternative_name';

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$is_variation   = $product_helper->is_variant();
		if ( $is_variation ) {
			$parent_product = $product_helper->get_parent_product();
		}

		if ( $is_variation ) {
			$name = wpdesk_get_variation_meta( $source, self::WOOCOMMERCE_CENEO_ALTERNATIVE_NAME, true );
		} else {
			$name = wpdesk_get_product_meta( $source, self::WOOCOMMERCE_CENEO_ALTERNATIVE_NAME, true );
		}
		if ( $is_variation && empty( $name ) ) {
			$name = wpdesk_get_product_meta( $parent_product, self::WOOCOMMERCE_CENEO_ALTERNATIVE_NAME, true );
			if ( ! empty( $name ) ) {
				$name = $this->get_variation_name($source);
			}
		}
		if ( empty( $name ) ) {
			return parent::get_value( $source );
		}

		return $name;
	}
}
