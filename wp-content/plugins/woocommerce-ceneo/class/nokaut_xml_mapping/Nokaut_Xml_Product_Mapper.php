<?php

namespace WPDesk\Nokaut_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Accessors\Property_Accessor;
use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Stock_Accessor;
use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Avail_Accessor;
use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Producer_Accessor;
use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Decorators\Accessor_Variant_Fallback;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Price_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Weight_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Url_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Simple_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Category_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Name_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Description_Accessor;
use WPDesk\Xml\Integrations\WCI_Nokaut;

/**
 * Nokaut product
 *
 * @package WPDesk\Nokaut_Xml_Mapping
 */
class Nokaut_Xml_Product_Mapper extends Abstract_Xml_Parent_Element_Mapper {
	public function __construct() {
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Property_Accessor( 'id' ), 'id' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Name_Accessor(), 'name' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Description_Accessor(), 'description' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Product_Url_Accessor(), 'url' ) );
		$this->add_mapper( new Accessor_CDATA_Element_NoEmpty_Mapper( new Product_ImageUrl_Accessor(), 'image' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Accessor_Variant_Fallback( new Product_Weight_Accessor() ), 'weight' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Product_Price_Accessor(), 'price' ) );
		$this->add_mapper( new Accessor_CDATA_Element_NoEmpty_Mapper( new Nokaut_Product_Producer_Accessor(), 'producer' ) );

		$category = new Product_Category_Accessor();
		$category->set_xml_category( WCI_Nokaut::get_instance() );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( $category, 'category' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Nokaut_Product_Stock_Accessor(), 'instock' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Nokaut_Product_Avail_Accessor(), 'availability' ) );
		$this->add_mapper( new Nokaut_Xml_Attributes_NoEmpty_Mapper( new Nokaut_Product_Attributes_Accessor( new Custom_Product_Attributes_Accessor() ), 'property') );
	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	protected function get_name() {
		return 'offer';
	}
}
