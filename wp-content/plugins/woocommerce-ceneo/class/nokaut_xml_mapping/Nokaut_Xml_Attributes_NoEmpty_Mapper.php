<?php

namespace WPDesk\Nokaut_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Nokaut attributes
 *
 * @package WPDesk\Nokaut_Xml_Mapping
 */
class Nokaut_Xml_Attributes_NoEmpty_Mapper extends Nokaut_Xml_Attributes_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->accessor->get_value( $source ) );
	}
}
