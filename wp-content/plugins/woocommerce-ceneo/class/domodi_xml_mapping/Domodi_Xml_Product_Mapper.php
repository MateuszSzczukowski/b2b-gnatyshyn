<?php

namespace WPDesk\Domodi_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Abstract_Xml_Parent_Element_Mapper;
use WPDesk\Accessors\Property_Accessor;
use WPDesk\Nokaut_Xml_Mapping\Accessors\Nokaut_Product_Producer_Accessor;
use WPDesk\Ceneo_Xml_Mapping\Accessors\Ceneo_Product_Avail_Accessor;
use WPDesk\Domodi_Xml_Mapping\Accessors\Domodi_Product_Gender_Accessor;
use WPDesk\Domodi_Xml_Mapping\Accessors\Domodi_Product_Sizes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Price_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Url_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_CDATA_Element_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Accessor_Simple_Element_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_Category_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Name_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Images_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Mappers\Image_NoEmpty_Mapper;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Product_ImageUrl_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Description_Accessor;


/**
 * Domodi product
 *
 * @package WPDesk\Domodi_Xml_Mapping
 */
class Domodi_Xml_Product_Mapper extends Abstract_Xml_Parent_Element_Mapper {
	public function __construct() {
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Property_Accessor( 'id' ), 'id' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Name_Accessor(), 'name' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Custom_Product_Description_Accessor(), 'desc' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Product_Url_Accessor(), 'url' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Ceneo_Product_Avail_Accessor(), 'avail' ) );
		$this->add_mapper( new Accessor_CDATA_Element_Mapper( new Product_Category_Accessor(), 'cat' ) );
		$this->add_mapper( new Accessor_CDATA_Element_NoEmpty_Mapper( new Nokaut_Product_Producer_Accessor(), 'brand' ) );
		$this->add_mapper( new Accessor_CDATA_Element_NoEmpty_Mapper( new Domodi_Product_Gender_Accessor(), 'gender' ) );
		$this->add_mapper( new Accessor_CDATA_Element_NoEmpty_Mapper( new Domodi_Product_Sizes_Accessor(), 'sizes' ) );
		$this->add_mapper( new Accessor_Simple_Element_Mapper( new Product_Price_Accessor(), 'price' ) );
		$this->add_mapper( new Images_NoEmpty_Mapper( new Image_NoEmpty_Mapper( new Product_ImageUrl_Accessor(), 'img', true), 'imgs' ) );
		$this->add_mapper( new Domodi_Xml_Attributes_NoEmpty_Mapper() );

	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	protected function get_name() {
		return 'offer';
	}
}
