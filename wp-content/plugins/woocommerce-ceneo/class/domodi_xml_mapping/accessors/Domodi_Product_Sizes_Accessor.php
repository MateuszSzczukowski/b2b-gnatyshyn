<?php

namespace WPDesk\Domodi_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves gender from \WC_Product
 * @package WPDesk\Domodi_Xml_Mapping\Accessors
 */
class Domodi_Product_Sizes_Accessor implements Accessor {

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$sizes          = [];
		$product_helper = new Product_Helper( $source );

		$attribute_meta_key = 'woocommerce_ceneo_sizes';
		if ( $product_helper->is_variant() ) {
			$value = $product_helper->get_variation_meta( $attribute_meta_key );
		} else {
			if ( $product_helper->is_variable() ) {
				foreach ( $product_helper->get_product()->get_children() as $child_id ) {
					$value = wpdesk_get_variation_meta( $child_id, $attribute_meta_key, true );
					if ( ! empty( $value ) ) {
						$sizes[] = $value;
					}
				}
			} else {
				$value = wpdesk_get_product_meta( $product_helper->get_product(), $attribute_meta_key, true );
			}
		}

		if ( ! empty( $value ) && empty( $sizes ) ) {
			$sizes[] = $value;
		}

		$result = ( ! empty( $sizes ) ) ? implode( ';', $sizes ) : '';

		return apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.$attribute_meta_key, $result, $source->get_id() );
	}
}
