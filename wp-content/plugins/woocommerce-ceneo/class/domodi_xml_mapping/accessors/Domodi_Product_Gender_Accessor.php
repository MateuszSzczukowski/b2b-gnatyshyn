<?php

namespace WPDesk\Domodi_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;

/**
 * Retrieves gender from \WC_Product
 * @package WPDesk\Domodi_Xml_Mapping\Accessors
 */
class Domodi_Product_Gender_Accessor implements Accessor {

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$attribute_meta_key = 'woocommerce_ceneo_gender';
		$gender = $source->get_meta( $attribute_meta_key, true);
		$gender = ( !empty( $gender ))? $gender : '';
		return apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX.$attribute_meta_key, $gender, $source->get_id() );

	}
}
