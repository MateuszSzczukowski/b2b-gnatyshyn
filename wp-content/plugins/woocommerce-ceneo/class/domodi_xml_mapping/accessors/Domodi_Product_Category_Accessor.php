<?php

namespace WPDesk\Domodi_Xml_Mapping\Accessors;

use WPDesk\Accessors\Accessor;
use WPDesk\Woocommerce_Xml_Mapping\Product_Helper;

/**
 * Retrieves categories from \WC_Product
 *
 * @package WPDesk\Domodi_Xml_Mapping\Accessors
 */
class Domodi_Product_Category_Accessor implements Accessor {

	/**
	 * @param \WC_Product $source
	 *
	 * @return string
	 */
	public function get_value( $source ) {
		$product_helper = new Product_Helper( $source );
		$is_variation   = $product_helper->is_variant();
		if ($is_variation) {
			$categories = get_the_terms( wpdesk_get_product_id( $product_helper->get_parent_product() ), 'product_cat' );
		} else {
			$categories = get_the_terms( wpdesk_get_product_id( $source ), 'product_cat' );
		}

		if ( ! empty( $categories ) ) {
			$catArray = [];
			foreach ( $categories as $cat ) {
				$catArray[] = $cat->name;
			}

			return implode( ' - ', $catArray );
		}

		return '';
	}
}
