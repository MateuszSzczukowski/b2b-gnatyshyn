<?php

namespace WPDesk\Domodi_Xml_Mapping;

use WPDesk\Abstract_Xml_Mapping\Xml_NoEmpty_Mapper;

/**
 * Domodi attributes
 *
 * @package WPDesk\Domodi_Xml_Mapping
 */
class Domodi_Xml_Attributes_NoEmpty_Mapper extends Domodi_Xml_Attributes_Mapper implements Xml_NoEmpty_Mapper {

	public function is_empty( $source ) {
		return empty( $this->array_accessor->get_value( $source ) );
	}
}
