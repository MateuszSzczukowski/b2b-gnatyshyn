<?php

namespace WPDesk\Woocommerce_Xml_Mapping\Mappers;

use WPDesk\Abstract_Xml_Mapping\Xml_Mapper;
use WPDesk\Accessors\Accessor;

/**
 * Base class for mapper accessor->attribute
 *
 * @package WPDesk\Woocommerce_Xml_Mapping
 */
abstract class Abstract_Accessor_Attribute_Mapper implements Xml_Mapper {
	/** @var string */
	protected $name;

	/** @var Accessor */
	protected $accessor;

	/**
	 * @param Accessor $accessor How to get access to source value
	 * @param string   $attribute_name Name of xml attribute.
	 */
	public function __construct( Accessor $accessor, $attribute_name ) {
		$this->accessor = $accessor;
		$this->name     = $attribute_name;
	}
}