<?php

namespace WPDesk\Abstract_Xml_Mapping;

/**
 * Base class for all Mappers that also creates xml elements
 *
 * @package WPDesk\Abstract_Xml_Mapping
 */
abstract class Abstract_Xml_Parent_Element_Mapper implements Xml_Mapper, Xml_Element_Parent_Factory {
	/** @var Xml_Mapper[] */
	protected $mappers;

	/**
	 * Appends mapper that will be fired on created element
	 *
	 * @param Xml_Mapper $mapper
	 */
	public function add_mapper( Xml_Mapper $mapper ) {
		$this->mappers[] = $mapper;
	}

	/**
	 * Map data from source to xml given element
	 *
	 * @param \DOMNode $element
	 * @param mixed    $source
	 *
	 * @return \DOMNode
	 */
	public function map_to_xml( \DOMNode $element, $source ) {
		$this->append_mappers_data( $element, $source );

		return $element;
	}

	/**
	 * Use all mappers and append data to xml structure
	 *
	 * @param \DOMNode          $element
	 * @param                   $source
	 */
	protected function append_mappers_data( \DOMNode $element, $source ) {
		if ( $this->mappers ) {
			foreach ( $this->mappers as $mapper ) {
				$this->append_mapper_data( $element, $source, $mapper );
			}
		}
	}

	/**
	 * Use single mapper and append data to xml structure
	 *
	 * @param \DOMNode   $element
	 * @param mixed      $source
	 * @param Xml_Mapper $mapper
	 */
	protected function append_mapper_data( \DOMNode $element, $source, Xml_Mapper $mapper ) {
		if ( $mapper instanceof Xml_NoEmpty_Mapper ) {
			if( $mapper->is_empty( $source ) ){
				return;
			}
		}

		if ( $mapper instanceof Xml_Element_Factory ) {
			$child = $this->append_factory( $mapper, $element );
			$mapper->map_to_xml( $child, $source );
		} else {
			$mapper->map_to_xml( $element, $source );
		}
	}

	/**
	 * Append data from Xml_Element_Factory to structure
	 *
	 * @param Xml_Element_Factory $mapper
	 * @param \DOMNode            $element
	 *
	 * @return \DOMNode
	 */
	private function append_factory( Xml_Element_Factory $mapper, \DOMNode $element ) {
		$child = $mapper->create_xml_element( $element->ownerDocument );
		$element->appendChild( $child );

		return $child;
	}

	/**
	 * Create xml element and return as node
	 *
	 * @param \DOMDocument $document
	 *
	 * @return \DOMNode
	 */
	public function create_xml_element( \DOMDocument $document ) {
		return $document->createElement( $this->get_name() );
	}

	/**
	 * Return element name
	 *
	 * @return string
	 */
	abstract protected function get_name();
}
