<?php

namespace WPDesk\Abstract_Xml_Mapping;

/**
 * For mappers that can also creates an xml element
 *
 * @package WPDesk\Abstract_Xml_Mapping
 */
interface Xml_Element_Factory extends Xml_Mapper {
	/**
	 * Create xml element and return as node
	 *
	 * @param \DOMDocument $document
	 *
	 * @return \DOMNode
	 */
	public function create_xml_element( \DOMDocument $document );
}