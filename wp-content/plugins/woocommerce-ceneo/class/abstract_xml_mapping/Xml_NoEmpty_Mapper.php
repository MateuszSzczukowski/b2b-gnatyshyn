<?php

namespace WPDesk\Abstract_Xml_Mapping;


/**
 * Base mapper interface. Mapper is a class that can map given source to an xml element
 *
 * @package WPDesk\Abstract_Xml_Mapping
 */
interface Xml_NoEmpty_Mapper {
	/**
	 * Map check if mapper has no data.
	 *
	 * @param mixed    $source
	 *
	 * @return bool
	 */
	public function is_empty( $source );
}
