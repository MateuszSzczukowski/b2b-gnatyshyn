<?php

namespace WPDesk\Abstract_Xml_Mapping;


/**
 * Base mapper interface. Mapper is a class that can map given source to an xml element
 *
 * @package WPDesk\Abstract_Xml_Mapping
 */
interface Xml_Mapper {
	/**
	 * Map data from source to xml given element
	 *
	 * @param \DOMNode $element
	 * @param mixed    $source
	 *
	 * @return \DOMNode
	 */
	public function map_to_xml( \DOMNode $element, $source );
}