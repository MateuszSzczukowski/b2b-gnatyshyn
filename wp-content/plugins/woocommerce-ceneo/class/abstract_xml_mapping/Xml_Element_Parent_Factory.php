<?php

namespace WPDesk\Abstract_Xml_Mapping;

/**
 * For mappers that can aggregate other mappers
 *
 * @package WPDesk\Abstract_Xml_Mapping
 */
interface Xml_Element_Parent_Factory extends Xml_Element_Factory {
	/**
	 * Appends mapper that will be fired on created element
	 *
	 * @param Xml_Mapper $mapper
	 *
	 * @return void
	 */
	public function add_mapper( Xml_Mapper $mapper );
}