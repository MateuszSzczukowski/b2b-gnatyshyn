<?php

/**
 * View helper for templates
 */
class View_Helper {

	/**
	 * @param array $args with array format ( see below ).
	 *   $args = [
	 *      (string) 'id' => (string) $id,
	 *      (string) 'label' => (string) $label,
	 *      (string) 'name' => (string) $name,
	 *      (string) 'class' => (string) $class,
	 *      (string) 'value' => (string) $value,
	 *      (string) 'options' => (array) $array_options ( @see CeneoAdmin::get_attributes_options() to get array fromat details ),
	 *      (string) 'multiple' => (bool) $multiple,
	 *   ]
	 * @return string.
	 */
	public function create_select_field( array $args ) {

		$id       = isset( $args['id'] ) ? $args['id'] : '';
		$label    = isset( $args['label'] ) ? $args['label'] : '';
		$name     = isset( $args['name'] ) ? $args['name'] : $id;
		$class    = isset( $args['class'] ) ? $args['class'] : $id;
		$options  = isset( $args['options'] ) && is_array( $args['options'] ) ? $args['options'] : array();
		$value    = isset( $args['value'] ) ? $args['value'] : '';
		$value    = is_array( $value ) ? $value : array( $value );
		$multiple = isset( $args['multiple'] ) ? $args['multiple'] : false;
		if( true === $multiple ){
			$multiple_param = 'multiple="multiple"';
		}else{
			$multiple_param = '';
			$value = isset($value[0])? array($value[0]): array('');
		}

		$string = '<label for="' . esc_attr($id) . '">' . esc_attr($label) . '</label>';
		$string .= '<select id="' . esc_attr($id) . '" name="' . esc_attr($name) . '" class="' . esc_attr($class) . '" '.$multiple_param.' >' . $this->create_select_options( $options, $value ) . '</select>';

		return $string;
	}


	/**
	 * @param array $options with array format @see CeneoAdmin::get_attributes_options().
	 * @param array $values if empty, $value should have empty strings ''.
	 *
	 * @return string
	 */
	private function create_select_options( array $options, array $values ) {
		$string = '';
		foreach ( $options as $key => $val ) {
			if ( is_array( $val ) ) {
				if ( isset( $val['optgroup'] ) && isset( $val['children'] ) ) {
					$string .= '<optgroup label="' . $val['optgroup'] . '">';
					$string .= $this->create_select_options( $val['children'], $values );
					$string .= '</optgroup>';
				} else {
					$string .= $this->create_select_options( $val, $values );
				}
			} else {
				$selected = in_array( $key, $values ) ? 'selected' : '';
				$string   .= '<option value="' . esc_attr($key) . '" ' . $selected . ' >' . esc_attr($val) . '</option>';
			}
		}

		return $string;
	}
}
