<?php

use WPDesk\Xml\Xml_Factory;
use WPDesk\Xml\Integrations\WCI_Ceneo;
use WPDesk\Xml\Integrations\WCI_Nokaut;
use WPDesk\Xml\Integrations\Category_Tree_Interface;

class CeneoAdmin {

	const NONCE_NAME       = 'ceneo_security';
	const NONCE_ACTION     = 'ceneo_save_settings';

	protected $_pluginNamespace = 'woocommerce_ceneo';

	/**
	 * @var Xml_Factory
	 */
	private $xml_factory;

	public function __construct( WooCommerce_Ceneo_Plugin $plugin ) {
		$this->plugin = $plugin;
		$this->xml_factory = new Xml_Factory( $this->plugin );

		WCI_Ceneo::set_path( $this->plugin->plugin_path );
		WCI_Nokaut::set_path( $this->plugin->plugin_path );

		// ceneo settings
		add_action( 'admin_init', array( $this, 'initSettingsAction' ) );
		add_action( 'admin_init', array( $this, 'updateSettingsAction' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'initCssAction' ), 75 );
		add_action( 'admin_enqueue_scripts', array( $this, 'initJsAction' ), 75 );
		add_action( 'admin_head', array( $this, 'initJsAction' ), 75 );
		add_action( 'admin_menu', array( $this, 'initAdminMenuAction' ), 70 );

		add_action( 'wp_ajax_ceneo_category_autocomplete', array( $this, 'ajaxCeneoCategoryAutocomplete' ) );

		add_action( 'woocommerce_product_after_variable_attributes', array(
			$this,
			'woocommerce_product_after_variable_attributes'
		), 10, 3 );

		add_action( 'woocommerce_save_product_variation', array( $this, 'woocommerce_save_product_variation' ), 10, 2 );

		add_action( 'woocommerce_ceneo_clear_generate_progress', array( $this, 'woocommerce_ceneo_clear_generate_progress' ) );

		add_filter( 'woocommerce_screen_ids', array( $this, 'woocommerce_screen_ids' ) );

		add_filter( 'handle_bulk_actions-edit-product', array( $this, 'bulk_action_handler' ), 10, 3 );
		add_filter( 'bulk_actions-edit-product', array( $this, 'register_bulk_actions' ) );
		add_action( 'admin_notices', array( $this, 'bulk_action_admin_notice' ) );

	}

	/**
	 * Woordpress hook. Displays admin notice about selected/deselected ceneo products.
	 */
	public function bulk_action_admin_notice() {
		if ( isset( $_GET['ceneo_bulk_status'] ) && isset( $_GET['ceneo_products'] ) ) {
			$products_count = is_numeric( $_GET['ceneo_products'] )? (int)$_GET['ceneo_products']: 0;
			$string         = ( 'ceneo_select_to_export' === $_GET['ceneo_bulk_status'] ) ? __( 'Dodano %d do', 'woocommerce_ceneo' ) : __( 'Usunięto %d z', 'woocommerce_ceneo' );
			printf( '<div id="ceneo-message" class="updated woocommerce-message"> <p>' .
			        _n( $string.' pliku XML dla porównywarek cen.',
				        $string.' pliku XML dla porównywarek cen.',
				        $products_count,
				        'woocommerce_ceneo'
			        ) . '</p> </div>', $products_count );
		}
	}

	/**
	 * Woordpress hook. Register new options on products overview.
	 *
	 * @param array $bulk_actions in format (see below).
	 * $bulk_actions = [
	 *      (string) $option_value => (string) $option_name,
	 *      ...
	 * ]
	 *
	 * @return array in same format as above.
	 */
	public function register_bulk_actions( array $bulk_actions ) {
		$bulk_actions['ceneo_select_to_export']   = __( 'Dodaj do porównywarek', 'woocommerce_ceneo' );
		$bulk_actions['ceneo_unselect_to_export'] = __( 'Usuń z porównywarek', 'woocommerce_ceneo' );
		return $bulk_actions;
	}


	/**
	 * Woordpress hook. Add flag to product that inform if it should be exported to xml file or not.
	 *
	 * @param string $redirect_to url where user should be redirected to.
	 * @param string $doaction action from selected method.
	 * @param array  $post_ids int[] with products id's.
	 *
	 * @return string with URL where user should be redirected to.
	 */
	public function bulk_action_handler( $redirect_to, $doaction, array $post_ids ) {
		if( 'ceneo_select_to_export' === $doaction || 'ceneo_unselect_to_export' === $doaction ) {
			if( is_array( $post_ids ) && !empty( $post_ids ) ) {
				$value = ( 'ceneo_select_to_export' === $doaction )? '' : '1';
				foreach ( $post_ids as $post_id ) {
					update_post_meta( $post_id, 'woocommerce_ceneo_disabled', $value );
				}

				return add_query_arg( [
					'ceneo_bulk_status' => $doaction,
					'ceneo_products' => count( $post_ids )
				], $redirect_to);
			}
		}
		return $redirect_to;
	}

	function woocommerce_screen_ids( $screen_ids ) {
		$screen_ids[] = 'woocommerce_page_woocommerce_ceneo';

		return $screen_ids;
	}

	public function woocommerce_ceneo_clear_generate_progress() {
		foreach ( WooCommerce_Ceneo_Plugin::get_price_comparison_items() as $key => $val ) {
			$class_name = Xml_Factory::get_generator_class_by_name( $key );
			if ( ! empty( $class_name ) ) {
				$class_name::clear_generate_progress();
			}
		}
	}

	public function woocommerce_save_product_variation( $variation_id, $i ) {
		if ( ! empty( $_POST[ $this->plugin->getNamespace() ] ) && ! empty( $_POST[ $this->plugin->getNamespace() ][ $i ] ) ) {
			update_post_meta( $variation_id, 'woocommerce_ceneo_disabled', '' );

			foreach ( $_POST[ $this->plugin->getNamespace() ][ $i ] as $name => $value ) {
				$name  = sanitize_text_field( $name );
				$value = sanitize_text_field( $value );
				if ( ! get_post_meta( $variation_id, 'woocommerce_ceneo_' . $name ) ) {
					add_post_meta( $variation_id, 'woocommerce_ceneo_' . $name, $value );
				} else {
					update_post_meta( $variation_id, 'woocommerce_ceneo_' . $name, $value );
				}
			}
		}
	}

	public static function get_gender_options(){
		return array(
			__('Żeńska', 'woocommerce_ceneo'),
			__('Męska', 'woocommerce_ceneo')
		);
	}

	public function woocommerce_product_after_variable_attributes( $loop, $variation_data, $variation ) {
		$ceneo = WCI_Ceneo::get_instance();
		$post  = get_post( $variation );
		echo $this->plugin->load_template( 'product_metabox', 'admin', array(
			'post'        => $post,
			'ceneoGroups' => $ceneo->get_ceneo_groups_array(),
			'loop'        => $loop,
			'plugin'      => $this->plugin,
			'view_helper' => new View_Helper(),
			'gender_options' => self::get_gender_options()
		) );
	}

	public function ajaxCeneoCategoryAutocomplete() {
		$result = array();
		$item   = ( isset( $_GET['item'] ) ? sanitize_text_field( $_GET['item'] ) : '' );
		$term   = ( isset( $_GET['term'] ) ? sanitize_text_field( $_GET['term'] ) : '' );

		$category = $this->xml_factory->get_category_mapper( $item );
		if ( ! empty( $term ) && $category instanceof Category_Tree_Interface ){
			$result = $category->get_category_tree_html_options_array( $term );
		}
		echo json_encode( $result );
		exit;
	}

	public function renderProductMetabox( $post ) {
		$ceneo = WCI_Ceneo::get_instance();

		echo $this->plugin->load_template( 'product_metabox', 'admin', array(
			'post'        => $post,
			'ceneoGroups' => $ceneo->get_ceneo_groups_array(),
			'plugin'      => $this->plugin,
			'view_helper' => new View_Helper(),
			'gender_options' => self::get_gender_options()
		) );

	}

	public function saveProductMetabox( $post_id ) {
		//if saving in a custom table, get post_ID

		if ( ! empty( $_POST[ $this->plugin->getNamespace() ] ) ) {
			update_post_meta( $post_id, 'woocommerce_ceneo_disabled', '' );


			foreach ( $_POST[ $this->plugin->getNamespace() ] as $name => $value ) {
				$name  = sanitize_text_field( $name );
				$value = sanitize_text_field( $value );
				if ( ! get_post_meta( $post_id, 'woocommerce_ceneo_' . $name ) ) {
					add_post_meta( $post_id, 'woocommerce_ceneo_' . $name, $value );
				} else {
					update_post_meta( $post_id, 'woocommerce_ceneo_' . $name, $value );
				}
			}
		}

	}

	/**
	 * action
	 */
	public function initMetaBoxes() {
		add_meta_box( 'woocommerce_ceneo', 'Ceneo / Nokaut / Domodi / Homebook', array(
			$this,
			'renderProductMetabox'
		), 'product', 'normal', 'high' );
	}

	public function initSettingsAction() {
		// ceneo fields
		add_action( 'add_meta_boxes', array( $this, 'initMetaBoxes' ) );
		add_action( 'save_post', array( $this, 'saveProductMetabox' ), 10, 1 );

	}

	/**
	 * wordpress action
	 *
	 * should-be-protected method to save/update settings when changed by POST
	 */
	public function updateSettingsAction() {
		if ( isset( $_POST[ self::NONCE_NAME ] ) && wp_verify_nonce( $_POST[ self::NONCE_NAME ], self::NONCE_ACTION ) && current_user_can( 'manage_options' ) ) {
			if ( isset( $_POST['option_page'] ) ) {
				if ( $_POST['option_page'] === 'woocommerce_ceneo_settings' ) {
					update_option( 'woocommerce_ceneo_zero_stock', '' );
					update_option( 'woocommerce_ceneo_cron_schedule', '' );
					update_option( 'woocommerce_ceneo_cron_items', '' );
					update_option( 'woocommerce_ceneo_variants', '' );
					update_option( 'woocommerce_ceneo_show_stock', '' );
					update_option( 'woocommerce_ceneo_disable_the_content_filter', '' );
					flush_rewrite_rules();
				}

				if ( in_array( $_POST['option_page'], array(
					'woocommerce_ceneo_settings',
					'woocommerce_ceneo_categories',
					'woocommerce_ceneo_attributes',
					'woocommerce_ceneo_links'
				) ) ) {
					if ( ! empty( $_POST[ $this->plugin->getNamespace() ] ) ) {
						$map = function( $func, array $arr) {
							array_walk_recursive($arr, function(&$v) use ($func) {
								$v = $func($v);
							});
							return $arr;
						};

						foreach ( $_POST[ $this->plugin->getNamespace() ] as $name => $value ) {
							$name  = sanitize_text_field( $name );
							$value = is_array( $value )? $map('sanitize_text_field', $value) : sanitize_text_field( $value );
							update_option( 'woocommerce_ceneo_' . $name, $value );
						}
					}
					do_action( 'woocommerce_ceneo_clear_generate_progress' );
				}
			}
		}
	}

	/**
	 * wordpress action
	 *
	 * inits css
	 */
	public function initCssAction() {
		$current_screen = get_current_screen();

		if ( in_array( $current_screen->id, array( 'woocommerce_page_woocommerce_ceneo', 'product' ) ) ) {
			wp_enqueue_style( 'ceneo_admin_style', $this->plugin->get_plugin_assets_url() . 'css/admin.css', array(), $this->plugin->scripts_version );
		}
	}

	/**
	 * wordpress action
	 *
	 * inits js
	 */
	public function initJsAction() {
		$wp_scripts     = wp_scripts();
		$current_screen = get_current_screen();

		if ( in_array( $current_screen->id, array( 'woocommerce_page_woocommerce_ceneo', 'product' ) ) ) {
			wp_enqueue_script( 'select2' );
			wp_enqueue_script( 'wc-enhanced-select' );
			wp_enqueue_script( 'woocommerce-ceneo-admin', $this->plugin->get_plugin_assets_url() . 'js/admin.js', array( 'jquery' ), $this->plugin->scripts_version );
			$translation_array = array(
				'server_error' => __( "Wystąpił problem, serwer http zwraca błąd. Spróbuj za kilka minut. Jeśli problem się powtarza skontaktuj się z nami pod adresem \n https://www.wpdesk.pl/support/", 'woocommerce_ceneo' )
			);
			wp_localize_script( 'woocommerce-ceneo-admin', 'ceneo_admin', $translation_array );
		}

	}

	/**
	 * wordpress action
	 *
	 * inits menu
	 */
	public function initAdminMenuAction() {
		$ceneo_page = add_submenu_page( 'woocommerce', __( 'Porównywarki Cen', $this->plugin->get_text_domain() ), __( 'Porównywarki Cen', $this->plugin->get_text_domain() ), 'manage_woocommerce', $this->plugin->getNamespace(), array(
			$this,
			'renderCeneoPage'
		) );
	}

	/**
	 * wordpress action
	 *
	 * renders ceneo submenu page
	 */
	public function renderCeneoPage() {
		$current_tab = ( empty( $_GET['tab'] ) ) ? 'links' : sanitize_text_field( urldecode( $_GET['tab'] ) );


		if ( ! in_array( $current_tab, array( 'settings', 'categories', 'attributes', 'links' ) ) ) {
			die( '!.' );
		} else {
			$product_categories = get_terms( 'product_cat' );

			$xml_urls = array();
			foreach ( WooCommerce_Ceneo_Plugin::get_price_comparison_items() as $key => $val ) {
				$class_name = Xml_Factory::get_generator_class_by_name( $key );
				if ( ! empty( $class_name ) ) {
					$xml_urls[$key] = site_url( $class_name::XML_URL );
				}
			}

			echo $this->plugin->load_template( 'submenu_ceneo', 'admin', array(
					'current_tab'        => $current_tab,
					'product_categories' => $product_categories,
					'avail_options'      => $this->get_avail_options(),
					'schedule_options'   => $this->get_schedule_options(),
					'ceneo'              => WCI_Ceneo::get_instance(),
					'nokaut'             => WCI_Nokaut::get_instance(),
					'xml_urls'           => $xml_urls,
					'plugin'             => $this->plugin,
					'view_helper'        => new View_Helper(),
					'attributes'         => $this->get_attributes_options(),
					'crone_items'        => WooCommerce_Ceneo_Plugin::get_price_comparison_items()
				)
			);
		}
	}


	/**
	 * @return array multidimensional with option groups (see below).
	 *  [
	 *      [
	 *        (string) $option_value => (string) $option_name,
	 *        ...
	 *      ],
	 *      [
	 *        (string) 'optgroup' => (string) $optgroup_label,
	 *        (string) 'children' => [ $option_value => $option_name, ... ]
	 *      ],
	 *      ...
	 *  ]
	 */
	private function get_attributes_options(){

		$attribute_taxonomies_options = array();

		$woocommerce_values = array(
			'_sku' => 'SKU'
		);

		foreach ( wc_get_attribute_taxonomies() as $tax ) {
			$attribute_taxonomies_options[$tax->attribute_id] = $tax->attribute_label;
		}

		return array(
			array( '' => __( 'Wybierz wartość/atrybut' , 'woocommerce_ceneo')),
			array( 'optgroup' => __('Wartości WooCommerce', 'woocommerce_ceneo'), 'children' => $woocommerce_values),
			array( 'optgroup' => __('Atrybuty WooCommerce', 'woocommerce_ceneo'), 'children' => $attribute_taxonomies_options )
		);
	}

	private function get_avail_options() {
		return array(
			''    => __( 'Usuń produkty z listy', 'woocommerce_ceneo' ),
			'1'   => __( 'Poinformuj, że sklep wyśle produkt w ciągu 24 godzin', 'woocommerce_ceneo' ),
			'3'   => __( 'Poinformuj, że sklep wyśle produkt w ciągu 3 dni', 'woocommerce_ceneo' ),
			'7'   => __( 'Poinformuj, że sklep wyśle produkt w ciągu 7 dni', 'woocommerce_ceneo' ),
			'14'  => __( 'Poinformuj, że sklep wyśle produkt w ciągu 14 dni ', 'woocommerce_ceneo' ),
			'90'  => __( 'Poinformuj, że towar jest na zamówienie', 'woocommerce_ceneo' ),
			'99'  => __( 'Poinformuj o braku dostępności', 'woocommerce_ceneo' ),
			'110' => __( 'Poinformuj, że towar dostępny jest w przedsprzedaży', 'woocommerce_ceneo' )
		);
	}

	private function get_schedule_options() {
		return array(
			'hourly'             => __( 'Co godzinę', 'woocommerce_ceneo' ),
			'every_two_hours'    => __( 'Co dwie godziny', 'woocommerce_ceneo' ),
			'every_three_hours'  => __( 'Co trzy godziny', 'woocommerce_ceneo' ),
			'every_six_hours'    => __( 'Co sześć godzin', 'woocommerce_ceneo' ),
			'every_twelve_hours' => __( 'Co dwanaście godzin', 'woocommerce_ceneo' ),
			'daily'              => __( 'Raz dziennie', 'woocommerce_ceneo' ),
			'every_two_days'     => __( 'Raz na 2 dni', 'woocommerce_ceneo' ),
			'every_three_days'   => __( 'Raz na 3 dni', 'woocommerce_ceneo' ),
			'once_a_week'        => __( 'Raz w tygodniu', 'woocommerce_ceneo' )
		);
	}
}
