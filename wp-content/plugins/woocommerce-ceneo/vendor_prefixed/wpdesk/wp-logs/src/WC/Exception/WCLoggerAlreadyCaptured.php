<?php

namespace CeneoVendor\WPDesk\Logger\WC\Exception;

class WCLoggerAlreadyCaptured extends \RuntimeException
{
}
