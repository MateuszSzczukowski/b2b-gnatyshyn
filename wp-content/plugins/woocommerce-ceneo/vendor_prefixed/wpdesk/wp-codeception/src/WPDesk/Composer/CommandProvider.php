<?php

namespace CeneoVendor\WPDesk\Composer\Codeception;

use CeneoVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests;
use CeneoVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests;
use CeneoVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests;
/**
 * Links plugin commands handlers to composer.
 */
class CommandProvider implements \CeneoVendor\Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [new \CeneoVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests(), new \CeneoVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests(), new \CeneoVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests()];
    }
}
