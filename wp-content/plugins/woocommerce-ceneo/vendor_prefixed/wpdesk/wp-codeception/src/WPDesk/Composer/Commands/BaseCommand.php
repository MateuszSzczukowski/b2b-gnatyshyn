<?php

namespace CeneoVendor\WPDesk\Composer\Codeception\Commands;

use CeneoVendor\Composer\Command\BaseCommand as CodeceptionBaseCommand;
use CeneoVendor\Symfony\Component\Console\Output\OutputInterface;
/**
 * Base for commands - declares common methods.
 *
 * @package WPDesk\Composer\Codeception\Commands
 */
abstract class BaseCommand extends \CeneoVendor\Composer\Command\BaseCommand
{
    /**
     * @param string $command
     * @param OutputInterface $output
     */
    protected function execAndOutput($command, \CeneoVendor\Symfony\Component\Console\Output\OutputInterface $output)
    {
        \passthru($command);
    }
}
