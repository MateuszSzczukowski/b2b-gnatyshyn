<?php

namespace CeneoVendor\WPDesk\Composer\Codeception;

use CeneoVendor\Composer\Composer;
use CeneoVendor\Composer\IO\IOInterface;
use CeneoVendor\Composer\Plugin\Capable;
use CeneoVendor\Composer\Plugin\PluginInterface;
/**
 * Composer plugin.
 *
 * @package WPDesk\Composer\Codeception
 */
class Plugin implements \CeneoVendor\Composer\Plugin\PluginInterface, \CeneoVendor\Composer\Plugin\Capable
{
    /**
     * @var Composer
     */
    private $composer;
    /**
     * @var IOInterface
     */
    private $io;
    public function activate(\CeneoVendor\Composer\Composer $composer, \CeneoVendor\Composer\IO\IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }
    public function getCapabilities()
    {
        return [\CeneoVendor\Composer\Plugin\Capability\CommandProvider::class => \CeneoVendor\WPDesk\Composer\Codeception\CommandProvider::class];
    }
}
