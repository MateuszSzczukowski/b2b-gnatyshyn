<?php

namespace CeneoVendor\WPDesk\Helper\Integration;

use CeneoVendor\WPDesk\Helper\Page\SettingsPage;
use CeneoVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use CeneoVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use CeneoVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
/**
 * Integrates WP Desk main settings page with WordPress
 *
 * @package WPDesk\Helper
 */
class SettingsIntegration implements \CeneoVendor\WPDesk\PluginBuilder\Plugin\Hookable, \CeneoVendor\WPDesk\PluginBuilder\Plugin\HookableCollection
{
    use HookableParent;
    /** @var SettingsPage */
    private $settings_page;
    public function __construct(\CeneoVendor\WPDesk\Helper\Page\SettingsPage $settingsPage)
    {
        $this->add_hookable($settingsPage);
    }
    /**
     * @return void
     */
    public function hooks()
    {
        $this->hooks_on_hookable_objects();
    }
}
