<?php

namespace CeneoVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassNotExists extends \RuntimeException
{
}
