<?php

namespace CeneoVendor\WPDesk\PluginBuilder\Storage;

class StorageFactory
{
    /**
     * @return PluginStorage
     */
    public function create_storage()
    {
        return new \CeneoVendor\WPDesk\PluginBuilder\Storage\WordpressFilterStorage();
    }
}
