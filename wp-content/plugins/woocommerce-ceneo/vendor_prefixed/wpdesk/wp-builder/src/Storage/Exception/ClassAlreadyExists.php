<?php

namespace CeneoVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassAlreadyExists extends \RuntimeException
{
}
