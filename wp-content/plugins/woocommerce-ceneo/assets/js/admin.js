jQuery.noConflict();
(function($) {
	$(function () {

		var variations_loaded = false;

        $(document).on('change', '.ceneo_group_selector', function(e) {
            selectGroup( this );
        });

		function selectGroup( element )
		{
			$(element).closest('.inspire-panel').find('.ceneo_group_container, .ceneo_group_container input').hide();
            $(element).closest('.inspire-panel').find('.ceneo_group_container input').prop('disabled', true);

			var groupKey = $(element).val();
			if (groupKey && groupKey.length > 0)
			{
                $(element).closest('.inspire-panel').find('.ceneo_groups_header').show();
                $(element).closest('.inspire-panel').find('div.ceneo_group_container.' + groupKey + '_group, div.ceneo_group_container.' + groupKey + '_group input').show();
                $(element).closest('.inspire-panel').find('div.ceneo_group_container.' + groupKey + '_group input').prop('disabled', false);
			} else {
                $(element).closest('.inspire-panel').find('.ceneo_groups_header').hide();
			}

		}

		function init_ui( $button ){
			let $item = $button.closest('.woocommerce_ceneo_item');
			$item.addClass('active init');
			$( '.generate_button' ).prop('disabled', true);
		}

		function update_ui( $button, data ){
			let $item = $button.closest('.woocommerce_ceneo_item');
			if( $item.hasClass('init') ){
				$item.removeClass('init');
				$item.addClass('progress');
			}
			if(data.status === "finished"){
				$item.removeClass('active progress');
				$( '.generate_button' ).prop('disabled', false);
			}else{
				let $progress = $item.find('.description_button_text .progress');
				$progress.find('.exported_products').html(data.exported_products);
				$progress.find('.all_product_count').html(data.all_product_count);
			}

		}

		function cronProcess( $button, init ){
			if( true === init ){
				init_ui( $button );
			}
			$.ajax({
				type: "GET",
				cache: false,
				url: ajaxurl,
				data: {
					'action': $button.val(),
					'init'  : init
				},
				success: function (data) {
					update_ui( $button, data );
					if( data.status === "process" ){
						cronProcess( $button, false );
					} else if( data.status === "wait" ) {
						setTimeout( function(){cronProcess( $button, false );}, 10000);
					}
				},
				error: function(){
					alert( ceneo_admin.server_error );
				}
			});
		}

		$(document).ready(function(){
        	$('.ceneo_group_selector').each(function() {
        		$(this).change();
			});
		})

		$("#woocommerce-product-data").on( "woocommerce_variations_loaded", function (event) {
				if (variations_loaded === false) {
					$(".ceneo_group_selector").each(function () {
						$(this).change();
					});
					jQuery(document).find(".woocommerce_variations .variation-needs-update").removeClass("variation-needs-update");
					variations_loaded = true;
				}
			}
		);

        jQuery('select.ceneo_category_autocomplete_select2').select2({
            minimumInputLength: 3,
            ajax: {
                method: 		'GET',
                url: 			ajaxurl,
                dataType: 		'json',
                data: function ( params ) {
          			return {
                		term:       params.term, // search query
                        action: 	'ceneo_category_autocomplete',
						item: $( this ).attr('data-item'),
            		};
        		},
                processResults: function( data ) {
                    var options = [];
                    if ( data ) {
                        // data is the array of arrays, and each of them contains ID and the Label of the option
                        jQuery.each( data, function( index, text ) { // do not forget that "index" is just auto incremented value
                            options.push( { id: text.value, text: text.text  } );
                        });

                    }
                    return {
                        results: options
                    };
                },
                cache: true
            }
        });

		jQuery('.select2-ceneo-settings').select2({
			language: {
				noResults: function (params) {
					return "Brak rezultatów.";
				}
			}
		});

		jQuery('.generate_button').on('click', function(e){
			e.preventDefault();
			cronProcess( $(this), true );
		});

		jQuery(document).on('click', '.woocommerce_ceneo_ajax_menu a', function(e){
			e.preventDefault();
			let $containerId = $( this ).attr('href');

			$( '.woocommerce_ceneo_container' ).removeClass('active');
			$( '.woocommerce_ceneo_ajax_menu a' ).removeClass('active');
			$( this ).addClass('active');
			$( $containerId ).addClass('active');
		});

	});
})(jQuery);
