# Copyright (C) 2021 WP Desk
# This file is distributed under the same license as the WooCommerce Porównywarki Cen XML plugin.
msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Porównywarki Cen XML 4.0.12\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce-ceneo\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-03-10T12:39:37+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: woocommerce_ceneo\n"

#. Plugin Name of the plugin
msgid "WooCommerce Porównywarki Cen XML"
msgstr ""

#. Plugin URI of the plugin
msgid "https://wpde.sk/ceneo"
msgstr ""

#. Description of the plugin
msgid "Integracja WooCommerce z Ceneo/Nokaut/Domodi/Homebook."
msgstr ""

#. Author of the plugin
msgid "WP Desk"
msgstr ""

#. Author URI of the plugin
msgid "https://www.wpdesk.pl/"
msgstr ""

#: class/ceneo_admin.php:60
msgid "Dodano %d do"
msgstr ""

#: class/ceneo_admin.php:60
msgid "Usunięto %d z"
msgstr ""

#: class/ceneo_admin.php:82
msgid "Dodaj do porównywarek"
msgstr ""

#: class/ceneo_admin.php:83
msgid "Usuń z porównywarek"
msgstr ""

#: class/ceneo_admin.php:147
msgid "Żeńska"
msgstr ""

#: class/ceneo_admin.php:148
msgid "Męska"
msgstr ""

#: class/ceneo_admin.php:299
msgid ""
"Wystąpił problem, serwer http zwraca błąd. Spróbuj za kilka minut. Jeśli problem się powtarza skontaktuj się z nami pod adresem \n"
" https://www.wpdesk.pl/support/"
msgstr ""

#: class/ceneo_admin.php:385
msgid "Wybierz wartość/atrybut"
msgstr ""

#: class/ceneo_admin.php:386
msgid "Wartości WooCommerce"
msgstr ""

#: class/ceneo_admin.php:387
msgid "Atrybuty WooCommerce"
msgstr ""

#: class/ceneo_admin.php:393
msgid "Usuń produkty z listy"
msgstr ""

#: class/ceneo_admin.php:394
msgid "Poinformuj, że sklep wyśle produkt w ciągu 24 godzin"
msgstr ""

#: class/ceneo_admin.php:395
msgid "Poinformuj, że sklep wyśle produkt w ciągu 3 dni"
msgstr ""

#: class/ceneo_admin.php:396
msgid "Poinformuj, że sklep wyśle produkt w ciągu 7 dni"
msgstr ""

#: class/ceneo_admin.php:397
msgid "Poinformuj, że sklep wyśle produkt w ciągu 14 dni "
msgstr ""

#: class/ceneo_admin.php:398
msgid "Poinformuj, że towar jest na zamówienie"
msgstr ""

#: class/ceneo_admin.php:399
msgid "Poinformuj o braku dostępności"
msgstr ""

#: class/ceneo_admin.php:400
msgid "Poinformuj, że towar dostępny jest w przedsprzedaży"
msgstr ""

#: class/ceneo_admin.php:406
msgid "Co godzinę"
msgstr ""

#: class/ceneo_admin.php:407
msgid "Co dwie godziny"
msgstr ""

#: class/ceneo_admin.php:408
msgid "Co trzy godziny"
msgstr ""

#: class/ceneo_admin.php:409
msgid "Co sześć godzin"
msgstr ""

#: class/ceneo_admin.php:410
msgid "Co dwanaście godzin"
msgstr ""

#: class/ceneo_admin.php:411
msgid "Raz dziennie"
msgstr ""

#: class/ceneo_admin.php:412
msgid "Raz na 2 dni"
msgstr ""

#: class/ceneo_admin.php:413
msgid "Raz na 3 dni"
msgstr ""

#: class/ceneo_admin.php:414
msgid "Raz w tygodniu"
msgstr ""

#: class/class-plugin.php:79
#: templates/admin/product_metabox.php:13
#: templates/admin/product_metabox.php:84
msgid "Ceneo"
msgstr ""

#: class/class-plugin.php:80
#: templates/admin/product_metabox.php:85
msgid "Nokaut"
msgstr ""

#: class/class-plugin.php:81
#: templates/admin/product_metabox.php:86
msgid "Domodi"
msgstr ""

#: class/class-plugin.php:82
msgid "Homebook"
msgstr ""

#: class/class-plugin.php:114
#: templates/admin/submenu_ceneo.php:6
#: templates/admin/submenu_ceneo_settings.php:10
msgid "Ustawienia"
msgstr ""

#: class/class-plugin.php:115
msgid "Docs"
msgstr ""

#: class/class-plugin.php:116
msgid "Support"
msgstr ""

#: templates/admin/product_metabox.php:4
msgid "Wyklucz produkt z pliku XML"
msgstr ""

#: templates/admin/product_metabox.php:9
msgid "Wyklucz wariant z pliku XML"
msgstr ""

#: templates/admin/product_metabox.php:29
msgid "Producent"
msgstr ""

#: templates/admin/product_metabox.php:40
#: templates/admin/submenu_ceneo_attributes.php:63
msgid "EAN"
msgstr ""

#: templates/admin/product_metabox.php:51
#: templates/admin/submenu_ceneo_attributes.php:79
msgid "ISBN"
msgstr ""

#: templates/admin/product_metabox.php:62
msgid "Nazwa produktu"
msgstr ""

#: templates/admin/product_metabox.php:73
msgid "Opis produktu"
msgstr ""

#: templates/admin/product_metabox.php:175
msgid "Płeć"
msgstr ""

#: templates/admin/product_metabox.php:194
msgid "Rozmiar"
msgstr ""

#: templates/admin/submenu_ceneo.php:5
msgid "Linki"
msgstr ""

#: templates/admin/submenu_ceneo.php:7
msgid "Mapowanie kategorii"
msgstr ""

#: templates/admin/submenu_ceneo.php:8
#: templates/admin/submenu_ceneo_attributes.php:9
msgid "Mapowanie atrybutów"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:6
#: templates/admin/submenu_ceneo_categories.php:17
#: templates/admin/submenu_ceneo_settings.php:7
msgid "Ustawienia zostały zapisane."
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:11
msgid "Przypisz atrybuty WooCommerce do odpowiednich atrybutów Ceneo."
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:20
msgid "Atrybuty integracyjne"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:30
msgid "Nazwa producenta"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:47
msgid "Kod producenta"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:95
msgid "OTC"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:119
msgid "Atrybuty dla mapowanych kategorii"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:123
#: templates/admin/submenu_ceneo_attributes.php:180
msgid "Kategoria sklepu"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:143
msgid "Nazwa atrybutu Ceneo"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:144
msgid "Nazwa atrybutu WooCommerce"
msgstr ""

#: templates/admin/submenu_ceneo_attributes.php:187
#: templates/admin/submenu_ceneo_categories.php:74
#: templates/admin/submenu_ceneo_settings.php:96
msgid "Zapisz zmiany"
msgstr ""

#: templates/admin/submenu_ceneo_categories.php:20
msgid "Mapowanie kategorii Ceneo"
msgstr ""

#: templates/admin/submenu_ceneo_categories.php:21
msgid "Przypisz kategorie WooCommerce do odpowiednich kategorii Ceneo."
msgstr ""

#: templates/admin/submenu_ceneo_categories.php:34
#: templates/admin/submenu_ceneo_categories.php:61
msgid "Wybierz kategorię"
msgstr ""

#: templates/admin/submenu_ceneo_categories.php:47
msgid "Mapowanie kategorii Nokaut"
msgstr ""

#: templates/admin/submenu_ceneo_categories.php:48
msgid "Przypisz kategorie WooCommerce do odpowiednich kategorii Nokaut."
msgstr ""

#: templates/admin/submenu_ceneo_links.php:1
msgid "Linki do plików XML"
msgstr ""

#: templates/admin/submenu_ceneo_links.php:3
#: templates/admin/submenu_ceneo_settings.php:12
msgid "Instrukcja instalacji i konfiguracji wtyczki &rarr;"
msgstr ""

#: templates/admin/submenu_ceneo_links.php:12
msgid "Link do pliku "
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:18
msgid "Status produktów niedostępnych"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:37
msgid "Częstotliwość aktualizacji pliku"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:56
msgid "Aktualizuj automatycznie pliki XML"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:76
msgid "Warianty produktów"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:85
msgid "Filtr the_content"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:89
msgid "Wyłącz filtr the_content dla opisów produktów"
msgstr ""

#: templates/admin/submenu_ceneo_settings.php:90
msgid "Wyłącz filtr <b>the_content</b> jeżeli w opisie produtów nie są używane shortcody i nie są włączone wtyczki modyfikujące opisy. Wyłączenie przyspiesza generowanie pliku XML."
msgstr ""
