<?php
/*
	Plugin Name: WooCommerce Porównywarki Cen XML
	Plugin URI: https://wpde.sk/ceneo
	Description: Integracja WooCommerce z Ceneo/Nokaut/Domodi/Homebook.
	Version: 4.0.12
	Author: WP Desk
	Author URI: https://www.wpdesk.pl/
	Text Domain: woocommerce_ceneo
	Domain Path: /lang/
	Requires at least: 5.2
    Tested up to: 5.7
    WC requires at least: 4.7
    WC tested up to: 5.2
    Requires PHP: 7.0


	Copyright 2019 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


$plugin_version           = '4.0.12';
$plugin_release_timestamp = '2021-03-05 15:32';
define( 'WOOCOMMERCE_CENEO_VERSION', $plugin_version );

$plugin_name        = 'WooCommerce Porównywarki Cen XML';
$product_id         = 'WooCommerce Ceneo';
$plugin_class_name  = 'WooCommerce_Ceneo_Plugin';
$plugin_text_domain = 'woocomerce-ceneo';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

$requirements = array(
	'php'     => '5.6',
	'wp'      => '4.5',
	'plugins' => array(
		array(
			'name'      => 'woocommerce/woocommerce.php',
			'nice_name' => 'WooCommerce',
		),
	),
);

require __DIR__ . '/inc/wpdesk-woo27-functions.php';
require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';
