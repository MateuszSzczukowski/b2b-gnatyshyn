<form action="" method="post">
	<?php settings_fields( 'woocommerce_ceneo_settings' ); ?>
	<?php wp_nonce_field( CeneoAdmin::NONCE_ACTION, CeneoAdmin::NONCE_NAME ); ?>


 	<?php if (isset($_POST['option_page']) && $_POST['option_page'] === 'woocommerce_ceneo_settings'): ?>
		<div id="message" class="updated fade"><p><strong><?php _e( 'Ustawienia zostały zapisane.', 'woocommerce_ceneo' ); ?></strong></p></div>
	<?php endif; ?>

	<h3><?php _e( 'Ustawienia', 'woocommerce_ceneo' ); ?></h3>

	<p><a href="https://wpde.sk/ceneo-docs" target="_blank"><?php _e( 'Instrukcja instalacji i konfiguracji wtyczki &rarr;', 'woocommerce_ceneo' ); ?></a></p>

	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="<?php $plugin->getNamespace().'_zero_stock'; ?>"><?php _e( 'Status produktów niedostępnych', 'woocommerce_ceneo' ); ?></label>
				</th>

				<td class="forminp forminp-text">
				<?php
					$params = array(
					'id'            => $plugin->getNamespace().'_zero_stock',
					'name'          => $plugin->getNamespace().'[zero_stock]',
					'class'			=> 'select2-ceneo-settings',
					'options'       => $avail_options,
					'label'			=> '',
					'value'			=> !empty( $this->getSettingValue('zero_stock') )? $this->getSettingValue('zero_stock'): '',
					);
					echo $view_helper->create_select_field( $params );
				?>
				</td>
			</tr>
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="<?php $plugin->getNamespace().'_cron_schedule'; ?>"><?php _e( 'Częstotliwość aktualizacji pliku', 'woocommerce_ceneo' ); ?></label>
				</th>

				<td class="forminp forminp-text">
					<?php
					$params = array(
						'id'      => $plugin->getNamespace() . '_cron_schedule',
						'name'    => $plugin->getNamespace() . '[cron_schedule]',
						'class'   => 'select2-ceneo-settings',
						'options' => $schedule_options,
						'label'   => '',
						'value'   => $this->getSettingValue( 'cron_schedule', 'hourly' ),
					);
					echo $view_helper->create_select_field( $params );
					?>
				</td>
			</tr>
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="<?php $plugin->getNamespace().'_cron_items'; ?>"><?php _e( 'Aktualizuj automatycznie pliki XML', 'woocommerce_ceneo' ); ?></label>
				</th>

				<td class="forminp forminp-text">
					<?php
					$params = array(
						'id'       => $plugin->getNamespace() . '_cron_items',
						'name'     => $plugin->getNamespace() . '[cron_items][]',
						'class'    => 'select2-ceneo-settings',
						'options'  => $crone_items,
						'label'    => '',
						'value'    => $this->getSettingValue( 'cron_items',  array_keys($crone_items)),
						'multiple' => true
					);
					echo $view_helper->create_select_field( $params, true );
					?>
				</td>
			</tr>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="woocommerce_ceneo_variants"><?php _e( 'Warianty produktów', 'woocommerce_ceneo' ); ?></label>
                </th>

                <td class="forminp forminp-text">
                    <label for="woocommerce_ceneo_variants"><input class="checkbox" <?php if ($this->getSettingValue('variants') != ''): ?>checked="checked"<?php endif; ?> id="woocommerce_ceneo_variants" name="woocommerce_ceneo[variants]" type="checkbox" /> Włącz wystawianie wariantów jako oddzielnych produktów na Ceneo</label>
                </td>
            </tr>
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ceneo_disable_the_content_filter"><?php _e( 'Filtr the_content', 'woocommerce_ceneo' ); ?></label>
				</th>

				<td class="forminp forminp-text">
					<label for="woocommerce_ceneo_disable_the_content_filter"><input class="checkbox" <?php if ($this->getSettingValue('disable_the_content_filter') != ''): ?>checked="checked"<?php endif; ?> id="woocommerce_ceneo_disable_the_content_filter" name="woocommerce_ceneo[disable_the_content_filter]" type="checkbox" /> <?php _e( 'Wyłącz filtr the_content dla opisów produktów', 'woocommerce_ceneo' ); ?></label>
					<p class="description"><?php _e( 'Wyłącz filtr <b>the_content</b> jeżeli w opisie produtów nie są używane shortcody i nie są włączone wtyczki modyfikujące opisy. Wyłączenie przyspiesza generowanie pliku XML.', 'woocommerce_ceneo' ); ?></p>
				</td>
			</tr>
		</tbody>
	</table>

	<p class="submit"><input type="submit" value="<?php _e( 'Zapisz zmiany', 'woocommerce_ceneo' ); ?>" class="button button-primary" id="submit" name="submitForm"></p>
</form>
