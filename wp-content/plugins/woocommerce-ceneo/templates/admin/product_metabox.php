<?php
use WPDesk\Woocommerce_Xml_Mapping\Accessors\Custom_Product_Attributes_Accessor;

$exclude_label = __( 'Wyklucz produkt z pliku XML', 'woocommerce_ceneo' ); ?>
<?php $loop_add_name = $loop_id = ''; ?>
<?php if ( isset( $args['loop'] ) ) : ?>
	<?php $loop_id = $args['loop']; ?>
	<?php $loop_add_name = '[' . $loop_id . ']'; ?>
	<?php $exclude_label = __( 'Wyklucz wariant z pliku XML', 'woocommerce_ceneo' ); ?>
<?php endif; ?>
<div class="inspire-panel">
	<?php if ( isset( $args['loop'] ) ) : ?>
        <h3 class="ceneo-variant"><?php _e( 'Ceneo', 'woocommerce_ceneo' ); ?></h3>
	<?php endif; ?>
	<div class="options-group">
		<p class="lonely">
			<?php
			$attr_disabled = get_post_meta($args['post']->ID, "woocommerce_ceneo_disabled", true);
			$attr_disabled = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_disabled", $attr_disabled, $args['post']->ID );
			?>
	    	<label><input type="checkbox" class="checkbox" value="1" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[disabled]" <?php if ($attr_disabled == 1): ?>checked="checked"<?php endif; ?>/> <?php echo $exclude_label; ?></label>
		</p>
	</div>

	<div class="options-group no-border-bottom">

		<p class="form-field">
			<label>
				<?php _e('Producent', 'woocommerce_ceneo'); ?>
			</label>
			<?php
			$attr_producent = get_post_meta($args['post']->ID, "woocommerce_ceneo_Producent", true);
			$attr_producent = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_Producent", $attr_producent, $args['post']->ID );
			?>
			<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[Producent]" value="<?php echo esc_attr($attr_producent); ?>"/>
		</p>

		<p class="form-field">
			<label>
				<?php _e('EAN', 'woocommerce_ceneo'); ?>
			</label>
			<?php
			$attr_ean = get_post_meta($args['post']->ID, "woocommerce_ceneo_EAN", true);
			$attr_ean = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_EAN", $attr_ean, $args['post']->ID );
			?>
			<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[EAN]" value="<?php echo esc_attr($attr_ean); ?>"/>
		</p>

		<p class="form-field">
			<label>
				<?php _e('ISBN', 'woocommerce_ceneo'); ?>
			</label>
			<?php
			$attr_isbn = get_post_meta($args['post']->ID, "woocommerce_ceneo_ISBN", true);
			$attr_isbn = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_ISBN", $attr_isbn, $args['post']->ID );
			?>
			<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[ISBN]" value="<?php echo esc_attr($attr_isbn); ?>"/>
		</p>

		<p class="form-field">
		    <label>
		        <?php _e('Nazwa produktu', 'woocommerce_ceneo'); ?>
		    </label>
			<?php
			$attr_alternative_name = get_post_meta($args['post']->ID, "woocommerce_ceneo_alternative_name", true);
			$attr_alternative_name = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_alternative_name", $attr_alternative_name, $args['post']->ID );
			?>
		    <input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[alternative_name]" value="<?php echo esc_attr($attr_alternative_name); ?>"/>
		</p>

		<p class="form-field">
		    <label>
		        <?php _e('Opis produktu', 'woocommerce_ceneo'); ?>
		    </label>
			<?php
			$attr_alternative_desc = get_post_meta($args['post']->ID, "woocommerce_ceneo_alternative_desc", true);
			$attr_alternative_desc = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_alternative_desc", $attr_alternative_desc, $args['post']->ID );
			?>
		    <textarea name="woocommerce_ceneo<?php echo $loop_add_name; ?>[alternative_desc]"><?php echo esc_attr($attr_alternative_desc); ?></textarea>
		</p>
	</div>

	<ul class="woocommerce_ceneo_ajax_menu woocommerce_ceneo_submenu_metabox">
		<li> <a href="#container-metabox-ceneo<?php echo $loop_id; ?>" class="active"><?php _e( 'Ceneo', 'woocommerce_ceneo' ); ?></a> </li><li>
			<a href="#container-metabox-nokaut<?php echo $loop_id; ?>"><?php _e( 'Nokaut', 'woocommerce_ceneo' ); ?></a> </li><li>
			<a href="#container-metabox-domodi<?php echo $loop_id; ?>"><?php _e( 'Domodi', 'woocommerce_ceneo' ); ?></a> </li>
	</ul>

	<div class="woocommerce_ceneo_container active" id="container-metabox-ceneo<?php echo $loop_id; ?>">
	<div class="options-group">
		<p class="form-field">
		    <label><?php echo _('Grupa produktów Ceneo'); ?></label>
		    <select name="woocommerce_ceneo<?php echo $loop_add_name; ?>[ceneo_group]" class="ceneo_group_selector">
		        <?php $ceneo_group = get_post_meta($args['post']->ID, 'woocommerce_ceneo_ceneo_group', true); ?>

		        <option value=""><?php echo _('Wybierz'); ?></option>
		        <?php foreach ($args['ceneoGroups'] as $key => $value): ?>
		            <option value="<?php echo $key; ?>" <?php if ($ceneo_group == $key): ?>selected="selected"<?php endif; ?>><?php echo $value['name']; ?></option>
		        <?php endforeach; ?>
		    </select>
		</p>

		<div class="hard-candy">
		    <p class="ceneo_groups_header"><strong><?php echo _('Dodatkowe pola produktów:'); ?></strong></p>

		    <div class="ceneo_dynamic_fields">
		        <?php foreach ($args['ceneoGroups'] as $group_key => $group): ?>
		            <div class="<?php echo esc_attr($group_key) ?>_group ceneo_group_container" style="display: none">
						<?php foreach ($group['fields'] as $key => $field): ?>
						    <div class="form-field">
								<label for=""><?php echo esc_attr($field['title']); ?></label>

								<fieldset><legend class="screen-reader-text"><span><?php echo esc_attr($field['title']); ?></span></legend>
								    <input type="text" placeholder="" value="<?php echo esc_attr(get_post_meta($args['post']->ID, 'woocommerce_ceneo_' . esc_attr($key), true)); ?>" style="" id="" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[<?php echo esc_attr($key); ?>]" class="input-text regular-input ">
								    <?php if (!empty($field['description'])): ?>
								        <p class="description"><?php echo esc_attr($field['description']); ?></p>
								    <?php endif; ?>
								</fieldset>
						    </div>
						<?php endforeach; ?>
		            </div>
		        <?php endforeach; ?>
		    </div>
		</div>
	</div>
	</div>
	<div class="woocommerce_ceneo_container" id="container-metabox-nokaut<?php echo $loop_id; ?>">
		<div class="options-group">
			<p class="form-field">
				<label>
					<?php echo _('Informacja o gwarancji'); ?>
				</label>
				<?php
				$attr_warranty = get_post_meta($args['post']->ID, "woocommerce_ceneo_warranty", true);
				$attr_warranty = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_warranty", $attr_warranty, $args['post']->ID );
				?>
				<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[warranty]" value="<?php echo esc_attr($attr_warranty); ?>"/>
			</p>
			<p class="form-field">
				<label>
					<?php echo _('MPN'); ?>
				</label>
				<?php
				$attr_mpn = get_post_meta($args['post']->ID, "woocommerce_ceneo_MPN", true);
				$attr_mpn = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_MPN", $attr_mpn, $args['post']->ID );
				?>
				<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[MPN]" value="<?php echo esc_attr($attr_mpn); ?>"/>
			</p>
			<p class="form-field">
				<label>
					<?php echo _('BDK'); ?>
				</label>
				<?php
				$attr_bdk = get_post_meta($args['post']->ID, "woocommerce_ceneo_BDK", true);
				$attr_bdk = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_BDK", $attr_bdk, $args['post']->ID );
				?>
				<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[BDK]" value="<?php echo esc_attr($attr_bdk); ?>"/>
			</p>
			<p class="form-field">
				<label>
					<?php echo _('OSDW'); ?>
				</label>
				<?php
				$attr_osdw = get_post_meta($args['post']->ID, "woocommerce_ceneo_OSDW", true);
				$attr_osdw = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_OSDW", $attr_bdk, $args['post']->ID );
				?>
				<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[OSDW]" value="<?php echo esc_attr($attr_osdw); ?>"/>
			</p>
		</div>
	</div>
	<div class="woocommerce_ceneo_container" id="container-metabox-domodi<?php echo $loop_id; ?>">
		<div class="options-group">
			<p class="form-field">
				<label>
					<?php _e('Płeć', 'woocommerce_ceneo'); ?>
				</label>
				<?php
				$attr_gender = get_post_meta($args['post']->ID, "woocommerce_ceneo_gender", true);
				$attr_gender = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_gender", $attr_gender, $args['post']->ID );
				?>
				<?php
				$params = array(
					'id'      => 'woocommerce_ceneo_' . $loop_id . '_gender',
					'name'    => 'woocommerce_ceneo' . $loop_add_name . '[gender]',
					'options' => array_combine( $gender_options, $gender_options ),
					'label'   => '',
					'value'   => esc_attr( $attr_gender ),
				);
				echo $view_helper->create_select_field( $params );
				?>
			</p>
			<p class="form-field">
				<label>
					<?php _e('Rozmiar', 'woocommerce_ceneo'); ?>
				</label>
				<?php
				$attr_sizes = get_post_meta($args['post']->ID, "woocommerce_ceneo_sizes", true);
				$attr_sizes = apply_filters( Custom_Product_Attributes_Accessor::WPDESK_FILTER_PRODUCT_META_PREFIX."woocommerce_ceneo_sizes", $attr_sizes, $args['post']->ID );
				?>
				<input type="text" name="woocommerce_ceneo<?php echo $loop_add_name; ?>[sizes]" value="<?php echo esc_attr($attr_sizes); ?>"/>
			</p>
		</div>
	</div>
</div>
