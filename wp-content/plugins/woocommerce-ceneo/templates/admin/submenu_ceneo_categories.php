<?php

$ceneoCategoriesOptions = $ceneo->get_ceneo_category_tree_html_options_array();
$nokautCategoriesOptions = $nokaut->get_nokaut_category_tree_html_options_array();


?>
<form action="" method="post">
	<?php settings_fields( 'woocommerce_ceneo_categories' ); ?>
	<?php wp_nonce_field( CeneoAdmin::NONCE_ACTION, CeneoAdmin::NONCE_NAME ); ?>

	<ul class="woocommerce_ceneo_ajax_menu woocommerce_ceneo_submenu">
		<li> <a href="#container-ceneo" class="active">Ceneo</a> </li>
		<li> <a href="#container-nokaut" >Nokaut</a> </li>
	</ul>
 	<?php if (isset($_POST['option_page']) && $_POST['option_page'] === 'woocommerce_ceneo_categories'): ?>
		<div id="message" class="updated fade"><p><strong><?php _e( 'Ustawienia zostały zapisane.', 'woocommerce_ceneo' ); ?></strong></p></div>
	<?php endif; ?>
	<div id="container-ceneo" class="woocommerce_ceneo_container active">
		<h3><?php _e( 'Mapowanie kategorii Ceneo', 'woocommerce_ceneo' ); ?></h3>
		<p><?php _e( 'Przypisz kategorie WooCommerce do odpowiednich kategorii Ceneo.', 'woocommerce_ceneo' ); ?></p>

	<table class="form-table">
		<tbody>
		<?php foreach ( $product_categories as $category ): ?>
				<?php $value = $plugin->getSettingValue('term_' . $category->term_id); ?>
				<tr valign="top">
					<th class="titledesc" scope="row"><?php echo $category->name; ?></th>
					<td class="forminp forminp-text">
						<select name="woocommerce_ceneo[term_<?php echo $category->term_id; ?>]"
						        id="woocommerce_category_<?php echo $category->slug; ?>"
						        class="ceneo_category_autocomplete_select2"
						        data-item="ceneo"
						        data-placeholder="<?php _e( 'Wybierz kategorię', 'woocommerce_ceneo' ); ?>">
							<?php if ( ! empty( $value ) ): ?>
								<option selected="selected" value="<?php echo esc_attr($value); ?>"><?php echo esc_attr($ceneoCategoriesOptions[ $value ]); ?></option>
							<?php endif; ?>
							<option value="">Wybierz kategorię...</option>
						</select>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	</div>
	<div id="container-nokaut" class="woocommerce_ceneo_container">
		<h3><?php _e( 'Mapowanie kategorii Nokaut', 'woocommerce_ceneo' ); ?></h3>
		<p><?php _e( 'Przypisz kategorie WooCommerce do odpowiednich kategorii Nokaut.', 'woocommerce_ceneo' ); ?></p>

		<table class="form-table">
			<tbody>
			<?php foreach ( $product_categories as $category ): ?>
				<?php $value = $plugin->getSettingValue('term_nokaut_' . $category->term_id); ?>
				<tr valign="top">
					<th class="titledesc" scope="row"><?php echo $category->name; ?></th>
					<td class="forminp forminp-text">
						<select name="woocommerce_ceneo[term_nokaut_<?php echo $category->term_id; ?>]"
						        id="woocommerce_category_nokaut_<?php echo $category->slug; ?>"
						        class="ceneo_category_autocomplete_select2"
						        data-item="nokaut"
						        data-placeholder="<?php _e( 'Wybierz kategorię', 'woocommerce_ceneo' ); ?>">
							<?php if ( ! empty( $value ) ): ?>
								<option selected="selected" value="<?php echo esc_attr($value); ?>"><?php echo esc_attr($nokautCategoriesOptions[ $value ]); ?></option>
							<?php endif; ?>
							<option value="">Wybierz kategorię...</option>
						</select>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<p class="submit"><input type="submit" value="<?php _e( 'Zapisz zmiany', 'woocommerce_ceneo' ); ?>" class="button button-primary" id="submit" name="submitForm"></p>
</form>
