<div class="wrap woocommerce woocommerce-ceneo">
	<div id="icon-options-general" class="icon32"><br /></div>

	<h2 class="nav-tab-wrapper">
		<a class="nav-tab <?php if ($args['current_tab'] === 'links'): ?>nav-tab-active<?php endif; ?>" href="<?php echo admin_url( 'admin.php?page=woocommerce_ceneo&tab=links' ); ?>"><?php echo __( 'Linki', 'woocommerce_ceneo' ); ?></a>
		<a class="nav-tab <?php if ($args['current_tab'] === 'settings'): ?>nav-tab-active<?php endif; ?>" href="<?php echo admin_url( 'admin.php?page=woocommerce_ceneo&tab=settings' ); ?>"><?php echo __( 'Ustawienia', 'woocommerce_ceneo' ); ?></a>
		<a class="nav-tab <?php if ($args['current_tab'] === 'categories'): ?>nav-tab-active<?php endif; ?>" href="<?php echo admin_url( 'admin.php?page=woocommerce_ceneo&tab=categories' ); ?>"><?php echo __( 'Mapowanie kategorii', 'woocommerce_ceneo' ); ?></a>
		<a class="nav-tab <?php if ($args['current_tab'] === 'attributes'): ?>nav-tab-active<?php endif; ?>" href="<?php echo admin_url( 'admin.php?page=woocommerce_ceneo&tab=attributes' ); ?>"><?php echo __( 'Mapowanie atrybutów', 'woocommerce_ceneo' ); ?></a>
	</h2>

	<?php echo $args['plugin']->load_template('submenu_ceneo_' . $args['current_tab'], 'admin', $args ); ?>
</div>
