	<h3><?php _e( 'Linki do plików XML', 'woocommerce_ceneo' ); ?></h3>

	<p><a href="https://wpde.sk/ceneo-docs" target="_blank"><?php _e( 'Instrukcja instalacji i konfiguracji wtyczki &rarr;', 'woocommerce_ceneo' ); ?></a></p>

	<table class="form-table">
		<tbody>
		<?php
		foreach( $crone_items as $key => $val ): ?>
			<?php if ( isset( $xml_urls[ $key ] ) ): ?>
			<tr valign="top" id="generate_<?php echo $key; ?>" style="width: 220px" class="woocommerce_ceneo_item">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ceneo_xml_link"><?php _e( 'Link do pliku '.$val, 'woocommerce_ceneo' ); ?></label>
				</th>
				<td class="forminp forminp-text">
					<input style="width:100%;" class="regular-text" value="<?php echo $xml_urls[$key]; ?>" id="woocommerce_<?php echo $key; ?>_xml_link" name="" type="text" readonly="readonly">
					<p class="description"><?php _e( 'Aby wymusić aktualizację kliknij <b>Generuj plik</b>.'); ?></p>
				</td>
				<td class="forminp forminp-text" style="width: 200px">
					<button type="button" class="generate_button" value="process_<?php echo $key; ?>_xml"> Generuj plik </button>
					<p class="description_button"><span class="description_button_wrapper"><img src="<?php echo esc_url( get_admin_url() . 'images/loading.gif' ); ?>"> <span class="description_button_text"><span class="init"><?php _e('Trwa przygotowywanie do eksportu', 'woocommerce-ceneo') ?></span> <span class="progress"><?php _e('<b>Postęp</b> <span class="exported_products"></span> na <span class="all_product_count"></span> produktów', 'woocommerce-ceneo'); ?></span></span></span> &nbsp; </p>
				</td>
			</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		</tbody>
	</table>
