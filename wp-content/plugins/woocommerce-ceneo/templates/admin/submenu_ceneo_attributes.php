<form action="" method="post">
	<?php settings_fields( 'woocommerce_ceneo_attributes' ); ?>
	<?php wp_nonce_field( CeneoAdmin::NONCE_ACTION, CeneoAdmin::NONCE_NAME ); ?>

 	<?php if (isset($_POST['option_page']) && $_POST['option_page'] === 'woocommerce_ceneo_attributes'): ?>
		<div id="message" class="updated fade"><p><strong><?php _e( 'Ustawienia zostały zapisane.', 'woocommerce_ceneo' ); ?></strong></p></div>
	<?php endif; ?>

	<h3><?php _e( 'Mapowanie atrybutów', 'woocommerce_ceneo' ); ?></h3>

	<p><?php _e( 'Przypisz atrybuty WooCommerce do odpowiednich atrybutów Ceneo.', 'woocommerce_ceneo' ); ?></p>

	<?php $integration_values = $plugin->getSettingValue('attr_integration', array());  ?>

	<div id="ceneo-admin-integration-attributes">
		<table class="wp-list-table widefat fixed striped">
			<tbody id="ceneo_categories_rows">
			<tr>
				<td class="ceneo-category">
					<h3> <?php _e('Atrybuty integracyjne', 'woocommerce_ceneo'); ?></h3>
					<table width="100%" class="wp-list-table widefat fixed striped">
						<thead>
						<tr>
							<th class="ceneo-category"> Nazwa atrybutu Ceneo </th>
							<th> Nazwa atrybutu WooCommerce	</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td> <?php _e('Nazwa producenta', 'woocommerce_ceneo'); ?> </td>
							<td>
								<?php
								$params = array(
								'id'            => $plugin->getNamespace().'_attr_integration_Producent',
								'name'          => $plugin->getNamespace().'[attr_integration][Producent]',
								'class'			=> 'select2-ceneo-settings',
								'options'       => $attributes,
								'label'			=> '',
								'value'			=> isset( $integration_values['Producent'] )? $integration_values['Producent']: '',
								);
								echo $view_helper->create_select_field( $params );

								?>
							</td>
						</tr>
						<tr>
							<td> <?php _e('Kod producenta', 'woocommerce_ceneo'); ?> </td>
							<td>
								<?php
								$params = array(
									'id'            => $plugin->getNamespace().'_attr_integration_SKU',
									'name'          => $plugin->getNamespace().'[attr_integration][SKU]',
									'class'			=> 'select2-ceneo-settings',
									'options'       => $attributes,
									'label'			=> '',
									'value'			=> isset( $integration_values['SKU'] )? $integration_values['SKU']: '',
								);
								echo $view_helper->create_select_field( $params );
								?>
							</td>
						</tr>
						<tr>
							<td> <?php _e('EAN', 'woocommerce_ceneo'); ?> </td>
							<td>
								<?php
								$params = array(
									'id'            => $plugin->getNamespace().'_attr_integration_EAN',
									'name'          => $plugin->getNamespace().'[attr_integration][EAN]',
									'class'			=> 'select2-ceneo-settings',
									'options'       => $attributes,
									'label'			=> '',
									'value'			=> isset( $integration_values['EAN'] )? $integration_values['EAN']: '',
								);
								echo $view_helper->create_select_field( $params );
								?>
							</td>
						</tr>
						<tr>
							<td> <?php _e('ISBN', 'woocommerce_ceneo'); ?> </td>
							<td>
								<?php
								$params = array(
									'id'            => $plugin->getNamespace().'_attr_integration_ISBN',
									'name'          => $plugin->getNamespace().'[attr_integration][ISBN]',
									'class'			=> 'select2-ceneo-settings',
									'options'       => $attributes,
									'label'			=> '',
									'value'			=> isset( $integration_values['ISBN'] )? $integration_values['ISBN']: '',
								);
								echo $view_helper->create_select_field( $params );
								?>
							</td>
						</tr>
						<tr>
							<td> <?php _e('OTC', 'woocommerce_ceneo'); ?> </td>
							<td>
								<?php
								$params = array(
									'id'            => $plugin->getNamespace().'_attr_integration_OTC',
									'name'          => $plugin->getNamespace().'[attr_integration][OTC]',
									'class'			=> 'select2-ceneo-settings',
									'options'       => $attributes,
									'label'			=> '',
									'value'			=> isset( $integration_values['OTC'] )? $integration_values['OTC']: '',
								);
								echo $view_helper->create_select_field( $params );
								?>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div id="ceneo-admin-category-attributes" style="margin-top: 30px">
		<h3> <?php _e('Atrybuty dla mapowanych kategorii', 'woocommerce_ceneo') ?> </h3>
		<table class="wp-list-table widefat fixed striped">
			<thead>
			<tr>
				<th class="ceneo-category"> <?php _e('Kategoria sklepu', 'woocommerce_ceneo'); ?> </th>
			</tr>
			</thead>

			<tbody id="ceneo_categories_rows">

			<?php foreach ( $product_categories as $category ): ?>
				<?php $value = $plugin->getSettingValue('term_' . $category->term_id);
					if( empty( $value ) ){
						continue;
					}

				$category_values = $plugin->getSettingValue( 'attr_cat_' . $category->term_id, array() );
				?>
				<tr>
					<td class="ceneo-category">
						<h3><?php echo $category->name; ?></h3>
						<table width="100%" class="wp-list-table widefat fixed striped">
							<thead>
							<tr>
								<th class="ceneo-category"> <?php _e('Nazwa atrybutu Ceneo', 'woocommerce_ceneo'); ?></th>
								<th> <?php _e('Nazwa atrybutu WooCommerce', 'woocommerce_ceneo'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ( $ceneo->get_category_attributes( $value ) as $attribute ): ?>
							<tr>
								<?php
									$attribute_name = (string)$attribute->Name;
									$attribute_postfix = ((string)$attribute->IsKeyAttribute === 'True' )? '*' : '';

								?>
								<td> <?php echo $attribute_name . $attribute_postfix; ?> </td>
								<td>
									<?php
									$params = array(
										'id'   => $plugin->getNamespace() . '_attr_cat_' . $category->term_id . '_' . $attribute_name,
										'name' => $plugin->getNamespace() . '[attr_cat_' . $category->term_id . '][' . $attribute_name . ']',
										'class'   => 'select2-ceneo-settings',
										'options' => $attributes,
										'label'   => '',
										'value'   => isset( $category_values[ $attribute_name ] ) ? $category_values[ $attribute_name ] : '',
									);
									echo $view_helper->create_select_field( $params );
									?>
								</td>
							</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
			<tfoot>
			<tr>
				<th class="ceneo-category">
					<?php _e('Kategoria sklepu', 'woocommerce_ceneo'); ?>
				</th>
			</tr>
			</tfoot>
		</table>
	</div>

	<p class="submit"><input type="submit" value="<?php _e( 'Zapisz zmiany', 'woocommerce_ceneo' ); ?>" class="button button-primary" id="submit" name="submitForm"></p>
</form>
