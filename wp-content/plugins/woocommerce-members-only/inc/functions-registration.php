<?php
/**
 * Functions for approval user registrations
 * @since 1.7.0
 */

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add any new fields to the registration form
 * @since 1.9.0
 */
function wcmo_register_form() {

	$registration_fields = wcmo_get_registration_fields();
	$enabled_fields = wcmo_get_enabled_registration_fields();

	if( ! empty( $enabled_fields['fields'] ) ) {

		$fields = $enabled_fields['fields'];

		// If we have a priority, use that to order the fields
		if( ! empty( $enabled_fields['priority'] ) ) {
			$fields = $enabled_fields['priority'];
			asort( $fields );
		}

		foreach( $fields as $field_id=>$field ) {

			// If the field isn't in our list, skip on
			if( ! isset( $enabled_fields['fields'][$field_id] ) ) {
				continue;
			}

			$value = isset( $_POST[$field_id] ) ? $_POST[$field_id] : '';
			$required = ( isset( $enabled_fields['required'][$field_id] ) && $enabled_fields['required'][$field_id] == 'on' ) ? true : false;

			if( isset( $registration_fields[$field_id]['type'] ) && $registration_fields[$field_id]['type'] == 'text' ) {

				woocommerce_form_field(
					$field_id,
					array(
						'type'        => 'text',
						'required'    => $required, // just adds an "*"
						'label'       => $registration_fields[$field_id]['label']
					),
					$value
				);

			} else if( isset( $registration_fields[$field_id]['type'] ) && $registration_fields[$field_id]['type'] == 'email' ) {

				woocommerce_form_field(
					$field_id,
					array(
						'type'        => 'email',
						'required'    => $required, // just adds an "*"
						'label'       => $registration_fields[$field_id]['label']
					),
					$value
				);

			} else if( isset( $registration_fields[$field_id]['type'] ) && $registration_fields[$field_id]['type'] == 'select' ) {

				woocommerce_form_field(
					$field_id,
					array(
						'type'        => 'select',
						'required'    => $required, // just adds an "*"
						'label'       => $registration_fields[$field_id]['label'],
						'options'			=> is_array( $registration_fields[$field_id]['options'] ) ? $registration_fields[$field_id]['options'] : array()
					),
					$value
				);

			} else if( isset( $registration_fields[$field_id]['type'] ) && $registration_fields[$field_id]['type'] == 'country' ) {

				wp_enqueue_script( 'wc-country-select' );
				woocommerce_form_field(
					$field_id,
					array(
						'type'      	=> 'country',
						'class'     	=> array( 'chzn-drop' ),
						'label'     	=> $registration_fields[$field_id]['label'],
						'placeholder' => __('Choose your country.'),
						'required'  	=> $required,
						'clear'     	=> true
					)
				);

			}

		}

	}

	// Add a field that allows the user to select their role when registering
	$enable_registration_field = get_option( 'wcmo_enable_registration_roles', 'no' );
	if( $enable_registration_field == 'yes' ) {

		$enabled_roles = wcmo_get_enabled_registration_roles();
		if( isset( $enabled_roles['include'] ) ) {

			// Get an array of enabled roles
			$all_roles = wcmo_get_assignable_user_roles();
			$roles = array();
			foreach( $enabled_roles['include'] as $id=>$enabled ) {

				if( $enabled == 'on' ) {
					$roles[$id] = $all_roles[$id];
				}

			}

			$value = isset( $_POST['wcmo_registration_role'] ) ? $_POST['wcmo_registration_role'] : '';

			// Now print our field
			woocommerce_form_field(
				'wcmo_registration_role',
				array(
					'type'        => 'select',
					'required'    => true, // just adds an "*"
					'label'       => apply_filters( 'wcmo_registration_form_role_label', __( 'Role', 'wcmo' ) ),
					'options'			=> $roles
				),
				$value
			);

		}

	}

}
add_action( 'woocommerce_register_form', 'wcmo_register_form', 10 );

/**
 * Validate extra fields
 * @since 1.9.0
 */
function wcmo_register_post( $username, $email, $errors ) {

	$registration_fields = wcmo_get_registration_fields();
	$enabled_fields = wcmo_get_enabled_registration_fields();

	if( ! empty( $enabled_fields['fields'] ) ) {

		foreach( $enabled_fields['fields'] as $field_id=>$field ) {

			$value = isset( $_POST[$field_id] ) ? $_POST[$field_id] : '';
			$required = ( isset( $enabled_fields['required'][$field_id] ) && $enabled_fields['required'][$field_id] == 'on' ) ? true : false;

			$registration_fields = wcmo_get_registration_fields();
			$enabled_fields = wcmo_get_enabled_registration_fields();

			if( $enabled_fields ) {

				foreach( $enabled_fields['fields'] as $field_id=>$field ) {

					$required = ( isset( $enabled_fields['required'][$field_id] ) && $enabled_fields['required'][$field_id] == 'on' ) ? true : false;

					// Check for any empty required fields
					if( $required && empty( $_POST[$field_id] ) ) {
						$errors->add(
							$field_id . '_error',
							apply_filters(
								'wcmo_registration_validation_error_' . $field_id,
								sprintf(
									__( 'Please enter a value for %s', 'wcmo' ),
									$registration_fields[$field_id]['label']
								),
								$field
							)
						);
					}

				}

			}

		}

	}

}
add_action( 'woocommerce_register_post', 'wcmo_register_post', 10, 3 );

/**
 * Save the extra field data
 * @since 1.9.0
 */
function wcmo_created_customer( $user_id ) {

	// Update user roles
	$user = get_user_by( 'id', $user_id );
	$roles = $user->roles;

	// Check for default user roles
	$default_roles = wcmo_get_default_user_roles();

	// Check if a role has been set in the registration form
	if( isset( $_POST['wcmo_registration_role'] ) ) {

		$registered_role = $_POST['wcmo_registration_role'];
		// Add it to the list of default roles
		$default_roles[] = $registered_role;

	}

	if( $roles ) {
		// Remove existing roles
		foreach( $roles as $role ) {
			$user->remove_role( $role );
		}
		// Add new default roles
		foreach( $default_roles as $role ) {
			$user->add_role( $role );
		}
		$roles = $default_roles;
	}

	// If user approval is required, save the roles as meta and set role to pending
	if( wcmo_get_user_approval() == 'yes' ) {

		$needs_approval = true;

		// If the user is selecting their role from the registration form
		// Then we need to confirm if it needs to be approved
		if( isset( $_POST['wcmo_registration_role'] ) ) {

			// Check if the role we're applying for needs to be approved
			$enabled_roles = wcmo_get_enabled_registration_roles();

			if( isset( $enabled_roles['approve'] ) && empty( $enabled_roles['approve'][$registered_role] ) ) {

				// This role doesn't need to be approved
				$needs_approval = false;

			}

		}

		$needs_approval = apply_filters( 'wcmo_user_needs_approval', $needs_approval, $user );

		if( $needs_approval ) {

			// Save these roles until the user is approved
			update_user_meta( $user_id, 'wcmo_user_roles', $roles );

			// Reset the role to pending
			$user->set_role( 'pending' );

			// Tell the admin
			wcmo_new_registration_email( $user_id );

			wc_add_notice(
				apply_filters(
					'wcmo_pending_user_registration_notice',
					__( 'Please note that your registration requires approval. You\'ll receive an email when your account has been approved.', 'wcmo' )
				),
				'notice'
			);

		}

	}

	// Check for any additional registration fields
	$registration_fields = wcmo_get_registration_fields();
	$enabled_fields = wcmo_get_enabled_registration_fields();

	if( ! empty( $enabled_fields['fields'] ) ) {

		foreach( $enabled_fields['fields'] as $field_id=>$field ) {

			$value = isset( $_POST[$field_id] ) ? $_POST[$field_id] : '';
			$required = ( isset( $enabled_fields['required'][$field_id] ) && $enabled_fields['required'][$field_id] == 'on' ) ? true : false;

			if ( isset( $_POST[$field_id] ) ) {
				update_user_meta( $user_id, $field_id, wc_clean( $_POST[$field_id] ) );
			}

		}

	}

}
add_action( 'woocommerce_created_customer', 'wcmo_created_customer' );
