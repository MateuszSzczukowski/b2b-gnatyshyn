<?php
/**
 * Settings functions for the admin
 * @package WCMO
 */

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Do a daily check for any expiring roles
 * @since 1.8.0
 */
function wcmo_check_expired_roles() {

	$expiring_roles = get_option( 'wcmo_expiring_roles', array() );

	if( empty( $expiring_roles ) ) {
		// There's no roles that are set to expire
		return;
	}

	// Find any users that have a role that is due to expire in one month


	// Find any users that have a role that is due to expire today
	$today = strtotime( 'today midnight' );

	foreach( $expiring_roles as $role ) {

		$args = array(
			'role'				=> $role,
			'limit'				=> 9999,
			'meta_query'  => array(
	      'relation'  => 'AND',
	      array(
	        'key'     => $role . '_expires',
	        'value'   => $today,
	      )
	    )
		);
		$expiring_users_q = new WP_User_Query( $args );
		$expiring_users = $expiring_users_q->get_results();

		if( $expiring_users ) {

			foreach( $expiring_users as $user ) {

				// Remove roles from expiring users
				$user->remove_role( $role );

				// Add expired role, e.g. Gold Member (expired)
				do_action( 'wcmo_after_expired_user_remove_role', $user, $role );

			}

		}

	}

}
add_action( 'woocommerce_scheduled_sales', 'wcmo_check_expired_roles' );
