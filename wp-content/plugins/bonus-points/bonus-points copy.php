<?php
/**
 * Plugin Name: Punkty promocyjne
 */



global $bc_db_version;
$c_db_version = '1.0';

function bc_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'bonus_points_history';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		user_id mediumint(9) NOT NULL,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		order_id smallint(9) NOT NULL,
		points smallint(9) NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'bc_db_version', $bc_db_version );
}
register_activation_hook( __FILE__, 'bc_install' );
function mysite_completed($order_id) {
    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $order = new WC_Order( $order_id );
    $user_id = $order->get_user_id();
    $results = $wpdb->get_results( "DELETE FROM $table_name WHERE `order_id`='".$order->get_id()."'" );
    $points = 0;
    foreach($order->get_items() as $item_id => $item){
        if(get_field('bonus_points_add',$item->get_product_id())){
            $points += floor($item->get_total()/100);
        }
    }
        $wpdb->insert($table_name, array(
        'user_id' => $order->get_customer_id(),
        'points' => $points,
        'order_id' => $order_id,
        'time'=>$order->get_date_created()->date("Y-m-d H:i:s")
    ));
    error_log("$order_id set to COMPLETED", 0);
}
function mysite_refunded($order_id) {
    $results = $wpdb->get_results( "DELETE FROM $table_name WHERE `order_id`='".$order->get_id()."'" );
    error_log("$order_id set to REFUNDED", 0);
}
function mysite_cancelled($order_id) {
    $results = $wpdb->get_results( "DELETE FROM $table_name WHERE `order_id`='".$order->get_id()."'" );
    error_log("$order_id set to CANCELLED", 0);
}
add_action( 'woocommerce_order_status_completed', 'mysite_completed');
add_action( 'woocommerce_order_status_refunded', 'mysite_refunded');
add_action( 'woocommerce_order_status_cancelled', 'mysite_cancelled');

function pb_options_page()
{
    add_submenu_page(
        'admin.php?page=wc-admin',
        'Punkty bobusowe',
        'Punkty bobusowe',
        'manage_options',
        'pb_options',
        'pb_options_page_html'
    );
}
add_action('admin_menu', 'pb_options_page');

function pb_options_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "wporg_options"
            settings_fields( 'wporg_options' );
            // output setting sections and their fields
            // (sections are registered for "wporg", each field is registered to a specific section)
            do_settings_sections( 'wporg' );
            // output save settings button
            submit_button( __( 'Save Settings', 'textdomain' ) );
            ?>
        </form>
    </div>
    <?php
}


function QuadLayers_add_support_endpoint() {
    add_rewrite_endpoint( 'punkty', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'QuadLayers_add_support_endpoint' );

function QuadLayers_support_query_vars( $vars ) {
    $vars[] = 'punkty';
    return $vars;
}
add_filter( 'query_vars', 'QuadLayers_support_query_vars', 0 );

function QuadLayers_add_support_link_my_account( $items ) {
    $items['punkty'] = 'Moje punkty';
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'QuadLayers_add_support_link_my_account' );

function QuadLayers_support_content() {
    echo '<h3>Twoje punkty</h3><p>Welcome to the support area. As a premium customer, manage your support tickets from here, you can submit a ticket if you have any issues with your website. We\'ll put our best to provide you with a fast and efficient solution</p>';
    echo do_shortcode( '[bonuspoints]' );
}
add_action( 'woocommerce_account_punkty_endpoint', 'QuadLayers_support_content' );

add_shortcode( 'bonuspoints', 'bonuspoints_shortcode_fn' );
function bonuspoints_shortcode_fn( $attributes ) {
    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $results = $wpdb->get_results( "SELECT * FROM $table_name WHERE `user_id`='".get_current_user_id()."'" );
    $content = '<ul class="points__history-list">';
    $z = 1;
    foreach($results as $result){
        $content.='<li class="">';
            $content.='<span class="title">' . $z . 'Zakupy ' . date("d.m.Y",strtotime($result->time)) . '</span>';
            if($result->points > 0) {
                $content.='<span class="value plus">' . $result->points . ' pkt</span>';
            } else {
                $content.='<span class="value minus">' . $result->points . ' pkt</span>';
            }
        $content.='</li>';
        $z++;
    }
    $content .= '</ul>';
    return $content;
}
add_shortcode( 'bonuspoints_used', 'bonuspoints_used_shortcode_fn' );
function bonuspoints_used_shortcode_fn( $attributes ) {
    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $results = $wpdb->get_results( "SELECT sum(points) as sum FROM $table_name WHERE points<0 and `user_id`='".get_current_user_id()."'" );
    $points = $results[0]->sum;
    return abs($points);
}
add_shortcode( 'bonuspoints_current', 'bonuspoints_current_shortcode_fn' );
function bonuspoints_current_shortcode_fn( $attributes ) {
    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $results = $wpdb->get_results( "SELECT sum(points) as sum FROM $table_name WHERE `user_id`='".get_current_user_id()."'" );
    $points = $results[0]->sum;
    return abs($points);
}
function action_woocommerce_cart_coupon(  ) {
    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $results = $wpdb->get_results( "SELECT sum(points) as sum FROM $table_name WHERE `user_id`='".get_current_user_id()."'" );
    $points = $results[0]->sum;
    if(WC()->session->get( 'bonus_points_use_value' )>0){
        $points = $points - WC()->session->get( 'bonus_points_use_value' );
    }
    echo '<br />
    Masz: '.$points.' punktów
    <label for="bonus_points">Kupon:</label>
    <input type="number" name="bonus_points" class="input-text" id="bonus_points" value="" placeholder="Ilość punktów">
    <input type="submit" class="button" name="apply_points" value="Zrealizuj punkty"><br />
    ';
};
add_action('init', 'myFunction');
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta( $order ){
    $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id;
    echo '<p><strong>Punkty bonusowe:</strong> ' . get_post_meta( $order_id, 'bonus_points', true ) . '</p>';
}
function myFunction(){
if(count($_POST)>0){
    $coupon = array (
      'post_title'    => 'coupon_discount',
      'post_status'   => 'publish',
      'post_type'     => 'shop_coupon'
    );

    $dynamic_discount = $_POST['bonus_points']*5;

    $new_coupon_id = wp_insert_post( $coupon );
    add_post_meta( $new_coupon_id , 'coupon_amount' , $dynamic_discount , true );

    global $current_user;
    if(isset($_POST['apply_points'])){
        if($_POST['bonus_points']>0){
            global $wpdb;
        	$table_name = $wpdb->prefix . 'bonus_points_history';
            $results = $wpdb->get_results( "SELECT sum(points) as sum FROM $table_name WHERE `user_id`='".get_current_user_id()."'" );
            $points = $results[0]->sum;
            if(WC()->session->get( 'bonus_points_use_value' )>0){
                $points = $points - WC()->session->get( 'bonus_points_use_value' );
            }
            if($points>0){
                if($_POST['bonus_points']>$points) $_POST['bonus_points'] = $points;
                foreach ( WC()->cart->get_cart() as $cart_item ){
                    if(get_field('bonus_points_buy',$cart_item['product_id'])){
                        WC()->session->set( 'bonus_points_use_product_id' , $cart_item['product_id'] );
                        WC()->session->set( 'bonus_points_use_value' , $_POST['bonus_points'] );
                    }
                }
            }else{
            }
        }else{
        }
    }else{
    }
}
}
add_action( 'woocommerce_checkout_order_processed', 'add_bonus_points',  10, 1  );

function add_bonus_points($order_id){
    if(WC()->session->get( 'bonus_points_use_value')>0){
        global $wpdb;
        $table_name = $wpdb->prefix . 'bonus_points_history';
        $order = wc_get_order( $order_id );
        update_post_meta( $order_id, 'bonus_points', WC()->session->get( 'bonus_points_use_value') );
        $wpdb->insert($table_name, array(
        'user_id' => $order->get_customer_id(),
        'points' => -WC()->session->get( 'bonus_points_use_value'),
        'order_id' => $order_id,
        'time'=>$order->get_date_created()->date("Y-m-d H:i:s")));
        WC()->session->set( 'bonus_points_use_product_id' , false );
        WC()->session->set( 'bonus_points_use_value' , false );
    }
}

function action_woocommerce_review_order_before_shipping(  ) {
    if(WC()->session->get( 'bonus_points_use_product_id') and WC()->session->get( 'bonus_points_use_value')){
        echo '<tr><td>Zniżka za punkty bonusowe</td><td>'.(WC()->session->get( 'bonus_points_use_value')*5).' zł</td></tr>';
    }
};

// add the action
add_action( 'woocommerce_review_order_before_shipping', 'action_woocommerce_review_order_before_shipping', 10, 0 );

// add the action
add_action( 'woocommerce_cart_coupon', 'action_woocommerce_cart_coupon', 10, 0 );

add_action( 'woocommerce_before_calculate_totals', 'add_custom_price', 9999, 1);
function add_custom_price( $cart ) {

    // This is necessary for WC 3.0+
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    // Avoiding hook repetition (when using price calculations for example)
    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;

    // Loop through cart items
    foreach ( $cart->get_cart() as $item ) {
        if($item['product_id']==WC()->session->get( 'bonus_points_use_product_id')){
            $item['data']->set_price( (($item['data']->get_regular_price()*$item['quantity']) - (5* WC()->session->get( 'bonus_points_use_value')) )/$item['quantity']);
        }
        //
    }
    WC()->cart->calculate_totals();
}
function ss_cart_updated( $cart_item_key, $cart ) {

    $product_id = $cart->cart_contents[ $cart_item_key ]['product_id'];
    if($product_id==WC()->session->get( 'bonus_points_use_product_id')){
        WC()->session->set( 'bonus_points_use_product_id' , false );
        WC()->session->set( 'bonus_points_use_value' , false );
    }

};
add_action( 'woocommerce_remove_cart_item', 'ss_cart_updated', 10, 2 );

add_action( 'edit_user_profile', 'wk_custom_user_profile_fields' );
add_action( 'show_user_profile', 'wk_custom_user_profile_fields' );

function wk_custom_user_profile_fields( $user )
{
    if ( defined('IS_PROFILE_PAGE') && IS_PROFILE_PAGE ) {
    $user_id = get_current_user_id();
    // If is another user's profile page
    } elseif (! empty($_GET['user_id']) && is_numeric($_GET['user_id']) ) {
    $user_id = $_GET['user_id'];
    // Otherwise something is wrong.
    } else {
    die( 'No user id defined.' );
}
    echo '<h3 class="heading">Custom Fields</h3>';

    global $wpdb;
	$table_name = $wpdb->prefix . 'bonus_points_history';
    $results = $wpdb->get_results( "SELECT sum(points) as sum FROM $table_name WHERE `user_id`='".$user_id."'" );
    $points = $results[0]->sum;
    if($points<=0) $points = 0;
    ?>

    <table class="form-table">
	    <tr>
            <th><label for="contact">Ilość punktów bonusowych</label></th>
	        <td>
                <input type="text" class="input-text form-control" name="" value="<?php echo $points?>" id="contact" disabled="disabled"/>
            </td>

	    </tr>
    </table>

    <?php
}
