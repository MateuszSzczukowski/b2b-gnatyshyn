<?php

/*
 * This file is part of the Monolog package.
 *
 * (c) Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace FakturowniaVendor\Monolog\Handler;

use FakturowniaVendor\Monolog\Logger;
use FakturowniaVendor\Monolog\Formatter\NormalizerFormatter;
use FakturowniaVendor\Doctrine\CouchDB\CouchDBClient;
/**
 * CouchDB handler for Doctrine CouchDB ODM
 *
 * @author Markus Bachmann <markus.bachmann@bachi.biz>
 */
class DoctrineCouchDBHandler extends \FakturowniaVendor\Monolog\Handler\AbstractProcessingHandler
{
    private $client;
    public function __construct(\FakturowniaVendor\Doctrine\CouchDB\CouchDBClient $client, $level = \FakturowniaVendor\Monolog\Logger::DEBUG, $bubble = \true)
    {
        $this->client = $client;
        parent::__construct($level, $bubble);
    }
    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
        $this->client->postDocument($record['formatted']);
    }
    protected function getDefaultFormatter()
    {
        return new \FakturowniaVendor\Monolog\Formatter\NormalizerFormatter();
    }
}
