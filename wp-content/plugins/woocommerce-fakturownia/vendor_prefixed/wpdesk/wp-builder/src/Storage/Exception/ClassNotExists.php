<?php

namespace FakturowniaVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassNotExists extends \RuntimeException
{
}
