<?php

namespace FakturowniaVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassAlreadyExists extends \RuntimeException
{
}
