<?php

namespace FakturowniaVendor\WPDesk\PluginBuilder\Storage;

class StorageFactory
{
    /**
     * @return PluginStorage
     */
    public function create_storage()
    {
        return new \FakturowniaVendor\WPDesk\PluginBuilder\Storage\WordpressFilterStorage();
    }
}
