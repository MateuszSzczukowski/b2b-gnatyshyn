<?php

namespace FakturowniaVendor\WPDesk\Composer\Codeception\Commands;

use FakturowniaVendor\Composer\Command\BaseCommand as CodeceptionBaseCommand;
use FakturowniaVendor\Symfony\Component\Console\Output\OutputInterface;
/**
 * Base for commands - declares common methods.
 *
 * @package WPDesk\Composer\Codeception\Commands
 */
abstract class BaseCommand extends \FakturowniaVendor\Composer\Command\BaseCommand
{
    /**
     * @param string $command
     * @param OutputInterface $output
     */
    protected function execAndOutput($command, \FakturowniaVendor\Symfony\Component\Console\Output\OutputInterface $output)
    {
        \passthru($command);
    }
}
