<?php

namespace FakturowniaVendor\WPDesk\Composer\Codeception;

use FakturowniaVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests;
use FakturowniaVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests;
use FakturowniaVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests;
/**
 * Links plugin commands handlers to composer.
 */
class CommandProvider implements \FakturowniaVendor\Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [new \FakturowniaVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests(), new \FakturowniaVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests(), new \FakturowniaVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests()];
    }
}
