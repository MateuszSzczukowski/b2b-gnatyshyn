<?php

namespace FakturowniaVendor\WPDesk\Composer\Codeception;

use FakturowniaVendor\Composer\Composer;
use FakturowniaVendor\Composer\IO\IOInterface;
use FakturowniaVendor\Composer\Plugin\Capable;
use FakturowniaVendor\Composer\Plugin\PluginInterface;
/**
 * Composer plugin.
 *
 * @package WPDesk\Composer\Codeception
 */
class Plugin implements \FakturowniaVendor\Composer\Plugin\PluginInterface, \FakturowniaVendor\Composer\Plugin\Capable
{
    /**
     * @var Composer
     */
    private $composer;
    /**
     * @var IOInterface
     */
    private $io;
    public function activate(\FakturowniaVendor\Composer\Composer $composer, \FakturowniaVendor\Composer\IO\IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }
    public function getCapabilities()
    {
        return [\FakturowniaVendor\Composer\Plugin\Capability\CommandProvider::class => \FakturowniaVendor\WPDesk\Composer\Codeception\CommandProvider::class];
    }
}
