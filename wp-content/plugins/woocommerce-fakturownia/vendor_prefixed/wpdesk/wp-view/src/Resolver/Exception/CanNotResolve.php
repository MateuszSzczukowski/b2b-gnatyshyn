<?php

namespace FakturowniaVendor\WPDesk\View\Resolver\Exception;

class CanNotResolve extends \RuntimeException
{
}
