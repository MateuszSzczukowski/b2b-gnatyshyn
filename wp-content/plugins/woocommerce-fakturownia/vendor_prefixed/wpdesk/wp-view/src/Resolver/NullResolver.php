<?php

namespace FakturowniaVendor\WPDesk\View\Resolver;

use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use FakturowniaVendor\WPDesk\View\Resolver\Exception\CanNotResolve;
/**
 * This resolver never finds the file
 *
 * @package WPDesk\View\Resolver
 */
class NullResolver implements \FakturowniaVendor\WPDesk\View\Resolver\Resolver
{
    public function resolve($name, \FakturowniaVendor\WPDesk\View\Renderer\Renderer $renderer = null)
    {
        throw new \FakturowniaVendor\WPDesk\View\Resolver\Exception\CanNotResolve("Null Cannot resolve");
    }
}
