<?php

namespace FakturowniaVendor\WPDesk\Logger;

use FakturowniaVendor\Monolog\Handler\HandlerInterface;
use FakturowniaVendor\Monolog\Logger;
use FakturowniaVendor\Monolog\Registry;
/**
 * Manages and facilitates creation of logger
 *
 * @package WPDesk\Logger
 */
class BasicLoggerFactory implements \FakturowniaVendor\WPDesk\Logger\LoggerFactory
{
    /** @var string Last created logger name/channel */
    private static $lastLoggerChannel;
    /**
     * Creates logger for plugin
     *
     * @param string $name The logging channel/name of logger
     * @param HandlerInterface[] $handlers Optional stack of handlers, the first one in the array is called first, etc.
     * @param callable[] $processors Optional array of processors
     * @return Logger
     */
    public function createLogger($name, $handlers = array(), array $processors = array())
    {
        if (\FakturowniaVendor\Monolog\Registry::hasLogger($name)) {
            return \FakturowniaVendor\Monolog\Registry::getInstance($name);
        }
        self::$lastLoggerChannel = $name;
        $logger = new \FakturowniaVendor\Monolog\Logger($name, $handlers, $processors);
        \FakturowniaVendor\Monolog\Registry::addLogger($logger);
        return $logger;
    }
    /**
     * Returns created Logger by name or last created logger
     *
     * @param string $name Name of the logger
     *
     * @return Logger
     */
    public function getLogger($name = null)
    {
        if ($name === null) {
            $name = self::$lastLoggerChannel;
        }
        return \FakturowniaVendor\Monolog\Registry::getInstance($name);
    }
}
