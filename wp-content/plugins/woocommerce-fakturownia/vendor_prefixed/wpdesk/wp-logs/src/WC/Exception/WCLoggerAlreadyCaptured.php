<?php

namespace FakturowniaVendor\WPDesk\Logger\WC\Exception;

class WCLoggerAlreadyCaptured extends \RuntimeException
{
}
