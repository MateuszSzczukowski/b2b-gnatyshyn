<?php

namespace FakturowniaVendor\WPDesk\Helper\Integration;

use FakturowniaVendor\WPDesk\Helper\Page\SettingsPage;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
/**
 * Integrates WP Desk main settings page with WordPress
 *
 * @package WPDesk\Helper
 */
class SettingsIntegration implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable, \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection
{
    use HookableParent;
    /** @var SettingsPage */
    private $settings_page;
    public function __construct(\FakturowniaVendor\WPDesk\Helper\Page\SettingsPage $settingsPage)
    {
        $this->add_hookable($settingsPage);
    }
    /**
     * @return void
     */
    public function hooks()
    {
        $this->hooks_on_hookable_objects();
    }
}
