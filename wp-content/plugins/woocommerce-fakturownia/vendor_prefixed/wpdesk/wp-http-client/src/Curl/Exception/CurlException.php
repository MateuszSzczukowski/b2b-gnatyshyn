<?php

namespace FakturowniaVendor\WPDesk\HttpClient\Curl\Exception;

use FakturowniaVendor\WPDesk\HttpClient\HttpClientRequestException;
/**
 * Base class for all curl exceptions.
 *
 * @package WPDesk\HttpClient\Curl\Exception
 */
class CurlException extends \RuntimeException implements \FakturowniaVendor\WPDesk\HttpClient\HttpClientRequestException
{
}
