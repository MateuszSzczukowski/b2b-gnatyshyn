<?php

namespace FakturowniaVendor\WPDesk\HttpClient\Curl\Exception;

class CurlTimedOutException extends \FakturowniaVendor\WPDesk\HttpClient\Curl\Exception\CurlException
{
}
