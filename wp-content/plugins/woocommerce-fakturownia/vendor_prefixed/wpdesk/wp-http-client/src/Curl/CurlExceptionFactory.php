<?php

namespace FakturowniaVendor\WPDesk\HttpClient\Curl;

use FakturowniaVendor\WPDesk\HttpClient\Curl\Exception\CurlException;
use FakturowniaVendor\WPDesk\HttpClient\Curl\Exception\CurlTimedOutException;
class CurlExceptionFactory
{
    /**
     * Convert curl code to appropriate exception class.
     *
     * @param int $curlCode Code from https://curl.haxx.se/libcurl/c/libcurl-errors.html
     * @param string $curlMessage
     * @return CurlException
     */
    public static function createCurlException($curlCode, $curlMessage)
    {
        switch ($curlCode) {
            case \CURLE_OPERATION_TIMEDOUT:
                return new \FakturowniaVendor\WPDesk\HttpClient\Curl\Exception\CurlTimedOutException($curlMessage, $curlCode);
            default:
                return self::createDefaultException($curlMessage, $curlCode);
        }
    }
    /**
     * Creates default Curl exception
     *
     * @param $code
     * @param $message
     * @param \Exception|null $prev
     * @return CurlException
     */
    public static function createDefaultException($code, $message, \Exception $prev = null)
    {
        return new \FakturowniaVendor\WPDesk\HttpClient\Curl\Exception\CurlException('Default exception: ' . $message, $code, $prev);
    }
}
