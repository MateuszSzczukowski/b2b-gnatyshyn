<?php

namespace FakturowniaVendor\WPDesk\HttpClient;

interface HttpClientOptions
{
    /**
     * @return string
     */
    public function getHttpClientClass();
}
