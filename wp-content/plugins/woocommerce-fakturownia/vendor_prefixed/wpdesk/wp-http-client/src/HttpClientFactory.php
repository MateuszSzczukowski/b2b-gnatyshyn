<?php

namespace FakturowniaVendor\WPDesk\HttpClient;

class HttpClientFactory
{
    /**
     * @param HttpClientOptions $options
     * @return HttpClient
     */
    public function createClient(\FakturowniaVendor\WPDesk\HttpClient\HttpClientOptions $options)
    {
        $className = $options->getHttpClientClass();
        return new $className();
    }
}
