<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Response\Traits;

use FakturowniaVendor\WPDesk\ApiClient\Response\AuthApiResponse;
trait AuthApiResponseDecorator
{
    use ApiResponseDecorator;
    /**
     * @return bool
     */
    public function isBadCredentials()
    {
        return $this->getResponseCode() === \FakturowniaVendor\WPDesk\ApiClient\Response\AuthApiResponse::RESPONSE_CODE_BAD_CREDENTIALS;
    }
    /**
     * Is bad credential because token expires
     *
     * @return bool
     */
    public function isTokenExpired()
    {
        return $this->isBadCredentials();
    }
    /**
     * Is bad credential because token is invalid
     *
     * @return bool
     */
    public function isTokenInvalid()
    {
        return $this->isBadCredentials();
    }
}
