<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Response;

use FakturowniaVendor\WPDesk\ApiClient\Response\Traits\AuthApiResponseDecorator;
class AuthApiResponse implements \FakturowniaVendor\WPDesk\ApiClient\Response\ApiResponse
{
    const RESPONSE_CODE_BAD_CREDENTIALS = 401;
    const RESPONSE_CODE_NOT_EXISTS = 404;
    use AuthApiResponseDecorator;
}
