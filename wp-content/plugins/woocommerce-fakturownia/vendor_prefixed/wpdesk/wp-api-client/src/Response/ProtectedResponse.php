<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Response;

use FakturowniaVendor\WPDesk\ApiClient\Response\Exception\TriedExtractDataFromErrorResponse;
use FakturowniaVendor\WPDesk\ApiClient\Response\Traits\ApiResponseDecorator;
/**
 * Response is protected in a way so when you try to get body of the response when an error occured you will get an exception
 *
 * Class ProtectedResponse
 * @package WPDesk\ApiClient\Response
 */
class ProtectedResponse implements \FakturowniaVendor\WPDesk\ApiClient\Response\Response
{
    use ApiResponseDecorator;
    public function getResponseBody()
    {
        if ($this->isError()) {
            throw \FakturowniaVendor\WPDesk\ApiClient\Response\Exception\TriedExtractDataFromErrorResponse::createWithClassInfo(\get_class($this->rawResponse), $this->getResponseCode());
        }
        return $this->rawResponse->getResponseBody();
    }
}
