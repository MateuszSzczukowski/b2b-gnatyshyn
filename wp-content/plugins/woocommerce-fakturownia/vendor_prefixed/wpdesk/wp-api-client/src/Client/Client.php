<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

use FakturowniaVendor\WPDesk\HttpClient\HttpClient;
use FakturowniaVendor\WPDesk\ApiClient\Request\Request;
use FakturowniaVendor\WPDesk\ApiClient\Response\Response;
use FakturowniaVendor\WPDesk\ApiClient\Serializer\Serializer;
interface Client
{
    /**
     * Send given request trough HttpClient
     *
     * @param Request $request
     * @return Response
     */
    public function sendRequest(\FakturowniaVendor\WPDesk\ApiClient\Request\Request $request);
    /**
     * @return HttpClient
     */
    public function getHttpClient();
    /**
     * @param HttpClient $client
     */
    public function setHttpClient(\FakturowniaVendor\WPDesk\HttpClient\HttpClient $client);
    /**
     * @return Serializer
     */
    public function getSerializer();
    /**
     * Returns api url. Always without ending /
     *
     * @return string
     */
    public function getApiUrl();
}
