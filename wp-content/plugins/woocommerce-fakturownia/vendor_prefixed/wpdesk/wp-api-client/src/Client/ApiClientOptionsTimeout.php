<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

interface ApiClientOptionsTimeout extends \FakturowniaVendor\WPDesk\ApiClient\Client\ApiClientOptions
{
    /**
     * @return int
     */
    public function getTimeout();
}
