<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

use FakturowniaVendor\WPDesk\Cache\CacheInfoResolver;
use FakturowniaVendor\WPDesk\Cache\HowToCache;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Request\AuthRequest;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Request\BasicRequest;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Request\Request;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PutSettingsRequest;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Request\Status\GetStatusRequest;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Response\ApiResponse;
use FakturowniaVendor\WPDesk\SaasPlatformClient\Response\RawResponse;
class RequestCacheInfoResolver implements \FakturowniaVendor\WPDesk\Cache\CacheInfoResolver
{
    const DEFAULT_CACHE_TTL = 86400;
    //24 hours
    const CACHE_TTL_ONE_MINUTE = 60;
    const OPTION_FS_SAAS_PLATFORM_VERSION_HASH = 'fs-saas-platform-version-hash';
    /**
     *
     * @param Request $request
     *
     * @return bool
     */
    private function prepareCacheKey($request)
    {
        return \md5($request->getEndpoint());
    }
    /**
     *
     * @param Request $request
     *
     * @return bool
     */
    public function isSupported($request)
    {
        if ($request instanceof \FakturowniaVendor\WPDesk\SaasPlatformClient\Request\BasicRequest) {
            return \true;
        }
        return \false;
    }
    /**
     *
     * @param Request $request
     *
     * @return bool
     */
    public function shouldCache($request)
    {
        if ($request instanceof \FakturowniaVendor\WPDesk\ApiClient\Client\ConnectKeyInfoRequest) {
            return \false;
        }
        if ($request instanceof \FakturowniaVendor\WPDesk\SaasPlatformClient\Request\Status\GetStatusRequest) {
            return \false;
        }
        if ($request instanceof \FakturowniaVendor\WPDesk\SaasPlatformClient\Request\BasicRequest) {
            if ('GET' === $request->getMethod()) {
                return \true;
            }
        }
        return \false;
    }
    /**
     *
     * @param Request $request
     *
     * @return HowToCache
     */
    public function prepareHowToCache($request)
    {
        $howToCache = new \FakturowniaVendor\WPDesk\Cache\HowToCache($this->prepareCacheKey($request), self::DEFAULT_CACHE_TTL);
        return $howToCache;
    }
    /**
     * @param ApiResponse $response
     *
     * @return bool
     */
    private function isPlatformVersionFromResponseChanged(\FakturowniaVendor\WPDesk\SaasPlatformClient\Response\ApiResponse $response)
    {
        $stored_hash = \get_option(self::OPTION_FS_SAAS_PLATFORM_VERSION_HASH, '');
        if ($stored_hash !== $response->getPlatformVersionHash()) {
            return \true;
        }
        return \false;
    }
    /**
     * @param ApiResponse $response
     */
    private function storePlatformVersionHashFromResponse(\FakturowniaVendor\WPDesk\SaasPlatformClient\Response\ApiResponse $response)
    {
        \update_option(self::OPTION_FS_SAAS_PLATFORM_VERSION_HASH, $response->getPlatformVersionHash());
    }
    /**
     *
     * @param Request $request
     * @param mixed $item
     *
     * @return bool
     */
    public function shouldClearCache($request, $item)
    {
        if ($request instanceof \FakturowniaVendor\WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PutSettingsRequest) {
            return \true;
        }
        if ($item instanceof \FakturowniaVendor\WPDesk\SaasPlatformClient\Response\ApiResponse && $this->isPlatformVersionFromResponseChanged($item)) {
            $this->storePlatformVersionHashFromResponse($item);
            return \true;
        }
        return \false;
    }
    /**
     *
     * @param Request $request
     * @param mixed $item
     *
     * @return string[]
     */
    public function shouldClearKeys($request, $item)
    {
        if ('GET' !== $request->getMethod()) {
            return [$this->prepareCacheKey($request)];
        }
        return [];
    }
}
