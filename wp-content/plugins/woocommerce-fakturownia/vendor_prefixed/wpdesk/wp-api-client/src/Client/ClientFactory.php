<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

use FakturowniaVendor\WPDesk\Cache\WordpressCache;
use FakturowniaVendor\WPDesk\HttpClient\HttpClientFactory;
use FakturowniaVendor\WPDesk\ApiClient\Serializer\SerializerFactory;
class ClientFactory
{
    /**
     * @param ApiClientOptions $options
     * @return Client
     */
    public function createClient(\FakturowniaVendor\WPDesk\ApiClient\Client\ApiClientOptions $options)
    {
        $httpClientFactory = new \FakturowniaVendor\WPDesk\HttpClient\HttpClientFactory();
        $serializerFactory = new \FakturowniaVendor\WPDesk\ApiClient\Serializer\SerializerFactory();
        $className = $options->getApiClientClass();
        $client = new $className($httpClientFactory->createClient($options), $serializerFactory->createSerializer($options), $options->getLogger(), $options->getApiUrl(), $options->getDefaultRequestHeaders(), $options instanceof \FakturowniaVendor\WPDesk\ApiClient\Client\ApiClientOptionsTimeout ? $options->getTimeout() : null);
        if ($options->isCachedClient()) {
            $client = new \FakturowniaVendor\WPDesk\ApiClient\Client\CachedClient($client, new \FakturowniaVendor\WPDesk\Cache\WordpressCache());
        }
        return $client;
    }
}
