<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

use Psr\Log\LoggerInterface;
use FakturowniaVendor\WPDesk\ApiClient\Serializer\SerializerOptions;
use FakturowniaVendor\WPDesk\HttpClient\HttpClientOptions;
interface ApiClientOptions extends \FakturowniaVendor\WPDesk\HttpClient\HttpClientOptions, \FakturowniaVendor\WPDesk\ApiClient\Serializer\SerializerOptions
{
    /**
     * @return LoggerInterface
     */
    public function getLogger();
    /**
     * @return string
     */
    public function getApiUrl();
    /**
     * @return array
     */
    public function getDefaultRequestHeaders();
    /**
     * @return bool
     */
    public function isCachedClient();
    /**
     * @return string
     */
    public function getApiClientClass();
}
