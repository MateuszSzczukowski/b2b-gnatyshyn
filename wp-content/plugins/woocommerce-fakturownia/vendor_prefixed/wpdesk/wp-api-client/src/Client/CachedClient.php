<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Client;

use Psr\SimpleCache\CacheInterface;
use FakturowniaVendor\WPDesk\Cache\CacheDispatcher;
use FakturowniaVendor\WPDesk\Cache\CacheInfoResolverCreator;
use FakturowniaVendor\WPDesk\Cache\CacheItemCreator;
use FakturowniaVendor\WPDesk\Cache\CacheItemVerifier;
use FakturowniaVendor\WPDesk\HttpClient\HttpClient;
use FakturowniaVendor\WPDesk\ApiClient\Request\Request;
use FakturowniaVendor\WPDesk\ApiClient\Response\Response;
use FakturowniaVendor\WPDesk\ApiClient\Serializer\Serializer;
class CachedClient implements \FakturowniaVendor\WPDesk\ApiClient\Client\Client, \FakturowniaVendor\WPDesk\Cache\CacheItemCreator, \FakturowniaVendor\WPDesk\Cache\CacheItemVerifier
{
    /** @var Client */
    private $client;
    /** @var CacheInterface */
    private $cache;
    /**
     * @var CacheDispatcher
     */
    private $cacheDispatcher;
    /**
     * CachedClient constructor.
     *
     * @param Client $decorated Decorated client
     * @param CacheInterface $cache
     */
    public function __construct(\FakturowniaVendor\WPDesk\ApiClient\Client\Client $decorated, \Psr\SimpleCache\CacheInterface $cache)
    {
        $this->client = $decorated;
        $this->cache = $cache;
        $this->cacheDispatcher = new \FakturowniaVendor\WPDesk\Cache\CacheDispatcher($cache, $this->getCacheInfoResolvers());
    }
    /**
     * Get cache info resolvers.
     *
     * @return RequestCacheInfoResolver[]
     */
    protected function getCacheInfoResolvers()
    {
        if ($this->client instanceof \FakturowniaVendor\WPDesk\Cache\CacheInfoResolverCreator) {
            return $this->client->createResolvers();
        } else {
            return [new \FakturowniaVendor\WPDesk\ApiClient\Client\RequestCacheInfoResolver()];
        }
    }
    /**
     * Create item to cache.
     *
     * @param Request $request
     * @return Response
     */
    public function createCacheItem($request)
    {
        return $this->client->sendRequest($request);
    }
    /**
     * Verify cache item.
     *
     * @param $object
     * @return Response;
     */
    public function getVerifiedItemOrNull($object)
    {
        if ($object instanceof \FakturowniaVendor\WPDesk\ApiClient\Response\Response) {
            return $object;
        }
        return null;
    }
    /**
     * Send request.
     *
     * @param Request $request
     * @return mixed|Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function sendRequest(\FakturowniaVendor\WPDesk\ApiClient\Request\Request $request)
    {
        $response = $this->cacheDispatcher->dispatch($request, $this, $this);
        return $response;
    }
    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->client->getHttpClient();
    }
    /**
     * @param HttpClient $client
     * @return mixed
     */
    public function setHttpClient(\FakturowniaVendor\WPDesk\HttpClient\HttpClient $client)
    {
        return $this->client->setHttpClient($client);
    }
    /**
     * @return Serializer
     */
    public function getSerializer()
    {
        return $this->client->getSerializer();
    }
    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->client->getApiUrl();
    }
}
