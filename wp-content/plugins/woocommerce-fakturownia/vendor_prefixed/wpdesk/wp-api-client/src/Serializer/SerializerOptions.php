<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Serializer;

interface SerializerOptions
{
    /**
     * @return string
     */
    public function getSerializerClass();
}
