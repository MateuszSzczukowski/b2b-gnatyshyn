<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Serializer;

class SerializerFactory
{
    /**
     * @param SerializerOptions $options
     * @return Serializer
     */
    public function createSerializer(\FakturowniaVendor\WPDesk\ApiClient\Serializer\SerializerOptions $options)
    {
        $className = $options->getSerializerClass();
        return new $className();
    }
}
