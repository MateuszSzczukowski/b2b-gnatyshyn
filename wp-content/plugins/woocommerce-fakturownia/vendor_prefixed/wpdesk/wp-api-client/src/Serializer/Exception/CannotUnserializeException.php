<?php

namespace FakturowniaVendor\WPDesk\ApiClient\Serializer\Exception;

/**
 * Thrown when serializer cannot unserialize string data
 *
 * @package WPDesk\ApiClient\Serializer\Exception
 */
class CannotUnserializeException extends \RuntimeException
{
}
