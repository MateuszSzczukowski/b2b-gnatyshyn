<?php

namespace FakturowniaVendor\WPDesk\Invoices\Exception;

class InvalidInvoiceDataException extends \RuntimeException
{
}
