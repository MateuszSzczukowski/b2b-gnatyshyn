<?php

namespace FakturowniaVendor\WPDesk\Invoices\Ajax;

use FakturowniaVendor\WPDesk\Invoices\Integration;
use FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox;
/**
 * Class AjaxHandler
 * Handles Ajax actions.
 *
 * @package WPDesk\Invoices
 */
abstract class AjaxHandler
{
    const RESPONSE_MESSAGE = 'message';
    const RESPONSE_CONTENT = 'content';
    const RESPONSE_STATUS = 'status';
    /**
     * @var OrderMetaBox
     */
    protected $orderMetaBox;
    /**
     * @var string
     */
    protected $integration_id;
    /**
     * AjaxCreateHandler constructor.
     *
     * @param string       $integration_id
     * @param OrderMetaBox $orderMetaBox
     */
    public function __construct($integration_id, \FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox $orderMetaBox)
    {
        $this->integration_id = $integration_id;
        $this->orderMetaBox = $orderMetaBox;
    }
    /**
     * Is integration type adequate to the request.
     * If no integration given in request then lets assume it's ok and valid.
     *
     * @return bool
     */
    protected function isCurrentIntegration()
    {
        return $this->integration_id === $this->getRequestValue('integration_id', $this->integration_id);
    }
    /**
     * Should return WordPress ajax action name
     *
     * @return string
     */
    protected abstract function getAjaxActionName();
    /**
     * This url is needed by other dependencies to build route to this handler
     *
     * @return string
     */
    public function getAjaxActionUrl()
    {
        $url = \admin_url('admin-ajax.php');
        $url = \add_query_arg('action', $this->getAjaxActionName(), $url);
        return $url;
    }
    /**
     * Get value from $_POST.
     *
     * @param string $name
     * @param string $default
     * @return string
     */
    protected function getRequestValue($name, $default = '')
    {
        if (isset($_REQUEST[$name])) {
            return $_REQUEST[$name];
        }
        return $default;
    }
    /**
     * Handle AJAX request.
     */
    public abstract function handleAjaxRequest();
}
