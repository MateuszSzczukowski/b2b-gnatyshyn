<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox;

abstract class MetaBoxFieldPrice extends \FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxField
{
    /**
     * Prepare field.
     *
     * @param string $value
     * @return array
     */
    protected function prepareField($value = '')
    {
        $field = parent::prepareField($value);
        $field['data_type'] = 'price';
        return $field;
    }
}
