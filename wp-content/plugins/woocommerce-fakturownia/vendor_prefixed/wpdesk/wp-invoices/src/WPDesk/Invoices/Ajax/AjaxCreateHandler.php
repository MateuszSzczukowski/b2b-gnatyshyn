<?php

namespace FakturowniaVendor\WPDesk\Invoices\Ajax;

use FakturowniaVendor\WPDesk\Invoices\Documents\Creator;
use FakturowniaVendor\WPDesk\Invoices\Documents\Pdf;
use FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * Class AjaxCreateHandler
 * Handles Ajax create document action.
 *
 * @package WPDesk\Invoices
 */
class AjaxCreateHandler extends \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxHandler implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var string
     */
    private $ajaxActionName = 'wpdesk_invoices_create';
    /**
     * @var Pdf
     */
    private $documentCreator;
    /**
     * AjaxCreateHandler constructor.
     *
     * @param string $integration_id
     * @param OrderMetaBox $orderMetaBox
     * @param Creator $documentCreator
     */
    public function __construct($integration_id, \FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox $orderMetaBox, \FakturowniaVendor\WPDesk\Invoices\Documents\Creator $documentCreator)
    {
        parent::__construct($integration_id, $orderMetaBox);
        $this->documentCreator = $documentCreator;
    }
    /**
     * Hooks.
     */
    public function hooks()
    {
        \add_action('wp_ajax_' . $this->ajaxActionName, array($this, 'handleAjaxRequest'));
    }
    /**
     * @inheritDoc
     */
    public function getAjaxActionName()
    {
        return $this->ajaxActionName;
    }
    /**
     * Handle AJAX request.
     */
    public function handleAjaxRequest()
    {
        if (!$this->isCurrentIntegration()) {
            return;
        }
        $response = array(self::RESPONSE_STATUS => 'error');
        $nonce = $this->getRequestValue('nonce');
        $order_id = $this->getRequestValue('order_id');
        if (\wp_verify_nonce($nonce, $this->ajaxActionName . $order_id)) {
            $response[self::RESPONSE_MESSAGE] = 'Ups! Something go wrong!';
            $order = \wc_get_order($order_id);
            if ($order) {
                try {
                    $this->documentCreator->createDocumentForOrder($order, \wp_unslash($_POST));
                    $response[self::RESPONSE_CONTENT] = $this->orderMetaBox->getContent($order);
                    $response[self::RESPONSE_STATUS] = 'ok';
                    unset($response[self::RESPONSE_MESSAGE]);
                } catch (\Exception $e) {
                    $response[self::RESPONSE_CONTENT] = $this->orderMetaBox->getContent($order);
                    $response[self::RESPONSE_MESSAGE] = $e->getMessage();
                }
            } else {
                $response[self::RESPONSE_MESSAGE] = 'Order not found!';
            }
        } else {
            $response[self::RESPONSE_MESSAGE] = 'Invalid nonce!';
        }
        \wp_send_json($response);
    }
}
