<?php

namespace FakturowniaVendor\WPDesk\Invoices\Documents;

/**
 * Abstract documents creator.
 *
 * @package WPDesk\Invoices\Documents
 */
abstract class AbstractCreator
{
    /**
     * @var Type.
     */
    protected $type;
    /**
     * @param Type $type
     */
    public function __construct(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $type)
    {
        $type->setCreator($this);
        $this->type = $type;
    }
    /**
     * Create document for order.
     *
     * @param \WC_Order     $order Order.
     * @param array         $data Posted data.
     * @param bool          $overwriteExisting Overwrite already created.
     *
     * @throws \Exception
     */
    public abstract function createDocumentForOrder($order, array $data, $overwriteExisting = \true);
    /**
     * Are document exists for order?
     *
     * @param \WC_Order $order Order.
     * @return bool
     */
    public abstract function areDocumentExistsForOrder($order);
    /**
     * Is auto create allowed for order?
     *
     * @param \WC_Order $order Order.
     * @return bool
     */
    public abstract function isAutoCreateAllowedForOrder($order);
}
