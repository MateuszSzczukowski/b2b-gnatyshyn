<?php

namespace FakturowniaVendor\WPDesk\Invoices\Order;

use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * Display documents on my account.
 */
class MyAccount implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var Type[]
     *
     */
    protected $supported_document_types;
    /**
     * MyAccount constructor.
     *
     * @param Type[] $supported_document_types .
     */
    public function __construct($supported_document_types)
    {
        $this->supported_document_types = $supported_document_types;
    }
    /**
     * @return void|null
     */
    public function hooks()
    {
        \add_action('woocommerce_view_order', array($this, 'viewDocuments'));
    }
    /**
     * @param Type $supportedDocumentType
     * @param \WC_Order $order
     */
    private function maybeViewDocumentForType(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $supportedDocumentType, $order)
    {
        $metaDataContent = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($supportedDocumentType->getMetaDataName(), $order);
        if ($supportedDocumentType->isMetadataContentValidForDocumentType($metaDataContent)) {
            if (!empty($supportedDocumentType->getDocumentView())) {
                $supportedDocumentType->getDocumentView()->render($supportedDocumentType, $order, $metaDataContent);
            }
        }
    }
    /**
     * @param int $order_id
     */
    public function viewDocuments($order_id)
    {
        $order = \wc_get_order($order_id);
        foreach ($this->supported_document_types as $supportedDocumentType) {
            $this->maybeViewDocumentForType($supportedDocumentType, $order);
        }
    }
}
