<?php

namespace FakturowniaVendor;

if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?>
<h4 class="invoices-created"><?php 
echo $createdLabel;
?></h4>
<div class="invoices-created" id="<?php 
echo $createdDivId;
?>">
<?php 
