<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox;

use FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
abstract class MetaBoxField
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $label;
    /**
     * MetaBoxField constructor.
     *
     * @param $id
     * @param $name
     * @param $label
     */
    public function __construct($id, $name, $label)
    {
        $this->id = $id;
        $this->name = $name;
        $this->label = $label;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Prepare field.
     *
     * @param string $value
     * @return array
     */
    protected function prepareField($value = '')
    {
        $field = array('id' => $this->id, 'name' => $this->name, 'label' => $this->label, 'value' => $value);
        return $field;
    }
    /**
     * Prepare value.
     *
     * @param \WC_Order $order Order.
     * @param MetadataContent $metadataContent Meta data.
     * @param OrderDefaults $orderDefaults Order defaults.
     * @return string
     */
    protected abstract function prepareValue(\WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent, \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $orderDefaults);
    /**
     * Render field.
     *
     * @param \WC_Order $order Order.
     * @param MetadataContent $metadataContent Meta data.
     * @param OrderDefaults $orderDefaults Order defaults.
     */
    public function render(\WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent, \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $orderDefaults)
    {
        \woocommerce_wp_text_input($this->prepareField($this->prepareValue($order, $metadataContent, $orderDefaults)));
    }
}
