<?php

namespace FakturowniaVendor;

/**
 * @var string $typeNameLabel
 * @var string $errorMessage
 */
if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?><div>
    <strong><?php 
echo $typeNameLabel;
?>:</strong>
    <span class="error-message"><?php 
echo $errorMessage;
?></span>
</div>
<?php 
