<?php

namespace FakturowniaVendor\WPDesk\Invoices;

use FakturowniaVendor\WPDesk\ApiClient\Client\Client;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxCreateHandler;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxEmailHandler;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults;
use FakturowniaVendor\WPDesk\Invoices\Documents\AbstractCreator;
use FakturowniaVendor\WPDesk\Invoices\Documents\Creator;
use FakturowniaVendor\WPDesk\Invoices\Documents\Pdf;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Field\FormField;
use FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\Invoices\Order\MyAccount;
use FakturowniaVendor\WPDesk\Invoices\OrdersTable\OrderColumn;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
abstract class Integration implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable, \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection, \FakturowniaVendor\WPDesk\Invoices\Documents\Pdf, \FakturowniaVendor\WPDesk\Invoices\Documents\Creator
{
    use HookableParent;
    /**
     * @var Type[];
     */
    protected $supportedDocumentTypes = array();
    /**
     * Api Client.
     *
     * @var Client
     */
    protected $apiClient;
    /**
     * @var string
     */
    protected $metaBoxTitle;
    /**
     * @var string
     */
    protected $id = 'invoices';
    /**
     * @var OrderMetaBox
     */
    private $orderMetaBox;
    /**
     * @var AjaxGetPdfHandler
     */
    private $ajaxGetPdfHandler;
    /**
     * @var AjaxCreateHandler
     */
    private $ajaxCreateHandler;
    /**
     * @var AjaxEmailHandler
     */
    private $ajaxEmailHandler;
    /**
     * Integration constructor.
     *
     * @TODO: give some method to override/decorate classes in hookable in child classes
     *
     * @param string $id Integration ID.
     * @param string $metaboxTitle Meta box title.
     * @param string      $createdLabel
     * @param string      $createdLabelNone
     * @param string      $emailButtonAltText
     * @param string      $confirmationMessage
     */
    public function __construct($id, $metaboxTitle = 'Invoices', $createdLabel = 'Created', $createdLabelNone = 'None', $emailButtonAltText = 'None', $confirmationMessage = 'Document %1$s already created! Overwrite?')
    {
        $this->id = $id;
        $this->metaBoxTitle = $metaboxTitle;
        $this->initApiClient();
        $this->orderMetaBox = new \FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox($this, $metaboxTitle, $createdLabel, $createdLabelNone, $emailButtonAltText, $confirmationMessage);
        $this->add_hookable($this->orderMetaBox);
        $this->ajaxCreateHandler = new \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxCreateHandler($this->id, $this->orderMetaBox, $this);
        $this->add_hookable($this->ajaxCreateHandler);
        $this->ajaxGetPdfHandler = new \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler($this->id, $this->orderMetaBox, $this);
        $this->add_hookable($this->ajaxGetPdfHandler);
        $this->ajaxEmailHandler = new \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxEmailHandler($this->id, $this->orderMetaBox, $this);
        $this->add_hookable($this->ajaxEmailHandler);
        $this->orderMetaBox->setAjaxCreateHandler($this->ajaxCreateHandler);
        $this->orderMetaBox->setAjaxEmailHandler($this->ajaxEmailHandler);
        $this->orderMetaBox->setAjaxGetPdfHandler($this->ajaxGetPdfHandler);
        $this->initFields();
        $this->initOrderColumn();
        $this->initSupportedDocumentTypes();
        $this->initDocumentCreators();
        $this->initIntegrations();
        $this->add_hookable(new \FakturowniaVendor\WPDesk\Invoices\Order\MyAccount($this->getSupportedDocumentTypes()));
    }
    /**
     * @return void
     */
    public function hooks()
    {
        $this->hooks_on_hookable_objects();
    }
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return AjaxGetPdfHandler
     */
    public function getAjaxGetPdfHandler()
    {
        return $this->ajaxGetPdfHandler;
    }
    /**
     * @return Type[]
     */
    public abstract function initSupportedDocumentTypes();
    /**
     * @param Type $documentType
     *
     * @return void
     */
    public function addSupportedDocumentType($documentType)
    {
        $this->supportedDocumentTypes[$documentType->getTypeName()] = $documentType;
    }
    /**
     * @return Type[]
     */
    public function getSupportedDocumentTypes()
    {
        return $this->supportedDocumentTypes;
    }
    /**
     * @param $typeName
     * @return Type|bool
     */
    public function getSupportedDocumentType($typeName)
    {
        if (isset($this->supportedDocumentTypes[$typeName])) {
            return $this->supportedDocumentTypes[$typeName];
        }
        return \false;
    }
    /**
     * @return Client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }
    /**
     * @param Client $apiClient
     */
    public function setApiClient($apiClient)
    {
        $this->apiClient = $apiClient;
    }
    /**
     * Init API Client
     *
     * @return Client
     */
    public abstract function initApiClient();
    /**
     * Inits checkout fields.
     *
     * @return FormField[]
     */
    public abstract function initFields();
    /**
     * Inits order column.
     *
     * @return OrderColumn
     */
    public abstract function initOrderColumn();
    /**
     * Inits document creators.
     *
     * @return AbstractCreator[]
     */
    public abstract function initDocumentCreators();
    /**
     * Inits integrations.
     *
     * @return Hookable[]
     */
    public abstract function initIntegrations();
    /**
     * @param \WC_Order $order
     * @param Type $documentType
     *
     * @return OrderDefaults
     */
    public abstract function prepareOrderDefaults($order, \FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType);
    /**
     * Create document for order.
     *
     * @param \WC_Order     $order Order.
     * @param array         $data Posted data.
     * @param bool          $overwriteExisting Overwrite already created.
     *
     * @throws \Exception
     */
    public abstract function createDocumentForOrder($order, array $data, $overwriteExisting = \true);
    /**
     * Get Document PDF.
     * @param MetadataContent $metadataContent
     *
     * @return string
     */
    public abstract function getDocumentPdf(\FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent);
    /**
     * @return bool
     */
    public function isDebugEnabled()
    {
        return \false;
    }
}
