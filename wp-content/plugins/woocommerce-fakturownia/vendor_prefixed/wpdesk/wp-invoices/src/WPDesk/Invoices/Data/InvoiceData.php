<?php

namespace FakturowniaVendor\WPDesk\Invoices\Data;

use FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem;
use FakturowniaVendor\WPDesk\Invoices\Field\VatNumber;
/**
 * Class InvoiceData
 *
 * Invoice data.
 * @package WPDesk\Invoices
 */
class InvoiceData
{
    /**
     * @var ClientData
     */
    private $clientData;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var InvoiceItem[]
     */
    private $items = array();
    /**
     * @var bool
     */
    private $pricesIncludeTax;
    /**
     * @var float
     */
    private $paidAmount = 0.0;
    /**
     * @var string
     */
    private $saleDate;
    /**
     * @var string
     */
    private $issueDate;
    /**
     * @var string
     */
    private $paymentMethod;
    /**
     * @var bool
     */
    private $zeroTax = \true;
    /**
     * Create from order.
     *
     * @param \WC_Order $order Order.
     * @param VatNumber|null $vatNumberField Vat Number field.
     * @param string $itemNameAttribute Attribute name for item name.
     *
     * @return InvoiceData
     */
    public static function createFromOrder($order, $vatNumberField, $itemNameAttribute)
    {
        $invoiceData = new self();
        $invoiceData->setClientData(\FakturowniaVendor\WPDesk\Invoices\Data\ClientData::createFromOrder($order, $vatNumberField));
        $invoiceData->setPricesIncludeTax(\wc_prices_include_tax());
        $invoiceData->setCurrency($order->get_currency());
        $items = $order->get_items(array(\FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem::ITEM_TYPE_LINE_ITEM, \FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem::ITEM_TYPE_FEE, \FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem::ITEM_TYPE_SHIPPING));
        foreach ($items as $item) {
            $invoiceItem = \FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem::createFromOrderItem($item, $itemNameAttribute);
            $invoiceData->addItem($invoiceItem);
            if (0.0 !== \floatval($invoiceItem->getTaxRate())) {
                $invoiceData->setZeroTax(\false);
            }
        }
        if ($order->is_paid()) {
            $invoiceData->setPaidAmount(\floatval($order->get_total()));
        }
        $invoiceData->setSaleDate(\date('Y-m-d', \current_time('timestamp')));
        $invoiceData->setIssueDate(\date('Y-m-d', \current_time('timestamp')));
        $invoiceData->setPaymentMethod($order->get_payment_method());
        return $invoiceData;
    }
    /**
     * Add item.
     *
     * @param InvoiceItem $item Item.
     */
    protected function addItem(\FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem $item)
    {
        $this->items[] = $item;
    }
    /**
     * @return InvoiceItem[]
     */
    public function getItems()
    {
        return $this->items;
    }
    /**
     * @param InvoiceItem[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }
    /**
     * @return ClientData
     */
    public function getClientData()
    {
        return $this->clientData;
    }
    /**
     * @param ClientData $clientData
     */
    public function setClientData($clientData)
    {
        $this->clientData = $clientData;
    }
    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
    /**
     * @return bool
     */
    public function isPricesIncludeTax()
    {
        return $this->pricesIncludeTax;
    }
    /**
     * @param bool $pricesIncludeTax
     */
    public function setPricesIncludeTax($pricesIncludeTax)
    {
        $this->pricesIncludeTax = $pricesIncludeTax;
    }
    /**
     * @return float
     */
    public function getPaidAmount()
    {
        return $this->paidAmount;
    }
    /**
     * @param float $paidAmount
     */
    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    }
    /**
     * @return string
     */
    public function getSaleDate()
    {
        return $this->saleDate;
    }
    /**
     * @param string $saleDate
     */
    public function setSaleDate($saleDate)
    {
        $this->saleDate = $saleDate;
    }
    /**
     * @return string
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }
    /**
     * @param string $issueDate
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;
    }
    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }
    /**
     * @return bool
     */
    public function isZeroTax()
    {
        return $this->zeroTax;
    }
    /**
     * @param bool $zeroTax
     */
    public function setZeroTax($zeroTax)
    {
        $this->zeroTax = $zeroTax;
    }
}
