<?php

namespace FakturowniaVendor\WPDesk\Invoices\Ajax;

use FakturowniaVendor\WPDesk\Invoices\Documents\Pdf;
use FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * Handles Ajax send email with document action.
 *
 * @package WPDesk\Invoices
 */
class AjaxEmailHandler extends \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxHandler implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var string
     */
    private $ajaxActionName = 'wpdesk_invoices_email';
    /**
     * @var Pdf
     */
    private $documentPdf;
    /**
     * AjaxEmailHandler constructor.
     *
     * @param string       $integration_id
     * @param OrderMetaBox $orderMetaBox
     * @param Pdf          $documentPdf
     */
    public function __construct($integration_id, \FakturowniaVendor\WPDesk\Invoices\Metabox\OrderMetaBox $orderMetaBox, \FakturowniaVendor\WPDesk\Invoices\Documents\Pdf $documentPdf)
    {
        parent::__construct($integration_id, $orderMetaBox);
        $this->documentPdf = $documentPdf;
    }
    /**
     * Hooks.
     */
    public function hooks()
    {
        \add_action('wp_ajax_' . $this->ajaxActionName, array($this, 'handleAjaxRequest'));
    }
    /**
     * @inheritDoc
     */
    public function getAjaxActionName()
    {
        return $this->ajaxActionName;
    }
    /**
     * Handle AJAX request.
     */
    public function handleAjaxRequest()
    {
        if (!$this->isCurrentIntegration()) {
            return;
        }
        $response = array(self::RESPONSE_STATUS => 'error');
        $nonce = $this->getRequestValue('nonce');
        $orderId = $this->getRequestValue('order_id');
        $metaData = $this->getRequestValue('metadata');
        $emailClass = $this->getRequestValue('email_class');
        $fileName = $this->getRequestValue('file_name', 'Document.pdf');
        if (\wp_verify_nonce($nonce, $this->ajaxActionName . $orderId . $metaData)) {
            $response[self::RESPONSE_MESSAGE] = 'Ups! Something go wrong!';
            $order = \wc_get_order($orderId);
            if ($order) {
                try {
                    $metadataContent = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($metaData, $order);
                    $metahash = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($metaData . '_hash', $order);
                    $download_url = \add_query_arg(['order_id' => $order->get_id(), 'type' => $metaData, 'invoice_download' => $metahash->get()], \get_site_url());
                    $mailer = \WC()->mailer();
                    $emails = $mailer->get_emails();
                    if (!empty($emails) && !empty($emails[$emailClass])) {
                        $pfdContent = $this->documentPdf->getDocumentPdf($metadataContent);
                        $emails[$emailClass]->trigger($order, $pfdContent, $fileName, $download_url);
                        $response[self::RESPONSE_CONTENT] = $this->orderMetaBox->getContent($order);
                        $response[self::RESPONSE_STATUS] = 'ok';
                        unset($response[self::RESPONSE_MESSAGE]);
                    } else {
                        $response[self::RESPONSE_MESSAGE] = 'Email class not found!';
                    }
                } catch (\Exception $e) {
                    $response[self::RESPONSE_CONTENT] = $this->orderMetaBox->getContent($order);
                    $response[self::RESPONSE_MESSAGE] = $e->getMessage();
                }
            } else {
                $response[self::RESPONSE_MESSAGE] = 'Order not found!';
            }
        } else {
            $response[self::RESPONSE_MESSAGE] = 'Invalid nonce!';
        }
        \wp_send_json($response);
    }
}
