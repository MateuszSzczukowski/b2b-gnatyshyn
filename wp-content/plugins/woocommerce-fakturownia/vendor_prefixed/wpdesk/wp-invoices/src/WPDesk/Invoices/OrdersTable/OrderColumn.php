<?php

namespace FakturowniaVendor\WPDesk\Invoices\OrdersTable;

use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Field\InvoiceAsk;
use FakturowniaVendor\WPDesk\Invoices\Integration;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
class OrderColumn implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var Integration
     */
    protected $integration;
    /**
     * @var string
     */
    protected $columnTitle;
    /**
     * @var InvoiceAsk
     */
    protected $invoiceAskField;
    /**
     * @var AjaxGetPdfHandler
     */
    protected $ajaxGetPdfHandler;
    /**
     * OrderMetaBox constructor.
     *
     * @param Integration     $integration
     * @param string          $columnTitle
     * @param InvoiceAsk|null $invoiceAskField
     * @param AjaxGetPdfHandler $ajaxGetPdfHandler
     */
    public function __construct(\FakturowniaVendor\WPDesk\Invoices\Integration $integration, $columnTitle, $invoiceAskField, \FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler $ajaxGetPdfHandler)
    {
        $this->integration = $integration;
        $this->columnTitle = $columnTitle;
        $this->invoiceAskField = $invoiceAskField;
        $this->ajaxGetPdfHandler = $ajaxGetPdfHandler;
    }
    /**
     * Hooks.
     */
    public function hooks()
    {
        \add_filter('manage_edit-shop_order_columns', array($this, 'orderColumns'), 11);
        \add_action('manage_shop_order_posts_custom_column', array($this, 'orderColumnContent'), 11);
    }
    /**
     * Add orders column.
     *
     * @param array $columns Columns.
     *
     * @return array
     */
    public function orderColumns(array $columns)
    {
        $cols2 = array();
        foreach ($columns as $key => $title) {
            if ($key == 'order_actions') {
                $cols2[$this->integration->getId()] = $this->columnTitle;
            }
            $cols2[$key] = $title;
        }
        if (empty($cols2[$this->integration->getId()])) {
            $cols2[$this->integration->getId()] = $this->columnTitle;
        }
        return $cols2;
    }
    /**
     * Display column content.
     *
     * @param string $column Column.
     */
    public function orderColumnContent($column)
    {
        if ($column === $this->integration->getId()) {
            global $the_order;
            $order = $the_order;
            $documentsRendered = $this->renderDocumentsContent($order);
            if ($documentsRendered === 0 && null != $this->invoiceAskField) {
                $this->maybeRenderInvoiceAskField($order);
            }
        }
    }
    /**
     * Maybe render invoice ask field.
     *
     * @param \WC_Order $order
     */
    private function maybeRenderInvoiceAskField($order)
    {
        $invoiceAskFieldValue = $this->invoiceAskField->getFromOrder($order);
        if ($invoiceAskFieldValue) {
            $label = $this->invoiceAskField->getLabel();
            include __DIR__ . '/views/html-column-created-document-invoice-ask.php';
        }
    }
    /**
     * Render documents for order.
     *
     * @param \WC_Order $order
     *
     * @return int Rendered documents count.
     */
    private function renderDocumentsContent($order)
    {
        $supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();
        $documentsRendered = 0;
        foreach ($supportedDocumentTypes as $supportedDocumentType) {
            $metaDataContent = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($supportedDocumentType->getMetaDataName(), $order);
            if ($supportedDocumentType->isMetadataContentValidForDocumentType($metaDataContent)) {
                $this->renderColumnDocumentContentForOrder($supportedDocumentType, $order, $metaDataContent);
                $documentsRendered++;
            }
        }
        return $documentsRendered;
    }
    /**
     * Render column document content.
     *
     * @param Type $documentType
     * @param \WC_Order $order
     * @param MetadataContent $metadataContent
     */
    public function renderColumnDocumentContentForOrder(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType, \WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent)
    {
        $documentMetadata = $documentType->prepareDocumentMetadata($metadataContent);
        $typeNameLabel = $documentMetadata->getTypeNameLabel();
        if ($documentMetadata->isError()) {
            if ($documentType->isAllowedForOrder($order)) {
                $errorMessage = $documentMetadata->getErrorMessage();
                include __DIR__ . '/views/html-column-created-document-error.php';
            }
        } else {
            $getPdfUrl = $this->ajaxGetPdfHandler->getAjaxActionUrlForMetadata($documentMetadata);
            $fileName = $this->ajaxGetPdfHandler->getPdfFilenameForMetadata($documentMetadata);
            $number = $documentMetadata->getNumber();
            if ($documentMetadata->isWarning()) {
                $linkClass = 'tips invoice-link-warning';
                $linkTip = $documentMetadata->getWarningMessage();
            } else {
                $linkClass = '';
                $linkTip = '';
            }
            include __DIR__ . '/views/html-column-created-document.php';
        }
    }
}
