<?php

namespace FakturowniaVendor;

/**
 * @var string $typeNameLabel
 * @var string $number
 * @var string $getPdfUrl
 * @var string $linkClass
 * @var string $linkTip
 */
if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?><div>
    <strong><?php 
echo $typeNameLabel;
?>:</strong>
    <a
        href="<?php 
echo \esc_attr($getPdfUrl);
?>"
        class="<?php 
echo \esc_attr($linkClass);
?>"
        title="<?php 
echo \esc_attr($linkTip);
?>"
        data-tip="<?php 
echo \esc_attr($linkTip);
?>"
        target="_blank"
    ><?php 
echo $number;
?></a>
</div>
<?php 
