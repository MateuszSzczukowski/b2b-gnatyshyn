<?php

namespace FakturowniaVendor\WPDesk\Invoices\Documents;

use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
/**
 * Document pdf.
 *
 * @package WPDesk\Invoices\Documents
 */
interface Pdf
{
    /**
     * Get Document PDF.
     * @param MetadataContent $metadataContent
     *
     * @return string
     */
    public function getDocumentPdf(\FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent);
}
