<?php

namespace FakturowniaVendor\WPDesk\Invoices\WooCommerce;

use FakturowniaVendor\WPDesk\ApiClient\Client\Client;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
trait IntegrationTrait
{
    /**
     * @var Type[];
     */
    protected $supportedDocumentTypes = array();
    /**
     * Api Client.
     *
     * @var Client
     */
    protected $apiClient;
    /**
     * @param Type $documentType
     *
     * @return mixed
     */
    public function addSupportedDocumentType(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType)
    {
        $this->supportedDocumentTypes[] = $documentType;
    }
    /**
     * @return Type[]
     */
    public function getSupportedDocumentTypes()
    {
        return $this->supportedDocumentTypes;
    }
}
