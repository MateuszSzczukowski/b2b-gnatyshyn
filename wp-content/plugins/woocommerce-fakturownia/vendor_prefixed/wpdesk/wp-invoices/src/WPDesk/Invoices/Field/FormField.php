<?php

namespace FakturowniaVendor\WPDesk\Invoices\Field;

use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
class FormField implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    const PRIORITY_AFTER_FLEXIBLE_CHECKOUT_FIELDS = 999999;
    /**
     * Field ID.
     *
     * @var string
     */
    private $fieldId;
    /**
     * Order meta field ID.
     *
     * @var string
     */
    private $orderMetaFieldId;
    /**
     * Text domain.
     *
     * @var string
     */
    private $textDomain;
    /**
     * Checkout field ID.
     *
     * @var string
     */
    private $checkoutFieldId;
    /**
     * @var string
     */
    private $addFieldAfter = 'billing_company';
    /**
     * @var string
     */
    private $addAdminFieldAfter = 'company';
    /**
     * @var bool
     */
    private $excludeFromCheckout = \false;
    /**
     * FormField constructor.
     *
     * @param string $fieldId Field ID.
     */
    public function __construct($fieldId)
    {
        $this->fieldId = $fieldId;
        $this->orderMetaFieldId = '_billing_' . $fieldId;
        $this->checkoutFieldId = 'billing_' . $fieldId;
    }
    /**
     * Hooks.
     */
    public function hooks()
    {
        if (!$this->excludeFromCheckout) {
            \add_filter('woocommerce_billing_fields', array($this, 'appendFieldToWCBillingFields'));
            \add_action('woocommerce_checkout_create_order', array($this, 'addMetaDataToOrder'), 10, 2);
            \add_filter('woocommerce_order_formatted_billing_address', array($this, 'addFieldToFormattedBillingAddress'), self::PRIORITY_AFTER_FLEXIBLE_CHECKOUT_FIELDS, 2);
            \add_filter('woocommerce_localisation_address_formats', array($this, 'addFieldToLocalisationAddressFormats'), 11);
            \add_filter('woocommerce_formatted_address_replacements', array($this, 'addAddressReplacements'), 11, 2);
        }
        \add_filter('woocommerce_admin_billing_fields', array($this, 'addAdminBillingField'));
    }
    /**
     * @return bool
     */
    public function isExcludeFromCheckout()
    {
        return $this->excludeFromCheckout;
    }
    /**
     * @param bool $excludeFromCheckout
     */
    public function setExcludeFromCheckout($excludeFromCheckout)
    {
        $this->excludeFromCheckout = $excludeFromCheckout;
    }
    /**
     * Get value from Order.
     *
     * @param \WC_Order $order Order.
     *
     * @return mixed
     */
    public function getFromOrder(\WC_Order $order)
    {
        return $order->get_meta($this->orderMetaFieldId);
    }
    /**
     * Add billing field.
     *
     * @param array $fields Fields.
     * @return array
     */
    public function appendFieldToWCBillingFields(array $fields)
    {
        $added = \false;
        $new_fields = array();
        foreach ($fields as $field_id => $field) {
            $new_fields[$field_id] = $field;
            if ($field_id === $this->addFieldAfter) {
                $added = \true;
                $field_priority = null;
                if (isset($field['priority']) && \is_numeric($field['priority'])) {
                    $field_priority = \intval($field['priority']);
                }
                $checkoutField = $this->prepareCheckoutField($field_priority);
                if (\is_array($checkoutField)) {
                    $new_fields[$this->checkoutFieldId] = $checkoutField;
                }
            }
        }
        if (!$added) {
            $checkoutField = $this->prepareCheckoutField();
            if (\is_array($checkoutField)) {
                $new_fields[$this->checkoutFieldId] = $checkoutField;
            }
        }
        return $new_fields;
    }
    /**
     * Prepare checkout field.
     *
     * @param null|int $field_priority Field priority
     * @return bool|array
     */
    protected function prepareCheckoutField($field_priority = null)
    {
        return \false;
    }
    /**
     * Add metadata to order.
     *
     * @param \WC_Order $order Order.
     * @param array     $data Data.
     */
    public function addMetaDataToOrder(\WC_Order $order, array $data)
    {
        if (isset($data[$this->checkoutFieldId])) {
            $order->update_meta_data($this->orderMetaFieldId, $data[$this->checkoutFieldId]);
        }
    }
    /**
     * Add admin billing field.
     *
     * @param array $fields
     * @return array
     */
    public function addAdminBillingField(array $fields)
    {
        $added = \false;
        $new_fields = array();
        foreach ($fields as $field_id => $field) {
            $new_fields[$field_id] = $field;
            if ($field_id === $this->addAdminFieldAfter) {
                $added = \true;
                $new_fields[$this->fieldId] = $this->prepareAdminField();
            }
        }
        if (!$added) {
            $new_fields[$this->fieldId] = $this->prepareAdminField();
        }
        return $new_fields;
    }
    /**
     * @param array     $fields
     * @param \WC_Order $order
     * @return array
     */
    public function addFieldToFormattedBillingAddress($fields, $order)
    {
        $fields[$this->fieldId] = $order->get_meta($this->getOrderMetaFieldId());
        return $fields;
    }
    /**
     * Add fields to address template. Tries to add them only once.
     *
     * @param array $formats
     *
     * @return array
     */
    public function addFieldToLocalisationAddressFormats($formats)
    {
        $key_value = "{" . $this->fieldId . "}";
        foreach ($formats as $country => $val) {
            if (\stripos($formats[$country], $key_value) === \false) {
                $formats[$country] = $val . "\n" . $key_value;
            }
        }
        return $formats;
    }
    /**
     * @param array $fields
     * @param array $args
     *
     * @return array
     */
    public function addAddressReplacements($fields, $args)
    {
        $fields['{' . $this->fieldId . '}'] = $args[$this->fieldId];
        return $fields;
    }
    /**
     * Prepare admin field.
     *
     * @return bool|array
     */
    protected function prepareAdminField()
    {
        return \false;
    }
    /**
     * @return string
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }
    /**
     * @return string
     */
    public function getOrderMetaFieldId()
    {
        return $this->orderMetaFieldId;
    }
    /**
     * @return string
     */
    public function getTextDomain()
    {
        return $this->textDomain;
    }
    /**
     * @return string
     */
    public function getCheckoutFieldId()
    {
        return $this->checkoutFieldId;
    }
    /**
     * @return string
     */
    public function getAddFieldAfter()
    {
        return $this->addFieldAfter;
    }
    /**
     * @return string
     */
    public function getAddAdminFieldAfter()
    {
        return $this->addAdminFieldAfter;
    }
}
