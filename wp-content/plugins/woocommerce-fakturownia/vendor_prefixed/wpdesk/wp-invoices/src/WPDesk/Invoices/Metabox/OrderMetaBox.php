<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox;

use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxCreateHandler;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxEmailHandler;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Integration;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
class OrderMetaBox implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var Integration
     */
    protected $integration;
    /**
     * @var string
     */
    protected $metaBoxTitle;
    /**
     * @var string
     */
    protected $createdLabel;
    /**
     * @var string
     */
    protected $createdLabelNone;
    /**
     * @var string
     */
    protected $emailButtonAltText;
    /**
     * @var string
     */
    protected $confirmationMessage;
    /**
     * @var AjaxGetPdfHandler
     */
    protected $ajaxGetPdfHandler;
    /**
     * @var AjaxEmailHandler
     */
    protected $ajaxEmailHandler;
    /**
     * @var AjaxCreateHandler
     */
    protected $ajaxCreateHandler;
    /**
     * OrderMetaBox constructor.
     *
     * @param Integration $integration
     * @param string      $metaBoxTitle
     * @param string      $createdLabel
     * @param string      $createdLabelNone
     * @param string      $emailButtonAltText
     * @param string      $confirmationMessage
     */
    public function __construct(\FakturowniaVendor\WPDesk\Invoices\Integration $integration, $metaBoxTitle, $createdLabel = 'Created', $createdLabelNone = 'None', $emailButtonAltText = 'Not set!', $confirmationMessage = 'Document %1$s already created! Overwrite?')
    {
        $this->integration = $integration;
        $this->metaBoxTitle = $metaBoxTitle;
        $this->createdLabel = $createdLabel;
        $this->createdLabelNone = $createdLabelNone;
        $this->emailButtonAltText = $emailButtonAltText;
        $this->confirmationMessage = $confirmationMessage;
    }
    /**
     * Hooks.
     */
    public function hooks()
    {
        \add_action('add_meta_boxes_shop_order', array($this, 'addMetaBox'), 11);
        \add_action('admin_head', array($this, 'addMetaBoxScriptToHeader'), 11);
        \add_action('admin_head', array($this, 'addMetaBoxCssToHeader'), 11);
    }
    /**
     * @param AjaxGetPdfHandler $ajaxGetPdfHandler
     */
    public function setAjaxGetPdfHandler(\FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler $ajaxGetPdfHandler)
    {
        $this->ajaxGetPdfHandler = $ajaxGetPdfHandler;
    }
    /**
     * @param AjaxEmailHandler $ajaxEmailHandler
     */
    public function setAjaxEmailHandler(\FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxEmailHandler $ajaxEmailHandler)
    {
        $this->ajaxEmailHandler = $ajaxEmailHandler;
    }
    /**
     * @param AjaxCreateHandler $ajaxCreateHandler
     */
    public function setAjaxCreateHandler(\FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxCreateHandler $ajaxCreateHandler)
    {
        $this->ajaxCreateHandler = $ajaxCreateHandler;
    }
    /**
     * Unique metabox id (also for div#).
     *
     * @return string
     */
    private function getMetaboxId()
    {
        return 'invoices_' . $this->integration->getId();
    }
    /**
     * Ads order metabox.
     */
    public function addMetaBox()
    {
        \add_meta_box($this->getMetaboxId(), $this->metaBoxTitle, array($this, 'render'), 'shop_order', 'side', 'low');
    }
    /**
     * Render metabox.
     *
     * @param \WP_Post $post
     */
    public function render($post)
    {
        global $thepostid;
        $thepostid = $post->ID;
        $order = \wc_get_order($post->ID);
        $supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();
        $createdLabel = $this->createdLabel;
        $createdLabelNone = $this->createdLabelNone;
        $createdDivId = 'created-' . $this->integration->getId();
        include __DIR__ . '/views/html-metabox-created.php';
        $rendered = \false;
        foreach ($supportedDocumentTypes as $supportedDocumentType) {
            $metaDataContent = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($supportedDocumentType->getMetaDataName(), $order);
            if ($supportedDocumentType->isMetadataContentValidForDocumentType($metaDataContent)) {
                $this->renderMetaBoxDocumentContentForOrder($supportedDocumentType, $order, $metaDataContent);
                $rendered = \true;
                if ($this->integration->isDebugEnabled()) {
                    $log_name = $supportedDocumentType->getMetaDataName() . '_log';
                    $debug_log = $order->get_meta($log_name, \true);
                    include __DIR__ . '/views/html-debug.php';
                }
            }
        }
        if (!$rendered) {
            include __DIR__ . '/views/html-metabox-created-none.php';
        }
        include __DIR__ . '/views/html-metabox-created-footer.php';
        foreach ($supportedDocumentTypes as $supportedDocumentType) {
            if ($supportedDocumentType->isAllowedForOrder($order)) {
                $metaDataContent = new \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent($supportedDocumentType->getMetaDataName(), $order);
                $this->renderMetaBoxCreateDocumentContentForOrder($supportedDocumentType, $order, $metaDataContent);
            }
        }
    }
    /**
     * Get metabox content.
     *
     * @param \WC_Order $order
     *
     * @return string
     */
    public function getContent(\WC_Order $order)
    {
        \ob_start();
        $this->render(\get_post($order->get_id()));
        $out = \ob_get_contents();
        \ob_end_clean();
        return $out;
    }
    /**
     * Render meta box content for order. This part of metabox created document.
     *
     * @param Type $documentType
     * @param \WC_Order $order
     * @param MetadataContent $metadataContent
     */
    private function renderMetaBoxDocumentContentForOrder(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType, $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent)
    {
        $documentMetadata = $documentType->prepareDocumentMetadata($metadataContent);
        $typeNameLabel = $documentMetadata->getTypeNameLabel();
        if ($documentMetadata->isError()) {
            if ($documentType->isAllowedForOrder($order)) {
                $errorMessage = $documentMetadata->getErrorMessage();
                include __DIR__ . '/views/html-metabox-created-document-error.php';
            }
        } else {
            $getPdfUrl = $this->ajaxGetPdfHandler->getAjaxActionUrlForMetadata($documentMetadata);
            $fileName = $this->ajaxGetPdfHandler->getPdfFilenameForMetadata($documentMetadata);
            $number = $documentMetadata->getNumber();
            $buttonEmailLabelAlt = $this->emailButtonAltText;
            $orderId = $order->get_id();
            $metadata = $metadataContent->getMetaDataName();
            $emailClass = $documentType->getEmailClass();
            $emailNonce = \wp_create_nonce($this->ajaxEmailHandler->getAjaxActionName() . $order->get_id() . $metadata);
            if ($documentMetadata->isWarning()) {
                $linkClass = 'tips invoice-link-warning';
                $linkTip = $documentMetadata->getWarningMessage();
            } else {
                $linkClass = '';
                $linkTip = '';
            }
            include __DIR__ . '/views/html-metabox-created-document.php';
        }
    }
    /**
     * Render meta box content for order. This part of metabox contains buttons for create document action.
     *
     * @param Type $documentType
     * @param \WC_Order $order Order.
     * @param MetadataContent $metadataContent Meta data.
     *
     */
    private function renderMetaBoxCreateDocumentContentForOrder(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType, \WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent)
    {
        $dataType = $documentType->getTypeName();
        $divId = 'invoice-document-' . $documentType->getTypeName();
        $buttonId = 'create-document-' . $documentType->getTypeName();
        $buttonLabelAlt = $documentType->getMetaBoxCreateButtonLabel();
        $buttonLabel = $documentType->getMetaBoxCreateButtonLabel();
        $parametersId = 'parameters-' . $documentType->getTypeName();
        $parmaetersLabel = $documentType->getParametersLabel();
        $parametersDivId = 'create-document-div-' . $documentType->getTypeName();
        $orderId = $order->get_id();
        $nonce = \wp_create_nonce($this->ajaxCreateHandler->getAjaxActionName() . $orderId);
        $confirmationMessage = '';
        $documentMetadata = $documentType->prepareDocumentMetadata($metadataContent);
        if (!empty($documentMetadata->getId())) {
            $confirmationMessage = \sprintf($this->confirmationMessage, $documentMetadata->getNumber());
        }
        include __DIR__ . '/views/html-metabox-create-button.php';
        include __DIR__ . '/views/html-metabox-parameters-header.php';
        $orderDefaults = $this->integration->prepareOrderDefaults($order, $documentType);
        foreach ($documentType->getOrderMetaboxFields()->getMetaBoxFields() as $metaBoxField) {
            $metaBoxField->render($order, $metadataContent, $orderDefaults);
        }
        include __DIR__ . '/views/html-metabox-parameters-footer.php';
    }
    /**
     * Adds meta box script to admin header.
     */
    public function addMetaBoxScriptToHeader()
    {
        $ajaxUrl = \admin_url('admin-ajax.php');
        $ajaxAction = $this->ajaxCreateHandler->getAjaxActionName();
        $ajaxActionEmail = $this->ajaxEmailHandler->getAjaxActionName();
        $integrationId = $this->integration->getId();
        $metaboxId = $this->getMetaboxId();
        include __DIR__ . '/views/html-metabox-script.php';
    }
    /**
     * Add MetaBox CSS to admin header.
     */
    public function addMetaBoxCssToHeader()
    {
        include __DIR__ . '/views/html-metabox-css.php';
    }
}
