<?php

namespace FakturowniaVendor\WPDesk\Invoices\WooCommerce;

use FakturowniaVendor\WPDesk\Invoices\Exception\DocumentAlreadyExistsException;
use FakturowniaVendor\WPDesk\Mutex\WordpressMySQLLockMutex;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
class DocumentCreatorForOrderStatus implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * @var \WPDesk\Invoices\Integration
     */
    private $integration;
    /**
     * @var string
     */
    private $orderStatus;
    /**
     * @var bool
     */
    private $proforma;
    /**
     * DocumentCreatorForOrderStatus constructor.
     *
     * @param \WPDesk\Invoices\Integration $integration Integration.
     * @param string|array                 $orderStatus Order status.
     * @param bool                         $proforma Create proforma.
     */
    public function __construct($integration, $orderStatus, $proforma)
    {
        $this->integration = $integration;
        if (!\is_array($orderStatus)) {
            $orderStatus = array($orderStatus);
        }
        $this->orderStatus = $orderStatus;
        foreach ($this->orderStatus as $key => $orderStatus) {
            $this->orderStatus[$key] = \str_replace('wc-', '', $orderStatus);
        }
        $this->proforma = $proforma;
    }
    public function hooks()
    {
        foreach ($this->orderStatus as $orderStatus) {
            \add_action('woocommerce_order_status_' . $orderStatus, array($this, 'maybeCreateDocument'), 10, 2);
        }
    }
    /**
     * Maybe create document.
     *
     * @param int       $order_id Order ID.
     * @param \WC_Order $order Order.
     */
    public function maybeCreateDocument($order_id, $order)
    {
        $mutex = \FakturowniaVendor\WPDesk\Mutex\WordpressMySQLLockMutex::fromOrder($order, '_invoice', 30);
        $mutex->acquireLock();
        $supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();
        foreach ($supportedDocumentTypes as $supportedDocumentType) {
            $creator = $supportedDocumentType->getCreator();
            if ($this->proforma === $supportedDocumentType->isProforma() && $supportedDocumentType->isAllowedForOrder($order) && $creator->isAutoCreateAllowedForOrder($order)) {
                try {
                    $creator->createDocumentForOrder($order, array(), \false);
                } catch (\Exception $e) {
                    // do nothing.
                }
            }
        }
        $mutex->releaseLock();
    }
}
