<?php

namespace FakturowniaVendor\WPDesk\Invoices\Email;

class StringAttachments
{
    /**
     * @var array
     */
    private $attachments;
    /**
     * StringAttachments constructor.
     *
     * @param array $attachments
     */
    public function __construct(array $attachments)
    {
        $this->attachments = $attachments;
    }
    /**
     * Add wordpress action.
     */
    public function addAction()
    {
        \add_action('phpmailer_init', array($this, 'addStringAttachments'));
    }
    /**
     * Remove wordpress action.
     */
    public function removeAction()
    {
        \remove_action('phpmailer_init', array($this, 'addStringAttachments'));
    }
    /**
     * Add attachemts to mail from string.
     *
     * @param \PHPMailer $phpmailer
     */
    public function addStringAttachments($phpmailer)
    {
        foreach ($this->attachments as $attachment) {
            $phpmailer->addStringAttachment($attachment['content'], $attachment['fileName']);
        }
    }
}
