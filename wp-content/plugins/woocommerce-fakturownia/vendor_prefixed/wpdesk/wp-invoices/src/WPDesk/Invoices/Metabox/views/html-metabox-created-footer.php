<?php

namespace FakturowniaVendor;

if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?>
    <div class="clear"></div>
</div>
<?php 
