<?php

namespace FakturowniaVendor;

/**
 * @var string $divId
 * @var string $buttonId
 * @var string $buttonLabelAlt
 * @var string $dataType
 * @var string $orderId
 * @var string $confirmationMessage
 * @var string $buttonLabel
 * @var string $parametersId
 * @var string $parmaetersLabel
 */
if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?><div class="invoice-document" id="<?php 
echo $divId;
?>">
    <a
        id="<?php 
echo $buttonId;
?>"
        href="#"
        class="button tips invoice-create-button"
        title="<?php 
echo $buttonLabelAlt;
?>"
        data-tip="<?php 
echo $buttonLabelAlt;
?>"
        data-type="<?php 
echo $dataType;
?>"
        data-order_id="<?php 
echo $orderId;
?>"
        data-nonce="<?php 
echo $nonce;
?>"
        data-confirmation="<?php 
echo \esc_attr($confirmationMessage);
?>"
    ><?php 
echo $buttonLabel;
?></a>
    <span class="invoice-parameters"><a
            href="#"
            id="<?php 
echo $parametersId;
?>"
            data-type="<?php 
echo $dataType;
?>"
        ><?php 
echo $parmaetersLabel;
?> <span class="invoice-expand-parameters">[+]</span></a></span>
</div>
<?php 
