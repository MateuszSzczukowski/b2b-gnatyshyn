<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metadata;

class DocumentMetadata
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $typeName;
    /**
     * @var string
     */
    private $typeNameLabel;
    /**
     * @var string
     */
    private $number;
    /**
     * @var int
     */
    private $code;
    /**
     * @var string
     */
    private $errorMessage;
    /**
     * @var MetadataContent
     */
    private $metadataContent;
    /**
     * @var string
     */
    private $warningMessage;
    /**
     * DocumentMetadata constructor.
     *
     * @param MetadataContent $metadataContent Metadata content.
     */
    public function __construct(\FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent)
    {
        $this->metadataContent = $metadataContent;
        $this->typeName = 'invoice';
        $this->typeNameLabel = 'invoice:';
        $this->id = 'not set';
        $this->number = 'not set';
        $this->code = 0;
        $this->errorMessage = 'not set';
    }
    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }
    /**
     * @return string
     */
    public function getTypeNameLabel()
    {
        return $this->typeNameLabel;
    }
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * @return MetadataContent
     */
    public function getMetadataContent()
    {
        return $this->metadataContent;
    }
    /**
     * @param string $typeName
     */
    protected function setTypeName($typeName)
    {
        $this->typeName = $typeName;
    }
    /**
     * @param string $typeNameLabel
     */
    protected function setTypeNameLabel($typeNameLabel)
    {
        $this->typeNameLabel = $typeNameLabel;
    }
    /**
     * @param string $id
     */
    protected function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @param string $number
     */
    protected function setNumber($number)
    {
        $this->number = $number;
    }
    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    /**
     * @param int $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }
    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }
    /**
     * @param MetadataContent $metadataContent
     */
    protected function setMetadataContent($metadataContent)
    {
        $this->metadataContent = $metadataContent;
    }
    /**
     * @return bool
     */
    public function isError()
    {
        if (isset($this->errorMessage) && '' !== $this->errorMessage) {
            return \true;
        }
        return \false;
    }
    /**
     * @return string
     */
    public function getWarningMessage()
    {
        return $this->warningMessage;
    }
    /**
     * @param string $warningMessage
     */
    public function setWarningMessage($warningMessage)
    {
        $this->warningMessage = $warningMessage;
    }
    /**
     * @return bool
     */
    public function isWarning()
    {
        if (isset($this->warningMessage) && '' !== $this->warningMessage) {
            return \true;
        }
        return \false;
    }
}
