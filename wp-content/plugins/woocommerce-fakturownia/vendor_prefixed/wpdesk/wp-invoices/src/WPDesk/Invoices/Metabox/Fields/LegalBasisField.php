<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox\Fields;

/**
 * Class LegalBasisField
 *
 * @package WPDesk\WooCommerceFakturownia\Metabox\Fields
 */
class LegalBasisField extends \FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxField
{
    /**
     * Prepare value.
     *
     * @param \WC_Order                                 $order Order.
     * @param \WPDesk\Invoices\Metadata\MetadataContent $metadata_content Meta data.
     * @param \WPDesk\Invoices\Data\OrderDefaults       $order_defaults Order defaults.
     *
     * @return string
     */
    protected function prepareValue(\WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadata_content, \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $order_defaults)
    {
        return $order_defaults->getDefault('legal_basis');
    }
}
