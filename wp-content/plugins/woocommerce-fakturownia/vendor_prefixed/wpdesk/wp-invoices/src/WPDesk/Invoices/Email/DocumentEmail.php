<?php

namespace FakturowniaVendor\WPDesk\Invoices\Email;

class DocumentEmail extends \WC_Email
{
    /**
     * @var string
     */
    private $templatePath = '';
    /**
     * @var string
     */
    protected $download_url;
    /**
     * @param $templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    }
    /**
     * Get content HTML.
     *
     * @return string
     */
    public function get_content_html()
    {
        \ob_start();
        \wc_get_template($this->template_html, array('order' => $this->object, 'download_url' => $this->download_url, 'email_heading' => $this->get_heading(), 'sent_to_admin' => \false, 'plain_text' => \false, 'email' => $this->recipient), $this->templatePath, $this->template_base);
        return \ob_get_clean();
    }
    /**
     * get_content_plain function.
     *
     * @return string
     */
    public function get_content_plain()
    {
        \ob_start();
        \wc_get_template($this->template_plain, array('order' => $this->object, 'download_url' => $this->download_url, 'email_heading' => $this->get_heading(), 'sent_to_admin' => \false, 'plain_text' => \true, 'email' => $this->recipient), $this->templatePath, $this->template_base);
        return \ob_get_clean();
    }
    /**
     * Initialise Settings Form Fields
     *
     * @return void
     */
    public function init_form_fields()
    {
        $this->form_fields = array('subject' => array('title' => \__('Subject', 'woocommerce'), 'type' => 'text', 'placeholder' => $this->subject, 'default' => ''), 'heading' => array('title' => \__('Email Heading', 'woocommerce'), 'type' => 'text', 'placeholder' => $this->heading, 'default' => ''), 'email_type' => array('title' => \__('Email type', 'woocommerce'), 'type' => 'select', 'description' => \__('Choose which format of email to send.', 'woocommerce'), 'default' => 'html', 'class' => 'email_type', 'options' => array('plain' => \__('Plain text', 'woocommerce'), 'html' => \__('HTML', 'woocommerce'), 'multipart' => \__('Multipart', 'woocommerce'))));
    }
    /**
     * @param \WC_Order $order
     * @param string    $pdfDocument
     * @param string    $fileName
     * @param string    $download_url
     */
    public function trigger($order, $pdfDocument, $fileName, $download_url)
    {
        if (!\is_object($order)) {
            $order = new \WC_Order(\absint($order));
        }
        if ($order) {
            $this->object = $order;
            $this->recipient = $order->get_billing_email();
            $this->download_url = $download_url;
            $this->placeholders['{order_date}'] = \date_i18n(\wc_date_format(), \strtotime($order->get_date_created()->getTimestamp()));
            $this->placeholders['{order_number}'] = $order->get_order_number();
        }
        if (!$this->get_recipient()) {
            return;
        }
        $attachments = array('pdf' => array('fileName' => $fileName, 'content' => $pdfDocument));
        $stringAttachments = new \FakturowniaVendor\WPDesk\Invoices\Email\StringAttachments($attachments);
        $stringAttachments->addAction();
        $this->send($this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), array());
        $stringAttachments->removeAction();
    }
}
