<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metadata;

class MetadataContent
{
    /**
     * @var string
     */
    protected $metaDataName;
    /**
     * @var \WC_Order
     */
    protected $order;
    public function __construct($metaDataName, \WC_Order $order)
    {
        $this->metaDataName = $metaDataName;
        $this->order = $order;
    }
    /**
     * @return \WC_Order
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * @return array
     */
    public function get()
    {
        return $this->order->get_meta($this->metaDataName);
    }
    /**
     * @param array $metaData
     * @param bool  $save
     */
    public function update($metaData, $save = \true)
    {
        $this->order->update_meta_data($this->metaDataName, $metaData);
        if ($save) {
            $this->order->save();
        }
    }
    /**
     * @return string
     */
    public function getMetaDataName()
    {
        return $this->metaDataName;
    }
}
