<?php

namespace FakturowniaVendor\WPDesk\Invoices\WooCommerce;

use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
interface Integration
{
    /**
     * @return void
     */
    public function initSupportedDocumentTypes();
    /**
     * @param Type $documentType
     *
     * @return mixed
     */
    public function addSupportedDocumentType(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType);
    /**
     * Init API Client
     */
    public function initApiClient();
}
