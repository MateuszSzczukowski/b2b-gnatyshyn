<?php

namespace FakturowniaVendor;

/**
 * @var string $label
 */
if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?><div>
    <?php 
echo $label;
?>
</div>
<?php 
