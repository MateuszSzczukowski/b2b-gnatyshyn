<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox\Fields;

/**
 * Class PaymentDateField
 *
 * @package WPDesk\WooCommerceFakturownia\Metabox\Fields
 */
class PaymentDateField extends \FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxFieldDate
{
    /**
     * Payment date for COD in days.
     *
     * @var int
     */
    private $payment_date_for_cod = 0;
    /**
     * MetaBoxField constructor.
     *
     * @param string $id ID.
     * @param string $name Name.
     * @param string $label Label.
     * @param string $payment_date_for_cod Payment date for COD in days.
     */
    public function __construct($id, $name, $label, $payment_date_for_cod)
    {
        parent::__construct($id, $name, $label);
        $this->payment_date_for_cod = \intval($payment_date_for_cod);
    }
    /**
     * Prepare value.
     *
     * @param \WC_Order                                 $order Order.
     * @param \WPDesk\Invoices\Metadata\MetadataContent $metadata_content Meta data.
     * @param \WPDesk\Invoices\Data\OrderDefaults       $order_defaults Order defaults.
     *
     * @return string
     */
    protected function prepareValue(\WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadata_content, \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $order_defaults)
    {
        return $order_defaults->getDefault('payment_date');
    }
}
