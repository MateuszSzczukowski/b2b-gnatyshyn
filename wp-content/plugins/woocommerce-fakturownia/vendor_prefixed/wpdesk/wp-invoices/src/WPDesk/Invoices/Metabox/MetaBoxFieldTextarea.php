<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox;

use FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
abstract class MetaBoxFieldTextarea extends \FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxField
{
    /**
     * Render field.
     *
     * @param \WC_Order $order Order.
     * @param MetadataContent $metadataContent Meta data.
     * @param OrderDefaults $orderDefaults Order defaults.
     */
    public function render(\WC_Order $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent, \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $orderDefaults)
    {
        \woocommerce_wp_textarea_input($this->prepareField($this->prepareValue($order, $metadataContent, $orderDefaults)));
    }
}
