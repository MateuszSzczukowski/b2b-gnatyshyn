<?php

namespace FakturowniaVendor\WPDesk\Invoices\Metabox;

/**
 * Metabox fields.
 *
 * @package WPDesk\Invoices\Metabox
 */
class OrderMetaboxFields
{
    /**
     * @var MetaBoxField[]
     */
    private $metaBoxFields = array();
    /**
     * Document type name.
     * Type name is used to identify document types in array (key) and is a part of HTML elements ID.
     *
     * @var string
     */
    private $typeName;
    /**
     * OrderMetaboxFields constructor.
     *
     * @param string      $typeName Type name.
     */
    public function __construct($typeName)
    {
        $this->typeName = $typeName;
    }
    protected function addMetaBoxField(\FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxField $metaBoxField)
    {
        $this->metaBoxFields[] = $metaBoxField;
    }
    /**
     * @return MetaBoxField[]
     */
    public function getMetaBoxFields()
    {
        return $this->metaBoxFields;
    }
    /**
     * @param MetaBoxField[] $metaBoxFields
     */
    public function setMetaBoxFields($metaBoxFields)
    {
        $this->metaBoxFields = $metaBoxFields;
    }
    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }
}
