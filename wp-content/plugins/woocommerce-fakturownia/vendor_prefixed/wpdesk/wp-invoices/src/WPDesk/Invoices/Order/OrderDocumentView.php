<?php

namespace FakturowniaVendor\WPDesk\Invoices\Order;

use WC_Order;
use FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use FakturowniaVendor\WPDesk\WooCommerceFakturownia\Plugin;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
/**
 * That class is for rendering document
 *
 * @package WPDesk\WooCommerceFakturownia\Metadata
 */
class OrderDocumentView extends \FakturowniaVendor\WPDesk\Invoices\Order\DocumentView
{
    /**
     * @var Renderer
     */
    private $renderer;
    /** @var AjaxGetPdfHandler */
    private $ajaxHandler;
    public function __construct(\FakturowniaVendor\WPDesk\Invoices\Ajax\AjaxGetPdfHandler $ajaxHandler, \FakturowniaVendor\WPDesk\View\Renderer\Renderer $renderer)
    {
        $this->ajaxHandler = $ajaxHandler;
        $this->renderer = $renderer;
    }
    /**
     * Render.
     *
     * @param Type $document_type Document type.
     * @param WC_Order $order Order.
     * @param MetadataContent $metadata_content Metadata content.
     */
    public function render(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $document_type, $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadata_content)
    {
        $document_metadata = $document_type->prepareDocumentMetadata($metadata_content);
        $type_name_label = $document_metadata->getTypeNameLabel();
        if (!$document_metadata->isError() & $document_type->show_document_in_my_account()) {
            $getPdfUrl = $this->ajaxHandler->getAjaxActionUrlForMetadata($document_metadata);
            $number = $document_metadata->getNumber();
            echo $this->renderer->render('myaccount/view-document', array('get_pdf_url' => $getPdfUrl, 'document_number' => $number, 'type_name_label' => $type_name_label));
        }
    }
}
