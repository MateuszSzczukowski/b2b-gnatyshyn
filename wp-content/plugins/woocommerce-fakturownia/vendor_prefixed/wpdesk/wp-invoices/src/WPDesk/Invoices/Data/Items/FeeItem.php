<?php

namespace FakturowniaVendor\WPDesk\Invoices\Data\Items;

class FeeItem extends \FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem
{
    /**
     * LineItem constructor.
     *
     * @param \WC_Order_Item_Fee $orderItem
     */
    public function __construct(\WC_Order_Item_Fee $orderItem)
    {
        parent::__construct($orderItem);
        $this->setQuantity(1);
        $this->setPriceFromOrderItem($orderItem);
    }
}
