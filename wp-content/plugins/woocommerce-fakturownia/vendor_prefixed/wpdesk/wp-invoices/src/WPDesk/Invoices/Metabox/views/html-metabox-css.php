<?php

namespace FakturowniaVendor;

if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?>
<style>
    body.post-type-shop_order .invoice-document {
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #dfdfdf;
    }
    body.post-type-shop_order h4.invoices-created {
        padding-bottom: 10px;
        border-bottom: 1px solid #dfdfdf;
    }
    body.post-type-shop_order .invoice-create-button {
        width: auto;
        text-align: center;
    }
    body.post-type-shop_order .invoice-email-button {
        float: right;
        display:inline-block;
        margin-bottom: 3px;
    }
    body.post-type-shop_order span.invoice-parameters {
        margin-top: 5px;
        margin-left: 10px;
        display: inline-block;
    }
    body.post-type-shop_order .invoice-parameters select {
        width: 95%;
    }
    body.post-type-shop_order .invoice-email-sent {
        color: green !important;
        font-weight: bold;
    }
    body.post-type-shop_order .invoice-email-error {
        color: red !important;
        font-weight: bold;
    }
    body.post-type-shop_order .invoice-link-warning {
        color: red !important;
        font-weight: bold;
    }
</style>
<?php 
