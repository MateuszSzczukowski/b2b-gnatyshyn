<?php

namespace FakturowniaVendor;

if (!\defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
?><div class="invoice-parameters" id="<?php 
echo $parametersDivId;
?>" style="display:none;">
<?php 
