<?php

namespace FakturowniaVendor\WPDesk\Invoices\Order;

use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
/**
 * Display document on MyAccount order view.
 */
abstract class DocumentView
{
    /**
     * Render.
     * @param Type $documentType
     * @param \WC_Order $order
     * @param MetadataContent $metadataContent
     */
    public abstract function render(\FakturowniaVendor\WPDesk\Invoices\Documents\Type $documentType, $order, \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadataContent);
}
