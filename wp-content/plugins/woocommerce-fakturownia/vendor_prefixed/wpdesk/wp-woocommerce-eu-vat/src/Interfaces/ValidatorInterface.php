<?php

/**
 * Validator interfaces.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Interfaces;

/**
 * Validates values.
 */
interface ValidatorInterface
{
    /**
     * Is valid value.
     *
     * @param string $vat_number Vat number.
     * @param string $country    Country ISO.
     *
     * @return array
     */
    public function validate($vat_number, $country);
}
