<?php

/**
 * EU Vat Integration: Main class.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\ViesOrderMetaBox;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Validator;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Checkout;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Formatter;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\MetaData;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Notices;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\VatField;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
use FakturowniaVendor\WPDesk\View\Renderer\SimplePhpRenderer;
use FakturowniaVendor\WPDesk\View\Resolver\ChainResolver;
use FakturowniaVendor\WPDesk\View\Resolver\DirResolver;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * EU Vat Integration.
 *
 * @package WPDesk\Integration\EUVAT
 */
abstract class WooCommerceEUVAT implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable, \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection
{
    use HookableParent;
    /**
     * Settings.
     *
     * @var Settings
     */
    protected $settings;
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    protected $shop_settings;
    /**
     * Logger.
     *
     * @var LoggerAwareInterface
     */
    protected $logger;
    /**
     * Renderer.
     *
     * @var Renderer
     */
    protected $renderer;
    /**
     * VAT number field name.
     *
     * @var string
     */
    static $vat_field_name;
    /**
     * Plugin name.
     *
     * @var string
     */
    private $plugin_name;
    /**
     * Plugin version.
     *
     * @var string
     */
    private $plugin_version;
    /**
     * Documentation link.
     *
     * @var string
     */
    private $doc_link;
    /**
     * Validator.
     *
     * @var ValidatorInterface
     */
    private $validator;
    const SCRIPTS_VERSION = 1;
    /**
     * WooCommerceEUVAT constructor.
     *
     * @param Settings $settings Settings.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings $settings)
    {
        $this->settings = $settings;
        $this->shop_settings = $this->get_shop_settings();
        $this->set_validator(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Validator($this->shop_settings));
        $this->set_vat_field_name(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\VatField::VAT_NUMBER_FIELD_ID);
        $this->set_logger(new \Psr\Log\NullLogger());
        $this->set_renderer(new \FakturowniaVendor\WPDesk\View\Renderer\SimplePhpRenderer(new \FakturowniaVendor\WPDesk\View\Resolver\DirResolver(__DIR__ . '/Integration/Views')));
    }
    /**
     * Set plugin name.
     *
     * @param string $plugin_name Plugin name.
     */
    public function set_plugin_name($plugin_name)
    {
        $this->plugin_name = $plugin_name;
    }
    /**
     * Get plugin name.
     *
     * @return string
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }
    /**
     * Set plugin version. The plugin version from which the library is supported
     *
     * @param string $plugin_version Plugin version.
     */
    public function set_plugin_version($plugin_version)
    {
        $this->plugin_version = $plugin_version;
    }
    /**
     * Get plugin version.
     *
     * @return string
     */
    public function get_plugin_version()
    {
        return $this->plugin_version;
    }
    /**
     * Set doc link.
     *
     * @param string $doc_link Documentation link.
     */
    public function set_doc_link($doc_link)
    {
        $this->doc_link = $doc_link;
    }
    /**
     * Get doc link.
     *
     * @return string
     */
    public function get_doc_link()
    {
        return $this->doc_link;
    }
    /**
     * Set vat number field name.
     *
     * @param string $vat_field_name Vat number field name.
     */
    public function set_vat_field_name($vat_field_name)
    {
        self::$vat_field_name = $vat_field_name;
    }
    /**
     * Get VAT field name.
     *
     * @return string
     */
    protected function get_vat_field_name()
    {
        return self::$vat_field_name ? self::$vat_field_name : \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\VatField::VAT_NUMBER_FIELD_ID;
    }
    /**
     * Set logger.
     *
     * @param LoggerInterface $logger
     */
    public function set_logger(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    /**
     * Get renderer.
     *
     * @param \WPDesk\View\Renderer\Renderer $renderer Renderer.
     */
    public function set_renderer(\FakturowniaVendor\WPDesk\View\Renderer\Renderer $renderer)
    {
        $this->renderer = $renderer;
    }
    /**
     * Set validator.
     *
     * @param ValidatorInterface $validator Validator.
     */
    public function set_validator($validator)
    {
        $this->validator = $validator;
    }
    /**
     * Get validator.
     *
     * @return ValidatorInterface
     */
    protected function get_validator()
    {
        return $this->validator;
    }
    /**
     * Get shop settings
     *
     * @return \WPDesk\WooCommerce\EUVAT\Settings\ShopSettings
     */
    protected function get_shop_settings()
    {
        return new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings();
    }
    /**
     * Can fire EU VAT library?
     *
     * @return bool
     */
    protected function is_eu_vat_plugins_enabled()
    {
        return $this->shop_settings->is_eu_vat_plugins_enabled();
    }
    /**
     * Load dependencies.
     */
    public function load_dependencies()
    {
        $this->add_hookable(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Notices($this->is_eu_vat_plugins_enabled(), $this->get_plugin_name(), $this->get_plugin_version(), $this->get_doc_link()));
        if ($this->settings->eu_vat_vies_validate && !$this->is_eu_vat_plugins_enabled() && \wc_tax_enabled()) {
            $meta_data = new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\MetaData($this->shop_settings);
            $this->add_hookable(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\VatField($this->settings));
            $this->add_hookable(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Checkout($this->shop_settings, $this->settings, $meta_data, $this->renderer, $this->get_validator()));
            $this->add_hookable($meta_data);
            $this->add_hookable(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\ViesOrderMetaBox($this->settings, $this->shop_settings, $this->renderer));
        }
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        $this->load_dependencies();
        $this->hooks_on_hookable_objects();
        \add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 30);
    }
    /**
     * Admin enqueue scripts.
     */
    public function admin_enqueue_scripts()
    {
        \wp_enqueue_style('euvat', \trailingslashit(\plugin_dir_url(__DIR__)) . 'src/assets/css/admin.css', '', self::SCRIPTS_VERSION);
    }
}
