<?php

/**
 * Vies: Response DTO.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies;

use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
/**
 * Add VIES meta box to Order.
 *
 * @package WPDesk\Integration\EUVAT
 */
class ViesOrderMetaBox implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    const META_BOX_ID = 'woocommerce_invoices_eu_vat';
    /**
     * Settings.
     *
     * @var Settings
     */
    private $settings;
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    private $shop_settings;
    /**
     * Renderer.
     *
     * @var Renderer
     */
    private $renderer;
    /**
     * MetaBox constructor.
     *
     * @param Settings $settings
     * @param ShopSettings $shop_settings Shop settings.
     * @param Renderer $renderer View renderer.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings $settings, \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings $shop_settings, \FakturowniaVendor\WPDesk\View\Renderer\Renderer $renderer)
    {
        $this->settings = $settings;
        $this->shop_settings = $shop_settings;
        $this->renderer = $renderer;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        \add_action('add_meta_boxes', array($this, 'add_meta_boxes_action'), 30);
    }
    /**
     * Add Meta Boxes.
     */
    public function add_meta_boxes_action()
    {
        \add_meta_box(self::META_BOX_ID, \__('EU VAT', 'woocommerce-fakturownia'), [$this, 'eu_vat_metabox_body'], 'shop_order', 'side');
    }
    /**
     * Meta box body.
     *
     * @param int $post_id Post ID.
     */
    public function eu_vat_metabox_body($post_id)
    {
        global $post, $theorder;
        if (!\is_object($theorder)) {
            $theorder = \wc_get_order($post_id);
        }
        if (!$this->is_eu_order($theorder)) {
            echo \wpautop(\esc_html__('This order is out of scope for EU VAT.', 'woocommerce-fakturownia'));
            //phpcs:ignore
            return;
        }
        $moss_meta_data = $this->get_order_vat_data($theorder);
        $countries = \WC()->countries->get_countries();
        $params['vat_field_label'] = !empty($this->settings->vat_field_label) ? $this->settings->vat_field_label : \__('VAT ID', 'woocommerce-fakturownia');
        $params['countries'] = $countries;
        $params = \array_merge($params, $moss_meta_data);
        $html = $this->renderer->render('vies-order-metabox', $params);
        echo $html;
        //phpcs:ignore
    }
    /**
     * Get order VAT Number data
     *
     * @param \WC_Order $order The order object.
     *
     * @return array
     */
    private function get_order_vat_data(\WC_Order $order)
    {
        return array('vat_number' => $order->get_meta('_vat_number', \true), 'vat_number_self_declared' => \wc_string_to_bool($order->get_meta('_vat_number_self_declared', \true)), 'valid' => \wc_string_to_bool($order->get_meta('_vat_number_is_valid', \true)), 'validated' => \wc_string_to_bool($order->get_meta('_vat_number_is_validated', \true)), 'billing_country' => $order->get_billing_country(), 'ip_address' => $order->get_customer_ip_address(), 'ip_country' => $order->get_meta('_customer_ip_country', \true), 'customer_self_declared_country' => \wc_string_to_bool($order->get_meta('_customer_self_declared_country', \true)));
    }
    /**
     * Is this is an EU order?
     *
     * @param \WC_Order $order Order.
     *
     * @return bool
     */
    protected function is_eu_order($order)
    {
        return \in_array($order->get_billing_country(), $this->shop_settings->get_eu_countries());
    }
}
