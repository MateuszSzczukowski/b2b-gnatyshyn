<?php

/**
 * Vies: EUVATExceptionInterface.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions;

/**
 * EUVATExceptionInterface thrown when VIES.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies\Response
 */
interface EUVATExceptionInterface
{
}
