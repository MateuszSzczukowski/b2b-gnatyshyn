<?php

/**
 * Vies: Client.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies;

use SoapFault;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;
/**
 * A client for the VIES SOAP web service
 */
class Client
{
    /**
     * URL to WSDL
     *
     * @var string
     */
    protected $wsdl = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';
    /**
     * SOAP client
     *
     * @var \SoapClient
     */
    protected $soapClient;
    /**
     * Constructor
     *
     * @param string $wsdl URL to WSDL
     */
    public function __construct($wsdl = null)
    {
        if ($wsdl) {
            $this->wsdl = $wsdl;
        }
    }
    /**
     * Check VAT
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return Response
     * @throws ViesCommunicationException
     */
    public function checkVat($countryCode, $vatNumber)
    {
        try {
            return $this->getSoapClient()->checkVat(array('countryCode' => $countryCode, 'vatNumber' => $vatNumber));
        } catch (\SoapFault $e) {
            throw new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException('Error communicating with VIES service', 0, $e);
        }
    }
    /**
     * Get SOAP client
     *
     * @return \SoapClient
     *
     * @throws SoapFault
     */
    protected function getSoapClient()
    {
        if (null === $this->soapClient) {
            $this->soapClient = new \SoapClient($this->wsdl, array('classmap' => array('checkVatResponse' => \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Response::class), 'user_agent' => 'Mozilla', 'exceptions' => \true));
        }
        return $this->soapClient;
    }
}
