<?php

/**
 * Vies: Validator.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies;

use Psr\Container\ContainerInterface;
use FakturowniaVendor\WPDesk\Persistence\PersistentContainer;
use FakturowniaVendor\WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\EUVATExceptionInterface;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\InvalidCountryCodeException;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;
/**
 * Validate VAT number in VIES.
 *
 * @package WPDesk\Integration\EUVAT
 */
class Validator implements \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface
{
    /**
     * Client.
     *
     * @var Client
     */
    private $client;
    /**
     * Country codes pattern.
     *
     * @var array
     */
    private $country_codes_patterns = array('AT' => 'U[A-Z\\d]{8}', 'BE' => '0\\d{9}', 'BG' => '\\d{9,10}', 'CY' => '\\d{8}[A-Z]', 'CZ' => '\\d{8,10}', 'DE' => '\\d{9}', 'DK' => '(\\d{2} ?){3}\\d{2}', 'EE' => '\\d{9}', 'EL' => '\\d{9}', 'ES' => '[A-Z]\\d{7}[A-Z]|\\d{8}[A-Z]|[A-Z]\\d{8}', 'FI' => '\\d{8}', 'FR' => '([A-Z]{2}|[A-Z0-9]{2})\\d{9}', 'GB' => '\\d{9}|\\d{12}|(GD|HA)\\d{3}', 'HR' => '\\d{11}', 'HU' => '\\d{8}', 'IE' => '[A-Z\\d]{8,10}', 'IT' => '\\d{11}', 'LT' => '(\\d{9}|\\d{12})', 'LU' => '\\d{8}', 'LV' => '\\d{11}', 'MT' => '\\d{8}', 'NL' => '\\d{9}B\\d{2}', 'PL' => '\\d{10}', 'PT' => '\\d{9}', 'RO' => '\\d{2,10}', 'SE' => '\\d{12}', 'SI' => '\\d{8}', 'SK' => '\\d{10}');
    /**
     * Data container for VIES validation.
     *
     * @var array
     */
    private $data = ['vat_number' => \false, 'validation' => \false];
    /**
     * @var string
     */
    private $full_vat_number = '';
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    private $shop_settings;
    /**
     * Persistent Container.
     *
     * @var PersistentContainer
     */
    private $persistence;
    /**
     * VIESValidation constructor.
     *
     * @param ShopSettings $shop_settings Shop settings.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings $shop_settings)
    {
        $this->shop_settings = $shop_settings;
        $this->set_client(new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Client());
        $this->set_persistence(new \FakturowniaVendor\WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer());
    }
    /**
     * Get persistence container.
     *
     * @return PersistentContainer
     */
    public function get_persistence()
    {
        return $this->persistence;
    }
    /**
     * Set persistence container.
     *
     * @param ContainerInterface $persistence Persistent.
     */
    public function set_persistence(\Psr\Container\ContainerInterface $persistence)
    {
        $this->persistence = $persistence;
    }
    /**
     * Set client.
     *
     * @param Client $client Client.
     */
    public function set_client(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Client $client)
    {
        $this->client = $client;
    }
    /**
     * Get client.
     *
     * @return Client
     */
    private function get_client()
    {
        return $this->client;
    }
    /**
     * Is valid in VIES.
     *
     * @param string $vat_number Vat number.
     * @param string $country Country ISO.
     *
     * @return bool
     * @throws InvalidCountryCodeException
     * @throws ViesCommunicationException
     */
    public function is_valid($vat_number, $country)
    {
        $country = $this->normalize_country_prefix($country);
        $vat_number = $this->normalize_vat_number($vat_number);
        $transient_name = 'wpdesk_vies_validations_' . $country . $vat_number;
        if (!empty($country) && !empty($vat_number)) {
            try {
                $is_valid = (bool) $this->get_persistence()->get($transient_name);
                return $is_valid;
            } catch (\Exception $e) {
                $this->is_valid_country_code($country);
                try {
                    $result = $this->get_client()->checkVat($country, $vat_number);
                    $is_valid = $result->isValid();
                    $this->get_persistence()->set($transient_name, $is_valid);
                    return $is_valid;
                } catch (\SoapFault $e) {
                    throw new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException(\__('Error communicating with the VAT validation server - please try again', 'woocommerce-fakturownia'));
                }
            }
        }
    }
    /**
     * Is valid country code.
     *
     * @param string $country Country slug.
     */
    private function is_valid_country_code($country)
    {
        if (!isset($this->country_codes_patterns[$country])) {
            throw new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\InvalidCountryCodeException('api', \__('Invalid country code', 'woocommerce-fakturownia'));
        }
    }
    /**
     * Normalize VAT number.
     *
     * @param $vat_number
     *
     * @return string|string[]
     */
    private function normalize_vat_number($vat_number)
    {
        return \str_replace(array(' ', '.', '-', ',', ', '), '', \trim(\strtoupper($vat_number)));
    }
    /**
     * Validate a number and store the result
     *
     * @param string $vat_number Vat number.
     * @param string $billing_country Billing country.
     *
     * @return array
     */
    public function validate($vat_number, $billing_country)
    {
        $vat_number = $this->get_formatted_vat_number($vat_number);
        try {
            $valid = $this->is_valid($vat_number, $billing_country);
            $this->data['vat_number'] = $vat_number;
            $this->data['full_vat_number'] = $this->full_vat_number;
            $this->data['validation'] = array('valid' => $valid, 'error' => \false);
        } catch (\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Exceptions\EUVATExceptionInterface $e) {
            $this->data['vat_number'] = $vat_number;
            $this->data['full_vat_number'] = $this->full_vat_number;
            $this->data['validation'] = array('valid' => null, 'error' => $e->getMessage());
        }
        return $this->data;
    }
    /**
     * Remove unwanted chars and the prefix from a VAT number.
     *
     * @param string $vat Vat number.
     *
     * @return string
     */
    private function get_formatted_vat_number($vat)
    {
        $this->full_vat_number = $vat;
        $eu_vat_countries = $this->shop_settings->get_eu_countries();
        $vat = \strtoupper(\str_replace(array(' ', '-', '_', '.'), '', $vat));
        if (\in_array(\substr($vat, 0, 2), \array_merge($eu_vat_countries, array('EL')))) {
            $vat = \substr($vat, 2);
        }
        return $vat;
    }
    /**
     * Normalize country prefix.
     *
     * @param string $country Country ISO.
     *
     * @return string
     */
    private function normalize_country_prefix($country)
    {
        switch ($country) {
            case 'GR':
                $vat_prefix = 'EL';
                break;
            case 'IM':
                $vat_prefix = 'GB';
                break;
            case 'MC':
                $vat_prefix = 'FR';
                break;
            default:
                $vat_prefix = $country;
                break;
        }
        return \trim(\strtoupper($vat_prefix));
    }
}
