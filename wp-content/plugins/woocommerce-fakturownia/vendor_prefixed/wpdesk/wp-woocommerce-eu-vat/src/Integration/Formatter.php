<?php

/**
 * EU Vat Integration: Formatter.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration;

use WC_Order;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * Format some data saved to order & localization.
 *
 * @package WPDesk\Integration\EUVAT
 */
class Formatter implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    private $shop_settings;
    /**
     * Formatter constructor.
     *
     * @param ShopSettings $shop_settings Shop settings.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings $shop_settings)
    {
        $this->shop_settings = $shop_settings;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        \add_filter('woocommerce_order_formatted_billing_address', [$this, 'formatted_billing_address_action'], 10, 2);
        \add_filter('woocommerce_formatted_address_replacements', [$this, 'output_company_vat_number_action'], 10, 2);
        \add_filter('woocommerce_localisation_address_formats', [$this, 'localisation_address_formats_action'], 10, 2);
    }
    /**
     * Add VAT ID to the formatted address array
     *
     * @param array    $address Address.
     * @param WC_Order $order   Order.
     *
     * @return array
     */
    public function formatted_billing_address_action($address, \WC_Order $order)
    {
        $vat_id = $order->get_meta('_vat_number', \true);
        if ($vat_id) {
            $address['vat_id'] = $vat_id;
        }
        return $address;
    }
    /**
     * Add {vat_id} placeholder.
     *
     * @param array $formats Formats.
     * @param array $args    Arguments.
     *
     * @return array
     */
    public function output_company_vat_number_action($formats, $args)
    {
        if (isset($args['vat_id'])) {
            // Translators: vat type like 20%.
            $formats['{vat_id}'] = \sprintf(\__('VAT ID: %s', 'woocommerce-fakturownia'), $args['vat_id']);
        } else {
            $formats['{vat_id}'] = '';
        }
        return $formats;
    }
    /**
     * Address formats.
     *
     * @param array $formats Formats.
     *
     * @return array
     */
    public function localisation_address_formats_action($formats)
    {
        foreach ($formats as $key => $format) {
            if ('default' === $key || \in_array($key, $this->shop_settings->get_eu_countries())) {
                $formats[$key] .= "\n{vat_id}";
            }
        }
        return $formats;
    }
}
