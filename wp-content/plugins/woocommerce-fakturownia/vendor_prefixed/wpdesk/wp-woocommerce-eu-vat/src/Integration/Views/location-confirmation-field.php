<?php

namespace FakturowniaVendor;

$isset = $params ?: [];
$is_checked = $params['is_checked'];
$countries = $params['countries'];
$billing_country = \is_callable(array(\WC()->customer, 'get_billing_country')) ? \WC()->customer->get_billing_country() : \WC()->customer->get_country();
?>
<p class="form-row location_confirmation terms">
	<label for="location_confirmation" class="checkbox">
		<input type="checkbox" class="input-checkbox" name="location_confirmation" <?php 
\checked($is_checked, \true);
?> id="location_confirmation" /> <span>
			<?php 
\printf(\__('I am established, have my permanent address, or usually reside within <strong>%s</strong>.', 'woocommerce-fakturownia'), $countries[$billing_country]);
?></span>
	</label>
</p>
<?php 
