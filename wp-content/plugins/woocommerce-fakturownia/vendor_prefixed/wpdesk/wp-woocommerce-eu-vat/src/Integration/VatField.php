<?php

/**
 * EU Vat Integration: VAT Field.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration;

use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT;
/**
 * Add VAT field to checkout.
 *
 * @package WPDesk\Integration\EUVAT
 */
class VatField implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    const VAT_NUMBER_FIELD_ID = 'vat_number';
    /**
     * Settings.
     *
     * @var Settings
     */
    private $settings;
    /**
     * VatField constructor.
     *
     * @param Settings $settings
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings $settings)
    {
        $this->settings = $settings;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        \add_filter('woocommerce_checkout_fields', array($this, 'add_vat_checkout_field_filter'), 20, 1);
        \add_filter('woocommerce_checkout_fields', array($this, 'add_required_classes'), 999999, 1);
    }
    /**
     * Add fields to checkout
     *
     * @param array $fields Fields.
     *
     * @return array $fields
     */
    public function add_vat_checkout_field_filter($fields)
    {
        $fields['billing'][\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name] = $this->vat_number_declaration();
        return $fields;
    }
    /**
     * Add
     *
     * @param array $fields
     *
     * @return array $fields
     */
    public function add_required_classes($fields)
    {
        $fields['billing'][\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name]['class'] = array('vat-number', 'update_totals_on_change', 'address-field', 'form-row-wide');
        return $fields;
    }
    /**
     * Add VAT number field.
     *
     * @return array
     */
    private function vat_number_declaration()
    {
        return array('label' => $this->settings->vat_field_label, 'placeholder' => $this->settings->vat_field_placeholder, 'required' => $this->settings->vat_field_required, 'clear' => \true, 'type' => 'text');
    }
}
