<?php

/**
 * EU Vat Integration: Order MetaData.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration;

use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT;
/**
 * Save order metadata.
 *
 * @package WPDesk\Integration\EUVAT
 */
class MetaData implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * Order meta.
     *
     * @var array
     */
    private $order_meta = ['validation' => ['valid' => \false], 'vat_number' => \false, 'full_vat_number' => ''];
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    private $shop_settings;
    /**
     * Is user checked self declared.
     *
     * @var bool
     */
    private $self_declared = \false;
    /**
     * MetaData constructor.
     *
     * @param ShopSettings $shop_settings Shop settings.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings $shop_settings)
    {
        $this->shop_settings = $shop_settings;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        \add_action('woocommerce_checkout_create_order', [$this, 'save_order_data_action']);
        \add_action('woocommerce_checkout_update_customer', [$this, 'save_customer_data_action']);
        \add_action('woocommerce_create_refund', [$this, 'save_refund_data_action']);
    }
    /**
     * Set results from validation.
     *
     * @param array $data Form data.
     * @param bool $self_declared Is self declared?.
     */
    public function set_results($data, $self_declared = \false)
    {
        $this->order_meta = $data;
        $this->self_declared = $self_declared;
    }
    /**
     * Get VAT number from order meta.
     *
     * @return string|null
     */
    private function get_vat_number()
    {
        return isset($this->order_meta['vat_number']) ? $this->order_meta['vat_number'] : null;
    }
    /**
     * Get validation status.
     *
     * @return bool|null
     */
    private function get_validation_status()
    {
        return isset($this->order_meta['validation']['valid']) ? $this->order_meta['validation']['valid'] : null;
    }
    /**
     * Save VAT Number to the order during checkout (WC 2.7.x).
     *
     * @param \WC_Order $order Order.
     *
     * @return void|false
     */
    public function save_order_data_action(\WC_Order $order)
    {
        $order->update_meta_data('_vat_number', $this->get_vat_number());
        $order->update_meta_data('_vat_number_self_declared', \true === $this->self_declared ? 'true' : 'false');
        $order->update_meta_data('_vat_number_is_validated', !\is_null($this->get_validation_status()) ? 'true' : 'false');
        $order->update_meta_data('_vat_number_is_valid', \true === $this->get_validation_status() ? 'true' : 'false');
        if (\false !== $this->shop_settings->get_ip_country()) {
            $order->update_meta_data('_customer_ip_country', $this->shop_settings->get_ip_country());
            $order->update_meta_data('_customer_self_declared_country', !empty($_POST['location_confirmation']) ? 'true' : 'false');
            //phpcs:ignore
        }
    }
    /**
     * Save VAT Number to the customer during checkout (WC 2.7.x).
     *
     * @param \WC_Order_Refund $refund Refund.
     */
    public function save_refund_data_action(\WC_Order_Refund $refund)
    {
        $order = \wc_get_order($refund->get_parent_id());
        $refund->update_meta_data('_vat_number', $order->get_meta('_vat_number', \true));
    }
    /**
     * Save VAT Number to the customer during checkout (WC 2.7.x).
     *
     * @param \WC_Customer $customer Customer.
     */
    public function save_customer_data_action(\WC_Customer $customer)
    {
        if (isset($this->order_meta['full_vat_number'])) {
            $customer->update_meta_data(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name, $this->order_meta['full_vat_number']);
        }
    }
}
