<?php

/**
 * EU Vat Integration: Checkout.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration;

use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Validator;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT;
/**
 * Checkout functionality.
 *
 * @package WPDesk\Integration\EUVAT
 */
class Checkout implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * Library settings.
     *
     * @var Settings
     */
    private $settings;
    /**
     * Shop settings.
     *
     * @var ShopSettings
     */
    private $shop_settings;
    /**
     * Meta data.
     *
     * @var MetaData
     */
    private $meta_data;
    /**
     * Vies validation.
     *
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * Renderer.
     *
     * @var Renderer
     */
    private $renderer;
    /**
     * Validation results.
     *
     * @var array
     */
    private $validation_results = ['validation' => ['valid' => \false], 'vat_number' => \false, 'full_vat_number' => ''];
    /**
     * Checkout constructor.
     *
     * @param ShopSettings $shop_settings EU VAT Number Main class.
     * @param Settings $settings Settings.
     * @param MetaData $meta_data Meta data.
     * @param Renderer $renderer Renderer.
     * @param ValidatorInterface $validator Vies validation.
     */
    public function __construct(\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\ShopSettings $shop_settings, \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings $settings, \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\MetaData $meta_data, \FakturowniaVendor\WPDesk\View\Renderer\Renderer $renderer, \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\Validator $validator)
    {
        $this->settings = $settings;
        $this->shop_settings = $shop_settings;
        $this->meta_data = $meta_data;
        $this->renderer = $renderer;
        $this->validator = $validator;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        \add_action('woocommerce_checkout_process', [$this, 'process_checkout_action']);
        \add_action('woocommerce_checkout_update_order_review', [$this, 'ajax_update_checkout_totals_action']);
        \add_action('woocommerce_review_order_before_submit', [$this, 'location_confirmation_action']);
        \add_action('woocommerce_review_order_before_submit', [$this, 'vat_confirmation_action']);
        \add_filter('woocommerce_get_tax_location', [$this, 'woocommerce_get_tax_location'], 10, 2);
    }
    /**
     * Set tax exception based on countries.
     *
     * @param bool $exempt Are they exempt.
     * @param string $billing_country Billing country of customer.
     * @param string $shipping_country Shipping country of customer.
     */
    public function maybe_set_vat_exempt($exempt, $billing_country, $shipping_country)
    {
        if ('billing' === \get_option('woocommerce_tax_based_on', 'billing')) {
            $base_country_match = \WC()->countries->get_base_country() === $billing_country;
        } else {
            $base_country_match = \in_array(\WC()->countries->get_base_country(), array($billing_country, $shipping_country));
        }
        if ($base_country_match && $this->settings->eu_vat_remove_vat_from_base_b2b || !$base_country_match) {
            \WC()->customer->set_is_vat_exempt($exempt);
        }
    }
    /**
     * Reset VAT exempt.
     */
    public function reset()
    {
        \WC()->customer->set_is_vat_exempt(\false);
    }
    /**
     * Validate the VAT number when the checkout form is processed.
     *
     * For B2C transactions, validate the IP only if this is a digital order.
     */
    public function process_checkout_action()
    {
        $form_data = \wc_clean($_POST);
        //phpcs:ignore
        $this->maybe_vat_exempt($form_data);
    }
    /**
     * Triggered when the totals are updated on the checkout.
     *
     * @param string $data Data.
     *
     * @return false|void
     */
    public function ajax_update_checkout_totals_action($data)
    {
        \parse_str($data, $form_data);
        $this->maybe_vat_exempt($form_data);
    }
    /**
     * Maybe vat exempt.
     *
     * @param $form_data
     *
     * @return bool
     */
    private function maybe_vat_exempt($form_data)
    {
        $this->reset();
        if (!\WC()->cart->needs_payment() || !$this->product_has_digital_goods_tax_class()) {
            return \false;
        }
        if (empty($form_data['billing_country']) && empty($form_data['shipping_country'])) {
            return \false;
        }
        if ($this->shop_settings->is_customer_from_base_country($form_data['billing_country'])) {
            return \false;
        }
        if (!empty($form_data[\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name])) {
            $this->exempt_vat_for_b2b($form_data);
        } else {
            $this->exempt_vat_for_customer($form_data);
        }
        return \true;
    }
    /**
     * Can exempt VAT for B2B transactions.
     *
     * @param array $post_arr Post data.
     */
    private function exempt_vat_for_b2b($post_arr)
    {
        $vat_eu_confirmed_by_user = isset($post_arr['vat_confirmation']);
        $billing_country = \wc_clean($post_arr['billing_country']);
        $shipping_country = \wc_clean(!empty($post_arr['shipping_country']) && !empty($post_arr['ship_to_different_address']) ? $post_arr['shipping_country'] : $post_arr['billing_country']);
        if (\in_array($billing_country, $this->shop_settings->get_eu_countries()) && !empty($post_arr[\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name])) {
            $this->validation_results = $this->validator->validate(\wc_clean($post_arr[\FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT::$vat_field_name]), $billing_country);
            $this->meta_data->set_results($this->validation_results, $vat_eu_confirmed_by_user);
            if (\true === (bool) $this->validation_results['validation']['valid'] || $vat_eu_confirmed_by_user) {
                $this->maybe_set_vat_exempt(\true, $billing_country, $shipping_country);
            } else {
                $fail_handler = $this->settings->eu_vat_failure_handling;
                switch ($fail_handler) {
                    case 'accept_with_vat':
                        break;
                    case 'accept_without_vat':
                        self::maybe_set_vat_exempt(\true, $billing_country, $shipping_country);
                        break;
                    default:
                        if (\false === $this->validation_results['validation']['valid']) {
                            // translators: %1$s vat number, %2$s country.
                            \wc_add_notice(\sprintf(\__('You have entered an invalid VAT number (%1$s) for your billing country (%2$s).', 'woocommerce-fakturownia'), $this->validation_results['vat_number'], $billing_country), 'error');
                        } else {
                            \wc_add_notice($this->validation_results['validation']['error'], 'error');
                        }
                        break;
                }
            }
        }
    }
    /**
     * Can exempt VAT for customer.
     *
     * @param array $post_arr Post data.
     */
    private function exempt_vat_for_customer($post_arr)
    {
        if ($this->settings->moss_validate_ip && $this->is_digital_goods_and_eu_country($post_arr['billing_country'])) {
            if ($this->is_self_declaration_required($this->shop_settings->get_ip_country(), $post_arr['billing_country']) && empty($post_arr['location_confirmation'])) {
                // translators: %1$s ip address, %2$s country.
                \wc_add_notice(\sprintf(\__('Your IP Address (%1$s) does not match your billing country (%2$s). European VAT laws require your IP address to match your billing country when purchasing digital goods in the EU. Please confirm you are located within your billing country using the checkbox below.', 'woocommerce-fakturownia'), \apply_filters('wc_eu_vat_self_declared_ip_address', \WC_Geolocation::get_ip_address()), \esc_attr($post_arr['billing_country'])), 'error');
            }
        }
    }
    /**
     * See if we need the user to self-declare location.
     *
     * @param string $ip_country IP country.
     * @param string $billing_country Billing country.
     *
     * @return bool
     */
    public function is_self_declaration_required($ip_country = null, $billing_country = null)
    {
        if (\is_null($ip_country)) {
            $ip_country = $this->shop_settings->get_ip_country();
        }
        if (\is_null($billing_country)) {
            $billing_country = \is_callable(array(\WC()->customer, 'get_billing_country')) ? \WC()->customer->get_billing_country() : \WC()->customer->get_country();
        }
        return (empty($ip_country) || \in_array($ip_country, $this->shop_settings->get_eu_countries(), \true) || \in_array($billing_country, $this->shop_settings->get_eu_countries(), \true)) && $ip_country !== $billing_country;
    }
    /**
     * Show checkbox for customer to confirm their location if geolocation fail.
     */
    public function location_confirmation_action()
    {
        $form_data = [];
        if (isset($_POST['post_data'])) {
            \parse_str($_POST['post_data'], $form_data);
        }
        $country = isset($form_data['billing_country']) ? $form_data['billing_country'] : '';
        if ($this->shop_settings->is_customer_from_base_country($country)) {
            return \false;
        }
        if ($this->settings->moss_validate_ip && $this->is_digital_goods_and_eu_country($country)) {
            if (\false === $this->validation_results['validation']['valid'] && $this->is_self_declaration_required() && empty($this->validation_results['vat_number'])) {
                $params['is_checked'] = isset($form_data['location_confirmation']);
                $params['countries'] = \WC()->countries->get_countries();
                $html = $this->renderer->render('location-confirmation-field', $params);
                echo $html;
                //phpcs:ignore
            }
        }
    }
    /**
     * Get billing country.
     *
     * @return string|void
     */
    private function get_billing_country()
    {
        return \is_callable(array(\WC()->customer, 'get_billing_country')) ? \WC()->customer->get_billing_country() : \WC()->customer->get_country();
    }
    /**
     * Show the checkbox for the customer to confirm the EU VAT number.
     * For those companies where the EU has not added a number to the VIES database.
     */
    public function vat_confirmation_action()
    {
        $form_data = [];
        if (isset($_POST['post_data'])) {
            \parse_str($_POST['post_data'], $form_data);
        }
        $country = isset($form_data['billing_country']) ? $form_data['billing_country'] : '';
        if ($this->settings->moss_validate_ip && $this->is_digital_goods_and_eu_country($country)) {
            $billing_country = $this->get_billing_country();
            if ('reject' === $this->settings->eu_vat_failure_handling && $this->validation_results['vat_number'] && \false === $this->validation_results['validation']['valid'] && $this->settings->moss_validate_ip && \in_array($billing_country, $this->shop_settings->get_eu_countries(), \true)) {
                $params['is_checked'] = isset($form_data['vat_confirmation']);
                //phpcs:ignore
                $params['countries'] = \WC()->countries->get_countries();
                $html = $this->renderer->render('vat-confirmation-field', $params);
                echo $html;
                //phpcs:ignore
            }
        }
    }
    /**
     * Check is digital goods and customer country.
     *
     * @param string $country Country slug.
     *
     * @return bool
     */
    private function is_digital_goods_and_eu_country($country)
    {
        return $this->cart_has_digital_goods() && $this->product_has_digital_goods_tax_class() && \in_array($country, $this->shop_settings->get_eu_countries());
    }
    /**
     * Sees if a cart contains anything non-shippable.
     *
     * @return bool
     */
    private function cart_has_digital_goods()
    {
        $has_digital_goods = \false;
        if (\WC()->cart->get_cart()) {
            foreach (\WC()->cart->get_cart() as $cart_item_key => $values) {
                /**
                 * Product.
                 *
                 * @var \WC_Product $_product
                 */
                $_product = $values['data'];
                if (!$_product->needs_shipping()) {
                    $has_digital_goods = \true;
                }
            }
        }
        return $has_digital_goods;
    }
    /**
     * Sees if a product has a digital goods tax class.
     *
     * @return bool
     */
    private function product_has_digital_goods_tax_class()
    {
        $has_tax_class = \false;
        if (\WC()->cart->get_cart()) {
            foreach (\WC()->cart->get_cart() as $cart_item_key => $values) {
                /**
                 * Product.
                 *
                 * @var \WC_Product $_product
                 */
                $_product = $values['data'];
                $tax_class = $_product->get_tax_class();
                if ('' === $tax_class) {
                    $tax_class = 'standard';
                }
                if (\in_array($tax_class, $this->settings->moss_tax_classes)) {
                    $has_tax_class = \true;
                }
            }
        }
        return $has_tax_class;
    }
    /**
     * Force Digital Goods tax class to use billing address.
     *
     * @param array $location Location.
     * @param string $tax_class Tax class.
     *
     * @return array
     */
    public function woocommerce_get_tax_location($location, $tax_class = '')
    {
        if (!empty(\WC()->customer) && \in_array(\sanitize_title($tax_class), $this->settings->moss_tax_classes, \true)) {
            return array(\WC()->customer->get_billing_country(), \WC()->customer->get_billing_state(), \WC()->customer->get_billing_postcode(), \WC()->customer->get_billing_city());
        }
        return $location;
    }
}
