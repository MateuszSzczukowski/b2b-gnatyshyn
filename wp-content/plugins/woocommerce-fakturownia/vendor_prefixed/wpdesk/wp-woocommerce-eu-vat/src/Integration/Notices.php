<?php

/**
 * EU Vat Integration: Notices.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration;

use FakturowniaVendor\WPDesk\Notice\Notice;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
/**
 * Adds notifications for the administrator about plugin activation and rounding taxes at subtotal.
 *
 * @package WPDesk\Integration\EUVAT
 */
class Notices implements \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable
{
    /**
     * Is EU VAT Number plugins is enabled.
     *
     * @var bool
     */
    private $is_eu_vat_plugins_enabled;
    /**
     * Plugin name.
     *
     * @var string
     */
    private $plugin_name;
    /**
     * Plugin version.
     *
     * @var string
     */
    private $plugin_version;
    /**
     * Documentation link.
     *
     * @var string
     */
    private $doc_link;
    /**
     * Notices constructor.
     *
     * @param bool   $is_eu_vat_plugins_enabled Is EU VAT Number plugins is enabled.
     * @param string $plugin_name               Plugin name.
     * @param string $plugin_version            Plugin version.
     * @param string $doc_link                  Doc link.
     */
    public function __construct($is_eu_vat_plugins_enabled, $plugin_name, $plugin_version, $doc_link)
    {
        $this->is_eu_vat_plugins_enabled = $is_eu_vat_plugins_enabled;
        $this->plugin_name = $plugin_name;
        $this->plugin_version = $plugin_version;
        $this->doc_link = $doc_link;
    }
    /**
     * Fires hooks.
     */
    public function hooks()
    {
        if ($this->is_eu_vat_plugins_enabled) {
            \add_action('admin_init', [$this, 'deactivate_eu_vat_notice_action'], 10);
        } else {
            if ('no' === \get_option('woocommerce_tax_round_at_subtotal', 'no')) {
                \add_action('admin_init', [$this, 'round_taxes_at_subtotal_level_notice_action'], 12);
            }
        }
    }
    /**
     * Show notice about disabling Integration EU VAT Number.
     */
    public function deactivate_eu_vat_notice_action()
    {
        // translators: %1$s - plugin name, %2$s - plugin version, %3$s - docs link about moss.
        new \FakturowniaVendor\WPDesk\Notice\Notice(\sprintf(\__('<strong>%1$s plugin from version %2$s supports MOSS.</strong> <a href="%3$s" target="_blank">Here</a> you can read how to configure it. You no longer need any other plugins for invoicing with MOSS standard. Please deactivate them.', 'woocommerce-fakturownia'), $this->plugin_name, $this->plugin_version, $this->doc_link), 'warning', \true);
    }
    /**
     * Show notice about rounding taxes at subtotal level.
     */
    public function round_taxes_at_subtotal_level_notice_action()
    {
        // translators: settings link.
        new \FakturowniaVendor\WPDesk\Notice\Notice(\sprintf(\__('Tax rounding at subtotals is off. Please check "Rounding" option for invoice plugin to function properly. To find it click <a href="%s">here</a> or go to Integration tax settings.', 'woocommerce-fakturownia'), \admin_url('admin.php?page=wc-settings&tab=tax')), 'error', \true);
    }
}
