<?php

namespace FakturowniaVendor;

$params = isset($params) ? $params : array();
$countries = $params['countries'];
$vat_field_label = $params['vat_field_label'];
$vat_number = $params['vat_number'];
$vat_number_self_declared = $params['vat_number_self_declared'];
$customer_self_declared_country = $params['customer_self_declared_country'];
$ip_address = $params['ip_address'];
$ip_country = $params['ip_country'];
$billing_country = $params['billing_country'];
$validated = $params['validated'];
$valid = $params['valid'];
?>
<table class="vies-validation">
	<tbody>
	<tr>
		<td><?php 
\_e('B2B', 'woocommerce-fakturownia');
?></td>
		<td><?php 
echo $vat_number ? \__('Yes', 'woocommerce-fakturownia') : \__('No', 'woocommerce-fakturownia');
?></td>
		<td></td>
	</tr>

	<?php 
if ($vat_number) {
    ?>
		<tr>
			<td><?php 
    echo $vat_field_label;
    ?></td>
			<td>
				<?php 
    echo \esc_html($vat_number);
    ?>
				<?php 
    if (!$validated) {
        echo '<span class="tips" data-tip="' . \__('Validation was not possible', 'woocommerce-fakturownia') . '">?<span>';
    } else {
        echo $valid ? '&#10004;' : '&#10008;';
    }
    ?>
			</td>
		</tr>
		<tr>
			<td><?php 
    \_e('Self declared', 'woocommerce-fakturownia');
    ?></td>
			<td><?php 
    echo $vat_number_self_declared ? '&#10004;' : '&#10008;';
    ?></td>
		</tr>
	<?php 
} else {
    ?>
		<tr>
			<td><?php 
    \_e('IP Address', 'woocommerce-fakturownia');
    ?></td>
			<td><?php 
    echo $ip_address ? \esc_html($ip_address) : \__('Unknown', 'woocommerce-fakturownia');
    ?></td>
		</tr>
		<tr>
			<td><?php 
    \_e('IP Country', 'woocommerce-fakturownia');
    ?></td>
			<td><?php 
    if ($ip_country) {
        echo \esc_html__($countries[$billing_country]) . ' ';
        if ($billing_country === $ip_country) {
            echo '<span class="euvat-success">&#10004;</span>';
        } elseif ($customer_self_declared_country) {
            \esc_html_e('(self-declared)', 'woocommerce-fakturownia');
        } else {
            echo '<span class="euvat-error">&#10008;</span>';
        }
    } else {
        \esc_html_e('Unknown', 'woocommerce-fakturownia');
    }
    ?>
		</tr>
		<tr>
			<td><?php 
    \_e('Billing Country', 'woocommerce-fakturownia');
    ?></td>
			<td><?php 
    echo $billing_country ? \esc_html($countries[$billing_country]) : \__('Unknown', 'woocommerce-fakturownia');
    ?></td>
		</tr>
		<tr>
			<td><?php 
    \_e('Self declared', 'woocommerce-fakturownia');
    ?></td>
			<td><?php 
    echo $customer_self_declared_country ? '&#10004;' : '&#10008;';
    ?></td>
		</tr>
	<?php 
}
?>
	</tbody>
</table>
<?php 
