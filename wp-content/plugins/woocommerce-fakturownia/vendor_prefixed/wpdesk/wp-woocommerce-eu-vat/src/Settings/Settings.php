<?php

/**
 * EU Vat Integration: Library Settings.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings;

/**
 * Library data object settings.
 *
 * @package WPDesk\Integration\EUVAT\Settings
 */
final class Settings
{
    /**
     * VAT field label.
     *
     * @var string
     */
    public $vat_field_label = '';
    /**
     * VAT number field placeholder.
     *
     * @var string
     */
    public $vat_field_placeholder = '';
    /**
     * Set VAT number as required field.
     *
     * @var bool
     */
    public $vat_field_required = \false;
    /**
     * Validate VAT number by VIES.
     *
     * @var bool
     */
    public $eu_vat_vies_validate = \false;
    /**
     * Remove EU VAT from B2B base country.
     *
     * @var bool
     */
    public $eu_vat_remove_vat_from_base_b2b = \false;
    /**
     * Type of failure for EU VAT.
     *
     * @var string
     */
    public $eu_vat_failure_handling = 'reject';
    /**
     * Tax classes or MOSS.
     *
     * @var array
     */
    public $moss_tax_classes = [];
    /**
     * Validate IP for MOSS.
     *
     * @var bool
     */
    public $moss_validate_ip = \false;
}
