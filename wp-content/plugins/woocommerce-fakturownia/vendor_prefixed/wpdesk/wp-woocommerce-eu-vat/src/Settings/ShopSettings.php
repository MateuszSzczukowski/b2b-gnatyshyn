<?php

/**
 * EU Vat Integration: ShopSettings.
 *
 * @package WPDesk\Integration\EUVAT.
 */
namespace FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings;

/**
 * Shop settings.
 *
 * @package WPDesk\Integration\EUVAT\Settings
 */
class ShopSettings
{
    /**
     * Is EU VAT Number enabled?
     *
     * @return bool
     */
    public function is_eu_vat_plugins_enabled()
    {
        $active_plugins = \get_option('active_plugins');
        $eu_vat_number = 'woocommerce-eu-vat-number/woocommerce-eu-vat-number.php';
        $eu_vat_compliance = 'woocommerce-eu-vat-compliance-premium/eu-vat-compliance-premium.php';
        if (\in_array($eu_vat_number, $active_plugins) || \in_array($eu_vat_compliance, $active_plugins)) {
            return \true;
        }
        return \false;
    }
    /**
     * @param $customer_country
     *
     * @return bool
     */
    public function is_customer_from_base_country($customer_country)
    {
        return $customer_country === \get_option('woocommerce_default_country');
    }
    /**
     * Get IP address country for user.
     *
     * @return string
     */
    public function get_ip_country()
    {
        $geoip = \WC_Geolocation::geolocate_ip();
        $ip_country = $geoip['country'];
        return $ip_country;
    }
    /**
     * Get EU Countries.
     *
     * @return array
     */
    public function get_eu_countries()
    {
        $countries = \WC()->countries->get_european_union_countries();
        if (isset($countries['GB'])) {
            unset($countries['GB']);
        }
        return $countries;
    }
}
