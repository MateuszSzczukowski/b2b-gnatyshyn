jQuery( document ).ready( function ( $ ) {

	function fakturownia_bill_or_invoice() {
		let element = jQuery( '#woocommerce_integration-fakturownia_document_type' );
		if ( element.length ) {
			let value = element.val();
			let bill = jQuery( '.fakturownia-bill' ).closest( 'tr' );
			let invoice_without_vat = jQuery( '.fakturownia-invoice-without-vat' ).closest( 'tr' );

			let bill_h3 = jQuery( 'h3.fakturownia-bill' );
			let invoice_without_vat_h3 = jQuery( 'h3.fakturownia-invoice-without-vat' );
			if ( value === 'bill' ) {
				bill.show();
				invoice_without_vat.hide();
				bill_h3.show();
				invoice_without_vat_h3.hide();
				var fakturownia_generate = jQuery('.wrap').find( '.option-generate' );
			} else {
				bill.hide();
				invoice_without_vat.show();
				bill_h3.hide();
				invoice_without_vat_h3.show();
			}
		}
	}

	function fakturownia_invoice_status_change( element ) {
		var manual_generate = 'r';
		var ask_manual_generate = 'n';
		if( element.val() !== manual_generate && element.val() !== ask_manual_generate ) {
			element.closest( 'tr' ).next().show();
		} else {
			element.closest( 'tr' ).next().hide();
		}
	}

	function fakturownia_exempt_tax_kind_change() {
		let element = jQuery( '#woocommerce_integration-fakturownia_exempt_tax_kind' );
		if ( element.length ) {
			let value = element.val();
			let pkwiu_attribute = jQuery( '#woocommerce_integration-fakturownia_pkwiu_attribute' );
			let legal_basis = jQuery( '#woocommerce_integration-fakturownia_legal_basis' );
			if ( value === 'none' ) {
				pkwiu_attribute.closest( 'tr' ).hide();
				legal_basis.closest( 'tr' ).hide();
			} else {
				pkwiu_attribute.closest( 'tr' ).show();
				legal_basis.closest( 'tr' ).show();
			}
		}
	}

	var fakturownia_exempt_tax_kind = jQuery( '#woocommerce_integration-fakturownia_exempt_tax_kind' );
	var fakturownia_document_type = jQuery( '#woocommerce_integration-fakturownia_document_type' );
	var fakturownia_synchronization = jQuery( '#woocommerce_integration-fakturownia_synchronization' );


	function fakturownia_synchronization_change() {
		let element = jQuery( '#woocommerce_integration-fakturownia_synchronization' );
		let synchronization_elements = jQuery( '.fakturownia-synchronization' ).closest( 'tr' );
		if ( element.is( ':checked' ) ) {
			synchronization_elements.show();
		} else {
			synchronization_elements.hide();
		}
	}

	var fakturownia_generate = jQuery('.wrap').find( '.option-generate' );
	jQuery.each( fakturownia_generate, function() {
		fakturownia_invoice_status_change( $(this) );
	});
	fakturownia_generate.change( function () {
		fakturownia_invoice_status_change( $( this ) );
	} );

	fakturownia_document_type.change( function () {
		fakturownia_bill_or_invoice();
	} );
	fakturownia_bill_or_invoice();

	fakturownia_synchronization.change( function () {
		fakturownia_synchronization_change();
	} );
	fakturownia_synchronization_change();



	fakturownia_exempt_tax_kind.change( function () {
		fakturownia_exempt_tax_kind_change();
	} );
	fakturownia_exempt_tax_kind_change();

	if ( jQuery().selectWoo ) {
		jQuery( '.fakturownia-select2' ).selectWoo();
	} else {
		if ( jQuery().select2 ) {
			jQuery( '.fakturownia-select2' ).select2();
		}
	}

	var fakturownia_moss = jQuery( '#woocommerce_integration-fakturownia_moss_vies_validation' );

	function fakturownia_moss_change() {
		let element = jQuery( '#woocommerce_integration-fakturownia_moss_vies_validation' );
		let moss_elements = jQuery( '.fakturownia-moss' ).closest( 'tr' );
		if ( element.is( ':checked' ) ) {
			moss_elements.show();
		} else {
			moss_elements.hide();
		}
	}

	fakturownia_moss.change( function () {
		fakturownia_moss_change();
	} );
	fakturownia_moss_change();

	jQuery( '.js-nav-tab-wrapper a' ).click( function () {
		jQuery( this ).parent().find( '.nav-tab-active' ).removeClass( 'nav-tab-active' );
		jQuery( this ).addClass( 'nav-tab-active' );
		jQuery( this ).closest( 'form' ).find( '.field-settings-tab-container' ).hide();
		jQuery( '.field-settings-' + jQuery( this ).attr( 'href' ).replace( '#', '' ) ).show();
		wpCookies.set( 'fakturownia-settings-tab', jQuery( this ).attr( 'href' ).replace( '#', '' ) );
	} );

	var tab_cookie = wpCookies.get( 'fakturownia-settings-tab' );
	if ( tab_cookie ) {
		var tab_element = jQuery( '.nav-tab-' + tab_cookie );
		tab_element.click();
	} else {
		jQuery( '.field-settings-tab-container' ).first().show()
	}

} );
