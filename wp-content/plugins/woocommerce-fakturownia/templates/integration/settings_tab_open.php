<?php
/**
 * Fakturownia settings tab template.
 *
 * @package WPDesk\WooCommerceFakturownia
 *
 * @var $data array
 */

?>
<div class="field-settings-tab-container field-settings-<?php echo sanitize_title( $data['title'] ); ?> <?php echo esc_attr( $data['class'] ); ?>" style="display: none">
	<table class="form-table">
