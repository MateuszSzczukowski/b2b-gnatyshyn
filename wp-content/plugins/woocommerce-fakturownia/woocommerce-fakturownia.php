<?php
/**
 * Plugin Name: WooCommerce Fakturownia
 * Plugin URI: https://www.wpdesk.pl/sklep/fakturownia-woocommerce/
 * Description: Wtyczka integrująca WooCommerce z programem do faktur online Fakturownia.
 * Version: 1.5.1
 * Author: WP Desk
 * Author URI: https://www.wpdesk.pl/
 * Text Domain: woocommerce-fakturownia
 * Domain Path: /lang/
 * Requires at least: 5.2
 * Tested up to: 5.6
 * WC requires at least: 4.6
 * WC tested up to: 5.1
 * Requires PHP: 7.0
 *
 * Copyright 2017 WP Desk Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package WooCommerce Fakturownia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/* THESE TWO VARIABLES CAN BE CHANGED AUTOMATICALLY */
$plugin_version           = '1.5.1';
$plugin_release_timestamp = '2021-02-24 09:46';

$plugin_name        = 'WooCommerce Fakturownia';
$plugin_class_name  = '\WPDesk\WooCommerceFakturownia\Plugin';
$plugin_text_domain = 'woocommerce-fakturownia';
$product_id         = 'WooCommerce Fakturownia';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

define( 'WOOCOMMERCE_FAKTUROWNIA_VERSION', $plugin_version );
define( 'WOOCOMMERCE_FAKTUROWNIA_DIR', $plugin_dir );
define( $plugin_class_name, $plugin_version );

$requirements = array(
	'php'     => '5.6',
	'wp'      => '4.5',
	'plugins' => array(
		array(
			'name'      => 'woocommerce/woocommerce.php',
			'nice_name' => 'WooCommerce',
		),
	),
);

require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';
