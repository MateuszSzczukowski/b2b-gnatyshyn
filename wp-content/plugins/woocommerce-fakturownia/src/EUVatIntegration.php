<?php
/**
 * Plugin: Integrate with EU VAT library.
 *
 * @package WPDesk\WooCommerceFakturownia
 */

namespace WPDesk\WooCommerceFakturownia;

use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\WooCommerceEUVAT;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * EU VAT Integration
 *
 * @package WPDesk\WooCommerceFakturownia
 */
class EUVatIntegration extends WooCommerceEUVAT implements Hookable {

	/**
	 * Is EU VAT plugins is enabled?
	 *
	 * @return bool
	 */
	protected function is_eu_vat_plugins_enabled() {
		return false;
	}

	/**
	 * Load dependencies.
	 *
	 * We don't need notices.
	 */
	public function load_dependencies() {
		if ( $this->settings->eu_vat_vies_validate && ! $this->is_eu_vat_plugins_enabled() ) {
			$meta_data = new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\MetaData( $this->shop_settings );
			$this->add_hookable( new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\VatField( $this->settings ) );
			$this->add_hookable( new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Integration\Checkout( $this->shop_settings, $this->settings, $meta_data, $this->renderer, $this->get_validator() ) );
			$this->add_hookable( $meta_data );
			$this->add_hookable( new \FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Vies\ViesOrderMetaBox( $this->settings, $this->shop_settings, $this->renderer ) );
		}
	}

}
