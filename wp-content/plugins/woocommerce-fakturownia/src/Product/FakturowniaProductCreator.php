<?php

namespace WPDesk\WooCommerceFakturownia\Product;

use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\WooCommerceFakturownia\Api\FakturowniaApi;

/**
 * Creates product in Fakturownia.
 */
class FakturowniaProductCreator {

	/**
	 * API.
	 *
	 * @var FakturowniaApi
	 */
	private $fakturownia_api;

	/**
	 * @var int
	 */
	private $warehouse_id;

	/**
	 * FakturowniaProductIdAjax constructor.
	 *
	 * @param FakturowniaApi $fakturownia_api .
	 */
	public function __construct( FakturowniaApi $fakturownia_api, $warehouse_id ) {
		$this->fakturownia_api = $fakturownia_api;
		$this->warehouse_id = $warehouse_id;
	}

	/**
	 * Create from Woocommerce product.
	 *
	 * @param \WC_Product $product .
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException .
	 */
	public function create_from_product( \WC_Product $product ) {
		$sku       = $product->get_sku();
		$image_url = '';
		if ( $product->get_image_id() ) {
			$image_src = wp_get_attachment_image_src( $product->get_image_id() );
			$image_url = $image_src[0];
		}

		$product_create_response = $this->fakturownia_api->create_product(
			apply_filters( 'fakturownia/core/create_product/name', $product->get_name(), $product ),
			apply_filters( 'fakturownia/core/create_product/sku', $sku, $product ),
			apply_filters( 'fakturownia/core/create_product/description', $product->get_description(), $product ),
			apply_filters( 'fakturownia/core/create_product/img_url', $image_url, $product )
		);

		return $product_create_response->get_product();
	}

	/**
	 * Set fakturownia product id on WooCommerce product.
	 *
	 * @param \WC_Product $product .
	 * @param string      $fakturownia_product_id .
	 */
	public function set_fakturownia_product_id_on_product( \WC_Product $product, $fakturownia_product_id ) {
		$product->update_meta_data( FakturowniaProductId::FAKTUROWNIA_PRODUCT_ID, $fakturownia_product_id );
		$product->save();
	}

}
