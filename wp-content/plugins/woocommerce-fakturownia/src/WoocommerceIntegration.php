<?php

namespace WPDesk\WooCommerceFakturownia;

use FakturowniaVendor\WPDesk\Invoices\Field\VatNumber;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WC_Integration;
use FakturowniaVendor\WPDesk\View\PluginViewBuilder;
use FakturowniaVendor\WPDesk\WooCommerce\EUVAT\Settings\Settings;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use WPDesk\WooCommerceFakturownia\Forms\Integration\AuthorizationForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\BillForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\DebugForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\ReceiptForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\StockForm;
use WPDesk\WooCommerceFakturownia\Webhoks\StockListener;
use \FakturowniaVendor\WPDesk\Forms\FormsCollection;

/**
 * Class WoocommerceIntegration
 *
 * @package WPDesk\WooCommerceFakturownia
 */
class WoocommerceIntegration extends WC_Integration {

	const INTEGRATION_ID = 'integration-fakturownia';
	const MODULE_TITLE = 'WooCommerce Fakturownia';

	/**
	 * @var InvoicesIntegration
	 */
	protected $invoices_integration;

	/**
	 * @var string
	 */
	private $calc_taxes;

	/**
	 * @var Renderer
	 */
	public static $renderer;

	/**
	 * @var LoggerInterface
	 */
	public static $logger;

	/**
	 * WoocommerceIntegration constructor.
	 */
	public function __construct() {
		$this->id                 = self::INTEGRATION_ID;
		$this->method_title       = __( 'Fakturownia', 'woocommerce-fakturownia' );
		$this->method_description = __( 'Wystawianie faktur w serwisie fakturownia.pl. <a href="https://www.wpdesk.pl/docs/fakturownia-woocommerce-docs/" target="_blank">Instrukcja konfiguracji &rarr;</a>' );

		$this->invoices_integration = new InvoicesIntegration( $this );
		$this->invoices_integration->hooks();

		$this->calc_taxes = 'yes' === get_option( 'woocommerce_calc_taxes', 'no' );

		$this->init_form_fields();
		$this->init_settings();

		add_action( 'woocommerce_update_options_integration_' . $this->id, [ $this, 'process_admin_options' ], 11 );
	}

	/**
	 * Settings mapping for EU VAT Number.
	 *
	 * @return Settings
	 */
	public function eu_vat_settings() {
		$settings                                  = new Settings();
		$settings->vat_field_enabled               = true;
		$settings->vat_field_label                 = __( 'VAT', 'woocommerce-fakturownia' );
		$settings->vat_field_placeholder           = __( 'Vat number', 'woocommerce-fakturownia' );
		$settings->eu_vat_vies_validate            = 'yes' === $this->get_option( InvoiceForm::OPTION_MOSS_VIES_VALIDATION, 'no' );
		$settings->eu_vat_remove_vat_from_base_b2b = false;
		$settings->eu_vat_failure_handling         = $this->get_option( InvoiceForm::OPTION_MOSS_FAILURE_HANDLING, 'reject' );
		$settings->moss_tax_classes                = $this->get_option( InvoiceForm::OPTION_MOSS_TAX_CLASSES, [] );
		$settings->moss_validate_ip                = 'yes' === $this->get_option( InvoiceForm::OPTION_MOSS_VALIDATE_IP, 'no' );

		return $settings;
	}

	/**
	 * Set logger.
	 *
	 * @param LoggerInterface LoggerInterface .
	 */
	public static function set_logger( LoggerInterface $logger ) {
		self::$logger = $logger;
	}

	/**
	 * Get renderer.
	 *
	 * @return LoggerInterface
	 */
	public function get_logger() {
		return self::$logger;
	}

	/**
	 * Set renderer.
	 *
	 * @param Renderer $renderer .
	 */
	public static function set_renderer( Renderer $renderer ) {
		self::$renderer = $renderer;
	}

	/**
	 * Get renderer.
	 *
	 * @return Renderer
	 */
	public function get_renderer() {
		return self::$renderer;
	}

	public function init_fields_setup() {
		if ( '' === $this->get_option( StockForm::OPTION_WAREHOUSE_WEBHOOK_TOKEN, '' ) ) {
			$this->update_option( StockForm::OPTION_WAREHOUSE_WEBHOOK_TOKEN, md5( rand( 1, 10000 ) + time() ) );
		}
	}

	/**
	 * Initialize integration settings form fields.
	 * @throws \Exception
	 */
	public function init_form_fields() {
		parent::init_form_fields();
		$this->init_fields_setup();

		$formsCollection = new FormsCollection();

		$stock = new Forms\Integration\StockForm();
		$stock->set_integration( $this->invoices_integration );
		$formsCollection->add_forms( [
			new Forms\Integration\AuthorizationForm(),
			new Forms\Integration\ReceiptForm(
				true
			),
			new Forms\Integration\InvoiceForm(
				$this->calc_taxes
			),
			new Forms\Integration\BillForm(
				! $this->calc_taxes
			),
			$stock,
			new Forms\Integration\DebugForm(),
		] );

		$this->form_fields = $formsCollection->get_forms_data();
	}

	/**
	 * Woocommerce hook override. It gets form fields, load template, render view and display.
	 *
	 * @param array $form_fields
	 * @param bool $echo
	 */
	public function generate_settings_html( $form_fields = array(), $echo = true ) {
		$form_fields = empty( $form_fields ) ? $this->get_form_fields() : $form_fields;

		echo $this->get_renderer()->render( 'integration/settings', array(
			'form_fields'       => $form_fields,
			'rendered_settings' => parent::generate_settings_html( $form_fields, false ),
			'text_domain'       => 'woocommerce-fakturownia',
			'module_title'      => self::MODULE_TITLE
		) );
	}

	/**
	 * Woocommerce hook. Starts tab container and return rendered view as string.
	 *
	 * @param string $key
	 * @param array $data
	 *
	 * @return string
	 */
	public function generate_tab_open_html( $key, $data ) {
		$defaults = array(
			'title' => '',
			'class' => ''
		);

		return $this->get_renderer()->render( 'integration/settings_tab_open', array(
			'data' => wp_parse_args( $data, $defaults )
		) );

	}

	/**
	 * Woocommerce hook. End tab container and return rendered view as string.
	 *
	 * @param string $key
	 * @param array $data
	 *
	 * @return string
	 */
	public function generate_tab_close_html( $key, $data ) {
		return $this->get_renderer()->render( 'integration/settings_tab_close' );
	}

	/**
	 * Get option: user token.
	 *
	 * @return string.
	 */
	public function getUserToken() {
		return $this->get_option( AuthorizationForm::OPTION_TOKEN );
	}

	/**
	 * Get option: user token.
	 *
	 * @return string.
	 */
	public function getDepartmentId() {
		return $this->get_option( AuthorizationForm::OPTION_DEPARTMENT_ID );
	}

	/**
	 * Get option: user name.
	 *
	 * @return string.
	 */
	public function getAccountName() {
		return $this->get_option( AuthorizationForm::OPTION_ACCOUNT_NAME );
	}

	/**
	 * Get option generate invoice.
	 *
	 * @return string
	 */
	public function getOptionGenerateInvoice() {
		return $this->get_option( InvoiceForm::OPTION_GENERATE_INVOICE );
	}

	/**
	 * Get option generate invoice.
	 *
	 * @return string
	 */
	public function getOptionGenerateReceipt() {
		return $this->get_option( ReceiptForm::OPTION_GENERATE_RECEIPT );
	}

	/**
	 * Get option invoice status.
	 *
	 * @return string|array
	 */
	public function getOptionGenerateInvoiceStatus() {
		return $this->get_option( InvoiceForm::OPTION_GENERATE_INVOICE_STATUS );
	}

	/**
	 * Get option invoice status.
	 *
	 * @return string|array
	 */
	public function getOptionGenerateReceiptStatus() {
		return $this->get_option( ReceiptForm::OPTION_GENERATE_RECEIPT_STATUS );
	}

	/**
	 * Get option generate bill.
	 *
	 * @return string
	 */
	public function getOptionGenerateBill() {
		return $this->get_option( BillForm::OPTION_GENERATE_BILL );
	}

	/**
	 * Get option bill status.
	 *
	 * @return string|array
	 */
	public function getOptionGenerateBillStatus() {
		return $this->get_option( BillForm::OPTION_GENERATE_BILL_STATUS );
	}

	/**
	 * Get option autosend invoice.
	 *
	 * @return string
	 */
	public function getOptionAutoSendInvoice() {
		return $this->get_option( InvoiceForm::OPTION_AUTOSEND_INVOICE, 'no' );
	}


	/**
	 * Get option invoice proforma status.
	 *
	 * @return array
	 */
	public function getOptionGenerateProformaStatus() {
		return (array) $this->get_option( InvoiceForm::OPTION_GENERATE_PROFORMA_STATUS, [] );
	}

	/**
	 * Get option autosend invoice proforma.
	 *
	 * @return string
	 */
	public function getOptionAutoSendInvoiceProforma() {
		return $this->get_option( InvoiceForm::OPTION_AUTOSEND_INVOICE_PROFORMA, 'no' );
	}

	/**
	 * Get option autosend invoice.
	 *
	 * @return string
	 */
	public function getOptionAutoSendReceipt() {
		return $this->get_option( ReceiptForm::OPTION_AUTOSEND_RECEIPT, 'no' );
	}

	/**
	 * Get option autosend bill.
	 *
	 * @return string
	 */
	public function getOptionAutoSendBill() {
		return $this->get_option( BillForm::OPTION_AUTOSEND_BILL, 'no' );
	}

	/**
	 * Get option place od issue.
	 *
	 * @return string
	 */
	public function getOptionPlaceOfIssue() {
		return $this->get_option( InvoiceForm::OPTION_PLACE_OF_ISSUE );
	}

	/**
	 * Get option payment date.
	 *
	 * @return string
	 */
	public function getOptionPaymentDate() {
		return $this->get_option( InvoiceForm::OPTION_PAYMENT_DATE );
	}

	/**
	 * Get option proforma payment date.
	 *
	 * @return string
	 */
	public function getProformaOptionPaymentDate() {
		return $this->get_option( InvoiceForm::OPTION_PAYMENT_TO_KIND );
	}

	/**
	 * @return string
	 */
	public function getOptionInvoiceLang() {
		return $this->get_option( InvoiceForm::OPTION_INVOICE_LANG );
	}

	/**
	 * Get option PKWiU.
	 *
	 * @return string
	 */
	public function getOptionReceiptLang() {
		return $this->get_option( ReceiptForm::OPTION_RECEIPT_LANG );
	}

	/**
	 * Get option proforma payment date.
	 *
	 * @return string
	 */
	public function getReceiptOptionPaymentDate() {
		return $this->get_option( ReceiptForm::OPTION_PAYMENT_DATE );
	}


	/**
	 * Get option comment content.
	 *
	 * @return string
	 */
	public function getOptionCommentContent() {
		return $this->get_option( InvoiceForm::OPTION_COMMENT_CONTENT );
	}

	/**
	 * Get option comment content for receipt.
	 *
	 * @return string
	 */
	public function getOptionCommentContentForReceipt() {
		return $this->get_option( ReceiptForm::OPTION_COMMENT_CONTENT );
	}

	/**
	 * Get option tax_rate_exempt.
	 *
	 * @return string
	 */
	public function getOptionTaxRateExempt() {
		return $this->get_option( InvoiceForm::OPTION_EXEMPT_TAX_KIND );
	}

	/**
	 * Get option legal basis.
	 *
	 * @return string
	 */
	public function getOptionLegalBasis() {
		return $this->get_option( InvoiceForm::OPTION_LEGAL_BASIS );
	}

	/**
	 * Get option PKWiU.
	 *
	 * @return string
	 */
	public function getOptionPkwiuAttribute() {
		return $this->get_option( InvoiceForm::OPTION_PKWIU_ATTRIBUTE );
	}

	/**
	 * Get option document_type.
	 *
	 * @return string
	 */
	public function getOptionDocumentType() {
		return $this->get_option(
			InvoiceForm::OPTION_DOCUMENT_TYPE,
			BillForm::OPTION_DOCUMENT_TYPE_BILL
		);
	}

	/**
	 * Get option sller name
	 *
	 * @return string
	 */
	public function getOptionSellerName() {
		return $this->get_option( InvoiceForm::OPTION_SELLER_NAME );
	}

	/**
	 * Get option synchronization.
	 *
	 * @return string
	 */
	public function getOptionSynchronization() {
		return $this->get_option( StockForm::OPTION_SYNCHRONIZATION );
	}

	/**
	 * Get option warehouse ID.
	 *
	 * @return string
	 */
	public function getWarehouseID() {
		return $this->get_option( StockForm::OPTION_WAREHOUSE_ID, '' );
	}

	/**
	 * Get option create products.
	 *
	 * @return string
	 */
	public function getOptionCreateProducts() {
		return $this->get_option( StockForm::OPTION_CREATE_PRODUCTS );
	}

	/**
	 * Get option warehouse webhook token.
	 *
	 * @return string
	 */
	public function getOptionWarehouseWebhookToken() {
		return $this->get_option( StockForm::OPTION_WAREHOUSE_WEBHOOK_TOKEN );
	}

	/**
	 * Get option if NIP in checkout should be validated.
	 *
	 * @return bool
	 */
	public function isOptionValidateCheckoutNip() {
		return $this->get_option( InvoiceForm::OPTION_VALIDATE_CHECKOUT_NIP ) === 'yes';
	}

	/**
	 * @return bool
	 */
	public function isDebugEnabled() {
		return $this->get_option( DebugForm::OPTION_DEBUG_ENABLED ) === 'yes';
	}

}
