<?php

namespace WPDesk\WooCommerceFakturownia\Forms;

/**
 * Interface ConditionalFormInterface
 *
 * @package WPDesk\WooCommerceFakturownia\Forms
 */
interface ConditionalFormInterface
{

	public function is_active();
}

