<?php

namespace WPDesk\WooCommerceFakturownia\Forms\Integration;

use FakturowniaVendor\WPDesk\Forms\AbstractForm;
use WPDesk\WooCommerceFakturownia\Api\Products\NoProductsFoundException;
use WPDesk\WooCommerceFakturownia\Webhoks\StockListener;
use WPDesk\WooCommerceFakturownia\InvoicesIntegration;

/**
 * Class StockForm
 *
 * @package WPDesk\WooCommerceFakturownia\Forms\Integration
 */
class StockForm extends AbstractForm {

	/**
	 * Unique form_id.
	 *
	 * @var string
	 */
	protected $form_id = 'stock';

	const OPTION_WAREHOUSE_WEBHOOK_TOKEN = 'warehouse_webhook_token';
	const OPTION_WAREHOUSE_ID            = 'warehouse_id';
	const OPTION_WAREHOUSE_WEBHOOK       = 'warehouse_webhook';
	const OPTION_SYNCHRONIZATION         = 'synchronization';
	const OPTION_CREATE_PRODUCTS         = 'create_products';

	/**
	 * @var InvoicesIntegration
	 */
	private $invoices_integration;

	public function set_integration( InvoicesIntegration $invoices_integration ) {
		$this->invoices_integration = $invoices_integration;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	private function warehouses_options() {
		$warehouses = [];
		if( !$this->invoices_integration->has_account_name() ){
			return $warehouses;
		}

		try {
			$warehouses_api = $this->invoices_integration->get_fakturownia_api()->get_warehouses()->get_warehouses();
		} catch ( \Exception $e ) {
			$warehouses_api = [];
		}
		if ( ! empty( $warehouses_api ) ) {
			foreach ( $warehouses_api as $warehouse ) {
				$name = $warehouse['name'];
				if( $warehouse['kind'] === 'main' ) {
					$general_warehouse = [ $warehouse['id'] => $warehouse['name'] . ' - Główny' ];
				} else {
					$warehouses[ $warehouse['id'] ] = $name;
				}

			}

			if( isset( $general_warehouse ) ) {
				$warehouses =  $general_warehouse + $warehouses;
			}

		}

		return $warehouses;
	}

	/**
	 * Create form data and return an associative array.
	 *
	 * @return array
	 */
	protected function create_form_data() {
		$warehouses                = $this->warehouses_options();
		$synchronization_css_class = 'fakturownia-synchronization';

		return [
			'magazyn'                            => array(
				'title'       => __( 'Magazyn', 'woocommerce-fakturownia' ),
				'type'        => 'tab_open',
				'description' => __( '', 'woocommerce-fakturownia' )
			),
			self::OPTION_SYNCHRONIZATION         => [
				'title'       => __( 'Synchronizacja', 'woocommerce-fakturownia' ),
				'label'       => __( 'Włącz synchronizację stanów magazynowych', 'woocommerce-fakturownia' ),
				'type'        => 'checkbox',
				'desc_tip'    => false,
				// Translators: link.
				'description' => sprintf( __( 'Wymagana jest konfiguracja w fakturownia.pl. %1$sZapoznaj się z dokumentacją%2$s', 'woocommerce-fakturownia' ),
					sprintf( '<a href="%1$s" target="blank">', esc_url( 'https://wpde.sk/webhook-fakturownia' ) ),
					'</a>'
				),
				'default'     => 'no',
			],
			self::OPTION_WAREHOUSE_ID            => [
				'title'       => __( 'ID magazynu', 'woocommerce-fakturownia' ),
				'label'       => __( 'Wybierz magazyn z którym chcesz się połączyć', 'woocommerce-fakturownia' ),
				'type'        => 'select',
				'options'     => $warehouses,
				'class'       => $synchronization_css_class,
			],
			self::OPTION_CREATE_PRODUCTS         => [
				'title'       => __( 'Nowe produkty', 'woocommerce-fakturownia' ),
				'label'       => __( 'Włącz dodawanie produktów do magazynu Fakturowni', 'woocommerce-fakturownia' ),
				'type'        => 'checkbox',
				'desc_tip'    => false,
				'description' => __( 'Uwaga! Dodawanie produktu nie ustawia ilości produktu w serwisie fakturownia.pl. Opcja umożliwia dodanie nowego produktu podczas wystawiania faktury. Produkt zostanie dodany jeśli nie został odnaleziony produkt po ID produktu lub kodzie produktu.', 'woocommerce-fakturownia' ),
				'default'     => 'no',
				'class'       => $synchronization_css_class,
			],
			self::OPTION_WAREHOUSE_WEBHOOK       => [
				'title'             => __( 'Adres webhooka', 'woocommerce-fakturownia' ),
				'type'              => 'text',
				'desc_tip'          => false,
				'default'           => site_url( '?' . StockListener::URL_PARAMETER . '=1' ),
				'custom_attributes' => array(
					'readonly' => 'readonly',
				),
				'class'             => $synchronization_css_class,
			],
			self::OPTION_WAREHOUSE_WEBHOOK_TOKEN => [
				'title'             => __( 'Token webhooka', 'woocommerce-fakturownia' ),
				'type'              => 'text',
				'desc_tip'          => false,
				'class'             => $synchronization_css_class,
				'custom_attributes' => array(
					'readonly' => 'readonly',
				),
			],
			'magazyn_end'                        => array(
				'type' => 'tab_close',
			),
		];
	}

}
