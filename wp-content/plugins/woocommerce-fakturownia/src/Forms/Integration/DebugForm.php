<?php

namespace WPDesk\WooCommerceFakturownia\Forms\Integration;

use FakturowniaVendor\WPDesk\Forms\AbstractForm;

/**
 * Class DebugForm
 *
 * @package WPDesk\WooCommerceFakturownia\Forms\Integration
 */
class DebugForm extends AbstractForm {

	/**
	 * Unique form_id.
	 *
	 * @var string
	 */
	protected $form_id = 'debug';

	const OPTION_DEBUG_ENABLED = 'debug_enabled';

	/**
	 * Create form data and return an associative array.
	 *
	 * @return array
	 */
	protected function create_form_data() {
		return array(
			'debug'                    => array(
				'title'       => __( 'Tryb debugowania', 'woocommerce-fakturownia' ),
				'type'        => 'tab_open',
				'description' => __( 'Zapis komunikatów do pliku', 'woocommerce-fakturownia' )
			),
			self::OPTION_DEBUG_ENABLED => array(
				'title'   => __( 'Tryb debugowania', 'woocommerce-fakturownia' ),
				'type'    => 'checkbox',
				'default' => 'no',
				'label'   => __( 'Włącz', 'woocommerce-fakturownia' ),
			),
			'debug_end'                => array(
				'type' => 'tab_close',
			),
		);
	}
}
