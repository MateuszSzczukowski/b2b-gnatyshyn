<?php

namespace WPDesk\WooCommerceFakturownia;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use FakturowniaVendor\WPDesk\Logger\WPDeskLoggerFactory;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin;
use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use FakturowniaVendor\WPDesk\View\Renderer\SimplePhpRenderer;
use FakturowniaVendor\WPDesk\View\Resolver\ChainResolver;
use FakturowniaVendor\WPDesk\View\Resolver\DirResolver;
use FakturowniaVendor\WPDesk\View\Resolver\WPThemeResolver;
use Psr\Log\NullLogger;
use WPDesk\WooCommerceFakturownia\Product\FakturowniaProductIdSaver;
use WPDesk\WooCommerceFakturownia\Tracker\SettingsDataProvider;
use WPDesk\WooCommerceFakturownia\Tracker\Tracker;

/**
 * Class Plugin
 *
 * @package WPDesk\WooCommerceFakturownia
 */
class Plugin extends AbstractPlugin implements LoggerAwareInterface, HookableCollection {
	use LoggerAwareTrait;
	use \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\TemplateLoad;
	use \FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookableParent;

	/**
	 * Scripts version.
	 *
	 * @var string
	 */
	private $scripts_version = WOOCOMMERCE_FAKTUROWNIA_VERSION . '.10';

	/**
	 * Renderer.
	 *
	 * @var \FakturowniaVendor\WPDesk\View\Renderer\Renderer;
	 */
	private $renderer;

	/**
	 * Plugin constructor.
	 *
	 * @param \FakturowniaVendor\WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( \FakturowniaVendor\WPDesk_Plugin_Info $plugin_info ) {
		parent::__construct( $plugin_info );

		$this->plugin_url        = $this->plugin_info->get_plugin_url();
		$this->plugin_path       = $this->plugin_info->get_plugin_dir();
		$this->template_path     = $this->plugin_info->get_text_domain();
		$this->plugin_namespace  = $this->plugin_info->get_text_domain();
		$this->template_path     = $this->plugin_info->get_text_domain();
		$this->settings_url      = admin_url( 'admin.php?page=wc-settings&tab=integration&section=integration-fakturownia' );
		$this->default_view_args = [ 'plugin_url' => $this->get_plugin_url() ];
	}

	/**
	 * Init.
	 */
	public function init() {
		$this->setLogger( $this->is_debug_mode() ? ( new WPDeskLoggerFactory() )->createWPDeskLogger() : new NullLogger() );
		$this->load_dependencies();
		$this->init_renderer();
		$this->hooks();
		WoocommerceIntegration::set_renderer( $this->renderer );
		WoocommerceIntegration::set_logger( $this->logger );
	}

	/**
	 * Load dependencies.
	 */
	private function load_dependencies() {
		$this->add_hookable( new \WooCommerceFakturowniaRegisterEmails() );
		$this->add_hookable( new ConfigInfo() );
		$this->add_hookable( new FakturowniaProductIdSaver() );

		$tracker_factory = new \WPDesk_Tracker_Factory();
		$tracker         = $tracker_factory->create_tracker( $this->plugin_info->get_plugin_dir() );
		$tracker->add_data_provider( new SettingsDataProvider() );

		$this->add_hookable( new Tracker() );
	}

	/**
	 * Set renderer.
	 */
	private function init_renderer() {
		$resolver = new ChainResolver();
		$resolver->appendResolver( new WPThemeResolver( $this->get_template_path() ) );
		$resolver->appendResolver( new DirResolver( trailingslashit( $this->plugin_path ) . 'templates' ) );
		$this->renderer = new SimplePhpRenderer( $resolver );
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		parent::hooks();
		add_filter( 'woocommerce_integrations', array( $this, 'add_woocommerce_integration' ), 20 );
		$this->hooks_on_hookable_objects();

	}

	/**
	 * Add WooCommerce integration.
	 *
	 * @param array $integrations Integrations.
	 *
	 * @return array
	 */
	public function add_woocommerce_integration( array $integrations ) {
		$integrations[] = WoocommerceIntegration::class;

		return $integrations;
	}

	/**
	 * Get plugin path.
	 *
	 * @return string
	 */
	public function get_plugin_path() {
		return $this->plugin_path;
	}

	/**
	 * Admin enqueue scripts.
	 */
	public function admin_enqueue_scripts() {
		$screen = get_current_screen();
		if ( in_array( $screen->id, array(
			'woocommerce_page_wc-settings',
			'edit-shop_order',
			'shop_order',
		), true ) ) {
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			wp_register_style(
				'fakturownia_admin_css',
				trailingslashit( $this->get_plugin_assets_url() ) . 'css/admin' . $suffix . '.css',
				array(),
				$this->scripts_version
			);
			wp_enqueue_style( 'fakturownia_admin_css' );
			wp_register_script(
				'fakturownia_admin_js',
				trailingslashit( $this->get_plugin_assets_url() ) . 'js/admin' . $suffix . '.js',
				array( 'jquery' ),
				$this->scripts_version,
				true
			);
			wp_enqueue_script( 'fakturownia_admin_js' );
		}
	}

	/**
	 * Returns true when debug mode is on.
	 *
	 * @return bool
	 */
	private function is_debug_mode() {
		$helper_options = get_option( 'wpdesk_helper_options', [] );

		return isset( $helper_options['debug_log'] ) && '1' === $helper_options['debug_log'];
	}

	/**
	 * Links filter.
	 *
	 * @param array $links Links.
	 *
	 * @return array
	 */
	public function links_filter( $links ) {
		$plugin_links = [
			'<a href="' . $this->settings_url . '">' . __( 'Ustawienia', 'woocommerce-fakturownia' ) . '</a>',
			'<a href="https://www.wpdesk.pl/docs/fakturownia-woocommerce-docs/">' . __( 'Dokumentacja', 'woocommerce-fakturownia' ) . '</a>',
			'<a href="https://www.wpdesk.pl/support/">' . __( 'Support', 'woocommerce-fakturownia' ) . '</a>',
		];
		return array_merge( $plugin_links, $links );
	}

}
