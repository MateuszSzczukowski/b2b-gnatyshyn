<?php

namespace WPDesk\WooCommerceFakturownia\Field\Validator;


use WP_Error;
use FakturowniaVendor\WPDesk\Invoices\Field\FormField;

/**
 * Validate Polish VAT number if the MOSS is disabled.
 *
 * @package WPDesk\WooCommerceFakturownia\Field\Validator
 */
class NipValidator {
	const INVALID_NIP_ERROR_CODE       = 100;
	const VALID_NIP_LENGTH             = 10;
	const VALID_NIP_LENGTH_WITH_PREFIX = 12;

	/** @var FormField */
	private $fieldToValidate;

	/** @var array[] */
	private $errorMessages = [];

	public function __construct( FormField $fieldToValidate ) {
		$this->fieldToValidate = $fieldToValidate;
	}

	/**
	 * Is valid when converted to number
	 *
	 * @param string $number
	 *
	 * @return bool
	 */
	private function isValidNipNumber( $number ) {
		$number = preg_replace( '/[^0-9]+/', '', $number );
		if ( strlen( $number ) !== self::VALID_NIP_LENGTH ) {
			return false;
		}

		$arrSteps = array( 6, 5, 7, 2, 3, 4, 5, 6, 7 );
		$intSum   = 0;

		for ( $i = 0; $i < 9; $i ++ ) {
			$intSum += $arrSteps[ $i ] * $number[ $i ];
		}

		$int          = $intSum % 11;
		$intControlNr = $int === 10 ? 0 : $int;

		return $intControlNr === (int) $number[9];
	}

	/**
	 * Is prefix and length valid
	 *
	 * @param string $value
	 *
	 * @return bool
	 */
	private function isPrefixAndLengthValid( $value ) {
		$length = strlen( $value );
		if ( $length === self::VALID_NIP_LENGTH_WITH_PREFIX ) {
			return strtoupper( substr( $value, 0, 2 ) ) === 'PL';
		}

		return $length === self::VALID_NIP_LENGTH;
	}

	/**
	 * Checks if message is valid and produces $errorMessages
	 *
	 * @param string $value
	 *
	 * @return bool
	 */
	private function isValid( $value ) {
		if ( $value === '' ) {
			return true;
		}

		$removed_hyphens = preg_replace( '/(-([0-9]))+/', '$2', $value );
		$valid           = true;
		$valid           = $valid && $this->isPrefixAndLengthValid( $removed_hyphens );
		$valid           = $valid && $this->isValidNipNumber( $removed_hyphens );

		if ( ! $valid ) {
			$this->errorMessages = [
				[
					'code'    => self::INVALID_NIP_ERROR_CODE,
					'message' => sprintf( __( 'Podany numer NIP (%s) jest nieprawidłowy', 'woocommerce-fakturownia' ), $value )
				]
			];
		}

		return $valid;
	}

	/**
	 * Appends validator error message to WP_Error
	 *
	 * @param \WP_Error $errors
	 */
	private function appendErrorMessages( \WP_Error $errors ) {
		foreach ( $this->errorMessages as $errorMessage ) {
			$errors->add( $errorMessage['code'], $errorMessage['message'] );
		}
	}

	/**
	 * Returns field value from given checkout data
	 *
	 * @param array $data Checkout data
	 *
	 * @return mixed
	 */
	private function getValueFromArray( array $data ) {
		return isset( $data[ $this->fieldToValidate->getCheckoutFieldId() ] ) ? $data[ $this->fieldToValidate->getCheckoutFieldId() ] : '';
	}

	/**
	 * Hook to checkout to ensure valid NIP
	 */
	public function guardValidCheckout() {
		add_action( 'woocommerce_after_checkout_validation', function ( array $data, WP_Error $errors ) {
			if ( ! $this->isValid( $this->getValueFromArray( $data ) ) ) {
				$this->appendErrorMessages( $errors );
			}
		}, 10, 2 );
	}
}
