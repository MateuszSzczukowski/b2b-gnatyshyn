<?php

namespace WPDesk\WooCommerceFakturownia\Data;

use WC_Order;
use \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\WooCommerceFakturownia\Documents\Receipt;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use \WPDesk\WooCommerceFakturownia\InvoicesIntegration;
use \WPDesk\WooCommerceFakturownia\WoocommerceIntegration;

/**
 * Class InvoiceOrderDefaults
 *
 * Set default order data for invoices.
 *
 * @package WPDesk\WooCommerceFakturownia\Data
 */
class InvoiceOrderDefaults extends OrderDefaults {

	/**
	 * Integration.
	 *
	 * @var InvoicesIntegration
	 */
	protected $integration;

	/**
	 * Payment method options.
	 *
	 * @var array
	 */
	private $payment_method_options = [];

	/**
	 * Payment method names
	 */
	const PAYMENT_METHOD_TRANSFER = 'transfer';
	const PAYMENT_METHOD_CARD     = 'card';
	const PAYMENT_METHOD_CASH     = 'cash';
	const PAYMENT_METHOD_COD      = 'cod';

	const PAID_STATUSES = [ 'completed', 'processing' ];

	/**
	 * InvoiceOrderDefaults constructor.
	 *
	 * @param WC_Order                                          $order         Order.
	 * @param InvoicesIntegration                               $integration   Integration.
	 * @param \FakturowniaVendor\WPDesk\Invoices\Documents\Type $document_type Document type.
	 */
	public function __construct( WC_Order $order, InvoicesIntegration $integration, $document_type ) {
		parent::__construct( $order, $document_type );

		$this->initPaymentMethodOptions();
		$this->integration = $integration;
		$payment_method    = $order->get_payment_method();

		$this->addDefault( 'issue_date', date( 'Y-m-d', current_time( 'timestamp' ) ) );
		$this->setPaidAmount( $order );

		if ( 'cod' === $payment_method ) {
			$payment_date_for_cod = date( 'Y-m-d', current_time( 'timestamp' ) + 60 * 60 * 24 * $integration->woocommerce_integration->getOptionPaymentDate() );
			$this->addDefault( 'payment_date', $payment_date_for_cod );
			$order_status = $order->get_status();
			if ( $order_status !== 'completed' ) {
				$this->addDefault( 'paid_amount', '0.0' );
			}
		} else {
			$this->addDefault( 'payment_date', date( 'Y-m-d', current_time( 'timestamp' ) ) );
		}

		$this->addDefault( 'payment_method', $payment_method );
		$this->addDefault( 'sale_date', date( 'Y-m-d', current_time( 'timestamp' ) ) );
		$this->addDefault( 'legal_basis', $integration->woocommerce_integration->getOptionLegalBasis() );

		$comment_option = $integration->woocommerce_integration->getOptionCommentContent();
		if ( Receipt::TYPE_NAME === $document_type->getTypeName() ) {
			$comment_option = $integration->woocommerce_integration->getOptionCommentContentForReceipt();
		}

		$this->addDefault(
			'comments',
			str_replace(
				InvoiceForm::SHORTCODE_ORDER_NUMBER,
				$order->get_order_number(),
				$comment_option
			)
		);
	}

	/**
	 * Set paid amount.
	 *
	 * @param \WC_Order $order Order.
	 */
	private function setPaidAmount( $order ) {
		$order_status = $order->get_status();
		if ( in_array( $order_status, self::PAID_STATUSES, true ) ) {
			$this->addDefault( 'paid_amount', $order->get_total() );
		} else {
			$this->addDefault( 'paid_amount', '0.0' );
		}
	}

	/**
	 * Init payment method options.
	 */
	private function initPaymentMethodOptions() {
		$this->payment_method_options = array(
			self::PAYMENT_METHOD_TRANSFER => __( 'Przelew', 'woocommerce-fakturownia' ),
			self::PAYMENT_METHOD_CARD     => __( 'Karta płatnicza', 'woocommerce-fakturownia' ),
			self::PAYMENT_METHOD_CASH     => __( 'Gotówka', 'woocommerce-fakturownia' ),
		);
	}

	/**
	 * Get payment method options.
	 *
	 * @return array
	 */
	public function getPaymentMethodOptions() {
		return $this->payment_method_options;
	}

}
