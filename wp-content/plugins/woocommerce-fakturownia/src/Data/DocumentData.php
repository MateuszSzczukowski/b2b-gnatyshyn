<?php

namespace WPDesk\WooCommerceFakturownia\Data;

use FakturowniaVendor\WPDesk\Invoices\Exception\InvalidInvoiceDataException;
use FakturowniaVendor\WPDesk\Invoices\Field\VatNumber as VatNumberAlias;
use WC_Order;
use FakturowniaVendor\WPDesk\Invoices\Data\InvoiceData;
use FakturowniaVendor\WPDesk\Invoices\Data\Items\InvoiceItem;
use WPDesk\WooCommerceFakturownia\Api\FakturowniaApi;
use WPDesk\WooCommerceFakturownia\Api\Products\NoProductsFoundException;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\Product\FakturowniaProductCreator;
use WPDesk\WooCommerceFakturownia\Product\FakturowniaProductId;
use WPDesk\WooCommerceFakturownia\WoocommerceIntegration;
use WPDesk\WooCommerceFakturownia\InvoicesIntegration;

/**
 * Decorator for InvoiceData class.
 */
class DocumentData extends InvoiceData {

	const INVOICE_STATUS          = 'status';
	const INVOICE_STATUS_PAID     = 'paid';
	const INVOICE_STATUS_SENT     = 'sent';
	const INVOICE_STATUS_ISSUED   = 'issued';
	const INVOICE_STATUS_PARTIAL  = 'partial';
	const INVOICE_STATUS_REJECTED = 'rejected';
	const INVOICE_REVERSE_CHARGE  = 'reverse_charge';

	const DOCUMENT_KIND      = 'kind';
	const INVOICE_NUMBER     = 'number';
	const INVOICE_PLACE      = 'place';
	const INVOICE_SELL_DATE  = 'sell_date';
	const INVOICE_ISSUE_DATE = 'issue_date';
	const INVOICE_PAYMENT_TO = 'payment_to';

	const SELLER_NAME      = 'seller_name';
	const SELLER_TAX_NO    = 'seller_tax_no';
	const SELLER_STREET    = 'seller_street';
	const SELLER_CITY      = 'seller_city';
	const SELLER_POST_CODE = 'seller_post_code';

	const CURRENCY = 'currency';

	const SELLER_PERSON       = 'seller_person';
	const SELLER_BANK         = 'seller_bank';
	const SELLER_BANK_ACCOUNT = 'seller_bank_account';

	const CALCULATED_FROM             = 'calculating_strategy';
	const CALCULATED_FROM_WITHOUT_TAX = 'net';
	const CALCULATED_FROM_WITH_TAX    = 'gross';

	const TAX_RATE              = 'tax';
	const TAX_RATE_TYPE_ZW      = 'zw';
	const TAX_RATE_TYPE_NP      = 'np';
	const TAX_RATE_TYPE_DISABLE = 'disabled'; // This value hide column.

	const DOCUMENT_ITEMS           = 'positions';
	const ITEM_PRICE_NET           = 'price_net';
	const ITEM_PRICE_GROSS         = 'price_gross';
	const ITEM_TOTAL_PRICE_GROSS   = 'total_price_gross';
	const ITEM_TOTAL_PRICE_NET     = 'total_price_net';
	const ITEM_QUANTITY            = 'quantity';
	const ITEM_QUANTITY_UNIT       = 'quantity_unit';
	const ITEM_QUANTITY_UNIT_VALUE = 'szt';
	const ITEM_FULL_NAME           = 'name';
	const ITEM_ADDITIONAL_INFO     = 'additional_info';

	const BUYER_NAME      = 'buyer_name';
	const BUYER_TAX_NO    = 'buyer_tax_no';
	const BUYER_STREET    = 'buyer_street';
	const BUYER_POST_CODE = 'buyer_post_code';
	const BUYER_CITY      = 'buyer_city';
	const BUYER_EMAIL     = 'buyer_email';
	const BUYER_PHONE     = 'buyer_phone';
	const BUYER_COUNTRY   = 'buyer_country';

	const COMMENTS = 'description';

	const OPTION_EXEMPT_TAX_KIND = 'exempt_tax_kind';

	const PKWIU                 = 'PKWiU';
	const ADDITIONAL_DATA_PKWIU = 'pkwiu';
	const ADDITIONAL_INFO_DESC  = 'additional_info_desc';
	const ADDITIONAL_INFO       = 'additional_info';

	const PAYMENT_TYPE = 'payment_type';

	const PRODUCT_ID = 'product_id';

	const INVOICE_LANG             = 'lang';
	const CLIENT_COUNTRY_OPTION    = 'client_country';

	/**
	 * Invoice data.
	 *
	 * @var \FakturowniaVendor\WPDesk\Invoices\Data\InvoiceData
	 */
	private $invoice_data;

	/**
	 * InvoiceIntegration
	 *
	 * @var InvoicesIntegration
	 */
	protected $invoice_integration;

	/**
	 * Fakturownia API.
	 *
	 * @var FakturowniaApi
	 */
	private $fakturownia_api;

	/**
	 * Product creator.
	 *
	 * @var FakturowniaProductCreator
	 */
	private $fakturownia_product_creator;

	/**
	 * Bank account.
	 *
	 * @var string
	 */
	private $bank_account_number;

	/**
	 * Bank account.
	 *
	 * @var string
	 */
	private $bank_account_name;

	/**
	 * Sale date format.
	 *
	 * @var string
	 */
	private $sale_date_format;

	/**
	 * Payment date.
	 *
	 * @var string
	 */
	private $payment_date;

	/**
	 * Numbering series.
	 *
	 * @var string
	 */
	private $numbering_series;

	/**
	 * Template name.
	 *
	 * @var string
	 */
	private $template_name;

	/**
	 * Place of issue.
	 *
	 * @var string
	 */
	private $place_of_issue;

	/**
	 * Recepient signature type.
	 *
	 * @var string
	 */
	private $recipient_signature_type;

	/**
	 * Recepient signature.
	 *
	 * @var string
	 */
	private $recipient_signature;

	/**
	 * Issuer signature.
	 *
	 * @var string
	 */
	private $issuer_signature;

	/**
	 * Legal basis.
	 *
	 * @var string
	 */
	private $legal_basis;

	/**
	 * Comments.
	 *
	 * @var string
	 */
	private $comments;

	/**
	 * Tax rate exempt.
	 *
	 * @var string
	 */
	private $tax_rate_exempt;

	/**
	 * Order total price
	 *
	 * @var float
	 */
	private $total_price;

	/**
	 * Order.
	 *
	 * @var WC_Order
	 */
	private $order;

	/**
	 * Has item zero vat
	 *
	 * @var bool
	 */
	private $has_zw = false;

	/**
	 * DocumentData constructor.
	 *
	 * @param InvoiceData         $invoice_data    Invoice data.
	 * @param InvoicesIntegration $integration     Integration.
	 * @param FakturowniaApi      $fakturownia_api Fakturownia API.
	 */
	public function __construct(
		InvoiceData $invoice_data,
		InvoicesIntegration $integration,
		FakturowniaApi $fakturownia_api
	) {
		$this->invoice_data        = $invoice_data;
		$this->invoice_integration = $integration;
		$this->fakturownia_api     = $fakturownia_api;

		if ( InvoiceForm::TAX_EXEMPT_OPTION_NONE !== $integration->woocommerce_integration->getOptionTaxRateExempt() && ! empty( $integration->woocommerce_integration->getOptionPkwiuAttribute() ) ) {
			$this->setPkwiuFromAttribute( $integration->woocommerce_integration->getOptionPkwiuAttribute() );
		}
	}

	/**
	 * Create from order.
	 *
	 * @param WC_Order             $order            Order.
	 * @param VatNumberAlias|null  $vat_number_field Vat number field.
	 * @param InvoiceOrderDefaults $defaults         Defaults.
	 * @param InvoicesIntegration  $integration      Integration.
	 *
	 * @return InvoiceData
	 * @throws InvalidInvoiceDataException .
	 */
	public static function createFromOrderAndDefaultsAndSettings(
		WC_Order $order,
		$vat_number_field,
		InvoiceOrderDefaults $defaults,
		InvoicesIntegration $integration
	) {

		$invoice_data = parent::createFromOrder( $order, $vat_number_field, '' );
		$invoice_data = new static( $invoice_data, $integration, $integration->get_fakturownia_api() );

		$invoice_data->setItems( $invoice_data->removeZeroPriceItems( $invoice_data->getItems() ) );

		if ( empty( $invoice_data->getItems() ) ) {
			throw new InvalidInvoiceDataException( __( 'Zamówienie nie zawiera pozycji o niezerowej wartości!', 'woocommerce-fakturownia' ) );
		}

		$invoice_data->setOrder( $order );
		$invoice_data->setTotalPrice( $order );
		$invoice_data->setPaidAmount( wc_format_decimal( $defaults->getDefault( 'paid_amount' ) ) );
		$invoice_data->setIssueDate( $defaults->getDefault( 'issue_date' ) );
		$invoice_data->setSaleDate( $defaults->getDefault( 'sale_date' ) );
		$invoice_data->setPaymentDate( $defaults->getDefault( 'payment_date' ) );
		$invoice_data->setPaymentMethod( $defaults->getDefault( 'payment_method' ) );
		$invoice_data->setComments( $defaults->getDefault( 'comments' ) );

		$invoice_data->setIssuerSignature( $defaults->getDefault( 'issuer_signature' ) );
		$invoice_data->setLegalBasis( $integration->woocommerce_integration->getOptionLegalBasis() );

		$invoice_data->setRecipientSignatureType( 'BPO' );
		$invoice_data->setPlaceOfIssue( $integration->woocommerce_integration->getOptionPlaceOfIssue() );

		$invoice_data->setTaxRateExempt( $integration->woocommerce_integration->getOptionTaxRateExempt() );

		$invoice_data->setProductNameOnItems();

		if ( 'yes' === $integration->woocommerce_integration->getOptionCreateProducts() ) {
			$warehouse_id = $integration->woocommerce_integration->getWarehouseID();
			$invoice_data->setFakturowniaProductCreator( new FakturowniaProductCreator( $integration->get_fakturownia_api(), $warehouse_id ) );
		}

		return $invoice_data;
	}

	/**
	 * Set product creator.
	 *
	 * @param FakturowniaProductCreator $fakturowania_product_creator .
	 */
	private function setFakturowniaProductCreator( FakturowniaProductCreator $fakturowania_product_creator ) {
		$this->fakturownia_product_creator = $fakturowania_product_creator;
	}

	/**
	 * Set product codes on items.
	 */
	protected function setProductNameOnItems() {
		$items = $this->getItems();
		foreach ( $items as $item_key => $item ) {
			if ( $item->getOrderItem()->is_type( 'line_item' ) ) {
				$product = $item->getOrderItem()->get_product();
				if ( $product instanceof \WC_Product ) {
					$product_name = $product->get_meta( FakturowniaProductId::FAKTUROWNIA_PRODUCT_ID );
					if ( ! empty( $product_name ) ) {
						$item->setName( $product_name );
					}
				}
			}
		}
	}

	/**
	 * Set order.
	 *
	 * @param WC_Order $order Order.
	 */
	protected function setOrder( WC_Order $order ) {
		$this->order = $order;
	}

	/**
	 * Get order.
	 *
	 * @return WC_Order
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * Set order price total
	 *
	 * @param WC_Order $order Order.
	 */
	protected function setTotalPrice( WC_Order $order ) {
		$this->total_price = floatval( $order->get_total() );
	}

	/**
	 * Get total price.
	 *
	 * @return float
	 */
	public function getTotalPrice() {
		return $this->total_price;
	}

	/**
	 * Add item.
	 *
	 * @param InvoiceItem $item Item.
	 */
	protected function addItem( InvoiceItem $item ) {
		$this->invoice_data->addItem( $item );
	}

	/**
	 * Get items.
	 *
	 * @return InvoiceItem[]
	 */
	public function getItems() {
		return $this->invoice_data->getItems();
	}

	/**
	 * Set items.
	 *
	 * @param InvoiceItem[] $items Items.
	 */
	public function setItems( $items ) {
		$this->invoice_data->setItems( $items );
	}

	/**
	 * Get client data.
	 *
	 * @return \FakturowniaVendor\WPDesk\Invoices\Data\ClientData
	 */
	public function getClientData() {
		return $this->invoice_data->getClientData();
	}

	/**
	 * Set client data.
	 *
	 * @param \FakturowniaVendor\WPDesk\Invoices\Data\ClientData $client_data Client data.
	 */
	public function setClientData( $client_data ) {
		$this->invoice_data->setClientData( $client_data );
	}

	/**
	 * Is prices include tax.
	 *
	 * @return bool
	 */
	public function isPricesIncludeTax() {
		return $this->invoice_data->isPricesIncludeTax();
	}

	/**
	 * Set prices include tax.
	 *
	 * @param bool $prices_include_tax Proces include tax.
	 */
	public function setPricesIncludeTax( $prices_include_tax ) {
		$this->invoice_data->setPricesIncludeTax( $prices_include_tax );
	}

	/**
	 * Get paid amount.
	 *
	 * @return float
	 */
	public function getPaidAmount() {
		return floatval( $this->round( $this->invoice_data->getPaidAmount() ) );
	}

	/**
	 * Set paid amount.
	 *
	 * @param float $paid_amount Paid amount.
	 */
	public function setPaidAmount( $paid_amount ) {
		$this->invoice_data->setPaidAmount( $paid_amount );
	}

	/**
	 * Get sale date.
	 *
	 * @return string
	 */
	public function getSaleDate() {
		return $this->invoice_data->getSaleDate();
	}

	/**
	 * Set sale date.
	 *
	 * @param string $sale_date Sale date.
	 */
	public function setSaleDate( $sale_date ) {
		$this->invoice_data->setSaleDate( $sale_date );
	}

	/**
	 * Get issue date.
	 *
	 * @return string
	 */
	public function getIssueDate() {
		return $this->invoice_data->getIssueDate();
	}

	/**
	 * Set issue date.
	 *
	 * @param string $issue_date Issue date.
	 */
	public function setIssueDate( $issue_date ) {
		$this->invoice_data->setIssueDate( $issue_date );
	}

	/**
	 * Get payment amount.
	 *
	 * @return string
	 */
	public function getPaymentMethod() {
		return $this->invoice_data->getPaymentMethod();
	}

	/**
	 * Set payment method.
	 *
	 * @param string $payment_method Payment method.
	 */
	public function setPaymentMethod( $payment_method ) {
		$this->invoice_data->setPaymentMethod( $payment_method );
	}

	/**
	 * Get currency.
	 */
	public function getCurrency() {
		return $this->invoice_data->getCurrency();
	}

	/**
	 * Set currency.
	 *
	 * @param string $currency Currency.
	 */
	public function setCurrency( $currency ) {
		$this->invoice_data->setCurrency( $currency );
	}

	/**
	 * Get bank account name.
	 *
	 * @return string
	 */
	public function getBankAccountName() {
		if ( '' === $this->bank_account_name ) {
			return null;
		}

		return $this->bank_account_name;
	}

	/**
	 * Set bank account name.
	 *
	 * @param string $bank_account_name Bank account.
	 */
	public function setBankAccountName( $bank_account_name ) {
		$this->bank_account_name = $bank_account_name;
	}

	/**
	 * Get bank account number.
	 *
	 * @return string
	 */
	public function getBankAccountNumber() {
		if ( '' === $this->bank_account_number ) {
			return null;
		}

		return $this->bank_account_number;
	}

	/**
	 * Set bank account number.
	 *
	 * @param string $bank_account_number Bank account.
	 */
	public function setBankAccountNumber( $bank_account_number ) {
		$this->bank_account_number = $bank_account_number;
	}

	/**
	 * Get place of issue.
	 *
	 * @return string
	 */
	public function getPlaceOfIssue() {
		return $this->place_of_issue;
	}

	/**
	 * Set place of issue.
	 *
	 * @param string $place_of_issue Place of issue.
	 */
	public function setPlaceOfIssue( $place_of_issue ) {
		$this->place_of_issue = $place_of_issue;
	}

	/**
	 * Get payment date.
	 *
	 * @return string Payment date.
	 */
	public function getPaymentDate() {
		return $this->payment_date;
	}

	/**
	 * Set payment date.
	 *
	 * @param string $payment_date Payment date.
	 */
	public function setPaymentDate( $payment_date ) {
		$this->payment_date = $payment_date;
	}

	/**
	 * Get numbering series.
	 *
	 * @return string
	 */
	public function getNumberingSeries() {
		return $this->numbering_series;
	}

	/**
	 * Set numbering series.
	 *
	 * @param string $numbering_series Numbering series.
	 */
	public function setNumberingSeries( $numbering_series ) {
		$this->numbering_series = $numbering_series;
	}

	/**
	 * Get template name.
	 *
	 * @return string
	 */
	public function getTemplateName() {
		if ( '' === $this->template_name ) {
			return null;
		}

		return $this->template_name;
	}

	/**
	 * Set template name.
	 *
	 * @param string $template_name Template name.
	 */
	public function setTemplateName( $template_name ) {
		$this->template_name = $template_name;
	}

	/**
	 * Get recipient signature type.
	 *
	 * @return string
	 */
	public function getRecipientSignatureType() {
		return $this->recipient_signature_type;
	}

	/**
	 * Set recipient signature type.
	 *
	 * @param string $recipient_signature_type Recipient signature type.
	 */
	public function setRecipientSignatureType( $recipient_signature_type ) {
		$this->recipient_signature_type = $recipient_signature_type;
	}

	/**
	 * Get recipient signature.
	 *
	 * @return string
	 */
	public function getRecipientSignature() {
		return $this->recipient_signature;
	}

	/**
	 * Set recipient signature.
	 *
	 * @param string $recipient_signature Recipient signature.
	 */
	public function setRecipientSignature( $recipient_signature ) {
		$this->recipient_signature = $recipient_signature;
	}

	/**
	 * Get issuer signature.
	 *
	 * @return string
	 */
	public function getIssuerSignature() {
		return $this->issuer_signature;
	}

	/**
	 * Set issuer signature.
	 *
	 * @param string $issuer_signature Issuer signature.
	 */
	public function setIssuerSignature( $issuer_signature ) {
		$this->issuer_signature = $issuer_signature;
	}

	/**
	 * Get comments.
	 *
	 * @return string
	 */
	public function getComments() {
		return $this->comments;
	}

	/**
	 * Set comments.
	 *
	 * @param string $comments Comments.
	 */
	public function setComments( $comments ) {
		$this->comments = $comments;
	}

	/**
	 * Get legal basis.
	 *
	 * @return string
	 */
	public function getLegalBasis() {
		return $this->legal_basis;
	}

	/**
	 * Set legal basis.
	 *
	 * @param string $legal_basis Legal basis.
	 */
	public function setLegalBasis( $legal_basis ) {
		$this->legal_basis = $legal_basis;
	}

	/**
	 * Get tax rate exempt.
	 *
	 * @return string
	 */
	public function getTaxRateExempt() {
		return $this->tax_rate_exempt;
	}

	/**
	 * Set tax rate exempt.
	 *
	 * @param string $tax_rate_exempt Tax rate exempt.
	 */
	public function setTaxRateExempt( $tax_rate_exempt ) {
		$this->tax_rate_exempt = $tax_rate_exempt;
	}

	/**
	 * @param float $price    Order price.
	 * @param float $tax_rate Tax price.
	 *
	 * @return float
	 */
	private function getTaxTotalPrice( $price, $tax_rate ) {
		return $price + $price * $tax_rate / 100 ;
	}

	/**
	 * Set PKWIU from attribute.
	 *
	 * @param string $attribute_name Attribute name.
	 */
	private function setPkwiuFromAttribute( $attribute_name ) {
		$items = $this->getItems();
		foreach ( $items as $item_key => $item ) {
			$order_item = $item->getOrderItem();
			if ( $order_item->is_type( 'line_item' ) ) {
				$product = $order_item->get_product();
				if ( $product instanceof \WC_Product ) {
					$pkwiu = $product->get_attribute( $attribute_name );
					if ( ! empty( $pkwiu ) ) {
						$item->addAdditionalData( self::ADDITIONAL_DATA_PKWIU, $pkwiu );
					}
				}
			}
		}
		$this->setItems( $items );
	}

	/**
	 * Find or create Fakturownia product for SKU.
	 *
	 * @param \WC_Product $product .
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException .
	 * @throws NoProductsFoundException .
	 */
	private function findOrCreateFakturowniaProductForSKU( \WC_Product $product ) {
		$sku = $product->get_sku();
		try {
			$fakturownia_product = $this->fakturownia_api->get_product_by_code( $sku );
		} catch ( NoProductsFoundException $e ) {
			if ( ! empty( $this->fakturownia_product_creator ) ) {
				$fakturownia_product = $this->fakturownia_product_creator->create_from_product( $product );
				$this->fakturownia_product_creator->set_fakturownia_product_id_on_product( $product, $fakturownia_product['id'] );
			} else {
				throw $e;
			}
		}

		return $fakturownia_product;
	}

	/**
	 * Prepare item identity.
	 *
	 * @param array       $item_data .
	 * @param InvoiceItem $item      .
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException .
	 * @throws NoProductsFoundException .
	 */
	private function prepareItemIdentity( array $item_data, InvoiceItem $item ) {
		if ( $this->invoice_integration->woocommerce_integration->getOptionSynchronization() === 'yes' ) {
			$order_item = $item->getOrderItem();
			if ( $order_item->is_type( 'line_item' ) ) {
				/** @var \WC_Product $product */
				$product                = $order_item->get_product();
				$is_variation = $product->is_type( 'variation' );

				$fakturownia_product_id = $product->get_meta( FakturowniaProductId::FAKTUROWNIA_PRODUCT_ID );

				if( $is_variation && empty( $fakturownia_product_id ) ) {
					$parent_id = $product->get_parent_id();
					$parent_product = wc_get_product( $parent_id );
					$fakturownia_product_id = $parent_product->get_meta( FakturowniaProductId::FAKTUROWNIA_PRODUCT_ID );
				}

				if ( ! empty( $fakturownia_product_id ) ) {
					$item_data[ self::PRODUCT_ID ] = $fakturownia_product_id;
					unset( $item_data[ self::ITEM_FULL_NAME ] );
				} else {
					if ( ! empty( $product->get_sku() ) ) {
						$fakturownia_product           = $this->findOrCreateFakturowniaProductForSKU( $product );
						$item_data[ self::PRODUCT_ID ] = $fakturownia_product['id'];
					} else {
						// Translators: product name.
						throw new NoProductsFoundException( sprintf( __( 'Produkt %1$s nie jest powiązany z żadnym produktem w Fakturowni oraz nie ma zdefiniowanego SKU.', 'woocommerce-fakturownia' ), $product->get_name() ) );
					}
				}
			}
		}
		if ( empty( $item_data[ self::PRODUCT_ID ] ) ) {
			$item_data[ self::ITEM_FULL_NAME ] = $item->getName();
		}

		return $item_data;
	}

	/**
	 * Remove items with 0 (zero) price.
	 *
	 * @param InvoiceItem[] $items .
	 *
	 * @return InvoiceItem[]
	 */
	private function removeZeroPriceItems( $items ) {
		foreach ( $items as $key => $item ) {
			if ( floatval( 0 ) === floatval( number_format( $item->getPrice(), 2 ) ) ) {
				unset( $items[ $key ] );
			}
		}

		return $items;
	}

	/**
	 * Get MOSS Counties.
	 *
	 * @return array
	 */
	protected function get_eu_countries() {
		return WC()->countries->get_european_union_countries();
	}

	/**
	 * Prepare items as array.
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException .
	 */
	protected function prepareItemsAsArray() {
		$items_data = [];

		$client_data = $this->getClientData();

		foreach ( $this->getItems() as $item ) {
			$item_data                             = [];
			$item_data                             = $this->prepareItemIdentity( $item_data, $item );
			$item_data[ self::ITEM_QUANTITY ]      = $item->getQuantity();
			$item_data[ self::ITEM_QUANTITY_UNIT ] = self::ITEM_QUANTITY_UNIT_VALUE;
			$item_data[ self::TAX_RATE ]           = floatval( $item->getTaxRate() );

			if ( $this->isPricesIncludeTax() ) {
				$item_data[ self::ITEM_PRICE_GROSS ]       = $item->getPrice();
				$item_data[ self::ITEM_TOTAL_PRICE_GROSS ] = $this->round( $item->getPrice() * $item->getQuantity() );
			} else {
				$item_data[ self::ITEM_PRICE_NET ]       = $item->getPrice();
				$item_data[ self::ITEM_PRICE_GROSS ]     = $this->getTaxTotalPrice( $item->getPrice(), $item->getTaxRate() );
				$item_data[ self::ITEM_TOTAL_PRICE_NET ] = $item->getPrice() * $item->getQuantity();
				$item_data[ self::ITEM_TOTAL_PRICE_GROSS ] = $this->getTaxTotalPrice( $item->getPrice(), $item->getTaxRate() ) * $item->getQuantity();
			}

			if ( 0.0 === $item->getTaxRate() && 'none' !== $this->getTaxRateExempt() ) {
				if ( $this->getTaxRateExempt() === $item->getTaxClass() ) {
					$item_data[ self::TAX_RATE ] = self::TAX_RATE_TYPE_ZW;
					$this->has_zw                = true;

					$pkwiu                                   = $item->getAdditionalDataByName( self::ADDITIONAL_DATA_PKWIU );
					$item_data[ self::ITEM_ADDITIONAL_INFO ] = ( null === $pkwiu ) ? '' : $pkwiu;
				}
			}

			$eu_vat_countries = $this->get_eu_countries();
			if ( ! in_array( $client_data->getCountry(), $eu_vat_countries ) ) {
				if ( (int) $item->getTaxRate() > 0 ) {
					$item_data[ self::TAX_RATE ] = floatval( $item->getTaxRate() );
				} else {
					$item_data[ self::TAX_RATE ] = self::TAX_RATE_TYPE_NP;
				}
			}

			$items_data[] = $item_data;
		}

		return $items_data;
	}

	/**
	 * Prepare recipient as array.
	 *
	 * @return array
	 */
	private function prapareRecipientAsArray() {
		$recipient   = [];
		$client_data = $this->getClientData();
		if ( $client_data->isComapny() ) {
			$recipient[ self::BUYER_NAME ] = wp_specialchars_decode( $client_data->getCompanyName() );
		} else {
			$recipient[ self::BUYER_NAME ] = wp_specialchars_decode( $client_data->getFirstName() . ' ' . $client_data->getLastName() );
		}

		$recipient[ self::BUYER_TAX_NO ] = $client_data->getVatNumber();
		$recipient[ self::BUYER_STREET ] = wp_specialchars_decode( $client_data->getAddress() );
		$address_2                       = wp_specialchars_decode( $client_data->getAddress2() );
		if ( ! empty( $address_2 ) && '' !== trim( $address_2 ) ) {
			$recipient[ self::BUYER_STREET ] .= ' ' . $address_2;
		}
		$recipient[ self::BUYER_POST_CODE ] = $client_data->getPostCode();

		$recipient[ self::BUYER_CITY ]    = wp_specialchars_decode( $client_data->getCity() );
		$recipient[ self::BUYER_EMAIL ]   = $client_data->getEmail();
		$recipient[ self::BUYER_PHONE ]   = $client_data->getPhone();
		$recipient[ self::BUYER_COUNTRY ] = $client_data->getCountry();

		return $recipient;
	}

	/**
	 * Is partial paid status
	 *
	 * @return bool
	 */
	private function is_partial_paid_status() {
		return ( $this->getTotalPrice() - $this->getPaidAmount() > 0 );
	}

	/**
	 * Prepare paid data as array
	 */
	private function preparePaidStatuses() {
		if ( InvoiceOrderDefaults::PAYMENT_METHOD_COD === $this->getPaymentMethod() && (float) $this->getPaidAmount() === 0.0 ) {
			$data[ self::INVOICE_STATUS ]      = self::INVOICE_STATUS_ISSUED;
			$data[ self::INVOICE_STATUS_PAID ] = 0;
		} elseif ( $this->getPaidAmount() === 0.0 ) {
			$data[ self::INVOICE_STATUS ]      = self::INVOICE_STATUS_ISSUED;
			$data[ self::INVOICE_STATUS_PAID ] = $this->getPaidAmount();
		} elseif ( $this->is_partial_paid_status() ) {
			$data[ self::INVOICE_STATUS ]      = self::INVOICE_STATUS_PARTIAL;
			$data[ self::INVOICE_STATUS_PAID ] = $this->getPaidAmount();
		} else {
			$data[ self::INVOICE_STATUS ] = self::INVOICE_STATUS_PAID;
		}

		return $data;
	}

	/**
	 * Prepare data as array.
	 *
	 * @return array
	 */
	public function prepareDataAsArray() {
		$data                                 = [];
		$data[ self::DOCUMENT_KIND ]          = 'vat';
		$data[ self::INVOICE_NUMBER ]         = null;
		$data[ self::INVOICE_PLACE ]          = wp_specialchars_decode( $this->getPlaceOfIssue() );
		$data[ self::INVOICE_ISSUE_DATE ]     = $this->getIssueDate();
		$data[ self::INVOICE_SELL_DATE ]      = $this->getSaleDate();
		$data[ self::INVOICE_PAYMENT_TO ]     = $this->getPaymentDate();
		$data[ self::INVOICE_REVERSE_CHARGE ] = false;
		$data[ self::CURRENCY ]               = $this->getCurrency();
		$data[ self::SELLER_TAX_NO ]          = null;
		$data[ self::COMMENTS ]               = wp_specialchars_decode( $this->getComments() );
		$data[ self::ADDITIONAL_INFO ]        = 0;
		$data[ self::DOCUMENT_ITEMS ]         = $this->prepareItemsAsArray();
		$data[ self::PAYMENT_TYPE ]           = $this->getRealPaymentMethodName( $this->getPaymentMethod() );
		$data[ self::INVOICE_LANG ]           = $this->get_client_lang();
		$data                                 += $this->prapareRecipientAsArray();
		$data                                 += $this->preparePaidStatuses();
		if ( $this->has_zw ) {
			$data[ self::ADDITIONAL_INFO_DESC ]   = self::PKWIU;
			$data[ self::OPTION_EXEMPT_TAX_KIND ] = $this->getLegalBasis();
		}

		$warehouse_id = $this->invoice_integration->woocommerce_integration->getWarehouseID();
		if ( 'yes' === $this->invoice_integration->woocommerce_integration->getOptionSynchronization() ) {
			$data['warehouse_id'] = (int) $warehouse_id;
		}

		$this->invoice_integration->get_logger()->debug( 'Fakturownia: ', $data );

		return $data;
	}

	/**
	 * Get client lang based on country.
	 *
	 * @return string
	 */
	protected function get_client_lang() {
		$client_data = $this->getClientData();
		$billing_lang        = strtolower( $client_data->getCountry() );
		if ( $this->invoice_integration->woocommerce_integration->getOptionInvoiceLang() === self::CLIENT_COUNTRY_OPTION ) {
			return $billing_lang;
		} elseif ( in_array( $this->invoice_integration->woocommerce_integration->getOptionInvoiceLang(), [ 'pl', 'en' ], true ) ) {
			return $this->invoice_integration->woocommerce_integration->getOptionInvoiceLang();
		} else {
			return 'pl';
		}
	}


	/**
	 * Convert to Json string.
	 */
	public function toJsonString() {
		return wp_json_encode( $this->prepareDataAsArray(), JSON_PRETTY_PRINT );
	}

	/**
	 * Roud number.
	 *
	 * @param float $float Float number.
	 *
	 * @return float
	 */
	protected function round( $float ) {
		return round( $float, 2 );
	}

	/**
	 * @param string $payment_method
	 *
	 * @return mixed|string
	 */
	public function getRealPaymentMethodName( $payment_method ) {
		if ( WC()->payment_gateways() ) {
			$payment_gateways = WC()->payment_gateways->payment_gateways();
		} else {
			$payment_gateways = array();
		}

		if( isset( $payment_gateways[ $payment_method ] ) ) {
			return $payment_gateways[ $payment_method ]->get_title();
		}

		return $payment_method;
	}

}
