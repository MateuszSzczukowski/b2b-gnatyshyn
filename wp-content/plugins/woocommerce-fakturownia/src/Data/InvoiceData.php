<?php

namespace WPDesk\WooCommerceFakturownia\Data;

use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use FakturowniaVendor\WPDesk\Invoices\Data\Items\LineItem;
use WPDesk\WooCommerceFakturownia\Documents\Receipt;
use WPDesk\WooCommerceFakturownia\GTU;
use WPDesk\WooCommerceFakturownia\ProcedureDesignations;

/**
 * Class InvoiceData
 *
 * Prepare data for invoice document
 *
 * @package WPDesk\WooCommerceFakturownia\Data
 */
class InvoiceData extends DocumentData {

	const MOSS_META_NAME = '_vat_number';

	/**
	 * Prepare data as array.
	 *
	 * @return array
	 */

	const DOCUMENT_KIND       = 'kind';
	const USE_MOSS            = 'use_moss';
	const FROM_INVOICE_ID     = 'from_invoice_id';
	const INVOICE_ID          = 'invoice_id';
	const ADDITIONAL_PARAMS   = 'additional_params';
	const EXCLUDE_FROM_STOCK  = 'exclude_from_stock_level';
	const MOSS_B2B_CLASS_NAME = 'nie podl. UE';
	const GTU_CODES           = 'gtu_codes';
	const PROCEDURE_CODES     = 'procedure_designations';

	/**
	 * Is B2B transaction?
	 *
	 * @return bool
	 */
	private function is_b2b_moss() {
		return 'yes' === $this->getOrder()->get_meta( 'is_vat_exempt' );
	}

	/**
	 * Is customer transaction?
	 *
	 * @return bool
	 */
	private function is_customer_moss() {
		$customer_self_declared = 'true' === $this->getOrder()->get_meta( '_customer_self_declared_country' );

		return $customer_self_declared;
	}

	/**
	 * Get receipt ID.
	 */
	private function get_receipt_id() {
		$receipt_meta = $this->getOrder()->get_meta( Receipt::META_DATA_NAME );

		return isset( $receipt_meta['id'] ) ? $receipt_meta['id'] : null;
	}

	/**
	 * @return string|null
	 */
	private function get_gtu_code( $item ) {
		if ( $item instanceof \WC_Order_Item_Product ) {
			$product_id = $item->get_product_id();
			$code       = get_post_meta( $product_id, GTU::GTU_CODE_KEY, true );
			if ( ! empty( $code ) ) {
				return $code;
			}
		}

		return null;
	}

	/**
	 * @return string|null
	 */
	private function get_transaction_code( $item ) {
		if ( $item instanceof \WC_Order_Item_Product ) {
			$product_id = $item->get_product_id();
			$code       = get_post_meta( $product_id, ProcedureDesignations::CODE_KEY, true );
			if ( ! empty( $code ) ) {
				return $code;
			}
		}

		return null;
	}

	/**
	 * @return array
	 */
	private function get_items_gtu_codes() {
		$codes = [];
		foreach ( $this->getItems() as $item ) {
			if ( $item instanceof LineItem ) {
				$order_item = $item->getOrderItem();
				$code       = $this->get_gtu_code( $order_item );
				if ( $code ) {
					$codes[] = $code;
				}
			}
		}

		return array_unique( $codes );
	}

	/**
	 * @return array
	 */
	private function get_items_procedure_codes() {
		$codes = [];
		foreach ( $this->getItems() as $item ) {
			if ( $item instanceof LineItem ) {
				$order_item = $item->getOrderItem();
				$code       = $this->get_transaction_code( $order_item );
				if ( $code ) {
					$codes[] = $code;
				}
			}
		}

		return array_unique( $codes );
	}


	/**
	 * Prepare data as array
	 *
	 * @return array
	 */
	public function prepareDataAsArray() {
		$data                        = parent::prepareDataAsArray();
		$data[ self::DOCUMENT_KIND ] = 'vat';
		$gtu_codes                   = $this->get_items_gtu_codes();
		$procedure_codes             = $this->get_items_procedure_codes();

		if ( ! empty( $gtu_codes ) ) {
			$data[ self::GTU_CODES ] = $gtu_codes;
		}

		if ( ! empty( $procedure_codes ) ) {
			$data[ self::PROCEDURE_CODES ] = $procedure_codes;
		}

		$data[ self::ADDITIONAL_INFO ] = 1;
		if ( $this->is_b2b_moss() || $this->is_customer_moss() ) {
			$data[ self::USE_MOSS ] = 1;
			unset( $data[ self::OPTION_EXEMPT_TAX_KIND ] );
		}

		if ( $this->is_b2b_moss() ) {
			foreach ( $data[ self::DOCUMENT_ITEMS ] as $item ) {
				$item['tax'] = self::MOSS_B2B_CLASS_NAME;
				$items[]     = $item;
			}
			$data[ self::DOCUMENT_ITEMS ] = $items;
			$data[ self::COMMENTS ]       = $data[ self::COMMENTS ] . PHP_EOL . __( 'Reverse charge', 'woocommerce-fakturownia' );
		}

		if ( $this->get_receipt_id() ) {
			$data[ self::FROM_INVOICE_ID ]    = $this->get_receipt_id();
			$data[ self::INVOICE_ID ]         = $this->get_receipt_id();
			$data[ self::ADDITIONAL_PARAMS ]  = 'for_receipt';
			$data[ self::EXCLUDE_FROM_STOCK ] = true;
		}

		return $data;
	}

}
