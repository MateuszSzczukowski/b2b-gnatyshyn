<?php

namespace WPDesk\WooCommerceFakturownia;

use FakturowniaVendor\WPDesk\Invoices\WooCommerce\DocumentCreatorForOrderStatus;
use WPDesk\WooCommerceFakturownia\Forms\Integration\BillForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\ReceiptForm;

class DocumentCreators {

	/**
	 * @var InvoicesIntegration
	 */
	private $integration;

	/**
	 * @param InvoicesIntegration $integration
	 */
	public function __construct( InvoicesIntegration $integration ) {
		$this->integration = $integration;
	}

	/**
	 * Add support for invoice
	 */
	public function init() {
		if ( $this->integration->woocommerce_integration->getOptionGenerateReceipt() === ReceiptForm::AUTO_GENERATE ) {
			$this->initReceiptDocumentCreator();
		}

		$this->initProformaDocumentCreator();

		if ( wc_tax_enabled() ) {
			if ( in_array(
				$this->integration->woocommerce_integration->getOptionGenerateInvoice(),
				array(
					InvoiceForm::AUTO_GENERATE,
					InvoiceForm::ASK_AND_AUTO_GENERATE,
				),
				true
			) ) {
				$this->initInvoiceDocumentCreator();
			}
		} else {
			if ( $this->integration->woocommerce_integration->getOptionDocumentType() === BillForm::OPTION_DOCUMENT_TYPE_INVOICE_WITHOUT_TAX ) {
				if ( in_array(
					$this->integration->woocommerce_integration->getOptionGenerateInvoice(),
					array(
						InvoiceForm::AUTO_GENERATE,
						InvoiceForm::ASK_AND_AUTO_GENERATE,
					),
					true
				) ) {
					$this->initInvoiceDocumentCreator();
				}
			} else {
				if ( in_array(
					$this->integration->woocommerce_integration->getOptionGenerateBill(),
					array(
						InvoiceForm::AUTO_GENERATE,
						InvoiceForm::ASK_AND_AUTO_GENERATE,
					),
					true
				) ) {
					$this->initBillDocumentCreator();
				}
			}
		}
	}

	/**
	 * Init invoice creator.
	 */
	public function initProformaDocumentCreator() {
		return new DocumentCreatorForOrderStatus( $this->integration, $this->integration->woocommerce_integration->getOptionGenerateProformaStatus(), false );
	}

	/**
	 * Init invoice creator.
	 */
	public function initInvoiceDocumentCreator() {
		return new DocumentCreatorForOrderStatus( $this->integration, $this->integration->woocommerce_integration->getOptionGenerateInvoiceStatus(), false );
	}

	/**
	 * Init invoice creator.
	 */
	public function initReceiptDocumentCreator() {
		return new DocumentCreatorForOrderStatus( $this->integration, $this->integration->woocommerce_integration->getOptionGenerateReceiptStatus(), false );
	}

	/**
	 * Init bill creator.
	 */
	public function initBillDocumentCreator() {
		return new DocumentCreatorForOrderStatus( $this->integration, $this->integration->woocommerce_integration->getOptionGenerateBillStatus(), false );
	}

}
