<?php

namespace WPDesk\WooCommerceFakturownia\Api;

/**
 * Class InvoicesException
 *
 * Returns all API exceptions
 *
 * @package WPDesk\WooCommerceFakturownia\Api
 */
class InvoicesException extends \RuntimeException {
}

