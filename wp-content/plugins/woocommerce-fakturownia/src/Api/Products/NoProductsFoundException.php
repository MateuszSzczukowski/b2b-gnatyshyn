<?php

namespace WPDesk\WooCommerceFakturownia\Api\Products;

/**
 * No products found exception.
 */
class NoProductsFoundException extends \RuntimeException {
}

