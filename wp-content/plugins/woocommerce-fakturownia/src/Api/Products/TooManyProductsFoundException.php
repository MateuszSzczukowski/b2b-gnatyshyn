<?php

namespace WPDesk\WooCommerceFakturownia\Api\Products;

/**
 * No products found exception.
 */
class TooManyProductsFoundException extends \RuntimeException {
}

