<?php

namespace WPDesk\WooCommerceFakturownia\Api\Invoice;

use WPDesk\WooCommerceFakturownia\Api\DocumentGetResponseJson;

/**
 * Class GetResponse
 *
 * @package WPDesk\WooCommerceFakturownia\Api\DomesticInvoice
 */
class GetResponseJson extends DocumentGetResponseJson {

}
