<?php

namespace WPDesk\WooCommerceFakturownia\Api\Invoice;

use WPDesk\WooCommerceFakturownia\Api\DocumentPostResponseJson;

/**
 * Class PostResponseJson
 *
 * @package WPDesk\WooCommerceFakturownia\Api\DomesticInvoice
 */
class PostResponseJson extends DocumentPostResponseJson {

}
