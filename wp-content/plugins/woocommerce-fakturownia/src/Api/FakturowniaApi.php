<?php

namespace WPDesk\WooCommerceFakturownia\Api;

use WPDesk\WooCommerceFakturownia\Api\Invoice\GetRequest;
use WPDesk\WooCommerceFakturownia\Api\Invoice\PostResponseJson;
use WPDesk\WooCommerceFakturownia\Api\Products\GetSingleRequest;
use WPDesk\WooCommerceFakturownia\Api\Products\GetSingleResponseJson;
use WPDesk\WooCommerceFakturownia\Api\Products\NoProductsFoundException;
use WPDesk\WooCommerceFakturownia\Api\Products\TooManyProductsFoundException;
use WPDesk\WooCommerceFakturownia\Data\DocumentData;
use WPDesk\WooCommerceFakturownia\InvoicesIntegration;
use WPDesk\WooCommerceFakturownia\Api\Invoice\PostRequest;
use WPDesk\WooCommerceFakturownia\Api\Products\PostRequest as ProductsPostRequest;
use WPDesk\WooCommerceFakturownia\Api\Products\PostResponseJson as ProductsPostResponseJson;

/**
 * Class FakturowniaApi
 *
 * @package WPDesk\WooCommerceFakturownia\Api
 */
class FakturowniaApi {

	/**
	 * User name.
	 *
	 * @var string
	 */
	private $token;

	/**
	 * ApiClient.
	 *
	 * @var InvoicesIntegration
	 */
	private $api_client;

	/**
	 * @var int
	 */
	private $department_id;

	/**
	 * @var int
	 */
	private $warehouse_id;

	/**
	 * @param \FakturowniaVendor\WPDesk\ApiClient\Client\Client $api_client    API Client.
	 * @param string                                            $account_name  Account name.
	 * @param string                                            $token         Token.
	 * @param int                                               $department_id Department ID.
	 */
	public function __construct( \FakturowniaVendor\WPDesk\ApiClient\Client\Client $api_client, $account_name, $token, $department_id, $warehouse_id ) {
		$this->api_client    = $api_client;
		$this->account_name  = $account_name;
		$this->token         = $token;
		$this->department_id = intval( $department_id );
		$this->warehouse_id = $warehouse_id;
	}

	/**
	 * @param array $invoice_data Invoice data.
	 *
	 * Return same data with department ID if exist.
	 *
	 * @return array
	 */
	private function pass_department_id_to_data( $invoice_data ) {
		if ( $this->department_id && is_array( $invoice_data ) ) {
			return array_merge(
				$invoice_data,
				[ 'department_id' => $this->department_id ]
			);
		}

		return $invoice_data;

	}


	/**
	 * Maybe throw exception from response.
	 *
	 * @param ResponseJson $response Response.
	 *
	 * @throws InvoicesException API Exception.
	 */
	private function maybe_throw_exception_from_response( ResponseJson $response ) {
		if ( $response->isError() ) {
			$response_body = $response->getResponseBody();

			$message = '';
			if ( isset( $response_body['message'] ) ) {
				if ( is_array( $response_body['message'] ) ) {
					foreach ( $response_body['message'] as $key => $response_message ) {
						$message .= $key . ' ' . $response_message[0];
					}
				} else {
					$message = $response_body['message'];
				}
			}
			$error_info = $response->get_error_info(
			// Translators: response code.
				sprintf( __( 'Błąd API Fakturownia! Kod błędu: %1$s %2$s.', 'woocommerce-fakturownia' ), $response->getResponseCode(), $message )
			);
			throw new InvoicesException( $error_info );
		}
	}

	/**
	 * Assert issue, sell and payment date
	 *
	 * @param array $data Document data.
	 *
	 * @return true
	 * @throws InvoicesException Exception.
	 */
	private function validate_dates( $data ) {
		if ( strtotime( $data['issue_date'] ) > strtotime( $data['payment_to'] ) ) {
			$error_info = __( 'Termin płatności faktury nie może być wcześniejszy niż data wystawienia!', 'woocommerce-fakturownia' );
			throw new InvoicesException( $error_info );
		}
		if ( isset( $data['sell_date'] ) && strtotime( $data['sell_date'] ) > strtotime( $data['payment_to'] ) ) {
			$error_info = __( 'Termin płatności faktury nie może być wcześniejszy niż data sprzedaży!', 'woocommerce-fakturownia' );
			throw new InvoicesException( $error_info );
		}

		return true;
	}

	/**
	 * Create document (invoice, bill, receipt).
	 *
	 * @param DocumentData $invoice_data Invoice data.
	 *
	 * @return PostResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function create_document( DocumentData $invoice_data ) {

		$data['api_token'] = $this->token;
		$data['invoice']   = $this->pass_department_id_to_data( $invoice_data->prepareDataAsArray() );
		$order = $invoice_data->getOrder();
		$this->validate_dates( $data['invoice'] );

		/**
		 * Document creation request filter.
		 *
		 * @var array     $data  Request data.
		 * @var \WC_order $order WC Order.
		 */
		$data    = apply_filters( 'fakturownia/core/api/request', $data, $order );
		$request = new PostRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$data
		);

		$create_invoice_raw_response = $this->api_client->sendRequest( $request );

		$create_invoice_response = new PostResponseJson(
			$create_invoice_raw_response
		);

		$this->maybe_throw_exception_from_response( $create_invoice_response );

		return $create_invoice_response;

	}

	/**
	 * Get invoice.
	 *
	 * @param int $invoice_id Invoice ID.
	 *
	 * @return DocumentGetResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_invoice( $invoice_id ) {
		$request = new GetRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$invoice_id
		);

		$invoice_raw_response = $this->api_client->sendRequest( $request );

		$invoice_response = new DocumentGetResponseJson(
			$invoice_raw_response
		);

		$this->maybe_throw_exception_from_response( $invoice_response );

		return $invoice_response;
	}

	/**
	 * Get invoice Pdf.
	 *
	 * @param int $invoice_id Invoice ID.
	 *
	 * @return Response
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_invoice_pdf( $invoice_id ) {
		$request = new GetRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$invoice_id,
			'pdf'
		);

		$invoice_raw_response = $this->api_client->sendRequest( $request );

		$response = new Response( $invoice_raw_response );

		return $response;
	}

	/**
	 * Get bill
	 *
	 * @param int $invoice_id Invoice ID.
	 *
	 * @return DocumentGetResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_bill( $invoice_id ) {
		$request = new GetRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$invoice_id
		);

		$invoice_raw_response = $this->api_client->sendRequest( $request );

		$invoice_response = new DocumentGetResponseJson(
			$invoice_raw_response
		);

		$this->maybe_throw_exception_from_response( $invoice_response );

		return $invoice_response;
	}

	/**
	 * Get bill Pdf.
	 *
	 * @param int $invoice_id Invoice ID.
	 *
	 * @return Response
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_bill_pdf( $invoice_id ) {
		$request = new GetRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$invoice_id,
			'pdf'
		);

		$invoice_raw_response = $this->api_client->sendRequest( $request );

		$response = new Response( $invoice_raw_response );

		return $response;
	}


	/**
	 * @return Warehouses\GetResponseJson
	 */
	public function get_warehouses() {
		$request = new Warehouses\GetRequest(
			$this->api_client->getApiUrl(),
			$this->token
		);

		$raw_response = $this->api_client->sendRequest( $request );

		$response = new Warehouses\GetResponseJson(
			$raw_response
		);

		$this->maybe_throw_exception_from_response( $response );

		return $response;
	}

	/**
	 * Get products.
	 *
	 * @param int $page Page.
	 *
	 * @return Products\GetResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_products( $page = 1 ) {
		$request = new \WPDesk\WooCommerceFakturownia\Api\Products\GetRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$page
		);

		$raw_response = $this->api_client->sendRequest( $request );

		$response = new Products\GetResponseJson(
			$raw_response
		);

		$this->maybe_throw_exception_from_response( $response );

		return $response;
	}

	/**
	 * Get all products.
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_all_products() {
		$all_products = array();
		$page         = 0;
		do {
			$page ++;
			$products_response = $this->get_products( $page );
			$products          = $products_response->get_products();
			$count_products    = count( $products );
			foreach ( $products as $product ) {
				$all_products[ $product['id'] ] = $product;
			}
		} while ( $count_products > 0 );

		return $all_products;
	}

	/**
	 * Query products.
	 *
	 * @param string $query .
	 * @param int    $page  Page.
	 *
	 * @return \WPDesk\WooCommerceFakturownia\Api\Products\GetResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function query_products( $query, $page = 1 ) {
		$request = new \WPDesk\WooCommerceFakturownia\Api\Products\QueryRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$this->warehouse_id,
			$query,
			$page
		);

		$raw_response = $this->api_client->sendRequest( $request );

		$response = new \WPDesk\WooCommerceFakturownia\Api\Products\GetResponseJson(
			$raw_response
		);

		$this->maybe_throw_exception_from_response( $response );

		return $response;
	}

	/**
	 * Get product by product code.
	 *
	 * @param string $code .
	 *
	 * @return array
	 * @throws \WPDesk\HttpClient\HttpClientRequestException .
	 * @throws TooManyProductsFoundException .
	 * @throws NoProductsFoundException .
	 */
	public function get_product_by_code( $code ) {
		$query_products_response = $this->query_products( $code );
		$products                = $query_products_response->get_products();
		$fakturownia_product     = false;
		foreach ( $products as $product ) {
			if ( isset( $product['code'] ) && $product['code'] === $code ) {
				if ( false === $fakturownia_product ) {
					$fakturownia_product = $product;
				} else {
					// Translators: product code.
					throw new TooManyProductsFoundException( sprintf( __( 'Istnieje wiele produków o kodzie: %1$s.', 'woocommerce-fakturownia' ), $code ) );
				}
			}
		}
		if ( false === $fakturownia_product ) {
			// Translators: product code.
			throw new NoProductsFoundException( sprintf( __( 'Nie znaleziono żadnego produku o kodzie: %1$s.', 'woocommerce-fakturownia' ), $code ) );
		}

		return $fakturownia_product;
	}

	/**
	 * Create product.
	 *
	 * @param string $name        .
	 * @param string $code        .
	 * @param string $description .
	 * @param string $image_url   .
	 *
	 * @return ProductsPostResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function create_product( $name, $code, $description = '', $image_url = '' ) {

		$data['api_token'] = $this->token;
		$data['product']   = array(
			'name'        => $name,
			'code'        => $code,
			'description' => $description,
			'image_url'   => $image_url,
		);

		$request = new ProductsPostRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$data
		);

		$create_product_raw_response = $this->api_client->sendRequest( $request );

		$create_product_response = new ProductsPostResponseJson(
			$create_product_raw_response
		);

		$this->maybe_throw_exception_from_response( $create_product_response );

		return $create_product_response;

	}

	/**
	 * Get product.
	 *
	 * @param string $id .
	 *
	 * @return GetSingleResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws \Exception API Exception.
	 */
	public function get_product( $id ) {
		$request = new GetSingleRequest(
			$this->api_client->getApiUrl(),
			$this->token,
			$this->warehouse_id,
			$id
		);

		$raw_response = $this->api_client->sendRequest( $request );

		$response = new GetSingleResponseJson(
			$raw_response
		);

		$this->maybe_throw_exception_from_response( $response );

		return $response;
	}

}
