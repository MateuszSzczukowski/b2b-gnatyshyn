<?php

namespace WPDesk\WooCommerceFakturownia;

use Exception;
use Psr\Log\LoggerInterface;
use WC_Order;
use FakturowniaVendor\WPDesk\ApiClient\Client\ClientFactory;
use FakturowniaVendor\WPDesk\Invoices\Documents\Type;
use FakturowniaVendor\WPDesk\Invoices\Field\InvoiceAsk;
use FakturowniaVendor\WPDesk\Invoices\Field\VatNumber;
use FakturowniaVendor\WPDesk\Invoices\OrdersTable\OrderColumn;
use FakturowniaVendor\WPDesk\View\Renderer\Renderer;
use WPDesk\WooCommerceFakturownia\Api\ClientOptions;
use WPDesk\WooCommerceFakturownia\Api\FakturowniaApi;
use WPDesk\WooCommerceFakturownia\Data\InvoiceOrderDefaults;
use WPDesk\WooCommerceFakturownia\Documents\DocumentType;
use FakturowniaVendor\WPDesk\Invoices\Exception\UnknownDocumentTypeException;
use FakturowniaVendor\WPDesk\Invoices\Integration;
use FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\Product\FakturowniaProductId;
use WPDesk\WooCommerceFakturownia\Product\FakturowniaProductIdAjax;
use WPDesk\WooCommerceFakturownia\Webhoks\StockListener;

/**
 * Class InvoicesIntegration
 *
 * @package WPDesk\WooCommerceFakturownia
 */
class InvoicesIntegration extends Integration {

	const VAT_NUMBER_FIELD_ID = 'nip';

	/**
	 * @var WoocommerceIntegration
	 */
	public $woocommerce_integration;

	/**
	 * Nip field.
	 *
	 * @var VatNumber
	 */
	private $vat_number_field;

	/**
	 * Invoice ask field.
	 *
	 * @var InvoiceAsk
	 */
	private $document_ask_field;

	/**
	 * Fakturownia API.
	 *
	 * @var FakturowniaApi
	 */
	private $fakturownia_api;

	/**
	 * Document types.
	 *
	 * @var DocumentType[];
	 */
	private $supported_document_types_by_metadata_name_and_type;

	/**
	 * API URL.
	 *
	 * @var string
	 */
	private $api_url = 'https://%s.fakturownia.pl/';

	/**
	 * \InvoicesIntegration constructor.
	 *
	 * @param WoocommerceIntegration $woocommerce_integration WooCommerce integration.
	 */
	public function __construct( WoocommerceIntegration $woocommerce_integration ) {
		$this->woocommerce_integration = $woocommerce_integration;
		parent::__construct(
			$woocommerce_integration->id,
			__( 'Fakturownia', 'woocommerce-fakturownia' ),
			__( 'Wystawiono', 'woocommerce-fakturownia' ),
			__( 'Brak', 'woocommerce-fakturownia' ),
			__( 'Wyślij email', 'woocommerce-fakturownia' ),
			// Translators: document number.
			__( 'Dla tego zamówienia istnieje dokument %1$s! Utworzyć nowy? Istniejący dokument należy obsłużyć w Fakturowni.', 'woocommerce-fakturownia' )
		);

		$api_client            = $this->getApiClient();
		$account_name          = $this->woocommerce_integration->getAccountName();
		$token                 = $this->woocommerce_integration->getUserToken();
		$department_id         = (int) $this->woocommerce_integration->getDepartmentId();
		$warehouse_id          = (int) $this->woocommerce_integration->getWarehouseID();
		$this->fakturownia_api = new FakturowniaApi( $api_client, $account_name, $token, $department_id, $warehouse_id );
		$this->initStockListener();
		$this->initFakturowniaProductId();
		$this->add_hookable( new DownloadDocument( $this ) );
	}

	/**
	 * @return Renderer
	 */
	public function get_renderer() {
		return $this->woocommerce_integration->get_renderer();
	}


	/**
	 * @return LoggerInterface
	 */
	public function get_logger() {
		return $this->woocommerce_integration->get_logger();
	}

	/**
	 * Init stock listener.
	 */
	private function initStockListener() {
		if ( 'yes' === $this->woocommerce_integration->getOptionSynchronization() ) {
			$this->add_hookable(
				new StockListener(
					$this->woocommerce_integration->getOptionWarehouseWebhookToken()
				)
			);
		}
	}

	/**
	 * Init stock listener.
	 */
	private function initFakturowniaProductId() {
		$this->add_hookable( new FakturowniaProductId( $this->fakturownia_api ) );
		$this->add_hookable( new FakturowniaProductIdAjax( $this->fakturownia_api ) );
	}

	/**
	 * Add supported document type.
	 *
	 * @param DocumentType $document_type Document type.
	 */
	public function addSupportedDocumentType( $document_type ) {
		parent::addSupportedDocumentType( $document_type );
		$this->supported_document_types_by_metadata_name_and_type[ $document_type->getMetaDataName() . $document_type->getMetaDataType() ] = $document_type;
	}


	/**
	 * Get document type by metadata name and type.
	 *
	 * @param string $metadata_name_and_type_name Metadata name and type name.
	 *
	 * @return DocumentType|bool
	 */
	public function get_document_type_by_metadata_name_and_type( $metadata_name_and_type_name ) {
		if ( isset( $this->supported_document_types_by_metadata_name_and_type[ $metadata_name_and_type_name ] ) ) {
			return $this->supported_document_types_by_metadata_name_and_type[ $metadata_name_and_type_name ];
		}

		return false;
	}

	public function initSupportedDocumentTypes() {
		$ajax_get_pdf_handler = $this->getAjaxGetPdfHandler();
		$documents            = new SupportedDocuments( $this, $ajax_get_pdf_handler );
		$documents->AddSupportForReceipt();
		$documents->AddSupportForInvoice();
		$documents->AddSupportForInvoiceWithoutVat();
		$documents->AddSupportForInvoiceProforma();
		$documents->AddSupportForInvoiceProFromaWithoutVat();
		$documents->AddSupportForInvoiceForeign();
		$documents->AddSupportForBill();
	}

	/**
	 * Get API.
	 *
	 * @return FakturowniaApi
	 */
	public function get_fakturownia_api() {
		return $this->fakturownia_api;
	}

	/**
	 * Init API Client
	 */
	public function initApiClient() {
		$client_options = new ClientOptions();
		$client_options->setCachedClient( true );
		$api_url = sprintf( $this->api_url, $this->woocommerce_integration->getAccountName() );
		$client_options->setApiUrl( $api_url );
		$client_factory = new ClientFactory();
		$api_client     = $client_factory->createClient( $client_options );
		$this->setApiClient( $api_client );

	}

	/**
	 *
	 * @return bool
	 */
	public function has_account_name(){
		return !empty($this->woocommerce_integration->getAccountName());
	}

	/**
	 * Inits document creators.
	 */
	public function initDocumentCreators() {
		$document_creators = new DocumentCreators( $this );
		$document_creators->init();
		$this->add_hookable( $document_creators->initBillDocumentCreator() );
		$this->add_hookable( $document_creators->initInvoiceDocumentCreator() );
		$this->add_hookable( $document_creators->initProformaDocumentCreator() );
		$this->add_hookable( $document_creators->initReceiptDocumentCreator() );
	}

	/**
	 * Should add field to option.
	 *
	 * @param string $option_value Option value.
	 *
	 * @return bool
	 */
	private function should_add_field_for_option( $option_value ) {
		if ( in_array(
			$option_value,
			[
				InvoiceForm::ASK_AND_AUTO_GENERATE,
				InvoiceForm::ASK_AND_NOT_GENERATE,
			],
			true
		) ) {
			return true;
		}

		return false;
	}

	/**
	 * Add fields for invoices.
	 */
	private function add_fields_for_invoices() {
		if ( $this->should_add_field_for_option( $this->woocommerce_integration->getOptionGenerateInvoice() ) ) {
			$this->document_ask_field = new InvoiceAsk(
				'faktura',
				__( 'Chcę otrzymać fakturę VAT', 'woocommerce-fakturownia' )
			);
			$this->add_hookable( $this->document_ask_field );
		}
	}

	/**
	 * Add fields for invoices without VAT.
	 */
	private function add_fields_for_invoices_without_vat() {
		if ( $this->should_add_field_for_option( $this->woocommerce_integration->getOptionGenerateInvoice() ) ) {
			$this->document_ask_field = new InvoiceAsk(
				'faktura',
				__( 'Chcę otrzymać fakturę bez VAT', 'woocommerce-fakturownia' )
			);
			$this->add_hookable( $this->document_ask_field );
		}
	}


	/**
	 * Add fields for bills.
	 */
	private function add_fields_for_bills() {
		if ( $this->should_add_field_for_option( $this->woocommerce_integration->getOptionGenerateBill() ) ) {
			$this->document_ask_field = new InvoiceAsk(
				'rachunek',
				__( 'Chcę otrzymać rachunek', 'woocommerce-fakturownia' )
			);
			$this->add_hookable( $this->document_ask_field );
		}
	}

	/**
	 * Inits checkout fields.
	 */
	public function initFields() {

		$this->vat_number_field = new VatNumber(
			self::VAT_NUMBER_FIELD_ID,
			__( 'VAT', 'woocommerce-fakturownia' ),
			__( 'Numer NIP', 'woocommerce-fakturownia' )
		);
		$this->add_hookable( $this->vat_number_field );

		if ( $this->woocommerce_integration->isOptionValidateCheckoutNip() && $this->woocommerce_integration->eu_vat_settings()->eu_vat_vies_validate ) {
			$eu_countries = $this->get_eu_countries();
			( new Field\Validator\EuVatValidator( $this->vat_number_field, $eu_countries ) )->guardValidCheckout();
		}

		/**
		 * Enable NIP validator if the MOSS validation is disabled.
		 */
		if ( $this->woocommerce_integration->isOptionValidateCheckoutNip() && ! $this->woocommerce_integration->eu_vat_settings()->eu_vat_vies_validate ) {
			( new Field\Validator\NipValidator( $this->vat_number_field ) )
				->guardValidCheckout();
		}

		if ( wc_tax_enabled() ) {
			$this->add_fields_for_invoices();
		} else {
			if ( 'invoice_without_tax' === $this->woocommerce_integration->getOptionDocumentType() ) {
				$this->add_fields_for_invoices_without_vat();
			} else {
				$this->add_fields_for_bills();
			}
		}
	}

	/**
	 * Get vat number field.
	 *
	 * @return VatNumber
	 */
	public function get_vat_number_field() {
		return $this->vat_number_field;
	}


	/**
	 * Get EU countries.
	 *
	 * @return string[]
	 */
	private function get_eu_countries() {
		return WC()->countries->get_european_union_countries();
	}

	/**
	 * Init integrations.
	 */
	public function initIntegrations() {
		$this->initEUVat();
		$this->add_hookable( new GTU() );
		$this->add_hookable( new ProcedureDesignations() );
	}

	/**
	 * Initialize EU VAT number.
	 */
	private function initEUVat() {
		if ( wc_tax_enabled() ) {
			$eu_vat    = new EUVatIntegration( $this->woocommerce_integration->eu_vat_settings() );
			$moss_link = 'https://www.wpdesk.pl/docs/fakturownia-woocommerce-docs/';
			$eu_vat->set_vat_field_name( 'billing_nip' );
			$eu_vat->set_plugin_name( __( 'Fakturownia WooCommerce', 'woocommerce-fakturownia' ) );
			$eu_vat->set_plugin_version( '1.4.0' );
			$eu_vat->set_doc_link( $moss_link );
			$this->add_hookable( $eu_vat );
		}
	}


	/**
	 * Inits order column.
	 */
	public function initOrderColumn() {
		$this->add_hookable(
			new OrderColumn( $this, __( 'Fakturownia', 'woocommerce-fakturownia' ), $this->document_ask_field, $this->getAjaxGetPdfHandler() )
		);
	}

	/**
	 * Prepare order defaults.
	 *
	 * @param WC_Order $order         Order.
	 * @param Type     $document_type Document type.
	 *
	 * @return InvoiceOrderDefaults
	 */
	public function prepareOrderDefaults( $order, Type $document_type ) {
		return new InvoiceOrderDefaults( $order, $this, $document_type );
	}


	/**
	 * Create document from data.
	 *
	 * @param WC_Order $order             Order.
	 * @param array    $data              Posted data.
	 * @param bool     $overwriteExisting Overwrite already created.
	 *
	 * @return bool
	 * @throws Exception .
	 */
	public function createDocumentForOrder( $order, array $data, $overwriteExisting = true ) {
		$type_name = '';
		if ( isset( $data['type'] ) ) {
			$type_name = $data['type'];
		}
		$type = $this->getSupportedDocumentType( $type_name );
		if ( $type ) {
			$type->getCreator()->createDocumentForOrder( $order, $data );
		} else {
			throw new UnknownDocumentTypeException( $type_name );
		}

		return true;
	}

	/**
	 * Get Document PDF.
	 *
	 * @param MetadataContent $metadata_content Meta data content.
	 *
	 * @return string
	 * @throws UnknownDocumentTypeException Exception.
	 */
	public function getDocumentPdf( MetadataContent $metadata_content ) {
		$metadata = $metadata_content->get();

		$document_type = $this->get_document_type_by_metadata_name_and_type(
			$metadata_content->getMetaDataName() . $metadata['typ']
		);

		if ( $document_type ) {
			return $document_type->getDocumentPdf( $metadata['id'] );
		} else {
			throw new UnknownDocumentTypeException(
				$metadata_content->getMetaDataName() . $metadata['typ']
			);
		}
	}


}
