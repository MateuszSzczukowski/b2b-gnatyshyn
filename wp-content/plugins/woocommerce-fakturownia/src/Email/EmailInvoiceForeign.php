<?php
/**
 * Email templates: Foreign Invoice
 */

use WPDesk\WooCommerceFakturownia\Plugin;

/**
 * Register foreign template email for foreign invoice.
 *
 */
class WooCommerceFakturowniaEmailInvoiceForeign extends WooCommerceFakturowniaBaseEmail {

	/**
	 * Constructor.
	 *
	 * @param string $plugin_path          Plugin path.
	 * @param string $plugin_template_path Plugin template path.
	 */
	public function __construct( $plugin_path, $plugin_template_path ) {

		$this->id             = 'woo_fakturownia_faktura';
		$this->title          = __( 'Faktura walutowa (Fakturownia)', 'woocommerce-fakturownia' );
		$this->description    = __( 'Email z fakturą walutową (Fakturownia).', 'woocommerce-fakturownia' );
		$this->heading        = __( 'Faktura walutowa do zamówienia', 'woocommerce-fakturownia' );
		$this->subject        = __( '[{site_title}] Faktura walutowa do zamówienia {order_number} - {order_date}', 'woocommerce-fakturownia' );
		$this->template_html  = 'emails/faktura.php';
		$this->template_plain = 'emails/plain/faktura.php';

		parent::__construct( $plugin_path, $plugin_template_path );

	}


}
