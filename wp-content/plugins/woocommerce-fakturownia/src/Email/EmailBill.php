<?php

use WPDesk\WooCommerceFakturownia\Plugin;

/**
 * Class WooCommerceFakturowniaEmailBill
 *
 * @package WPDesk\WooCommerceFakturownia\Email
 */
class WooCommerceFakturowniaEmailBill extends WooCommerceFakturowniaBaseEmail {

	/**
	 * EmailBill WooCommerceFakturowniaEmailBill.
	 *
	 * @param string $plugin_path Plugin path.
	 * @param string $plugin_template_path Plugin template path.
	 */
	public function __construct( $plugin_path, $plugin_template_path ) {

		$this->id             = 'woo_fakturownia_rachunek';
		$this->title          = __( 'Rachunek (Fakturownia)', 'woocommerce-fakturownia' );
		$this->description    = __( 'Email z rachunkiem.', 'woocommerce-fakturownia' );
		$this->heading        = __( 'Rachunek do zamówienia', 'woocommerce-fakturownia' );
		$this->subject        = __( '[{site_title}] Rachunek do zamówienia {order_number} - {order_date}', 'woocommerce-fakturownia' );
		$this->template_html  = 'emails/rachunek.php';
		$this->template_plain = 'emails/plain/rachunek.php';

		parent::__construct( $plugin_path, $plugin_template_path );

	}

}
