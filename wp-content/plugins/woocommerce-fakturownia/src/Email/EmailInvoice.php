<?php

use WPDesk\WooCommerceFakturownia\Plugin;

/**
 * Class WooCommerceFakturowniaEmailInvoice
 *
 */
class WooCommerceFakturowniaEmailInvoice extends WooCommerceFakturowniaBaseEmail {

	/**
	 * WooCommerceFakturowniaEmailInvoice constructor.
	 *
	 * @param string $plugin_path Plugin path.
	 * @param string $plugin_template_path Plugin template path.
	 */
	public function __construct( $plugin_path, $plugin_template_path ) {

		$this->id             = 'woo_fakturownia_faktura';
		$this->title          = __( 'Faktura (Fakturownia)', 'woocommerce-fakturownia' );
		$this->description    = __( 'Email z fakturą (Fakturownia).', 'woocommerce-fakturownia' );
		$this->heading        = __( 'Faktura do zamówienia', 'woocommerce-fakturownia' );
		$this->subject        = __( '[{site_title}] Faktura do zamówienia {order_number} - {order_date}', 'woocommerce-fakturownia' );
		$this->template_html  = 'emails/faktura.php';
		$this->template_plain = 'emails/plain/faktura.php';

		parent::__construct( $plugin_path, $plugin_template_path );

	}


}
