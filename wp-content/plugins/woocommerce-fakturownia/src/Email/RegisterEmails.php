<?php

/**
 * Class RegisterEmails
 *
 * @package WPDesk\WooCommerceFakturownia\Email
 */
class WooCommerceFakturowniaRegisterEmails implements FakturowniaVendor\WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\PluginAccess;

	/**
	 * Hooks
	 */
	public function hooks() {
		add_filter( 'woocommerce_email_classes', [ $this, 'register_emails' ], 11 );
	}

	/**
	 * Register emails in WooCommerce.
	 *
	 * @param array $emails Emails.
	 *
	 * @return array
	 */
	public function register_emails( array $emails ) {
		$emails[ \WooCommerceFakturowniaEmailInvoice::class ]         = new \WooCommerceFakturowniaEmailInvoice( $this->plugin->get_plugin_path(), $this->plugin->get_template_path() );
		$emails[ \WooCommerceFakturowniaEmailInvoiceForeign::class ]  = new \WooCommerceFakturowniaEmailInvoiceForeign( $this->plugin->get_plugin_path(), $this->plugin->get_template_path() );
		$emails[ \WooCommerceFakturowniaEmailInvoiceProForma::class ] = new \WooCommerceFakturowniaEmailInvoiceProForma( $this->plugin->get_plugin_path(), $this->plugin->get_template_path() );
		$emails[ \WooCommerceFakturowniaEmailBill::class ]            = new \WooCommerceFakturowniaEmailBill( $this->plugin->get_plugin_path(), $this->plugin->get_template_path() );
		$emails[ \WooCommerceFakturowniaEmailReceipt::class ]            = new \WooCommerceFakturowniaEmailReceipt( $this->plugin->get_plugin_path(), $this->plugin->get_template_path() );

		return $emails;
	}

}
