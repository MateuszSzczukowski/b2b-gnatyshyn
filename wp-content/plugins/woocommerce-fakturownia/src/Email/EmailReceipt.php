<?php

use WPDesk\WooCommerceFakturownia\Plugin;

/**
 * Class WooCommerceFakturowniaEmailReceipt
 *
 * @package WPDesk\WooCommerceFakturownia\Email
 */
class WooCommerceFakturowniaEmailReceipt extends WooCommerceFakturowniaBaseEmail {

	/**
	 * EmailBill WooCommerceFakturowniaEmailReceipt.
	 *
	 * @param string $plugin_path Plugin path.
	 * @param string $plugin_template_path Plugin template path.
	 */
	public function __construct( $plugin_path, $plugin_template_path ) {

		$this->id             = 'woo_fakturownia_receipt';
		$this->title          = __( 'Paragom (Fakturownia)', 'woocommerce-fakturownia' );
		$this->description    = __( 'Email z paragonem.', 'woocommerce-fakturownia' );
		$this->heading        = __( 'Paragon do zamówienia', 'woocommerce-fakturownia' );
		$this->subject        = __( '[{site_title}] Paragon do zamówienia {order_number} - {order_date}', 'woocommerce-fakturownia' );
		$this->template_html  = 'emails/paragon.php';
		$this->template_plain = 'emails/plain/paragon.php';

		parent::__construct( $plugin_path, $plugin_template_path );

	}

}
