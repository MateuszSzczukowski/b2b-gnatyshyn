<?php

namespace WPDesk\WooCommerceFakturownia\Documents;

use http\Exception\RuntimeException;
use WC_Order;
use WPDesk\WooCommerceFakturownia\Api\Invoice\PostResponseJson;
use WPDesk\WooCommerceFakturownia\Data\BillData;
use WPDesk\WooCommerceFakturownia\Forms\Integration\BillForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\InvoicesIntegration;
use WPDesk\WooCommerceFakturownia\Data\DocumentData;
use WPDesk\WooCommerceFakturownia\Data\InvoiceOrderDefaults;
use WPDesk\WooCommerceFakturownia\API\DocumentGetResponseJson;
use WPDesk\WooCommerceFakturownia\API\InvoicesException;
use WPDesk\WooCommerceFakturownia\WoocommerceIntegration;

/**
 * Class DomesticInvoiceCreator
 *
 * @package WPDesk\WooCommerceFakturownia\Documents
 */
class BillCreator extends DocumentTypeCreator {

	/**
	 * Create from order.
	 *
	 * @param WC_Order                              $order            Order.
	 * @param \FakturowniaVendor\WPDesk\Invoices\Field\VatNumber|null $vat_number_field Vat number field.
	 * @param InvoiceOrderDefaults                  $defaults         Defaults.
	 * @param InvoicesIntegration                   $integration      Integration.
	 *
	 * @return BillData
	 */
	protected function createFromOrderAndDefaultsAndSettings(
		WC_Order $order,
		$vat_number_field,
		InvoiceOrderDefaults $defaults,
		InvoicesIntegration $integration
	) {
		return BillData::createFromOrderAndDefaultsAndSettings(
			$order,
			$vat_number_field,
			$defaults,
			$integration
		);
	}

	/**
	 * Create document in API.
	 *
	 * @param DocumentData $bill_data Bill data.
	 *
	 * @return PostResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws InvoicesException API Exception.
	 */
	protected function createDocument( DocumentData $bill_data, $order ) {
		$integration = $this->type->getIntegration();
		if ( ! $this->isAllowedForOrder( $order ) ) {
			$error_info = __( 'Ten typ dokumentu nie jest obsługiwany!', 'woocomerce-fakturownia' );
			throw new \RuntimeException( $error_info );
		}

		return $integration->get_fakturownia_api()->create_document( $bill_data );
	}

	/**
	 * Is allowed for order.
	 *
	 * @param \WC_Order $order Order.
	 *
	 * @return bool
	 */
	public function isAllowedForOrder( $order ) {
		if ( wc_tax_enabled() ) {
			return false;
		}
		return ! ( $this->type->getIntegration()->woocommerce_integration->getOptionDocumentType() !== BillForm::OPTION_DOCUMENT_TYPE_BILL );

	}

	/**
	 * Get document from API.
	 *
	 * @param int $bill_id Bill ID.
	 *
	 * @return DocumentGetResponseJson
	 * @throws \WPDesk\HttpClient\HttpClientRequestException HTTP Exception.
	 * @throws InvoicesException API Exception.
	 */
	protected function getDocument( $bill_id ) {
		$integration = $this->type->getIntegration();
		return $integration->get_fakturownia_api()->get_bill( $bill_id );
	}

	/**
	 * Is auto create allowed for order?
	 *
	 * @param \WC_Order $order Order.
	 * @return bool
	 */
	public function isAutoCreateAllowedForOrder( $order ) {
		$integration = $this->type->getIntegration();
		if ( InvoiceForm::AUTO_GENERATE === $integration->woocommerce_integration->getOptionGenerateBill() ) {
			return true;
		}
		if ( InvoiceForm::ASK_AND_AUTO_GENERATE === $integration->woocommerce_integration->getOptionGenerateBill() ) {
			if ( '1' === $order->get_meta( '_billing_rachunek', 'true' ) ) {
				return true;
			}
		}
		return false;
	}

}
