<?php

namespace WPDesk\WooCommerceFakturownia\Tracker;

use WPDesk\WooCommerceFakturownia\Forms\Integration\BillForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\InvoiceForm;
use WPDesk\WooCommerceFakturownia\Forms\Integration\StockForm;
use WPDesk\WooCommerceFakturownia\WoocommerceIntegration;

/**
 * Provides Fakturownia settings data for tracker.
 *
 */
class SettingsDataProvider implements \WPDesk_Tracker_Data_Provider {

	const OPTION_VALUE_NO  = 'no';
	const OPTION_VALUE_YES = 'yes';

	/**
	 * Are bills enabled.
	 *
	 * @return bool
	 */
	private function is_bills() {
		if ( ! wc_tax_enabled() && BillForm::OPTION_DOCUMENT_TYPE === BillForm::OPTION_DOCUMENT_TYPE_BILL ) {
			return true;
		}

		return false;
	}


	/**
	 * Provides Fakturownia data
	 *
	 * @return array
	 */
	public function get_data() {
		$integrations = WC()->integrations->get_integrations();
		/** @var WoocommerceIntegration $fakturownia_integration */
		$fakturownia_integration = $integrations[ WoocommerceIntegration::INTEGRATION_ID ];

		if ( $this->is_bills() ) {
			$generate = $fakturownia_integration->get_option( BillForm::OPTION_GENERATE_BILL );
			$autosend = $fakturownia_integration->get_option( BillForm::OPTION_AUTOSEND_BILL );
		} else {
			$generate = $fakturownia_integration->get_option( InvoiceForm::OPTION_GENERATE_INVOICE );
			$autosend = $fakturownia_integration->get_option( InvoiceForm::OPTION_AUTOSEND_INVOICE );
		}

		$synchronization = $fakturownia_integration->get_option( StockForm::OPTION_SYNCHRONIZATION );

		$validate_checkout_nip = $fakturownia_integration->get_option( InvoiceForm::OPTION_VALIDATE_CHECKOUT_NIP );

		$plugin_data = [
			'generate'                                => $generate,
			'autosend'                                => $autosend,
			StockForm::OPTION_SYNCHRONIZATION         => $synchronization,
			InvoiceForm::OPTION_VALIDATE_CHECKOUT_NIP => $validate_checkout_nip,
		];

		if ( self::OPTION_VALUE_YES === $synchronization ) {
			$plugin_data[ StockForm::OPTION_CREATE_PRODUCTS ] = $fakturownia_integration->get_option( StockForm::OPTION_CREATE_PRODUCTS );
		} else {
			$plugin_data[ StockForm::OPTION_CREATE_PRODUCTS ] = 'no';
		}

		return [ 'woocommerce_fakturownia' => $plugin_data ];

	}

}
