<?php

namespace WPDesk\WooCommerceFakturownia\Tracker;

/**
 * Handles WP Desk Tracker hooks.
 */
class TrackerBox {

	const PLUGIN_FILE = 'woocommerce-fakturownia/woocommerce-fakturownia.php';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'activated_plugin', array( $this, 'maybe_display_tracker_box' ), 10, 2 );
	}

	/**
	 * Activated plugin.
	 *
	 * @param string $plugin .
	 * @param bool   $network_wide .
	 */
	public function maybe_display_tracker_box( $plugin, $network_wide ) {
		if ( $network_wide ) {
			return;
		}
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			return;
		}
		if ( ! apply_filters( 'wpdesk_tracker_enabled', true ) ) {
			return;
		}
		if ( self::PLUGIN_FILE === $plugin ) {
			$options = get_option( 'wpdesk_helper_options', array() );
			if ( empty( $options ) ) {
				$options = array();
			}
			if ( empty( $options['wpdesk_tracker_agree'] ) ) {
				$options['wpdesk_tracker_agree'] = '0';
			}
			$wpdesk_tracker_skip_plugin = get_option( 'wpdesk_tracker_skip_woocommerce_fakturownia', '0' );
			if ( '0' === $options['wpdesk_tracker_agree'] && '0' === $wpdesk_tracker_skip_plugin ) {
				update_option( 'wpdesk_tracker_notice', '1' );
				update_option( 'wpdesk_tracker_skip_woocommerce_fakturownia', '1' );
				if ( ! apply_filters( 'wpdesk_tracker_do_not_ask', false ) ) {
					wp_safe_redirect( admin_url( 'admin.php?page=wpdesk_tracker&plugin=' . self::PLUGIN_FILE ) );
					exit;
				}
			}
		}
	}

}
