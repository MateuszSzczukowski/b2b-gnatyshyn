<?php

namespace WPDesk\WooCommerceFakturownia\Tracker;

use FakturowniaVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\WooCommerceFakturownia\WoocommerceIntegration;

/**
 * Handles WP Desk Tracker hooks.
 */
class Tracker implements Hookable {

	const PLUGIN_FILE = 'woocommerce-fakturownia/woocommerce-fakturownia.php';


	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'wpdesk_tracker_notice_screens', array( $this, 'add_screens_for_tracker_notice' ) );
		add_filter( 'wpdesk_track_plugin_deactivation', array( $this, 'add_plugin_to_track_plugin_deactivation' ) );

		add_filter( 'plugin_action_links_' . self::PLUGIN_FILE, array( $this, 'add_opt_in_out_link_to_plugin_links' ), 9 );

	}

	/**
	 * Add plugin to track plugin deactivation.
	 *
	 * @param array $plugins Plugins.
	 *
	 * @return mixed
	 */
	public function add_plugin_to_track_plugin_deactivation( array $plugins ) {
		$plugins[ self::PLUGIN_FILE ] = self::PLUGIN_FILE;
		return $plugins;
	}

	/**
	 * Add screen(s) to display traker notice.
	 *
	 * @param array $screens .
	 *
	 * @return array
	 */
	public function add_screens_for_tracker_notice( $screens ) {
		$current_screen = get_current_screen();
		if ( 'woocommerce_page_wc-settings' === $current_screen->id ) {
			if ( isset( $_GET['tab'] ) && 'integration' === $_GET['tab']
			    && ( ( isset( $_GET['section'] ) && WoocommerceIntegration::INTEGRATION_ID === $_GET['section'] ) || empty( $_GET['section'] ) )
			) {
				$screens[] = $current_screen->id;
			}
		}
		return $screens;
	}

	/**
	 * Plugin links.
	 *
	 * @param array $links .
	 *
	 * @return array
	 */
	public function add_opt_in_out_link_to_plugin_links( $links ) {
		if ( ! apply_filters( 'wpdesk_tracker_enabled', true ) || apply_filters( 'wpdesk_tracker_do_not_ask', false ) ) {
			return $links;
		}
		$options = get_option( 'wpdesk_helper_options', array() );
		if ( ! is_array( $options ) ) {
			$options = array();
		}
		if ( empty( $options['wpdesk_tracker_agree'] ) ) {
			$options['wpdesk_tracker_agree'] = '0';
		}
		$plugin_links = array();
		if ( '0' === $options['wpdesk_tracker_agree'] ) {
			$opt_in_link    = admin_url( 'admin.php?page=wpdesk_tracker&plugin=' . self::PLUGIN_FILE );
			$plugin_links[] = '<a href="' . $opt_in_link . '">' . __( 'Opt-in', 'flexible-shipping-ups' ) . '</a>';
		} else {
			$opt_in_link    = admin_url( 'plugins.php?wpdesk_tracker_opt_out=1&plugin=' . self::PLUGIN_FILE );
			$plugin_links[] = '<a href="' . $opt_in_link . '">' . __( 'Opt-out', 'flexible-shipping-ups' ) . '</a>';
		}
		return array_merge( $plugin_links, $links );
	}

}
