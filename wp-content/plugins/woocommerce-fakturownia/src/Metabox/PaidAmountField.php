<?php

namespace WPDesk\WooCommerceFakturownia\Metabox;

/**
 * Class Paid_Amount_Field
 *
 * @package WPDesk\WooCommerceFakturownia\Metabox
 */
class PaidAmountField extends \FakturowniaVendor\WPDesk\Invoices\Metabox\MetaBoxFieldPrice
{

	/**
	 * Prepare value.
	 *
	 * @param \WC_Order                                 $order Order.
	 * @param \FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadata_content Meta data.
	 * @param \FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults       $order_defaults Order defaults.
	 *
	 * @return string
	 */
	protected function prepareValue(
		\WC_Order $order,
		\FakturowniaVendor\WPDesk\Invoices\Metadata\MetadataContent $metadata_content,
		\FakturowniaVendor\WPDesk\Invoices\Data\OrderDefaults $order_defaults
	) {
		return $order_defaults->getDefault( 'paid_amount' );
	}

}
