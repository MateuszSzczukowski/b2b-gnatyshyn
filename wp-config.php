<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_gnatyshynB2B' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uI;7wS5NhOA%FbRR(NWnJg2Fn{G!V@em:*A!n#`^ofZMJVK02tKHupzfgN>UV%h!' );
define( 'SECURE_AUTH_KEY',  'EFM?$i5zF9QGZ3N)_{Y48NqpV{?V2[>*4T[~[zOvgp+e_Wtia&uhHnB&_=zlYl[2' );
define( 'LOGGED_IN_KEY',    'LYf*>!ks1+v(]1]v@CZl/qo00/~4=>E&rYa) T4ysvv_V0h#&BF5vo@#(LkHuMBz' );
define( 'NONCE_KEY',        'g*T^<Dn.Zq:HWyzpcP<ugeUy@R&U|zT_R`%0b%UYS0RW8tu]:5xmPK5[@N@:bTD3' );
define( 'AUTH_SALT',        '%$xUG/R S[G kyp}G#l78-:)?,Rl)Eo0Y!ll(A/r)KHI!aG0v?@6pSUpv7DC(`:8' );
define( 'SECURE_AUTH_SALT', '<C}G?,/kS_s8+`^_IOm#f7I1&goBDYO6~]:SBx_x:6#QMwJ^E0at%S!iKojo9vkz' );
define( 'LOGGED_IN_SALT',   'XcyJRseayRuH+/_J].i?*ij^A[o$S>zOgaN5_wmHzEmeX6nIC>^h-WYXkLP*7mw@' );
define( 'NONCE_SALT',       'Rdm?%J7jjWh7*pZLQae|G4(*Rtm=+{0Bd%g:@#]tI~6^rhjY^N=BL[q$k`k?5c9[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'lao_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
